import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiServiceProvider } from '../api-service/api-service';
import { StorageServiceProvider } from '../storage-service/storage-service';

@Injectable()
export class VideoOfflineProvider {
  video: any;
  checkTags: boolean;
  user: any;

  constructor(public http: HttpClient, public service: ApiServiceProvider, public storage: StorageServiceProvider) {
    this.user = this.storage.getObject('distributor');
    this.checkTags = true;
  }

  initPlayer(video, second) {
    this.video = video;
    this.checkTime(second);
  }

  checkTime(second) {
    if (this.video.tags != null) {
      let that = this;
      Object.keys(this.video.tags).forEach(key => {
        let item = that.video.tags[key];
        if (second >= item.time) {
          that.applyTag(item.tag.Id);
          delete that.video.tags[key];
          if (that.video.tags.length == 0) {
            that.checkTags = false;
          }
        }
      });
    }
  }

  applyTag(tag) {
    let params = {
      user_id: this.user.Id,
      tag: tag
    }

    this.service.postData('applyTag', params);
  }

}
