import { Injectable } from '@angular/core';

@Injectable()
export class StorageServiceProvider {
  label: string;
  label_touch: string;

  constructor(){
    this.label = 'distributor';
    this.label_touch = 'infusion_id';
  }
  
  set(key, value) {
    window.localStorage[key] = value;
    return this.get(key);
  }

  get(key) {
    return window.localStorage[key] || null;
  }

  setObject(key, value) {
    window.localStorage[key] = JSON.stringify(value);
    return this.get(key);
  }

  getObject(key) {
    return JSON.parse(window.localStorage[key] || null);
  }

  clear(){
    this.set(this.label, null);
  }

  clearTouch(){
    this.set(this.label_touch, null);
  }

}
