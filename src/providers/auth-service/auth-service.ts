import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class AuthServiceProvider {
  apiUrl: string;

  constructor(public http: Http) {
    this.apiUrl = "http://127.0.0.1:8000/";
  }

  postData(credentials, type){
    return new Promise((resolve, reject)=> {
      let headers = new Headers();
      this.http.post(this.apiUrl + type, JSON.stringify(credentials), {headers: headers}).subscribe(res => {
        resolve(res.json());
      }), (err) => {
        reject(err);
      };
    });
  }

}
