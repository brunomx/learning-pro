import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiServiceProvider } from '../api-service/api-service';
import { TranslateService } from '../../../node_modules/@ngx-translate/core';
import { LoadingController, Events, AlertController } from 'ionic-angular';
import Player from "@vimeo/player";
import { DatabaseProvider } from '../database/database';

@Injectable()
export class VimeoServiceProvider {
  video: any;
  private player: Player;
  promisse: any;
  loader: any;
  user: any;
  duration: any;

  constructor(public http: HttpClient, public service: ApiServiceProvider, public translate: TranslateService, public loadingCtrl: LoadingController, private database: DatabaseProvider, public events: Events, public alertCtrl: AlertController) {
    this.duration = 0;
    this.database.getUser().then((user) => {
      this.user = user;
    });

    this.translate.get("LOADING").subscribe((result: string) => {
      this.loader = this.loadingCtrl.create({
        content: result,
      });
      this.loader.present();
    });
  }

  initPlayer(video) {
    this.video = video;
    this.player = new Player('video-embed', {});
    let that = this;

    if(this.video.duration == null){
      this.player.getDuration().then((seconds) => {
        that.video.duration = seconds;
        that.duration = seconds;
      });
    }

    this.player.ready().then((res) => {
      that.loader.dismiss();
      if (that.video.watch_time == 0 || that.video.progress == 100) {
        that.player.play();
      } else {
        that.player.setCurrentTime(that.video.watch_time).then((res) => {
          that.player.play();
        });
      }
    });

    this.player.on('pause', function (res) {
      that.saveWatchTime(Math.round(res.seconds), 0);
    });

    this.player.on('ended', function (res) {
      that.saveWatchTime(Math.round(res.seconds), 1);
      that.events.publish('endVideo', true);
    });
  }

  stopPlayer() {
    let that = this;
    this.player.getCurrentTime().then(function (seconds) {
      that.saveWatchTime(Math.round(seconds), 0);
    });
  }

  saveWatchTime(seconds, completed) {
    let params = {
      video_id: this.video.id,
      user: this.user,
      watch_time: seconds,
      completed: completed,
      duration: this.duration,
    };

    this.service.postDataWithToken('userVideo-register', params).then((res) => {
    }, (err) => {
      this.loader.dismiss();
      let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('ERROR'),
          subTitle: this.translate.instant('ERROR_AVAILABLE'),
          buttons: ['Ok']
        });
      alert.present();
    });
  }
}