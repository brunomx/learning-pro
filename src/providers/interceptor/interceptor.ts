import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { DatabaseProvider } from '../database/database';

@Injectable()
export class InterceptorProvider implements HttpInterceptor {
  
  constructor(public database: DatabaseProvider) {
    
  }
  
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{    
    let promisse = this.database.getUser();
    return Observable.fromPromise(promisse)
    .mergeMap(user => {
      if (user == null){
        user = { access_token : false};
      }
      
      let cloneReq = this.addToken(request, user.access_token);
      return next.handle(cloneReq).pipe(
          catchError(error => {
            return [];
          })
        );
      });
    }
    
    private addToken(request: HttpRequest<any>, token: any){
      if(token){
        let clone: HttpRequest<any>;
        clone = request.clone({
          setHeaders: {
            Accept: `application/json`,
            'Content-Type': `text/html`,
            Authorization: `Bearer ${token}`
          }
        });
        return clone;
      }
      return request;
    }
  }
