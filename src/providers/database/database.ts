import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { Device } from '@ionic-native/device/ngx';

declare var window; 

@Injectable()
export class DatabaseProvider {
  db: any;
  tables: any;
  dbo: any;
  
  constructor(public http: HttpClient, private sqlite: SQLite, public device: Device) {
    this.dbo = null;
    this.tables = [
      {
        name: 'prefefences',
        columns: [
          { name: 'id', type: 'integer primary key' },
          { name: 'language', type: 'text' },
        ]
      },
      {
        name: 'users',
        columns: [
          { name: 'id', type: 'integer primary key' },
          { name: 'first_name', type: 'text' },
          { name: 'last_name', type: 'text' },
          { name: 'email', type: 'text' },
          { name: 'birth', type: 'text' },
          { name: 'gender', type: 'text' },
          { name: 'rg', type: 'text' },
          { name: 'cpf', type: 'text' },
          { name: 'aff_code', type: 'text' },
          { name: 'aff_id', type: 'integer' },
          { name: 'access_token', type: 'text' },
          { name: 'token_type', type: 'text' },
          { name: 'user_id', type: 'integer' },
        ]
      },
      {
        name: 'devices',
        columns: [
          { name: 'id', type: 'integer primary key' },
          { name: 'user_id', type: 'integer' },
          { name: 'email', type: 'text' },
          { name: 'ud', type: 'integer' },
        ]
      },
      {
        name: 'modules',
        columns: [
          { name: 'id', type: 'integer primary key' },
          { name: 'name', type: 'integer' },
          { name: 'description', type: 'text' },
          { name: 'param', type: 'text' },
          { name: 'route', type: 'text' },
          { name: 'icon', type: 'text' },
          { name: 'sequence', type: 'integer' },
          { name: 'module_id', type: 'integer' },
        ]
      },
    ];
  }
  
  private query(query, bindings = []) {
    try {
      let that = this;
      return new Promise(function (resolve, reject) {
        that.dbo.transaction(function (transaction) {
          transaction.executeSql(query, bindings, function(transaction, result) {
            return resolve(result);
          });
        }, reject);
      });
    } catch (error) {
      console.log(error);
    }
  }

  private fetch = function (result) {
    if (result.rows.length > 0) {
      return result.rows.item(0);
    } else {
      return null;
    }
  }

  private fetchAll(result) {
    if (result.rows.length > 0) {
      return result.rows;
    } else {
      return null;
    }
  }
  
  public start() {
    Object.keys(this.tables).forEach(index => {
      let table = this.tables[index], columns = [];
      Object.keys(table.columns).forEach(i => {
        columns.push(table.columns[i].name + ' ' + table.columns[i].type);
      });
      let query = 'CREATE TABLE IF NOT EXISTS ' + table.name + ' (' + columns.join(',') + ')';
      this.query(query);
    });
  }
  
  public createDatabase() {
    let that = this;
    if (this.dbo == null) {
      if (this.device.platform != null) {
        return new Promise(function (resolve, reject) {
          that.sqlite.create({
            name: 'data.db',
            location: 'default'
          }).then((db: SQLiteObject) => {
            that.dbo = db;
            that.start()
            resolve(db);
          }).catch((e) => {
            reject(e);
          });
        });
      } else {
        return new Promise((resolve, reject) => {
          try {
            that.dbo = window.openDatabase('data.db', '1.0', 'data', 65536);
            that.start();
            resolve(that.dbo)
          } catch (error) {
            reject(error);
          }
        });
      }
    }
  }
  
  public savePreferences(item){
    let query = "", values = [];
    if(item.id == null){
      query = "INSERT INTO prefefences (language) VALUES (?)";
      values = [item.language];
    } else {
      query = "UPDATE prefefences SET language = ? WHERE id = ?";
      values = [item.language, item.id];
    }
    
    this.query(query, values);
  }
  
  public saveUser(data) {
    let query = "", values = [];
    return this.getUserFromUser_id(data.user.id).then((user) => {
      if (user == null) {
        query = "INSERT INTO users (first_name, last_name, email, birth, gender, rg, cpf, aff_code, aff_id, token_type, access_token, user_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        values = [data.user.first_name, data.user.last_name, data.user.email, data.user.birth, data.user.gender, data.user.rg, data.user.cpf, data.user.username, null, data.token_type , data.access_token, data.user.id];
      } else {
        query = "UPDATE users SET first_name = ?, last_name = ?, email = ?, birth = ?, gender = ?, rg = ?, cpf = ?, aff_code = ?, aff_id = ?, access_token = ?, token_type = ?  WHERE id = ?";
        values = [data.user.first_name, data.user.last_name, data.user.email, data.user.birth, data.user.gender, data.user.rg, data.user.cpf, data.user.username, null, data.token_type , data.access_token, user.id];
      }
      return this.query(query, values);
    });
  }

  public saveModule(data) {    
      let query = "", values = [];
      query = "INSERT INTO modules (name, description, param, route, icon, sequence, module_id) VALUES (?, ?, ?, ?, ?, ?, ?)";
      values = [data.name, data.description, data.param, data.route, data.icon, data.sequence, data.id];
      return this.query(query, values);
  }

  public getModules() {
    let that = this,
      query = "SELECT * FROM modules";

    if (this.dbo == null) {
      return this.createDatabase().then((db) => {
        return this.query(query, []).then((res) => {
          return that.fetchAll(res);
        });
      })
    } else {
      return this.query(query, []).then((res) => {
        return that.fetchAll(res);
      });
    }
  }

  public getFirstModule() {
    let that = this,
      query = "SELECT * FROM modules";

    if (this.dbo == null) {
      return this.createDatabase().then((db) => {
        return this.query(query, []).then((res) => {
          return that.fetch(res);
        });
      })
    } else {
      return this.query(query, []).then((res) => {
        return that.fetch(res);
      });
    }
  }
  
  public getUser(){
    let that = this,
    query = "SELECT * FROM users";
    
    if (this.dbo == null) {
      return this.createDatabase().then((db) => {
        return this.query(query, []).then((res) => {
          return that.fetch(res);
        });
      })
    } else {
      return this.query(query, []).then((res) => {
        return that.fetch(res);
      });
    }
  }
  
  public deleteUser(id){
    let that = this,
    query = "DELETE FROM users WHERE id = ?",
    values = [id];
    return this.query(query, values).then((res) => {
      that.deleteModules();
      return that.fetch(res);
    });
  }

  public deleteModules() {
    let that = this,
      query = "DELETE FROM modules";
    return this.query(query, []).then((res) => {
      return that.fetch(res);
    });
  }
  
  private getUserFromUser_id(user_id){
    let that = this,
    query = "SELECT id FROM users WHERE user_id = ?",
    values = [user_id];
    return this.query(query, values).then((res) => {
      return that.fetch(res);
    });
  } 
  
  public getLanguage(){
    let that = this;
    let query = "SELECT id,language FROM prefefences";
    if (this.dbo == null) {
      return this.createDatabase().then((db) => {
        return that.query(query, []).then((res) => {
          return that.fetch(res);
        });
      })
    } else {
      return that.query(query, []).then((res) => {
        return that.fetch(res);
      });
    }
  }
}
