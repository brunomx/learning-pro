import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Platform, ModalController } from 'ionic-angular';
import { Device } from '@ionic-native/device/ngx';
import { ApiServiceProvider } from '../api-service/api-service';
import { StorageServiceProvider } from '../storage-service/storage-service';

@Injectable()
export class PushNotificationsProvider {
  oneSignalId: string;
  user: any;
  OneSignal: any;
  projectNumberGoogle: string;
  
  constructor(public http: HttpClient, public platform: Platform, private device: Device,  public service: ApiServiceProvider, public storage: StorageServiceProvider, private modal: ModalController) {
    this.oneSignalId = 'b98069fb-331e-45c5-8e6a-ab658c1bbaba';
    this.projectNumberGoogle = '469172553493';
  }
  
  init(user){
    const that = this;
    that.platform.ready().then((readySource) => {
      if(that.device.uuid != null){
        that.OneSignal = window["plugins"].OneSignal;
        that.user = user;
        that.OneSignal.getIds(function(ids) {
          that.OneSignal.sendTag("platform", that.device.platform);
          let params = {
            user: that.user,
            token: ids.pushToken,
            player_ids: ids.userId,
            manufacturer: that.device.manufacturer,
            model: that.device.model,
            platform: that.device.platform,
            serial: that.device.serial,
            uuid: that.device.uuid,
            version: that.device.version,
          }
          that.service.postDataWithToken('contact-device', params);
        });
        
        that.OneSignal
        .startInit(that.oneSignalId, that.projectNumberGoogle)
        .handleNotificationReceived(function(data){
          let alert  = {
            title: data.payload.title,
            message: data.payload.body,
            data: data.payload.additionalData,
          }

          const alertModal = that.modal.create('AlertModalPage', { alert: alert , received: true });
          alertModal.present();
        })
        .handleNotificationOpened(function(data){
          let alert  = {
            title: data.notification.payload.title,
            message: data.notification.payload.body,
            data: data.notification.payload.additionalData,
          }

          const alertModal = that.modal.create('AlertModalPage', { alert: alert , received: true });
          alertModal.present();
        })
        .inFocusDisplaying(that.OneSignal.OSInFocusDisplayOption.None)
        .endInit();
      }
    });
  }
}
