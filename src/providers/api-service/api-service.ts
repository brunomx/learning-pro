import { Http, Headers } from '@angular/http';  
import { Injectable } from '@angular/core';
import { Events } from "ionic-angular";

@Injectable()
export class ApiServiceProvider {
  apiUrl: string;

  constructor(public http: Http, private events: Events) {
    this.apiUrl = "http://bigprofy-env.eba-dxdk5gwn.sa-east-1.elasticbeanstalk.com/api/";
    // this.apiUrl = "http://18.231.88.224/api/";
  }

  getData(type, params) {
    return new Promise((resolve, reject) => {
      this.http.get(this.apiUrl + type + "?" + this.serializeObj(params)).subscribe(res => {
        resolve(res); 
      }, (err) => {
        reject(err);
      });
    });
  }

  getDataWithToken(type, params) {
    let that = this;
    return new Promise((resolve, reject) => {
      let token = params.user.token_type + ' ' + params.user.access_token,
      headers = new Headers({ 'Accept': 'application/json', 'Authorization': token});
      this.http.get(this.apiUrl + type, { headers: headers }).subscribe(res => {
        resolve(res.json());
      }, (err) => {
          if (err.status == 401) {
            that.logout();
          }
        reject(err);
      });
    });
  }

  postDataWithToken(type, params) {
    let that = this;
    return new Promise((resolve, reject) => {
      let token = params.user.token_type + ' ' + params.user.access_token,
      headers = new Headers({ 'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': token });
      delete params.user;
      this.http.post(this.apiUrl + type, JSON.stringify(params), { headers: headers }).subscribe(res => {
        resolve(res.json());
      }, (err) => {
          if (err.status == 401) {
            that.logout();
          }
        reject(err);
      });
    });
  }

  postData(type, params) {
    return new Promise((resolve, reject) => {
      let headers = new Headers({ 'Content-Type': 'application/json', 'Accept': 'application/json' });
      this.http.post(this.apiUrl + type, JSON.stringify(params), { headers: headers }).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
    });
  }

  serializeObj(obj) {
    let result = [];
    for (let property in obj) {
      result.push(encodeURIComponent(property) + "=" + encodeURIComponent(obj[property]));
    }

    return result.join("&");
  }

  logout() {
    this.events.publish('logout');
  }
}
