import { Directive, Renderer, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[progress]'
})
export class ProgressDirective {
  @Input('progress') progress: number;

  constructor(private renderer: Renderer, private elementRef: ElementRef) {
  }
  ngOnInit() {
    this.renderer.setElementStyle(this.elementRef.nativeElement, 'width', this.progress+'%');
  }
}
