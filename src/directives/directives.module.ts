import { NgModule } from '@angular/core';
import { ProgressDirective } from './progress/progress';
@NgModule({
	declarations: [ProgressDirective],
	imports: [],
	exports: [ProgressDirective]
})
export class DirectivesModule {}
