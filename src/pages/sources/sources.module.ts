import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SourcesPage } from './sources';
import { TranslateModule } from '../../../node_modules/@ngx-translate/core';

@NgModule({
  declarations: [
    SourcesPage,
    TranslateModule,
  ],
  imports: [
    IonicPageModule.forChild(SourcesPage),
  ],
})
export class SourcesPageModule {}
