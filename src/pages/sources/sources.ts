import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Events, LoadingController, MenuController } from 'ionic-angular';
import { TranslateService } from '../../../node_modules/@ngx-translate/core';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { SourceVideoPage } from '../source-video/source-video';
import { SourceAudioPage } from '../source-audio/source-audio';
import { SourcePdfPage } from '../source-pdf/source-pdf';
import { DatabaseProvider } from '../../providers/database/database';

@IonicPage()
@Component({
  selector: 'page-sources',
  templateUrl: 'sources.html',
})
export class SourcesPage {
  video: any;
  audio: any;
  pdf: any;
  loader: any;
  result: any;
  souces: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private translate: TranslateService, private service: ApiServiceProvider, public alertCtrl: AlertController, public events: Events, private loading: LoadingController, private database: DatabaseProvider, public menuCtrl: MenuController) {
    this.video = SourceVideoPage;
    this.audio = SourceAudioPage;
    this.pdf = SourcePdfPage;
  }

  ionViewWillEnter() {
    this.translate.get("LOADING").subscribe((result: string) => {
      this.loader = this.loading.create({
        content: result,
      });
      this.loader.present();
      this.database.getUser().then((user) => {
        if (user != null) {
          this.getData({ user: user });
        }
      });
    });
  }

  getData(params){
    this.service.getDataWithToken('material', params).then((res) => {
      this.loader.dismiss();
      this.result = res;
      if (this.result.status == true) {
        this.navParams.data.souces = this.result.materials;
        this.events.publish('loadSources', this.result.materials);
      } else {
        let alert = this
          .alertCtrl
          .create({
            title: this.translate.instant('ERROR'),
            subTitle: this.translate.instant('ERROR_AVAILABLE'),
            buttons: ['Ok']
          });
        alert.present();
      }
    }, (err) => {
      let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('ERROR'),
          subTitle: this.translate.instant('ERROR_AVAILABLE'),
          buttons: ['Ok']
        });
      alert.present();
    });
  }

}
