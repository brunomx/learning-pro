import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DisclosureProdutsPage } from './disclosure-produts';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    DisclosureProdutsPage,
  ],
  imports: [
    IonicPageModule.forChild(DisclosureProdutsPage),
    TranslateModule
  ],
})
export class DisclosureProdutsPageModule {}
