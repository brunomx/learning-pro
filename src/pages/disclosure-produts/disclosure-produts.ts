import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { TranslateService } from '@ngx-translate/core';
import { Device } from '@ionic-native/device/ngx';
import { StorageServiceProvider } from '../../providers/storage-service/storage-service';

@IonicPage()
@Component({
  selector: 'page-disclosure-produts',
  templateUrl: 'disclosure-produts.html',
})
export class DisclosureProdutsPage {
  loader: any;
  result: any;
  links: any;
  user: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public service: ApiServiceProvider, public loadingCtrl: LoadingController, public alertCtrl: AlertController, private socialSharing: SocialSharing, private clipboard: Clipboard, public translate: TranslateService, private device: Device, private storage: StorageServiceProvider, private loading: LoadingController) {
    this.user = this.storage.getObject('distributor');
    this.links = [];
  }

  ionViewDidLoad() {
    if (this.navParams.data.hasOwnProperty('links') == false) {
      this.translate.get("LOADING").subscribe((result: string) => {
        this.loader = this.loading.create({
          content: result,
        });
        this.loader.present();
      });

      let data = {
        user_id: this.user.Id,
        affId: this.user.AffId,
        affCode: this.user.AffCode,
      }

      this.service.postData('listLinksForAffiliateTabs', data).then((res) => {
        this.loader.dismiss();
        this.result = res;
        if (this.result.status == true) {
          this.links = this.result.links;
        } else {
          let alert = this
          .alertCtrl
          .create({
            title: this.translate.instant('ERROR'),
            subTitle: this.translate.instant('ERROR_AVAILABLE'),
            buttons: ['Ok']
          });
          alert.present();
        }
      }, (err) => {
        this.loader.dismiss();
        let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('ERROR'),
          subTitle: this.translate.instant('ERROR_AVAILABLE'),
          buttons: ['Ok']
        });
        alert.present();
      });
    } else {
      this.links = this.navParams.data.links;
    }
  }

  copy(item) {
    let url = item.redirect + '?' + item.code;
    this.clipboard.copy(url).then((res) => {
      let alert = this
      .alertCtrl
      .create({
        title: 'Link',
        subTitle: this.translate.instant('COPIED_SUCCESSFULLY'),
        buttons: ['Ok']
      });
      alert.present();
    }).catch((err) => {
      let alert = this
      .alertCtrl
      .create({
        title: this.translate.instant('ERROR'),
        subTitle: this.translate.instant('ERROR_AVAILABLE'),
        buttons: ['Ok']
      });
      alert.present();
    });
  }
  
  share(item, social) {
    this.translate.get("LOADING").subscribe((result: string) => {
      this.loader = this.loadingCtrl.create({
        content: result,
      });
      this.loader.present();
    });
    switch (social) {
      case 'facebook':
      let url = item.redirect + '?' + item.code;
      this.socialSharing.shareViaFacebookWithPasteMessageHint(null, null, url, item.name).then((res) => {
        this.loader.dismiss();
      }).catch((err) => {
        this.loader.dismiss();
      });
      break;
      case 'whatsapp':
      if (this.device.platform == "iOS") {
        item.image = null;
      }
      this.socialSharing.shareViaWhatsApp(item.name, item.image, item.url).then((res) => {
        this.loader.dismiss();
      }).catch((err) => {
        this.loader.dismiss();
      });
      break;
      case 'twitter':
      this.socialSharing.shareViaTwitter(item.name, item.image, item.url).then((res) => {
        this.loader.dismiss();
      }).catch((err) => {
        this.loader.dismiss();
      });
      break;
    }
  }

}
