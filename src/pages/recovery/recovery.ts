import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, AbstractControl, Validators } from '../../../node_modules/@angular/forms';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { TranslateService } from '../../../node_modules/@ngx-translate/core';

@IonicPage()
@Component({
  selector: 'page-recovery',
  templateUrl: 'recovery.html',
})
export class RecoveryPage {
  formGroup: FormGroup;
  email: AbstractControl;
  loader: any;
  result: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private view: ViewController, public formBuilder: FormBuilder, public service: ApiServiceProvider, public loadingCtrl: LoadingController, public translate: TranslateService, public alertCtrl: AlertController) {
    this.formGroup = formBuilder.group({
      email: ['', Validators.email]
    });

    this.email = this.formGroup.controls['email'];
  }

  ionViewDidLoad() {
  }

  closeModal() {
    this.view.dismiss();
  }

  send(){
    this.loader = this.loadingCtrl.create({
      content: this.translate.instant('LOADING'),
    });
    this.loader.present();

    let params = {
      email: this.email.value,
    }

    this.service.postData('recoveryPassword', params).then((res) => {
      this.loader.dismiss();
      this.result = res;
      if (this.result.status == true) {
        let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('SENT'),
          subTitle: this.translate.instant('SENT_PASSWORD'),
          buttons: [
            {
              text: 'Ok',
              handler: () => {
                this.closeModal();
              }
            }
          ]
        });
      alert.present();
      } else {
        let alert = this
          .alertCtrl
          .create({
            title: this.translate.instant('ERROR'),
            subTitle: this.translate.instant('ERROR_REGISTRATION'),
            buttons: ['Ok']
          });
        alert.present();
      }
    }, (err) => {
      this.loader.dismiss();
      let alert = this
        .alertCtrl
        .create({
            title: this.translate.instant('ERROR'),
            subTitle: this.translate.instant('ERROR_REGISTRATION'),
            buttons: ['Ok']
        });
      alert.present();
    });
  }

}
