import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecoveryPage } from './recovery';
import { TranslateModule } from '../../../node_modules/@ngx-translate/core';

@NgModule({
  declarations: [
    RecoveryPage,
  ],
  imports: [
    IonicPageModule.forChild(RecoveryPage),
    TranslateModule
  ],
})
export class RecoveryPageModule {}
