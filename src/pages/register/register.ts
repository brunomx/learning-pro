import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  constructor(public navParams: NavParams, private view: ViewController) {
  }

  ionViewDidLoad() {
  }

  closeModal(){
    this.view.dismiss();
  }

}