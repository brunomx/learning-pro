import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DailyPage } from './daily';
import { TranslateModule } from '../../../node_modules/@ngx-translate/core';

@NgModule({
  declarations: [
    DailyPage,
  ],
  imports: [
    IonicPageModule.forChild(DailyPage),
    TranslateModule
  ],
})
export class DailyPageModule {}
