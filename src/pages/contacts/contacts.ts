import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, Events } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { TranslateService } from '../../../node_modules/@ngx-translate/core';
import { ContactDetailsPage } from '../contact-details/contact-details';
import { ContactsCompletePage } from '../contacts-complete/contacts-complete';
import { ContactsDraftPage } from '../contacts-draft/contacts-draft';
import { DatabaseProvider } from '../../providers/database/database';

@IonicPage()
@Component({
  selector: 'page-contacts',
  templateUrl: 'contacts.html',
})
export class ContactsPage {
  loader: any;
  user: any;
  result: any;
  contacts: any;
  complete: any;
  draft: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public service: ApiServiceProvider, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public translate: TranslateService, public events: Events, private database: DatabaseProvider) {
    this.complete = ContactsCompletePage;
    this.draft = ContactsDraftPage;

    this.events.subscribe('reloadContacts', (data) => {
      if (data == true) {
        this.loadResults(false);
      }
    });
  }

  ionViewDidEnter(){
    if(this.result != undefined){
      this.loadResults(false);
    }
  }

  ionViewDidLoad() {
    this.loadResults(true);
  }

  loadResults(empty){
    this.translate.get("LOADING").subscribe((result: string) => {
      this.loader = this.loadingCtrl.create({
        content: result,
      });
      this.database.getUser().then((user) => {
        if (user != null) {
          this.loader.present();
          this.getData(user, empty);
        }
      });
    });
  }

  getData(user, empty) {
    let params = {
      user: user,
    }

    this.service.getDataWithToken('contact', params).then((res) => {
      this.loader.dismiss();
      this.result = res;
      if (this.result.status == true && this.result.contacts.length > 0) {
        let contacts = [];
        Object.keys(this.result.contacts).forEach(key => {
          let item = this.result.contacts[key];
          item.stars = this.countStars(item.stars);
          contacts.push(item);
        });

        this.navParams.data.contacts = contacts;
        this.events.publish('loadContacts', contacts);
      } else {
        if (empty){
          this.register();
        }
      }
    }, (err) => {
      this.events.publish('loadContacts', false);
      this.loader.dismiss();
      let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('ERROR'),
          subTitle: this.translate.instant('ERROR_AVAILABLE'),
          buttons: ['Ok']
        });
      alert.present();
    });
  }

  countStars(stars) {
    let itens = [];
    for (let i = 0; i < stars; i++) {
      itens.push(i);
    }
    return itens;
  }

  register() {
    this.navCtrl.push(ContactDetailsPage, {
      contact: null,
    });
  }

  edit(contact) {
    this.navCtrl.push(ContactDetailsPage, {
      contact: contact,
    });
  }
}
