import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DisclosurePage } from './disclosure';
import { TranslateModule } from '../../../node_modules/@ngx-translate/core';

@NgModule({
  declarations: [
    DisclosurePage,
  ],
  imports: [
    IonicPageModule.forChild(DisclosurePage),
    TranslateModule,
  ],
})
export class DisclosurePageModule {}
