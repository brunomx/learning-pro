import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Events } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { TranslateService } from '../../../node_modules/@ngx-translate/core';
import { StorageServiceProvider } from '../../providers/storage-service/storage-service';
import { DisclosureBusinessPage } from '../disclosure-business/disclosure-business';
import { DisclosureProdutsPage } from '../disclosure-produts/disclosure-produts';

@IonicPage()
@Component({
  selector: 'page-disclosure',
  templateUrl: 'disclosure.html',
})
export class DisclosurePage {
  loader: any;
  result: any;
  links: any;
  user: any;
  business: any;
  produts: any;
  
  constructor(public navCtrl: NavController, public navParams: NavParams,  public events: Events, public service: ApiServiceProvider, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public translate: TranslateService, private storage: StorageServiceProvider) {
    this.user = this.storage.getObject('distributor');
    this.translate.get("LOADING").subscribe((result: string) => {
      this.loader = this.loadingCtrl.create({
        content: result,
      });
      this.loader.present();
    });
    
    this.business = DisclosureBusinessPage;
    this.produts = DisclosureProdutsPage;
  }
  
  ionViewDidLoad() {
    let data = {
      user_id: this.user.Id,
      affId: this.user.AffId,
      affCode: this.user.AffCode,
    }
    
    this.service.postData('listLinksForAffiliateTabs', data).then((res) => {
      this.loader.dismiss();
      this.result = res;
      if (this.result.status == true) {
        this.navParams.data.links = this.result.links;
        this.events.publish('loadLinks', this.result.links);
      } else {
        let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('ERROR'),
          subTitle: this.translate.instant('ERROR_AVAILABLE'),
          buttons: ['Ok']
        });
        alert.present();
      }
    }, (err) => {
      this.loader.dismiss();
      let alert = this
      .alertCtrl
      .create({
        title: this.translate.instant('ERROR'),
        subTitle: this.translate.instant('ERROR_AVAILABLE'),
        buttons: ['Ok']
      });
      alert.present();
    });
  }
}
