import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AudioOfflinePage } from './audio-offline';
import { TranslateModule } from '../../../node_modules/@ngx-translate/core';
import { IonicAudioModule } from '../../../node_modules/ionic-audio';

@NgModule({
  declarations: [
    AudioOfflinePage,
  ],
  imports: [
    IonicPageModule.forChild(AudioOfflinePage),
    TranslateModule,
    IonicAudioModule,
  ],
})
export class AudioOfflinePageModule {}
