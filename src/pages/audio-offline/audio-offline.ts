import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ViewController } from 'ionic-angular';
import { File } from '../../../node_modules/@ionic-native/file/ngx';
import { FileTransferObject, FileTransfer } from '../../../node_modules/@ionic-native/file-transfer/ngx';
import { Network } from '../../../node_modules/@ionic-native/network/ngx';
import { TranslateService } from '../../../node_modules/@ngx-translate/core';
import { Device } from '@ionic-native/device/ngx';
import { AudioProvider } from '../../../node_modules/ionic-audio';

@IonicPage()
@Component({
  selector: 'page-audio-offline',
  templateUrl: 'audio-offline.html',
})
export class AudioOfflinePage {
  audio: any;
  downloading: boolean;
  doload3g: boolean;
  fileTransfer: FileTransferObject;
  downloadProgress: number;
  targetPath: string;
  track: { src: string; title: any; preload: string; };
  downloadStart: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private network: Network, private file: File, private zone: NgZone, private translate: TranslateService, public alert: AlertController, private view: ViewController, private device: Device, private transfer: FileTransfer, private _audioProvider: AudioProvider) {
    this.audio = this.navParams.data.audio;
    this.downloadStart = this.navParams.data.download;
    this.downloading = true;
    this.fileTransfer = this.transfer.create();
    this.downloadProgress = 0;
    let that = this;
    this.fileTransfer.onProgress((progress) => {
      that.zone.run(() => {
        that.downloadProgress = parseInt(((progress.loaded / progress.total) * 100).toFixed(2));
      });
    });

    if (this.device.platform == "Android") {
      this.targetPath = this.file.dataDirectory;
    } else {
      this.targetPath = this.file.documentsDirectory;
    }
  }

  ionViewDidLoad() {
    if (this.downloadStart) {
      this.download();
    } else {
      this.downloading = false;
      this.audio.targetFile = this.targetPath + this.removerAcentos(this.audio.name) + ".mp3";
      if (this.device.platform != "Android") {
        this.audio.targetFile = this.audio.targetFile.replace(/^file:\/\//, '');
      }

      this.track = {
        src: this.audio.targetFile,
        title: this.audio.name,
        preload: 'metadata'
      }
    }
  }

  onTrackFinished(track: any) {
    // console.log('Track finished', track);
  }

  download() {
    if (this.network.type == 'wifi' || this.doload3g) {
      let that = this;
      this.file.getFreeDiskSpace().then((space) => {
        let sizeFile = that.audio.size / 1000;
        if (space > sizeFile) {
          that.downloading = true;
          that.audio.targetFile = that.targetPath + that.removerAcentos(that.audio.name) + ".mp3";
          that.fileTransfer.download(that.audio.audio_url, that.audio.targetFile).then((res) => {
            that.downloading = false;
            that.downloadProgress = 0;
            if (that.device.platform != "Android") {
              that.audio.targetFile = that.audio.targetFile.replace(/^file:\/\//, '');
            }
            that.track = {
              src: that.audio.targetFile,
              title: that.audio.name,
              preload: 'metadata'
            }
            that.audio.exists = true;
          }, (error) => {
            that.downloading = false;
            that.downloadProgress = 0;
            let alert = that
              .alert
              .create({
                title: that.translate.instant('ERROR'),
                subTitle: that.translate.instant('MEMORY_FULL'),
                buttons: ['Ok']
              });
            alert.present();
          });
        } else {
          let alert = that
            .alert
            .create({
              title: that.translate.instant('ERROR'),
              subTitle: that.translate.instant('MEMORY_FULL'),
              buttons: ['Ok']
            });
          alert.present();
        }
      }, (err) => {
        let alert = that
          .alert
          .create({
            title: that.translate.instant('ERROR'),
            subTitle: that.translate.instant('MEMORY_FULL'),
            buttons: ['Ok']
          });
        alert.present();
      });
    } else {
      const confirm = this.alert.create({
        title: this.translate.instant('WIFI_NETWORK_TITLE'),
        message: this.translate.instant('WIFI_NETWORK'),
        buttons: [
          {
            text: this.translate.instant('DOWNLOAD_LATER'),
            handler: () => {
              this.view.dismiss();
            }
          },
          {
            text: this.translate.instant('DOWNLOAD_NOW'),
            handler: () => {
              this.doload3g = true;
              this.download();
            }
          }
        ]
      });
      confirm.present();
    }
  }

  closeModal() {
    this.doload3g = false;
    this._audioProvider.pause();
    this.view.dismiss(this.audio);
  }

  removerAcentos(s) {
    let map = { "â": "a", "Â": "A", "à": "a", "À": "A", "á": "a", "Á": "A", "ã": "a", "Ã": "A", "ê": "e", "Ê": "E", "è": "e", "È": "E", "é": "e", "É": "E", "î": "i", "Î": "I", "ì": "i", "Ì": "I", "í": "i", "Í": "I", "õ": "o", "Õ": "O", "ô": "o", "Ô": "O", "ò": "o", "Ò": "O", "ó": "o", "Ó": "O", "ü": "u", "Ü": "U", "û": "u", "Û": "U", "ú": "u", "Ú": "U", "ù": "u", "Ù": "U", "ç": "c", "Ç": "C" }
    return s.replace(/[\W\[\] ]/g, function (a) { return map[a] || a }).split(' ').join('_');
  }

}
