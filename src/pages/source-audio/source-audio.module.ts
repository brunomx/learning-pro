import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SourceAudioPage } from './source-audio';
import { TranslateModule } from '../../../node_modules/@ngx-translate/core';

@NgModule({
  declarations: [
    SourceAudioPage,
  ],
  imports: [
    IonicPageModule.forChild(SourceAudioPage),
    TranslateModule
  ],
})
export class SourceAudioPageModule {}
