import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ModalController, ViewController } from 'ionic-angular';
import { Device } from '../../../node_modules/@ionic-native/device/ngx';
import { File } from '../../../node_modules/@ionic-native/file/ngx';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { TranslateService } from '../../../node_modules/@ngx-translate/core';
import { DatabaseProvider } from '../../providers/database/database';

@IonicPage()
@Component({
  selector: 'page-source-audio',
  templateUrl: 'source-audio.html',
})
export class SourceAudioPage {
  loader: any;
  result: any;
  audios: any;
  groups: any[];
  categories: any[];
  targetPath: any;
  extensao: string;
  files: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private service: ApiServiceProvider, public alertCtrl: AlertController, private translate: TranslateService, private loading: LoadingController, private device: Device, private file: File, private modal: ModalController, private view: ViewController, private database: DatabaseProvider) {
    this.groups = [];
    this.extensao = '.mp3';
    if (this.device.platform == "Android") {
      this.targetPath = this.file.dataDirectory;
    } else {
      this.targetPath = this.file.documentsDirectory;
    }
  }
  
  ionViewDidLoad() {
    if (this.navParams.data.hasOwnProperty('souces') == false) {
      this.translate.get("LOADING").subscribe((result: string) => {
        this.loader = this.loading.create({
          content: result,
        });
        this.loader.present();
        this.database.getUser().then((user) => {
          if (user != null) {
            this.getData({ user: user });
          }
        });
      });
    } else {
      this.checkAudio(this.navParams.data.souces);
    }
  }
  getData(params) {
    this.service.getDataWithToken('material', params).then((res) => {
      this.loader.dismiss();
      this.result = res;
      if (this.result.status == true) {
        this.checkAudio(this.result.materials);
      } else {
        let alert = this
          .alertCtrl
          .create({
            title: this.translate.instant('ERROR'),
            subTitle: this.translate.instant('ERROR_AVAILABLE'),
            buttons: ['Ok']
          });
        alert.present();
      }
    }, (err) => {
      this.loader.dismiss();
      let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('ERROR'),
          subTitle: this.translate.instant('ERROR_AVAILABLE'),
          buttons: ['Ok']
        });
      alert.present();
    });
  }

  checkAudio(audios) {
    let elements = [],
      that = this;
    this.files = [];
    Object.keys(audios).forEach(key => {
      let item = audios[key];
      if (item.hasOwnProperty('type')) {
        if (item.type == 1) {
          this.files.push(item);
          if (this.device.platform != null) {
            this.checkFile(item);
          }
          if (item.category_id != null) {
            that.populate(item);
          } else {
            elements.push(item);
          }
        }
      }
    });
    if (this.navParams.data.hasOwnProperty('push')) {
      if (this.navParams.data.push.source == '1') {
        this.openPush(this.navParams.data.push);
      }
    }
    this.audios = elements;
    this.order();
  }

  openPush(data) {
    this.navParams.data = {};
    Object.keys(this.files).forEach(key => {
      let item = this.files[key];
      if (item.id == data.item_id) {
        this.open(item);
      }
    });
  }

  populate(item) {
    if (this.groups[item.category_id] != undefined) {
      this.groups[item.category_id].itens.push(item);
    } else {
      this.groups[item.category_id] = { name: item.category_name, itens: [] };
      this.groups[item.category_id].itens.push(item);
    }
  }

  order() {
    let orderGroup = [],
      that = this;
    Object.keys(this.groups).forEach(key => {
      if (this.groups[key].hasOwnProperty('itens')) {
        orderGroup.push(this.groups[key]);
      }
    });
    that.categories = orderGroup.reverse();
  }

  checkFile(item) {
    let fileName = this.removerAcentos(item.name) + this.extensao;
    this.file.checkFile(this.targetPath, fileName).then((res) => {
      item.exists = true;
    }, (err) => {
      item.exists = false;
    });
  }

  open(audio){
    const modal = this.modal.create('AudioOnlinePage', { audio: audio });
    modal.present();
  }

  download(audio){
    const modal = this.modal.create('AudioOfflinePage', { audio: audio, download: true });
    modal.onDidDismiss((res) => {
        audio = res;
    });
    modal.present();
  }

  openLocal(audio){
    const modal = this.modal.create('AudioOfflinePage', { audio: audio, download: false });
    modal.present();
  }

  delete(audio){
    let fileName = this.removerAcentos(audio.name) + this.extensao;
    this.file.removeFile(this.targetPath, fileName).then((res) => {
      audio.exists = false;
      let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('FILE'),
          subTitle: this.translate.instant('DELETED'),
          buttons: ['Ok']
        });
      alert.present().then((res) => {
        this.view.dismiss();
      });
    }, (err) => {
      let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('REMOVE_VIDEO_TITLE'),
          subTitle: this.translate.instant('REMOVE_VIDEO'),
          buttons: ['Ok']
        });
      alert.present();
    });
  }

  removerAcentos(s) {
    let map = { "â": "a", "Â": "A", "à": "a", "À": "A", "á": "a", "Á": "A", "ã": "a", "Ã": "A", "ê": "e", "Ê": "E", "è": "e", "È": "E", "é": "e", "É": "E", "î": "i", "Î": "I", "ì": "i", "Ì": "I", "í": "i", "Í": "I", "õ": "o", "Õ": "O", "ô": "o", "Ô": "O", "ò": "o", "Ò": "O", "ó": "o", "Ó": "O", "ü": "u", "Ü": "U", "û": "u", "Û": "U", "ú": "u", "Ú": "U", "ù": "u", "Ù": "U", "ç": "c", "Ç": "C" }
    return s.replace(/[\W\[\] ]/g, function (a) { return map[a] || a }).split(' ').join('_');
  }

}
