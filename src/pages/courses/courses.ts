import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { TranslateService } from '../../../node_modules/@ngx-translate/core';
import { VideoCoursePage } from '../video-course/video-course';
import { DatabaseProvider } from '../../providers/database/database';

@IonicPage()
@Component({
  selector: 'page-courses',
  templateUrl: 'courses.html',
})
export class CoursesPage {
  loader: any;
  result: any;
  courses: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public service: ApiServiceProvider, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public translate: TranslateService, private database: DatabaseProvider) {
    this.translate.get("LOADING").subscribe((result: string) => {
      this.loader = this.loadingCtrl.create({
        content: result,
      });
      this.loader.present();
    });
  }

  ionViewWillEnter() {
    this.database.getUser().then((user) => {
      if(user != null){
        this.getData({ user: user });
      }
    });
  }

  getData(params){
    this.service.getDataWithToken('course', params).then((res) => {
      this.loader.dismiss();
      this.result = res;
      if (this.result.status == true) {
        this.courses = this.result.courses;
        if (this.navParams.data.hasOwnProperty('push')) {
          this.openPush(this.navParams.data.push);
        }
      } else {
        let alert = this
          .alertCtrl
          .create({
            title: this.translate.instant('ERROR'),
            subTitle: this.translate.instant('ERROR_AVAILABLE'),
            buttons: ['Ok']
          });
        alert.present();
      }
    }, (err) => {
      this.loader.dismiss();
      let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('ERROR'),
          subTitle: this.translate.instant('ERROR_AVAILABLE'),
          buttons: ['Ok']
        });
      alert.present();
    });
  }
  
  openPush(data) {
    this.navParams.data = {};
    Object.keys(this.courses).forEach(key => {
      let item = this.courses[key];
      if (item.id == data.course_id) {
        item.push = data;
        this.openVideos(item);
      }
    });
  }

  openVideos(course) {
    this.navCtrl.push(VideoCoursePage, {
      id: course.id,
      name: course.name,
      image: course.image,
      push: course.push == undefined ? null : course.push,
    });
  }

}
