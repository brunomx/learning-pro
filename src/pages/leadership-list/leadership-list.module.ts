import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LeadershipListPage } from './leadership-list';

@NgModule({
  declarations: [
    LeadershipListPage,
  ],
  imports: [
    IonicPageModule.forChild(LeadershipListPage),
  ],
})
export class LeadershipListPageModule {}
