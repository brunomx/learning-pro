import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { TranslateService } from '../../../node_modules/@ngx-translate/core';
import { DatabaseProvider } from '../../providers/database/database';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@IonicPage()
@Component({
  selector: 'page-leadership-list',
  templateUrl: 'leadership-list.html',
})
export class LeadershipListPage {
  qualifications: any;
  loader: any;
  result: any;
  month: number;
  months: string[];
  month_name: string;
  year: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, private service: ApiServiceProvider, public alertCtrl: AlertController, private translate: TranslateService, private loading: LoadingController, private database: DatabaseProvider, private socialSharing: SocialSharing, public loadingCtrl: LoadingController) {
    this.qualifications = [];
    let date = new Date();
    this.year = date.getFullYear();
  }

  ionViewDidEnter() {
    if (this.navParams.data.hasOwnProperty('qualifications') == false) {
      this.translate.get("LOADING").subscribe((result: string) => {
        this.loader = this.loading.create({
          content: result,
        });
        this.loader.present();
        this.database.getUser().then((user) => {
          if (user != null) {
            this.getData({ user: user });
          }
        });
      });
    } else {
      this.qualifications = this.navParams.data.qualifications;
    }
  }

  getData(params) {
    this.service.getDataWithToken('qualification', params).then((res) => {
      this.loader.dismiss();
      this.result = res;
      if (this.result.status == true) {
        this.qualifications = this.result.qualifications;
      } else {
        let alert = this
          .alertCtrl
          .create({
            title: this.translate.instant('ERROR'),
            subTitle: this.translate.instant('ERROR_AVAILABLE'),
            buttons: ['Ok']
          });
        alert.present();
      }
    }, (err) => {
      this.loader.dismiss();
      let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('ERROR'),
          subTitle: this.translate.instant('ERROR_AVAILABLE'),
          buttons: ['Ok']
        });
      alert.present();
    });
  }

  share(item, social) {
    this.translate.get("LOADING").subscribe((result: string) => {
      this.loader = this.loadingCtrl.create({
        content: result,
      });
      this.loader.present();
    });
    let message = item.name + ' - ' + item.level_name;
    switch (social) {
      case '1':
        this.socialSharing.shareViaFacebook(message, item.image, null).then((res) => {
          this.loader.dismiss();
        }).catch((err) => {
          this.loader.dismiss();
        });
        break;
      case '2':
        this.socialSharing.shareViaTwitter(message, item.image).then((res) => {
          this.loader.dismiss();
        }).catch((err) => {
          this.loader.dismiss();
        });
        break;
      case '3':
        this.socialSharing.shareViaWhatsApp(message, item.image, null).then((res) => {
          this.loader.dismiss();
        }).catch((err) => {
          this.loader.dismiss();
        });
        break;
    }
  }
}
