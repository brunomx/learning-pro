import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MeetingMapPage } from './meeting-map';

@NgModule({
  declarations: [
    MeetingMapPage,
  ],
  imports: [
    IonicPageModule.forChild(MeetingMapPage),
  ],
})
export class MeetingMapPageModule {}
