import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Events, } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { TranslateService } from '../../../node_modules/@ngx-translate/core';
import { DatabaseProvider } from '../../providers/database/database';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';

declare var google;

@IonicPage()
@Component({
  selector: 'page-meeting-map',
  templateUrl: 'meeting-map.html',
})
export class MeetingMapPage {
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  meetings: any;
  loader: any;
  result: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private service: ApiServiceProvider, public alertCtrl: AlertController, private translate: TranslateService, private loading: LoadingController, private database: DatabaseProvider, private events: Events, public loadingCtrl: LoadingController, public geolocation: Geolocation, private launchNavigator: LaunchNavigator) {
    this.meetings = null;
  }

  ionViewDidLoad() {
    this.events.subscribe('loadMeetings', (meetings) => {
      if (meetings == false) {
        this.translate.get("LOADING").subscribe((result: string) => {
          this.loader = this.loading.create({
            content: result,
          });
          this.loader.present();
          this.database.getUser().then((user) => {
            if (user != null) {
              this.getData({ user: user });
            }
          });
        });
      } else {
        this.meetings = meetings;
        this.loadMap();
      }
    });
  }

  getData(params) {
    this.service.getDataWithToken('meeting', params).then((res) => {
      this.loader.dismiss();
      this.result = res;
      if (this.result.status == true) {
        this.meetings = this.result.meetings;
        this.loadMap();
      } else {
        let alert = this
          .alertCtrl
          .create({
            title: this.translate.instant('ERROR'),
            subTitle: this.translate.instant('ERROR_AVAILABLE'),
            buttons: ['Ok']
          });
        alert.present();
      }
    }, (err) => {
      this.loader.dismiss();
      let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('ERROR'),
          subTitle: this.translate.instant('ERROR_AVAILABLE'),
          buttons: ['Ok']
        });
      alert.present();
    });
  }

  checkLocations(){
    if (this.meetings != null){
      Object.keys(this.meetings).forEach(key => {
        let item = this.meetings[key];
        this.addMarker(item, key);
      });
    }
  }

  loadMap(){
    this.translate.get("LOADING").subscribe((result: string) => {
      this.loader = this.loading.create({
        content: result,
      });
      this.loader.present();
      this.geolocation.getCurrentPosition().then((position) => {
        let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        let mapOptions = {
          center: latLng,
          zoom: 5,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
        if (this.meetings != null) {
          this.checkLocations();
        }
        this.loader.dismiss();
      }, 
      (err) => {
        let latLng = new google.maps.LatLng('-8.0462087', '-34.8955189');

        let mapOptions = {
          center: latLng,
          zoom: 5,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
        if (this.meetings != null) {
          this.checkLocations();
        }
        this.loader.dismiss();
      });
    });
  }

  addMarker(item, key) {
    let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      icon: 'assets/imgs/marker.png',
      position: { lat: item.address_latitude, lng: item.address_longitude },
      title: item.name
    }),
    content = '<div class="content-map"><strong>' + item.name + '</strong><br/>';
    content += '<br/><strong> Endereço:</strong> ' + item.address_address;
    content += '<br/><br/><strong> Data:</strong> ' + item.date;
    content += '<br><br><button id="tap" class="button button-dark button-full text-center" index="' + key + '">' + this.translate.instant('DRIVE_PLACE') +'</button></div>';

    this.addInfoWindow(marker, content);
  }

  addInfoWindow(marker, content) {
    let infoWindow = new google.maps.InfoWindow({
      content: content
    });

    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
      google.maps.event.addListenerOnce(infoWindow, 'domready', () => {
        document.getElementById('tap').addEventListener('click', (res) => {
          let key = document.getElementById('tap').getAttribute('index');
          this.goLocation(key);
          infoWindow.close();
        });
      });
    });
  }

  goLocation(key){
    let location = this.meetings[key],
    options: LaunchNavigatorOptions = {
      start: null,
    }

    this.launchNavigator.navigate([location.address_latitude, location.address_longitude], options).then((res) => {
    }, 
    (err) => {
      let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('ERROR'),
          subTitle: this.translate.instant('NOT_TRACE'),
          buttons: ['Ok']
        });
      alert.present();
    });
  }
}