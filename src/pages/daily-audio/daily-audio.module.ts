import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DailyAudioPage } from './daily-audio';
import { TranslateModule } from '../../../node_modules/@ngx-translate/core';

@NgModule({
  declarations: [
    DailyAudioPage,
    TranslateModule
  ],
  imports: [
    IonicPageModule.forChild(DailyAudioPage),
  ],
})
export class DailyAudioPageModule {}
