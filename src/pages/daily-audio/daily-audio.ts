import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, AlertController, LoadingController } from 'ionic-angular';
import { DailyAudioProdutoPage } from '../daily-audio-produto/daily-audio-produto';
import { DailyAudioMotivacionalPage } from '../daily-audio-motivacional/daily-audio-motivacional';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { TranslateService } from '../../../node_modules/@ngx-translate/core';
import { DatabaseProvider } from '../../providers/database/database';

@IonicPage()
@Component({
  selector: 'page-daily-audio',
  templateUrl: 'daily-audio.html',
})
export class DailyAudioPage {

  produto: any;
  motivacional: any;
  loader: any;
  result: any;
  souces: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private service: ApiServiceProvider, public events: Events, public alertCtrl: AlertController, private translate: TranslateService, private loading: LoadingController, private database: DatabaseProvider) {
    this.produto = DailyAudioProdutoPage;
    this.motivacional = DailyAudioMotivacionalPage;
  }

  ionViewWillEnter() {
    this.translate.get("LOADING").subscribe((result: string) => {
      this.loader = this.loading.create({
        content: result,
      });
      this.loader.present();
      this.database.getUser().then((user) => {
        if (user != null) {
          this.getData({ user: user });
        }
      });
    });
  }

  getData(params){
    this.service.getDataWithToken('audio', params).then((res) => {
      this.loader.dismiss();
      this.result = res;
      if (this.result.status == true) {
        this.navParams.data.audios = this.result.audios;
        this.events.publish('loadAudios', this.result.audios);
      } else {
        let alert = this
          .alertCtrl
          .create({
            title: this.translate.instant('ERROR'),
            subTitle: this.translate.instant('ERROR_AVAILABLE'),
            buttons: ['Ok']
          });
        alert.present();
      }
    }, (err) => {
      this.loader.dismiss();
      let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('ERROR'),
          subTitle: this.translate.instant('ERROR_AVAILABLE'),
          buttons: ['Ok']
        });
      alert.present();
    });
  }

}
