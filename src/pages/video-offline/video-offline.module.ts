import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VideoOfflinePage } from './video-offline';
import { TranslateModule } from '../../../node_modules/@ngx-translate/core';

@NgModule({
  declarations: [
    VideoOfflinePage,
  ],
  imports: [
    IonicPageModule.forChild(VideoOfflinePage),
    TranslateModule
  ],
})
export class VideoOfflinePageModule {}
