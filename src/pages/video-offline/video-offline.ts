import { Component} from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { TranslateService } from '../../../node_modules/@ngx-translate/core';
import { Device } from '../../../node_modules/@ionic-native/device/ngx';
import { VideoOfflineProvider } from '../../providers/video-offline/video-offline';
import { File } from '../../../node_modules/@ionic-native/file/ngx';

@IonicPage()
@Component({
  selector: 'page-video-offline',
  templateUrl: 'video-offline.html',
})
export class VideoOfflinePage {
  video: any;
  targetPath: string;
  currentTime: any;
  target: any;
  promisse: NodeJS.Timer;

  constructor(public navCtrl: NavController, public navParams: NavParams, private device: Device, private view: ViewController, public translate: TranslateService, private file: File, public alertCtrl: AlertController, private offline: VideoOfflineProvider) {
    this.video = this.navParams.data.video;
  }

  ionViewDidLoad() {
    if (this.device.platform != "Android") {
      this.video.targetFile = this.video.targetFile.replace(/^file:\/\//, '');
    }

    let that = this;
    this.promisse = setInterval(() => {
      document.getElementById('video-player').ontimeupdate = function (event) {
        that.target = event.target;
        let seconds = parseInt(that.target.currentTime,10);
        if(seconds > 0){
          that.offline.initPlayer(that.video, seconds);
        }
      }
    }, 1000);
  }

  closeModal() {
    clearInterval(this.promisse);
    this.view.dismiss();
  }

  delete() {
    if (this.device.platform == "Android") {
      this.targetPath = this.file.dataDirectory;
    } else {
      this.targetPath = this.file.syncedDataDirectory;
    }

    let fileName = this.video.path + ".mp4";

    this.file.removeFile(this.targetPath, fileName).then((res) => {
      let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('SUCCESSFULLY_REMOVED'),
          subTitle: this.translate.instant('SUCCESSFULLY_REMOVED_TEXT'),
          buttons: ['Ok']
        });
      alert.present().then((res) => {
        this.view.dismiss();
      });
    }, (err) => {
      let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('REMOVE_VIDEO_TITLE'),
          subTitle: this.translate.instant('REMOVE_VIDEO'),
          buttons: ['Ok']
        });
      alert.present();
    });
  }

}
