import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MeetingDetailsPage } from './meeting-details';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    MeetingDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(MeetingDetailsPage),
    TranslateModule
  ],
})
export class MeetingDetailsPageModule {}
