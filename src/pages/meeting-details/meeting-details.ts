import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, LoadingController } from 'ionic-angular';
import { TranslateService } from '../../../node_modules/@ngx-translate/core';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';

@IonicPage()
@Component({
  selector: 'page-meeting-details',
  templateUrl: 'meeting-details.html',
})
export class MeetingDetailsPage {
  meeting: any;
  loader: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private view: ViewController, private launchNavigator: LaunchNavigator, public alertCtrl: AlertController, private translate: TranslateService, private loading: LoadingController) {
    this.meeting = this.navParams.data.meeting;
  }

  closeModal() {
    this.view.dismiss();
  }

  goLocation() {
    let options: LaunchNavigatorOptions = {
        start: null,
      }

    this.launchNavigator.navigate([this.meeting.address_latitude, this.meeting.address_longitude], options).then((res) => {
      
    },
      (err) => {
        let alert = this
          .alertCtrl
          .create({
            title: this.translate.instant('ERROR'),
            subTitle: this.translate.instant('NOT_TRACE'),
            buttons: ['Ok']
          });
        alert.present();
      });
  }

}
