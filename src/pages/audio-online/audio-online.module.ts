import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AudioOnlinePage } from './audio-online';
import { TranslateModule } from '../../../node_modules/@ngx-translate/core';
import { IonicAudioModule } from '../../../node_modules/ionic-audio';
import { LazyLoadImageModule } from 'ng-lazyload-image';

@NgModule({
  declarations: [
    AudioOnlinePage,
  ],
  imports: [
    IonicPageModule.forChild(AudioOnlinePage),
    TranslateModule,
    IonicAudioModule,
    LazyLoadImageModule,
  ],
})
export class AudioOnlinePageModule {}
