import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, LoadingController, Events } from 'ionic-angular';
import { TranslateService } from '../../../node_modules/@ngx-translate/core';
import { DatabaseProvider } from '../../providers/database/database';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { AudioProvider, AudioTrackComponent } from 'ionic-audio';
import { Device } from '@ionic-native/device/ngx';

@IonicPage()
@Component({
  selector: 'page-audio-online',
  templateUrl: 'audio-online.html',
})
export class AudioOnlinePage {
  audio: any;
  loader: any;
  current: any;
  user: any;
  track: any;
  duration: any;
  image: any;
  setTime: any;
  interval: NodeJS.Timeout;
  @ViewChild(AudioTrackComponent) audioTrack: AudioTrackComponent;
  intervalPlay: NodeJS.Timeout;
  finished: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private view: ViewController, public translate: TranslateService, private database: DatabaseProvider, public service: ApiServiceProvider, public alertCtrl: AlertController, private _audioProvider: AudioProvider, private loading: LoadingController, private device: Device) {
    this.duration = 0;
    this.audio = this.navParams.data.audio;
    this.database.getUser().then((user) => {
      this.user = user;
    });
    this.track = null;
    this.setTime = true;
    this.finished = false;
  }
  ionViewWillEnter() {
  }

  ionViewDidLoad() {
    this.translate.get("LOADING").subscribe((result: string) => {
      this.loader = this.loading.create({
        content: result,
      });
      this.loader.present();
    });
    this.image = this.navParams.data.audio.image;
    this.track = {
      src: this.audio.path,
      title: this.audio.name,
      preload: 'metadata'
    }
  }

  checkLoad() {
    this.current.play();
    let that = this;
    this.interval = setInterval(function () {
      if (that.device.platform == "Android") {
        if (that.current.isPlaying == true && that.current._isLoading == false) {
          if (that.audio.progress != 100) {
            that.audioTrack.seekTo(that.audio.watch_time);
          }
          clearInterval(that.interval);
          that.isPlaying();
        }
      } else {
        if (that.current._progress > 0){
          if (that.audio.progress != 100) {
            that.audioTrack.seekTo(that.audio.watch_time);
          }
          clearInterval(that.interval);
          that.isPlaying();
        }
      }
    }, 1000);
  }

  isPlaying() {
    let that = this;
    this.intervalPlay = setInterval(function () {
      if (that.audioTrack.isPlaying) {
        that.loader.dismiss();
        clearInterval(that.intervalPlay);
      }
    }, 1000);
  }

  onTrackFinished(track: any) {
    if(this.finished == false){
      this.finished = true;
      this.saveWatchTime(this.current.progress, 1);
    }
  }

  onTrackPause(track: any) {
    this.saveWatchTime(this.current.progress, 0);
  }

  onTrackLoad(track) {
    this.current = track;
    if (this.setTime == true){
      this.setTime = false;
      this.checkLoad();
    }
  }

  closeModal() {
    this.audioTrack.pause();
    this.onTrackPause(null);
    this.view.dismiss(this.audio);
  }

  saveWatchTime(seconds, completed) {
    if (this.audio.duration == null) {
      this.duration = this.current.duration;
    }

    if (seconds == 0 && completed == 1) {
      seconds = this.current.duration;
    }

    if (this.current.duration == seconds && completed == 0){
      completed = 1;
    }

    let params = {
      audio_id: this.audio.id,
      user: this.user,
      watch_time: seconds,
      completed: completed,
      duration: this.duration
    };

    this.service.postDataWithToken('userAudio-register', params).then((res) => {
    }, (err) => {
      this.loader.dismiss();
      let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('ERROR'),
          subTitle: this.translate.instant('ERROR_AVAILABLE'),
          buttons: ['Ok']
        });
      alert.present();
    });
  }
}
