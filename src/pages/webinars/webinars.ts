import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, ModalController } from 'ionic-angular';
import { TranslateService } from '../../../node_modules/@ngx-translate/core';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { DatabaseProvider } from '../../providers/database/database';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@IonicPage()
@Component({
  selector: 'page-webinars',
  templateUrl: 'webinars.html',
})
export class WebinarsPage {
  loader: any;
  result: any;
  videos: any;
  user: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private translate: TranslateService, private loadingCtrl: LoadingController, private service: ApiServiceProvider, public alertCtrl: AlertController, private database: DatabaseProvider, private browser: InAppBrowser, private modal: ModalController) {
  }

  ionViewWillEnter() {
    this.translate.get("LOADING").subscribe((result: string) => {
      this.loader = this.loadingCtrl.create({
        content: result,
      });
      this.loader.present();
      this.database.getUser().then((user) => {
        if (user != null) {
          this.getData({ user: user });
        }
      });
    });
  }

  getData(params){
    this.service.getDataWithToken('webinar', params).then((res) => {
      this.loader.dismiss();
      this.result = res;
      if (this.result.status == true) {
        this.videos = this.result.webinars;
      } else {
        let alert = this
          .alertCtrl
          .create({
            title: this.translate.instant('ERROR'),
            subTitle: this.translate.instant('ERROR_AVAILABLE'),
            buttons: ['Ok']
          });
        alert.present();
      }
    }, (err) => {
      this.loader.dismiss();
      let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('ERROR'),
          subTitle: this.translate.instant('ERROR_AVAILABLE'),
          buttons: ['Ok']
        });
      alert.present();
    });
  }

  open(video) {
    if (video.url != null){
      this.browser.create(video.url, '_blank', 'location=yes');
    } else {
      const modalOnline = this.modal.create('VideoOnlinePage', { video: video });
      modalOnline.present();
    }
  }

}
