import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WebinarsPage } from './webinars';
import { TranslateModule } from '../../../node_modules/@ngx-translate/core';

@NgModule({
  declarations: [
    WebinarsPage,
  ],
  imports: [
    IonicPageModule.forChild(WebinarsPage),
    TranslateModule
  ],
})
export class WebinarsPageModule {}
