import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { TranslateService } from '../../../node_modules/@ngx-translate/core';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { StorageServiceProvider } from '../../providers/storage-service/storage-service';

@IonicPage()
@Component({
  selector: 'page-notes',
  templateUrl: 'notes.html',
})
export class NotesPage {
  loader: any;
  result: any;
  months: string[];
  notes: { mounth: any; };
  user: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private loadingCtrl: LoadingController, private alertCtrl: AlertController, public translate: TranslateService, public service: ApiServiceProvider, private storage: StorageServiceProvider) {
    this.months = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
    this.notes = { mounth: null };
    this.user = this.storage.getObject('distributor');
  }

  ionViewDidLoad() {
    this.translate.get("LOADING").subscribe((result: string) => {
      this.loader = this.loadingCtrl.create({
        content: result,
      });
      this.loader.present();
    });

    let params = {
      user_id: this.user.Id,
      affId: this.user.AffId,
      page: 0
    }
    
    this.service.postData('listAffiliates2', params).then((res) => {
      this.loader.dismiss();
      this.result = res;
      if (this.result.status == true) {
        this.populateNotes();
      } else {
        let alert = this
          .alertCtrl
          .create({
            title: this.translate.instant('ERROR'),
            subTitle: this.translate.instant('ERROR_AVAILABLE'),
            buttons: ['Ok']
          });
        alert.present();
      }
    }, (err) => {
      this.loader.dismiss();
      let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('ERROR'),
          subTitle: this.translate.instant('ERROR_AVAILABLE'),
          buttons: ['Ok']
        });
      alert.present();
    });
  }

  populateNotes() {
    if (this.result.affiliates.length > 0) {
      let that = this;
      Object.keys(this.result.affiliates).forEach(key => {
        let item = this.result.affiliates[key],
          data_parts = item.data.split(' '),
          data = data_parts[0].split('/');

        if (that.notes.mounth == null) {
          that.notes.mounth = [];
          let obj = {
            name: that.months[parseInt(data[1]) - 1] + ' - ' + data[2],
            itens: [],
            id: parseInt(data[2].substr(2, 4) + data[1]),
          };
          that.notes.mounth.name = that.months[parseInt(data[1]) - 1];
          item.data = data_parts[0];
          obj.itens.push(item);
          that.notes.mounth[obj.id] = obj;
        } else {
          Object.keys(that.notes.mounth).forEach(key2 => {
            let mounth = that.notes.mounth[key2];
            data_parts = item.data.split(' '),
              data = data_parts[0].split('/');
            if (mounth.id == parseInt(data[2].substr(2, 4) + data[1])) {
              item.data = data_parts[0];
              let checkItem = true;
              Object.keys(mounth.itens).forEach(key3 => {
                let person = mounth.itens[key3];
                if (checkItem) {
                  if (person.Id == item.Id) {
                    checkItem = false;
                  }
                }
              });
              if (checkItem) {
                mounth.itens.push(item);
              }
            } else {
              if (data_parts.length > 1) {
                let obj = {
                  name: that.months[parseInt(data[1]) - 1] + ' - ' + data[2],
                  itens: [],
                  id: parseInt(data[2].substr(2, 4) + data[1]),
                };
                item.data = data_parts[0];
                obj.itens.push(item);
                let checkMouth = true;
                Object.keys(that.notes.mounth).forEach(key4 => {
                  let mounth2 = that.notes.mounth[key4];
                  if (checkMouth) {
                    if (mounth2.id == obj.id) {
                      checkMouth = false;
                    }
                  }
                });
                if (checkMouth) {
                  that.notes.mounth[obj.id] = obj
                }
              }
            }
          });
        }
      });
      this.orderMouth();
    } else {
      let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('ERROR'),
          subTitle: this.translate.instant('ERROR_AVAILABLE'),
          buttons: ['Ok']
        });
      alert.present();
    }
  }

  orderMouth() {
    let orderMouth = [],
      that = this;
    Object.keys(that.notes.mounth).forEach(key => {
      if (that.notes.mounth[key].hasOwnProperty('itens')) {
        orderMouth.push(that.notes.mounth[key]);
      }
    });

    that.notes.mounth = orderMouth.reverse();
  }

}
