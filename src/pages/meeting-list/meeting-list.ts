import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ModalController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { TranslateService } from '../../../node_modules/@ngx-translate/core';
import { DatabaseProvider } from '../../providers/database/database';

@IonicPage()
@Component({
  selector: 'page-meeting-list',
  templateUrl: 'meeting-list.html',
})
export class MeetingListPage {
  meetings: any;
  loader: any;
  result: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private service: ApiServiceProvider, public alertCtrl: AlertController, private translate: TranslateService, private loading: LoadingController, private database: DatabaseProvider, public loadingCtrl: LoadingController, private modal: ModalController) {
  }

  ionViewDidEnter() {
    if (this.navParams.data.hasOwnProperty('meetings') == false) {
      this.translate.get("LOADING").subscribe((result: string) => {
        this.loader = this.loading.create({
          content: result,
        });
        this.loader.present();
        this.database.getUser().then((user) => {
          if (user != null) {
            this.getData({ user: user });
          }
        });
      });
    } else {
      this.meetings = this.navParams.data.meetings;
    }
  }

  getData(params) {
    this.service.getDataWithToken('meeting', params).then((res) => {
      this.loader.dismiss();
      this.result = res;
      if (this.result.status == true) {
        this.meetings = this.result.meetings;
      } else {
        let alert = this
          .alertCtrl
          .create({
            title: this.translate.instant('ERROR'),
            subTitle: this.translate.instant('ERROR_AVAILABLE'),
            buttons: ['Ok']
          });
        alert.present();
      }
    }, (err) => {
      this.loader.dismiss();
      let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('ERROR'),
          subTitle: this.translate.instant('ERROR_AVAILABLE'),
          buttons: ['Ok']
        });
      alert.present();
    });
  }

  open(meeting) {
    const modalMeeting = this.modal.create('MeetingDetailsPage', { meeting: meeting });
    modalMeeting.present();
  }
}