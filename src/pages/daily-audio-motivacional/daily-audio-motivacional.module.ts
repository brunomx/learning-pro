import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DailyAudioMotivacionalPage } from './daily-audio-motivacional';
import { TranslateModule } from '../../../node_modules/@ngx-translate/core';

@NgModule({
  declarations: [
    DailyAudioMotivacionalPage,
  ],
  imports: [
    IonicPageModule.forChild(DailyAudioMotivacionalPage),
    TranslateModule
  ],
})
export class DailyAudioMotivacionalPageModule {}
