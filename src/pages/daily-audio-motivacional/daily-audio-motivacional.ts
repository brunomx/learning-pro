import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ModalController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { TranslateService } from '../../../node_modules/@ngx-translate/core';
import { DatabaseProvider } from '../../providers/database/database';

@IonicPage()
@Component({
  selector: 'page-daily-audio-motivacional',
  templateUrl: 'daily-audio-motivacional.html',
})
export class DailyAudioMotivacionalPage {
  loader: any;
  result: any;
  audios: any;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, private service: ApiServiceProvider, public alertCtrl: AlertController, private translate: TranslateService, private loading: LoadingController, private modal: ModalController, private database: DatabaseProvider) {
    this.audios = [];
  }
  
  ionViewDidEnter() {
    if (this.navParams.data.hasOwnProperty('audios') == false) {
      this.translate.get("LOADING").subscribe((result: string) => {
        this.loader = this.loading.create({
          content: result,
        });
        this.loader.present();
        this.database.getUser().then((user) => {
          if (user != null) {
            this.getData({ user: user });
          }
        });
      });
    } else {
      this.audios = this.navParams.data.audios;
    }
  }

  getData(params) {
    this.service.getDataWithToken('audio', params).then((res) => {
      this.loader.dismiss();
      this.result = res;
      if (this.result.status == true) {
        this.audios = this.result.audios;
      } else {
        let alert = this
          .alertCtrl
          .create({
            title: this.translate.instant('ERROR'),
            subTitle: this.translate.instant('ERROR_AVAILABLE'),
            buttons: ['Ok']
          });
        alert.present();
      }
    }, (err) => {
      this.loader.dismiss();
      let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('ERROR'),
          subTitle: this.translate.instant('ERROR_AVAILABLE'),
          buttons: ['Ok']
        });
      alert.present();
    });
  }

  open(audio){
    let that = this;
    audio.audio_url = audio.path;
    const modal = this.modal.create('AudioOnlinePage', { audio: audio });
    modal.onDidDismiss((audio) => {
      if (audio != undefined) {
        that.database.getUser().then((user) => {
          if (user != null) {
            that.translate.get("LOADING").subscribe((result: string) => {
              that.loader = that.loading.create({
                content: result,
              });
              that.loader.present();
              that.getData({ user: user });
            });
          }
        });
      }
    });
    modal.present();
  }
  
}
