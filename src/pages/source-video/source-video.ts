import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ModalController, Events, App, Platform, Tabs } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { TranslateService } from '../../../node_modules/@ngx-translate/core';
import { Device } from '../../../node_modules/@ionic-native/device/ngx';
import { File } from '../../../node_modules/@ionic-native/file/ngx';
import { DatabaseProvider } from '../../providers/database/database';
import { SourceAudioPage } from '../source-audio/source-audio';

@IonicPage()
@Component({
  selector: 'page-source-video',
  templateUrl: 'source-video.html',
})
export class SourceVideoPage {
  loader: any;
  result: any;
  videos: any;
  groups: any[];
  categories: any[];
  video: any;
  thumb: any;
  isAudio: boolean;
  isDownload: boolean;
  targetPath: any;
  isFile: boolean;
  showEmbed: boolean;
  sources: any;
  push: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private service: ApiServiceProvider, public alertCtrl: AlertController, private translate: TranslateService, private loading: LoadingController, private file: File, private device: Device, private modal: ModalController, private events: Events, private database: DatabaseProvider, private app: App, public platform: Platform, private tabs: Tabs) {
    this.groups = [];
  }

  ionViewDidLoad() {
    this.events.subscribe('loadSources', (sources) => {
      if (sources == false) {
        this.translate.get("LOADING").subscribe((result: string) => {
          this.loader = this.loading.create({
            content: result,
          });
          this.loader.present();
          this.database.getUser().then((user) => {
            if (user != null) {
              this.getData({ user: user });
            }
          });
        });
      } else {
        this.sources = sources;
        this.checkVideo(sources);
      }
    });
  }

  getData(params){
    this.service.getDataWithToken('material', params).then((res) => {
      this.loader.dismiss();
      this.result = res;
      if (this.result.status == true) {
        this.sources = this.result.materials;
        this.checkVideo(this.result.materials);
      } else {
        let alert = this
          .alertCtrl
          .create({
            title: this.translate.instant('ERROR'),
            subTitle: this.translate.instant('ERROR_AVAILABLE'),
            buttons: ['Ok']
          });
        alert.present();
      }
    }, (err) => {
      this.loader.dismiss();
      let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('ERROR'),
          subTitle: this.translate.instant('ERROR_AVAILABLE'),
          buttons: ['Ok']
        });
      alert.present();
    });
  }

  checkVideo(videos) {
    let elements = [],
      that = this;
    this.videos = [];
    Object.keys(videos).forEach(key => {
      let item = videos[key];
      if (item.hasOwnProperty('type')) {
        if (item.type == 2) {
          if (item.category_id != null) {
            that.populate(item.category_id, item.category_name, item);
          } else {
            elements.push(item);
          }
        }
      }
    });
    this.videos = elements;
    if (this.navParams.data.hasOwnProperty('push')) {
      if (this.navParams.data.push.source == '2') {
        this.openPush(this.navParams.data.push);
      }
    }
    this.checkEmpty();
    this.order();
  }

  openPush(data) {
    this.navParams.data = {};
    Object.keys(this.videos).forEach(key => {
      let item = this.videos[key];
      if (item.id == data.item_id) {
        this.video = item;
        this.openModalVideo();
      }
    });
  }

  checkEmpty() {
    this.platform.ready().then(() => {
      let currentTab = 0;
      if (this.tabs != null) {
        currentTab = this.tabs.getActiveChildNavs()[0].index;
      } else {
        currentTab = this.app.getRootNav().getActiveChildNav();
      }

      if (this.videos.length == 0) {
        let pdfs = 0, audios = 0;
        Object.keys(this.sources).forEach(key => {
          let item = this.sources[key];
          if (item.type == "1") {
            audios += 1;
          }
          if (item.type == "0") {
            pdfs += 1;
          }
        });
        if (audios > 0 && currentTab != 1) {
          if (this.app.getRootNav().getActiveChildNav() == undefined) {
            this.tabs.select(this.tabs.getByIndex(1));
          } else {
            this.app.getRootNav().getActiveChildNav().select(1);
          }
        } else if (pdfs > 0 && currentTab != 2) {
          if (this.app.getRootNav().getActiveChildNav() == undefined) {
            this.tabs.select(this.tabs.getByIndex(2));
          } else {
            this.app.getRootNav().getActiveChildNav().select(2);
          }
        }
      }
    });
  }

  populate(indice, name, item) {
    if (this.groups[indice] != undefined) {
      this.groups[indice].itens.push(item);
    } else {
      this.groups[indice] = { name: name, itens: [] };
      this.groups[indice].itens.push(item);
    }
  }

  order() {
    let orderGroup = [],
      that = this;
    Object.keys(this.groups).forEach(key => {
      if (this.groups[key].hasOwnProperty('itens')) {
        orderGroup.push(this.groups[key]);
      }
    });
    that.categories = orderGroup.reverse();
  }

  openModalOpcoes(video) {
    this.video = video;
    this.thumb = video.thumb;
    this.isAudio = video.audio_url == null ? false : true;
    if (video.download == null || video.download == '') {
      this.isDownload = false;
    } else {
      this.isDownload = true;
    }

    // if (this.video.watch) {
    if (true) {
      if (this.isDownload) {
        this.checkFile();
      } else {
        this.openModalVideo();
      }
    } else {
      let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('OOPS'),
          subTitle: this.translate.instant('OOPS_TEXT'),
          buttons: ['Ok']
        });
      alert.present();
    }
  }

  checkFile() {
    if (this.device.platform == "Android") {
      this.targetPath = this.file.dataDirectory;
    } else {
      this.targetPath = this.file.documentsDirectory;
    }
    let fileName = this.video.path + ".mp4";

    this.video.targetFile = this.targetPath + fileName;
    this.file.checkFile(this.targetPath, fileName).then((res) => {
      const modalOffline = this.modal.create('VideoOfflinePage', { video: this.video });
      modalOffline.present();
    }, (err) => {
      this.isFile = false
      this.showEmbed = false;
      const modalOpcoes = this.modal.create('VideoOptionsPage', { video: this.video, isFile: this.isFile, isDownload: this.isDownload, isAudio: this.isAudio });
      modalOpcoes.present();
    });
  }

  openModalVideo() {
    const modalOnline = this.modal.create('VideoOnlinePage', { video: this.video });
    modalOnline.present();
  }

  removerAcentos(s){
    let map={"â":"a","Â":"A","à":"a","À":"A","á":"a","Á":"A","ã":"a","Ã":"A","ê":"e","Ê":"E","è":"e","È":"E","é":"e","É":"E","î":"i","Î":"I","ì":"i","Ì":"I","í":"i","Í":"I","õ":"o","Õ":"O","ô":"o","Ô":"O","ò":"o","Ò":"O","ó":"o","Ó":"O","ü":"u","Ü":"U","û":"u","Û":"U","ú":"u","Ú":"U","ù":"u","Ù":"U","ç":"c","Ç":"C"}
    return s.replace(/[\W\[\] ]/g,function(a){return map[a]||a}).split(' ').join('_'); 
  }

}
