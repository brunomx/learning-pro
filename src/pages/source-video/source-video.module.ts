import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SourceVideoPage } from './source-video';
import { TranslateModule } from '../../../node_modules/@ngx-translate/core';

@NgModule({
  declarations: [
    SourceVideoPage,
  ],
  imports: [
    IonicPageModule.forChild(SourceVideoPage),
    TranslateModule,
  ],
})
export class SourceVideoPageModule {}
