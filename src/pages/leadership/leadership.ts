import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, AlertController, LoadingController } from 'ionic-angular';
import { LeadershipMonthPage } from '../leadership-month/leadership-month';
import { LeadershipListPage } from '../leadership-list/leadership-list';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { TranslateService } from '../../../node_modules/@ngx-translate/core';
import { DatabaseProvider } from '../../providers/database/database';

@IonicPage()
@Component({
  selector: 'page-leadership',
  templateUrl: 'leadership.html',
})
export class LeadershipPage {
  monthPage: typeof LeadershipMonthPage;
  listPage: typeof LeadershipListPage;
  loader: any;
  result: any;
  month: any;
  year: any;
  months: string[];

  constructor(public navCtrl: NavController, public navParams: NavParams, private service: ApiServiceProvider, public events: Events, public alertCtrl: AlertController, private translate: TranslateService, private loading: LoadingController, private database: DatabaseProvider) {
    this.monthPage = LeadershipMonthPage;
    this.listPage = LeadershipListPage;
    let date = new Date();
    this.months = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
    this.month = this.months[date.getMonth()];
    this.year = date.getFullYear();
  }

  ionViewWillEnter() {
    this.translate.get("LOADING").subscribe((result: string) => {
      this.loader = this.loading.create({
        content: result,
      });
      this.loader.present();
      this.database.getUser().then((user) => {
        if (user != null) {
          this.getData({ user: user });
        }
      });
    });
  }

  getData(params) {
    this.service.getDataWithToken('qualification', params).then((res) => {
      this.loader.dismiss();
      this.result = res;
      if (this.result.status == true) {
        this.navParams.data.qualifications = this.result.qualifications;
        this.events.publish('loadLeadership', this.result.qualifications);
      } else {
        let alert = this
          .alertCtrl
          .create({
            title: this.translate.instant('ERROR'),
            subTitle: this.translate.instant('ERROR_AVAILABLE'),
            buttons: ['Ok']
          });
        alert.present();
      }
    }, (err) => {
      this.loader.dismiss();
      let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('ERROR'),
          subTitle: this.translate.instant('ERROR_AVAILABLE'),
          buttons: ['Ok']
        });
      alert.present();
    });
  }
}
