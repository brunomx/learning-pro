import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { TranslateService } from '../../../node_modules/@ngx-translate/core';
import { DatabaseProvider } from '../../providers/database/database';

@IonicPage()
@Component({
  selector: 'page-language',
  templateUrl: 'language.html',
})
export class LanguagePage {
  languages: { name: string; key: string; }[];
  current: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private view: ViewController, private translate: TranslateService, private database: DatabaseProvider) {
    this.current = { id: null, language: null };
 
  }

  ionViewDidLoad() {
    this.database.getLanguage().then((res) => {
      if(res != null){
        this.current = res;
      }
      this.languages = [
        { name: "English", key: 'en' },
        { name: "Español", key: 'es' },
        { name: "Italiano", key: 'it' },
        { name: "Português", key: 'pt' },
      ];
    });
  }

  closeModal() {
    this.view.dismiss();
  }

  change(key){
    this.current.language = key;
  }

  save(){
    this.translate.use(this.current.language);
    this.database.savePreferences(this.current);
    this.view.dismiss();
  }

}
