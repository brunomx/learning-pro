import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, ModalController, Events, Tabs } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { TranslateService } from '../../../node_modules/@ngx-translate/core';
import { DatabaseProvider } from '../../providers/database/database';
import { PrefixNot } from '@angular/compiler';

@IonicPage()
@Component({
  selector: 'page-daily-audio-produto',
  templateUrl: 'daily-audio-produto.html',
})
export class DailyAudioProdutoPage {
  loader: any;
  result: any;
  audios: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private service: ApiServiceProvider, public alertCtrl: AlertController, private translate: TranslateService, private loading: LoadingController, private modal: ModalController, private events: Events, private database: DatabaseProvider, private tabs: Tabs) {
    this.audios = [];
  }

  ionViewDidLoad() {
    this.events.subscribe('loadAudios', (audios) => {
      if (audios == false) {
        this.translate.get("LOADING").subscribe((result: string) => {
          this.loader = this.loading.create({
            content: result,
          });
          this.loader.present();
          this.database.getUser().then((user) => {
            if (user != null) {
              this.getData({ user: user });
            }
          });
        });
      } else {
        this.audios = audios;
        if(this.navParams.data.hasOwnProperty('push')){
          this.openPush(this.navParams.data.push);
        } else {
          this.checkEmpty();
        }
      }
    });
  }

  getData(params){
    this.service.getDataWithToken('audio', params).then((res) => {
      this.loader.dismiss();
      this.result = res;
      if (this.result.status == true) {
        this.audios = this.result.audios;
      } else {
        let alert = this
          .alertCtrl
          .create({
            title: this.translate.instant('ERROR'),
            subTitle: this.translate.instant('ERROR_AVAILABLE'),
            buttons: ['Ok']
          });
        alert.present();
      }
    }, (err) => {
      this.loader.dismiss();
      let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('ERROR'),
          subTitle: this.translate.instant('ERROR_AVAILABLE'),
          buttons: ['Ok']
        });
      alert.present();
    });
  }

  checkEmpty() {
    let empty = true;
    Object.keys(this.audios).forEach(key => {
      let item = this.audios[key];
      if(item.type == "1"){
        empty = false;
      }
    });

    let currentTab = this.tabs.getActiveChildNavs()[0].index;
    if (empty && currentTab != 1){
      this.navCtrl.parent.select(1);
    }
  }

  openPush(data){
    this.navParams.data = {};
    Object.keys(this.audios).forEach(key => {
      let item = this.audios[key];
      if (item.id == data.item_id) {
        this.open(item);
      }
    });
  }

  open(audio){
    let that = this;
    audio.audio_url = audio.path;
    const modal = this.modal.create('AudioOnlinePage', { audio: audio });
    modal.onDidDismiss((audio) => {
      if (audio != undefined) {
        that.database.getUser().then((user) => {
          if (user != null) {
            that.translate.get("LOADING").subscribe((result: string) => {
              that.loader = that.loading.create({
                content: result,
              });
              that.loader.present();
              that.getData({ user: user });
            });
          }
        });
      }
    });
    modal.present();
  }
}
