import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DailyAudioProdutoPage } from './daily-audio-produto';

@NgModule({
  declarations: [
    DailyAudioProdutoPage,
  ],
  imports: [
    IonicPageModule.forChild(DailyAudioProdutoPage),
  ],
})
export class DailyAudioProdutoPageModule {}
