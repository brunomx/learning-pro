import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController, AlertController, Events, Platform } from 'ionic-angular';
import { TranslateService } from '../../../node_modules/@ngx-translate/core';
import { Device } from '../../../node_modules/@ionic-native/device/ngx';
import { FormBuilder, FormGroup, AbstractControl, Validators } from '../../../node_modules/@angular/forms';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { StorageServiceProvider } from '../../providers/storage-service/storage-service';
import { DailyPage } from '../daily/daily';
import { FingerprintAIO } from '../../../node_modules/@ionic-native/fingerprint-aio/ngx';
import { DatabaseProvider } from '../../providers/database/database';
import { DailyAudioPage } from '../daily-audio/daily-audio';
import { ContactsPage } from '../contacts/contacts';
import { FirstPage } from '../first/first';
import { DisclosurePage } from '../disclosure/disclosure';
import { SourcesPage } from '../sources/sources';
import { AlertsPage } from '../alerts/alerts';
import { MeetingPage } from '../meeting/meeting';
import { LeadershipPage } from '../leadership/leadership';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  formGroup: FormGroup;
  username: AbstractControl;
  password: AbstractControl;
  loader: any;
  result: any;
  touchAvaliable: boolean;
  touchId: boolean;
  hasSession: boolean;
  video: HTMLElement;  
  videoLoad: boolean;
  touchIdOff: boolean;
  modules: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modal: ModalController, public translate: TranslateService, public formBuilder: FormBuilder, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public service: ApiServiceProvider, public device: Device, private storage: StorageServiceProvider, private events: Events, public platform: Platform, private faio: FingerprintAIO, public database: DatabaseProvider) {
    this.formGroup = formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.compose([Validators.required, Validators.minLength(4)])]
    });

    this.username = this.formGroup.controls['username'];
    this.password = this.formGroup.controls['password'];
    this.videoLoad = false;

    this.modules = [
      { component: DailyPage, params: { course_id: 2 }, route: 'course.video.index/2' },
      { component: DailyAudioPage, params: {}, route: 'audio.index'},
      { component: ContactsPage, params: {} },
      { component: FirstPage, params: { course_id: 1 }, route: 'course.video.index/1' },
      { component: DisclosurePage, params: {} },
      { component: SourcesPage, params: {}, route: 'material.index' },
      { component: AlertsPage, params: {}, route: 'notification.index' },
      { component: MeetingPage, params: {}, route: 'meeting.index' },
      { component: LeadershipPage, params: {}, route: 'qualification.index' },
    ];
  }

  
  ionViewWillEnter() {
    this.platform.ready().then(() => {
      this.faio.isAvailable().then((res) => {
        this.touchAvaliable = true;
        if (this.storage.get('infusion_id') != null) {
          this.touchId = true;
          this.touchIdOff = false;
        } else {
          this.touchId = false;
          this.touchIdOff = true;
        }
      }).catch((err) => {
        this.touchAvaliable = false;
        this.touchId = false;
        this.touchIdOff = true;
      });
      this.database.getUser().then((user) => {
        if(user != null){
          this.database.getFirstModule().then((module) => {
            if(module == null){
              this.navCtrl.setRoot(DailyPage);
            } else {
              this.redirect(module);
            }
          });
        } else {
          this.hasSession = false;
          this.video = document.getElementById('video');
          this.database.getLanguage().then((res) => {
            if (res == null) {
              const modalLanguage = this.modal.create('LanguagePage', {});
              modalLanguage.present();
            }
          });
        }
      });
    });
  }

  login() {
    this.loader = this.loadingCtrl.create();
    this.loader.present();

    let params = {
      email: this.username.value,
      password: this.password.value,
      uuid: this.device.uuid,
    }

    this.service.postData('auth/login', params).then((res) => {
      this.loader.dismiss();
      this.result = res;
      if (this.result.status == true) {
        this.database.saveUser(this.result).then((res) => {
          this.events.publish('afterLogin', this.result.user);
          if (this.result.user.hasOwnProperty('modules')){
            Object.keys(this.result.user.modules).forEach(key => {
              const module = this.result.user.modules[key];
              this.database.saveModule(module);
              if (this.result.user.modules.length - 1 == parseInt(key)){                
                // this.navCtrl.setRoot(DailyPage);
                this.redirect(this.result.user.modules[0]);
              }
            });
          } else {
            this.navCtrl.setRoot(DailyPage);
          }
        });
        // this.storage.setObject('distributor', this.result.data);
        // @todo Criar login com touch
        // if (this.touchAvaliable && this.storage.get('infusion_id') == null) {
        //   let that = this,
        //     confirm = this.alertCtrl.create({
        //       title: this.translate.instant('TOUCH_TITLE'),
        //       message: this.translate.instant('TOUCH_TITLE_MESSAGE'),
        //       buttons: [
        //         {
        //           text: this.translate.instant('REGISTER_AFTER'),
        //           handler: () => {
        //             // that.events.publish('afterLogin', that.result.data);
        //             that.navCtrl.setRoot(DailyPage,{menu: true});
        //           }
        //         },
        //         {
        //           text: this.translate.instant('REGISTER_NOW'),
        //           handler: () => {
        //             that.touchRegister(that.result.data);
        //           }
        //         },
        //       ]
        //     });
        //   confirm.present();
        // } else {
        //   // this.events.publish('afterLogin', this.result.data);
          // this.navCtrl.setRoot(DailyPage,{menu: true});
        // }
      } else {
        let alert = this
          .alertCtrl
          .create({
            title: this.translate.instant('ERROR'),
            subTitle: this.translate.instant('INVALID_LOGIN'),
            buttons: ['Ok']
          });
        alert.present();
      }
    }, (err) => {
      this.loader.dismiss();
      let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('ERROR'),
          subTitle: this.translate.instant('INVALID_LOGIN'),
          buttons: ['Ok']
        });
      alert.present();
    });
  }

  redirect(first){
    Object.keys(this.modules).forEach(key => {
      const module = this.modules[key];
      if (first.param == null) {
        if (module.route == first.route) {
          this.navCtrl.setRoot(module.component);
        }
      } else {
        if (module.route === first.route + '/' + first.param) {
          this.navCtrl.setRoot(module.component, module.param);
        }
      }
    });
  }

  recovery() {
    const modalRecovery = this.modal.create('RecoveryPage');
    modalRecovery.present();
  }

  register() {
    const modalRegister = this.modal.create('RegisterPage');
    modalRegister.present();
  }

  touchRegister(user) {
    this.faio.show({
      clientId: 'com.monterest.app',
      clientSecret: this.password.value,
      disableBackup: true,
      localizedFallbackTitle: this.translate.instant('TOUCH_CANCEL'),
      localizedReason: this.translate.instant('TOUCH_MESSAGE2')
    }).then((res) => {
      this.events.publish('afterLogin', user);
      this.storage.set('infusion_id', user.Id);
      this.navCtrl.setRoot(DailyPage,{menu: true});
    }).catch((err) => {
      this.events.publish('afterLogin', user);
      this.navCtrl.setRoot(DailyPage,{menu: true});
    });
  }

  changeLogin(){
    this.touchId = false;
    this.touchIdOff = true;
  }

  touchLogin() {
    this.loader = this.loadingCtrl.create({
      content: this.translate.instant('AUTHENTICATING'),
    });
    this.loader.present();

    let params = {
      infusion_id: this.storage.get('infusion_id'),
      uuid: this.device.uuid,
    }
    this.faio.show({
      clientId: 'br.com.maquinamultinivel.app',
      clientSecret: 'password',
      disableBackup: true,
      localizedFallbackTitle: this.translate.instant('TOUCH_CANCEL'),
      localizedReason: this.translate.instant('TOUCH_MESSAGE')
    }).then((res) => {
      this.service.postData('authenticateTouch', params).then((res) => {
        this.loader.dismiss();
        this.result = res;

        if (this.result.status == true) {
          let user = this.result.user;
          user.token = this.result.token_type = " " + this.result.access_token;
          // this.storage.setObject('distributor', user);
          // this.events.publish('afterLogin', this.result.data);
          this.navCtrl.setRoot(DailyPage,{menu: true});
        } else {
          let alert = this
            .alertCtrl
            .create({
              title: this.translate.instant('ERROR'),
              subTitle: this.translate.instant('INVALID_LOGIN'),
              buttons: ['Ok']
            });
          alert.present();
        }
      }, (err) => {
        this.loader.dismiss();
        let alert = this
          .alertCtrl
          .create({
            title: this.translate.instant('ERROR'),
            subTitle: this.translate.instant('INVALID_LOGIN'),
            buttons: ['Ok']
          });
        alert.present();
      });
    }).catch((err) => {
      this.loader.dismiss();
      let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('ERROR'),
          subTitle: this.translate.instant('INVALID_LOGIN'),
          buttons: ['Ok']
        });
      alert.present();
    });
  }

}
