import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ModalController, Platform, Events } from 'ionic-angular';
import { TranslateService } from '../../../node_modules/@ngx-translate/core';
import { FormBuilder, FormGroup, AbstractControl, Validators } from '../../../node_modules/@angular/forms';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { ContactFieldType, Contacts } from '../../../node_modules/@ionic-native/contacts/ngx';
import { ContactsPage } from '../contacts/contacts';
import { DatabaseProvider } from '../../providers/database/database';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@IonicPage()
@Component({
  selector: 'page-contact-details',
  templateUrl: 'contact-details.html',
})
export class ContactDetailsPage {
  filter: ContactFieldType[] = ["name.formatted"];
  dados: any;
  title: string;
  formGroup: FormGroup;
  loader: any;
  result: any;
  contactGroups: { 'id': string; 'name': any; }[];
  contactTypes: { 'id': string; 'name': any; }[];
  name: AbstractControl;
  nickname: AbstractControl;
  phone: AbstractControl;
  group: AbstractControl;
  types: AbstractControl;
  observations: AbstractControl;
  entrepreneur: AbstractControl;
  credility: AbstractControl;
  networking: AbstractControl;
  empowerment: AbstractControl;
  dreamer: AbstractControl;
  invitation: AbstractControl;
  closing: AbstractControl;
  fup: AbstractControl;
  plan: AbstractControl;
  register: AbstractControl;
  comments: AbstractControl;
  email: AbstractControl;
  user: any;
  id: string;
  manual: boolean;
  change: boolean;
  importBack: boolean;
  url: string;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder, public service: ApiServiceProvider, public loadingCtrl: LoadingController, public translate: TranslateService, public alertCtrl: AlertController, private modal: ModalController, public platform: Platform, public contacts: Contacts, public events: Events, private database: DatabaseProvider, private socialSharing: SocialSharing) {
    this.dados = navParams.data.contact;
    this.manual = false;
    this.change = false;
    this.importBack = false;
    this.id = null;
    this.url = 'http://192.168.15.2:8002/share/';

    this.database.getUser().then((user) => {
      this.user = user;
    });
    
    this.contactGroups = [
      { 'id': '1', 'name': 'FAMILY' },
      { 'id': '2', 'name': 'FRIENDS' },
      { 'id': '3', 'name': 'JOB' },
      { 'id': '4', 'name': 'SCHOOL' },
      { 'id': '5', 'name': 'COLLEGE' },
      { 'id': '6', 'name': 'TRAVELS' },
      { 'id': '7', 'name': 'SERVICE_PROVIDER' },
      { 'id': '8', 'name': 'PARTIES' },
      { 'id': '9', 'name': 'OTHERS' },
    ];
    
    this.contactTypes = [
      { 'id': '1', 'name': 'HOT' },
      { 'id': '2', 'name': 'WARM' },
      { 'id': '3', 'name': 'COLD' },
    ];
    
    this.platform.ready().then((readySource) => {
      // console.log('Width: ' + platform.width());
      // console.log('Height: ' + platform.height());
    });
    
    this.formGroup = formBuilder.group({
      name: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
      nickname: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
      phone: ['', Validators.required],
      email: ['', Validators.email],
      group: [],
      types: [],
      observations: [],
      entrepreneur: [],
      credility: [],
      networking: [],
      empowerment: [],
      dreamer: [],
      invitation: ['', Validators.minLength(10)],
      plan: ['', Validators.minLength(10)],
      fup: ['', Validators.minLength(10)],
      closing: ['', Validators.minLength(10)],
      register: [],
      comments: [],
    });
    
    this.name = this.formGroup.controls['name'];
    this.nickname = this.formGroup.controls['nickname'];
    this.phone = this.formGroup.controls['phone'];
    this.email = this.formGroup.controls['email'];
    this.group = this.formGroup.controls['group'];
    this.types = this.formGroup.controls['types'];
    this.observations = this.formGroup.controls['observations'];
    this.entrepreneur = this.formGroup.controls['entrepreneur'];
    this.credility = this.formGroup.controls['credility'];
    this.networking = this.formGroup.controls['networking'];
    this.empowerment = this.formGroup.controls['empowerment'];
    this.dreamer = this.formGroup.controls['dreamer'];
    this.invitation = this.formGroup.controls['invitation'];
    this.plan = this.formGroup.controls['plan'];
    this.fup = this.formGroup.controls['fup'];
    this.closing = this.formGroup.controls['closing'];
    this.register = this.formGroup.controls['register'];
    this.comments = this.formGroup.controls['comments'];
    
    if (this.dados != null) {
      this.loadData();
    }
  }
  

  loadData() {
    this.name.setValue(this.dados.first_name);
    this.nickname.setValue(this.dados.last_name);
    this.email.setValue(this.dados.email);
    
    this.importBack = this.dados.import;

    if(this.importBack){
      this.dados.phone = this.dados.oldPhone;
      this.formatPhone(this.dados);
    } else {
      this.formatPhone(this.dados);
    }

    this.phone.setValue(this.dados.phone);
    this.group.setValue(this.dados.grupo == undefined ? null : this.dados.grupo);
    this.types.setValue(this.dados.type == undefined ? null : this.dados.type);
    this.entrepreneur.setValue(this.dados.entrepreneur == '0' || this.dados.entrepreneur == undefined ? false : true);
    this.credility.setValue(this.dados.credility == '0' || this.dados.credility == undefined ? false : true);
    this.networking.setValue(this.dados.networking == '0' || this.dados.networking == undefined ? false : true);
    this.empowerment.setValue(this.dados.empowerment == '0' || this.dados.empowerment == undefined ? false : true);
    this.dreamer.setValue(this.dados.dreamer == '0' || this.dados.dreamer == undefined ? false : true);
    this.register.setValue(this.dados.register == '0' || this.dados.register == undefined ? false : true);
    this.invitation.setValue(this.dados.invitation == undefined ? null : this.dados.invitation);
    this.plan.setValue(this.dados.plan == undefined ? null : this.dados.plan);
    this.fup.setValue(this.dados.fup == undefined ? null : this.dados.fup);
    this.closing.setValue(this.dados.closing == undefined ? null : this.dados.closing);
    this.observations.setValue(this.dados.observation == undefined ? '' : this.dados.observation);
    this.comments.setValue(this.dados.comments == undefined ? '' : this.dados.comments);
    this.id = this.dados.id == undefined ? null : this.dados.id;
    this.manual = true;
  }

  formatPhone(item){
    if(item.phone.length > 0){
      if(item.phone.length <= 9){
        if(item.phone.length == 9){
          item.phone = item.phone.substr(0, 5) + "-" + item.phone.substr(5);
        } else if(item.phone.length == 8){
          item.phone = item.phone.substr(0, 4) + "-" + item.phone.substr(4);
        }
      } else {
        let phone = item.phone.match(/\d/g).join("");
        if(phone.length == 14){
          let country = phone.substr(0, 3),
          ddd = phone.substr(3, 2),
          number = phone.substr(5);
          
          if(number.length == 9){
            number = number.substr(0, 5) + "-" + number.substr(5);
          } else if(number.length == 8){
            number = number.substr(0, 4) + "-" + number.substr(4);
          }
          
          item.phone = country + ' ' + ddd + ' ' + number;
        } else if(phone.length == 13){
          let country = phone.substr(0, 2),
          ddd = phone.substr(2, 2),
          number = phone.substr(4);
          
          if(number.length == 9){
            number = number.substr(0, 5) + "-" + number.substr(5);
          } else if(number.length == 8){
            number = number.substr(0, 4) + "-" + number.substr(4);
          }
          
          item.phone = '+' + country + ' ' + ddd + ' ' + number;
        } else if(phone.length == 12){

          if(phone.substr(0, 1) == '0'){
            let ddd = phone.substr(0, 3),
            number = phone.substr(3);
            
            if(number.length == 9){
              number = number.substr(0, 5) + "-" + number.substr(5);
            } else if(number.length == 8){
              number = number.substr(0, 4) + "-" + number.substr(4);
            }
            
            item.phone = ddd + ' ' + number;
          } else {
            let country = phone.substr(0, 2),
            ddd = phone.substr(2, 2),
            number = phone.substr(4);
            
            if(number.length == 9){
              number = number.substr(0, 5) + "-" + number.substr(5);
            } else if(number.length == 8){
              number = number.substr(0, 4) + "-" + number.substr(4);
            }
            
            item.phone = '+' + country + ' ' + ddd + ' ' + number;
          }
        } else if(phone.length == 11){
          if(phone.substr(0, 1) == '0'){
            let ddd = phone.substr(0, 3),
            number = phone.substr(3);
            
            if(number.length == 9){
              number = number.substr(0, 5) + "-" + number.substr(5);
            } else if(number.length == 8){
              number = number.substr(0, 4) + "-" + number.substr(4);
            }
            
            item.phone = ddd + ' ' + number;
          } else {
            let ddd = phone.substr(0, 2),
            number = phone.substr(2);
            
            if(number.length == 9){
              number = number.substr(0, 5) + "-" + number.substr(5);
            } else if(number.length == 8){
              number = number.substr(0, 4) + "-" + number.substr(4);
            }
            
            item.phone = ddd + ' ' + number;
          }
        } else {
          let ddd = phone.substr(0, 2),
          number = phone.substr(2);
          
          if(number.length == 9){
            number = number.substr(0, 5) + "-" + number.substr(5);
          } else if(number.length == 8){
            number = number.substr(0, 4) + "-" + number.substr(4);
          }
          
          item.phone = ddd + ' ' + number;
        }
      }
    }
  }
  
  manualEnabled() {
    this.manual = true;
  }
  
  search() {
    const modalOnline = this.modal.create('ContactImporterPage');
    modalOnline.onDidDismiss((contact) => {
      if (contact != undefined) {
        let fullName = contact.name.formatted.split(' '),
        emailImport = contact.emails != null ? contact.emails[0].value : '',
        phoneImport = contact.phoneNumbers != null ? contact.phoneNumbers[0].value : '';
        
        this.name.setValue(fullName.shift());
        this.nickname.setValue(fullName.join(' '));
        this.email.setValue(emailImport);
        let phone = phoneImport.match(/\d/g).join(""),
        ddd = phone.substr(0, 2),
        number = phone.substr(2);
        if (number.length == 9) {
          number = number.substr(0, 5) + "-" + number.substr(5);
        } else {
          number = number.substr(0, 4) + "-" + number.substr(4);
        }
        
        this.phone.setValue('(' + ddd + ') ' + number);
        this.manual = true;
        this.importBack = true;
        this.dados = contact;
        this.dados.import_id = contact.id;
      }
    });
    modalOnline.present();
  }
  
  import(val = ''){
    this.loader = this.loadingCtrl.create({
      content: this.translate.instant('LOADING'),
    });
    this.loader.present();
    this.contacts.find(this.filter, { filter: val, multiple: true, hasPhoneNumber: true }).then(conts => {
      conts.sort(function (a, b) {
        if (a.name.formatted < b.name.formatted) return -1;
        if (a.name.formatted > b.name.formatted) return 1;
        return 0;
      });
      this.prepareSaveImport(conts);
    }, (err) => {
      let alert = this
      .alertCtrl
      .create({
        title: this.translate.instant('ERROR'),
        subTitle: this.translate.instant('ERROR_AVAILABLE'),
        buttons: ['Ok']
      });
      alert.present();
    });
  }
  
  prepareSaveImport(contacts){
    let persons = [];
    Object.keys(contacts).forEach(key => {
      let item = contacts[key],
      person = {
        name: '',
        nickname: '',
        email: '',
        phone: '',
        import_id: item.id,
      };
      
      if(item.name.formatted != null){
        let fullName = item.name.formatted.split(' ');
        person.name = fullName.shift();
        person.nickname = fullName.join(' ');
      }
      
      if(item.emails != null){
        person.email = item.emails[0].value;
      }
      
      if(item.phoneNumbers != null){
        let phoneImport = item.phoneNumbers[0].value,
        phone = phoneImport.match(/\d/g).join("");
        
        person.phone = phone;
      }
      
      persons.push(person);
    });
    this.saveImport(persons);
  }

  saveImport(persons){
    let params = {
      contacts: persons,
      user: this.user
    }

    this.service.postDataWithToken('contact-import-register', params).then((res) => {
      this.loader.dismiss();
      this.result = res;
      if (this.result.status == true) {
        let msg = '';
        if (this.result.data.create == 0){
          msg = 'Todos os contatos da agenda já foram importados';
        } else if (this.result.data.create == 1){
          msg = this.result.data.create + " contato foi importado";
        } else {
          msg = this.result.data.create + " contatos foram importados";
        }
        let alert = this
        .alertCtrl
        .create({
          title: msg,
          buttons: [ {
            text: 'Ok',
            handler: () => {
              this.navCtrl.pop();
            }
          }]
        });
        alert.present();
      } else {
        let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('ERROR'),
          subTitle: this.translate.instant('ERROR_SAVE'),
          buttons: ['Ok']
        });
        alert.present();
      }
    }, (err) => {
      this.loader.dismiss();
      let alert = this
      .alertCtrl
      .create({
        title: this.translate.instant('ERROR'),
        subTitle: this.translate.instant('ERROR_SAVE'),
        buttons: ['Ok']
      });
      alert.present();
    });
  }
  
  back() {
    this.navCtrl.pop();
  }

  delete() {
    let confirm = this
      .alertCtrl
      .create({
        title: this.translate.instant('CONFIRM_DELETE'),
        subTitle: this.dados.first_name == null ? '' : this.dados.first_name,
        buttons: [
          {
            text: this.translate.instant('TOUCH_CANCEL'),
          },
          {
            text: this.translate.instant('CONFIRM'),
            handler: () => {
              this.deleteContact();
            }
          }
        ]
      });
    confirm.present();
  }

  deleteContact(){
    this.loader = this.loadingCtrl.create({
      content: this.translate.instant('LOADING'),
    });

    this.loader.present();

    let params = {
      contact_id: this.dados.id,
      user: this.user
    }

    this.service.postDataWithToken('contact-remove', params).then((res) => {
      this.loader.dismiss();
      this.result = res;
      if (this.result.status == true) {
        let alert = this
          .alertCtrl
          .create({
            title: this.translate.instant('SUCCESSFULLY_DELETE'),
            buttons: [
              {
                text: 'Ok',
                handler: () => {
                  this.navCtrl.pop();
                }
              }
            ]
          });
        alert.present();
      } else {
        let alert = this
          .alertCtrl
          .create({
            title: this.translate.instant('ERROR'),
            subTitle: this.translate.instant('ERROR_SAVE'),
            buttons: ['Ok']
          });
        alert.present();
      }
    }, (err) => {
      this.loader.dismiss();
      let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('ERROR'),
          subTitle: this.translate.instant('ERROR_SAVE'),
          buttons: ['Ok']
        });
      alert.present();
    });
  }
  
  send() {
    this.loader = this.loadingCtrl.create({
      content: this.translate.instant('LOADING'),
    });
    this.loader.present();
    
    let params = {
      name: this.name.value,
      nickname: this.nickname.value,
      phone: this.phone.value,
      email: this.email.value,
      group: this.group.value,
      types: this.types.value,
      observations: this.observations.value,
      entrepreneur: this.entrepreneur.value == null ? false : this.entrepreneur.value,
      credility: this.credility.value == null ? false : this.credility.value,
      networking: this.networking.value == null ? false : this.networking.value,
      empowerment: this.empowerment.value == null ? false : this.empowerment.value,
      dreamer: this.dreamer.value == null ? false : this.dreamer.value,
      invitation: this.invitation.value,
      plan: this.plan.value,
      fup: this.fup.value,
      closing: this.closing.value,
      register: this.register.value == null ? false : this.register.value,
      comments: this.comments.value,
      id: this.id,
      import_id: null,
      user: this.user,
    }

    if(this.importBack){
      params.import_id = this.dados.import_id;
    }

    this.service.postDataWithToken('contact-register', params).then((res) => {
      this.loader.dismiss();
      this.result = res;
      if (this.result.status == true) {
        let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('SAVE_CONTACTS'),
          buttons: [
            {
              text: 'Ok',
              handler: () => {
                this.change = false;
                this.navCtrl.setRoot(ContactsPage);
              }
            }
          ]
        });
        alert.present();
      } else {
        let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('ERROR'),
          subTitle: this.translate.instant('ERROR_SAVE'),
          buttons: ['Ok']
        });
        alert.present();
      }
    }, (err) => {
      this.loader.dismiss();
      let alert = this
      .alertCtrl
      .create({
        title: this.translate.instant('ERROR'),
        subTitle: this.translate.instant('ERROR_SAVE'),
        buttons: ['Ok']
      });
      alert.present();
    });
  }

  share(){
    console.log(this.dados, this.user);

    this.translate.get("LOADING").subscribe((result: string) => {
      this.loader = this.loadingCtrl.create({
        content: result,
      });
      this.loader.present();
    });

    this.socialSharing.shareViaWhatsApp('Convite', null, this.url + this.user.aff_code ).then((res) => {
      this.loader.dismiss();
    }).catch((err) => {
      this.loader.dismiss();
    });
  }
}
