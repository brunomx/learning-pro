import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DisclosureBusinessPage } from './disclosure-business';
import { TranslateModule } from '../../../node_modules/@ngx-translate/core';

@NgModule({
  declarations: [
    DisclosureBusinessPage,
  ],
  imports: [
    IonicPageModule.forChild(DisclosureBusinessPage),
    TranslateModule,
  ],
})
export class DisclosureBusinessPageModule {}
