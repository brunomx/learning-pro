import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VideoOptionsPage } from './video-options';
import { TranslateModule } from '../../../node_modules/@ngx-translate/core';

@NgModule({
  declarations: [
    VideoOptionsPage,
  ],
  imports: [
    IonicPageModule.forChild(VideoOptionsPage),
    TranslateModule,
  ],
})
export class VideoOptionsPageModule {}
