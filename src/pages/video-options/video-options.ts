import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController, AlertController } from 'ionic-angular';
import { Network } from '../../../node_modules/@ionic-native/network/ngx';
import { TranslateService } from '../../../node_modules/@ngx-translate/core';
import { File } from '../../../node_modules/@ionic-native/file/ngx';
import { FileTransferObject, FileTransfer } from '../../../node_modules/@ionic-native/file-transfer/ngx';

@IonicPage()
@Component({
  selector: 'page-video-options',
  templateUrl: 'video-options.html',
})
export class VideoOptionsPage {
  video: any;
  isFile: any;
  isDownload: any;
  showVideo: boolean;
  isVimeo: boolean;
  isAudio: boolean;
  doload3g: boolean;
  fileTransfer: FileTransferObject;
  downloadProgress: number;
  downloading: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams, private view: ViewController, private modal: ModalController, private network: Network, public alertCtrl: AlertController, public translate: TranslateService, private file: File, private transfer: FileTransfer, public zone: NgZone) {
    this.video = this.navParams.data.video;
    this.isFile = this.navParams.data.isFile;
    this.isDownload = this.navParams.data.isDownload;
    this.doload3g = false;
    this.downloading = false;
    this.fileTransfer = this.transfer.create();
    this.downloadProgress = 0;
    let that = this;
    this.fileTransfer.onProgress((progress) => {
      that.zone.run(() => {
        that.downloadProgress = parseInt(((progress.loaded / progress.total) * 100).toFixed(2));
      });
    });
  }

  ionViewDidLoad() {
  }

  closeModal() {
    this.doload3g = false;
    this.view.dismiss();
  }

  openModalVideo(download) {
    this.video.offline = false;
    if (download) {
      this.showVideo = false;
      this.isVimeo = false;
      if (this.isDownload) {
        this.downloadFile();
      }
    } else {
      this.isVimeo = true;
      const modalOnline = this.modal.create('VideoOnlinePage', { video: this.video});
      modalOnline.present();
    }
  }

  downloadFile() {
    if (this.network.type == 'wifi' || this.doload3g) {
      this.file.getFreeDiskSpace().then((space) => {
        let sizeFile = this.video.size / 1000;
        if (space > sizeFile) {
          this.downloading = true;
          this.fileTransfer.download(this.video.download, this.video.targetFile).then((res) => {
            const modalOffline = this.modal.create('VideoOfflinePage', { video: this.video, download: true  });
            modalOffline.onDidDismiss(() => {
               this.closeModal();
            });
            modalOffline.present().then((res) => {
              this.downloading = false;
              this.downloadProgress = 0;
            });
          }, (error) => {
            this.downloading = false;
            let alert = this
              .alertCtrl
              .create({
                title: this.translate.instant('ERROR'),
                subTitle: this.translate.instant('MEMORY_FULL'),
                buttons: ['Ok']
              });
            alert.present();
          });
        } else {
          let alert = this
            .alertCtrl
            .create({
              title: this.translate.instant('ERROR'),
              subTitle: this.translate.instant('MEMORY_FULL'),
              buttons: ['Ok']
            });
          alert.present();
        }
      }, (err) => {
        let alert = this
          .alertCtrl
          .create({
            title: this.translate.instant('ERROR'),
            subTitle: this.translate.instant('MEMORY_FULL'),
            buttons: ['Ok']
          });
        alert.present();
      });
    } else {
      const confirm = this.alertCtrl.create({
        title: this.translate.instant('WIFI_NETWORK_TITLE'),
        message: this.translate.instant('WIFI_NETWORK'),
        buttons: [
          {
            text: this.translate.instant('DOWNLOAD_LATER'),
            handler: () => {
              this.view.dismiss();
            }
          },
          {
            text: this.translate.instant('DOWNLOAD_NOW'),
            handler: () => {
              this.doload3g = true;
              this.downloadFile();
            }
          }
        ]
      });
      confirm.present();
    }
  }

}
