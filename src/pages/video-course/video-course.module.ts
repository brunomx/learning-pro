import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VideoCoursePage } from './video-course';

@NgModule({
  declarations: [
    VideoCoursePage,
  ],
  imports: [
    IonicPageModule.forChild(VideoCoursePage),
  ],
})
export class VideoCoursePageModule {}
