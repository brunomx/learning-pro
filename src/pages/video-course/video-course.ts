import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController, AlertController, ModalController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { Device } from '@ionic-native/device/ngx';
import { File } from '../../../node_modules/@ionic-native/file/ngx';
import { TranslateService } from '../../../node_modules/@ngx-translate/core';
import { DatabaseProvider } from '../../providers/database/database';

@IonicPage()
@Component({
  selector: 'page-video-course',
  templateUrl: 'video-course.html',
})
export class VideoCoursePage {
  title: any;
  loader: any;
  result: any;
  videos: any;
  video: any;
  thumb: any;
  isAudio: boolean;
  isDownload: boolean;
  course_id: any;
  targetPath: any;
  showEmbed: boolean;
  isFile: boolean;
  user: any;
  image: any;
  push: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public service: ApiServiceProvider, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public translate: TranslateService, private device: Device, private file: File, private modal: ModalController, private database: DatabaseProvider) {
    this.title = this.navParams.data.name;
    this.course_id = this.navParams.data.id;
    this.push = this.navParams.data.push;
  }

  ionViewWillEnter() {
    this.image = this.navParams.data.image;
    this.translate.get("LOADING").subscribe((result: string) => {
      this.loader = this.loadingCtrl.create({
        content: result,
      });
      this.loader.present();
    });

    this.database.getUser().then((user) => {
      if(user != null){
        let params = {
          course_id: this.course_id,
          user: user,
        }
        this.getData(params);
      }
    });
  }

  getData(params) {
    this.service.getDataWithToken('course/' + params.course_id, params).then((res) => {
      this.loader.dismiss();
      this.result = res;
      if (this.result.status == true) {
        this.videos = this.result.data.videos;
        if (this.push != null){
          this.openPush(this.push);
        }
      } else {
        let alert = this
          .alertCtrl
          .create({
            title: this.translate.instant('ERROR'),
            subTitle: this.translate.instant('ERROR_AVAILABLE'),
            buttons: ['Ok']
          });
        alert.present();
      }
    }, (err) => {
      this.loader.dismiss();
      let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('ERROR'),
          subTitle: this.translate.instant('ERROR_AVAILABLE'),
          buttons: ['Ok']
        });
      alert.present();
    });
  }

  openPush(data) {
    this.push = null;
    Object.keys(this.videos).forEach(key => {
      let item = this.videos[key];
      if (item.id == data.item_id) {
        this.openModalOpcoes(item);
      }
    });
  }

  openModalOpcoes(video) {
    this.video = video;
    this.thumb = video.thumbnail;
    this.isAudio = video.audio == null ? false : true;
    if (video.url == null) {
      this.isDownload = false;
    } else {
      this.isDownload = true;
    }

    if (this.video.watch) {
      if (this.isDownload) {
        this.checkFile();
      } else {
        this.openModalVideo();
      }
    } else {
      let that = this,
      alert = this
      .alertCtrl
      .create({
        title: this.translate.instant('OOPS'),
        subTitle: this.translate.instant('OOPS_TEXT'),
        buttons:[
          {
            text: this.translate.instant('SEE_AFTER'),
          },
          {
            text: this.translate.instant('SEE_NOW'),
            handler: function(){
              that.openBefore();
            }
          }
        ]
      });
      alert.present();
    }
  }

  openBefore() {
    Object.keys(this.videos).forEach(key => {
      let item = this.videos[key];
      if (item.id == this.video.before_id) {
        this.openModalOpcoes(item);
      }
    });
  }

  checkFile() {
    if (this.device.platform == "Android") {
      this.targetPath = this.file.dataDirectory;
    } else {
      this.targetPath = this.file.documentsDirectory;
    }
    let fileName = this.video.path + ".mp4";

    this.video.targetFile = this.targetPath + fileName;

    this.file.checkFile(this.targetPath, fileName).then((res) => {
      const modalOffline = this.modal.create('VideoOfflinePage', { video: this.video });
      modalOffline.present();
    }, (err) => {
      this.isFile = false
      this.showEmbed = false;
      const modalOpcoes = this.modal.create('VideoOptionsPage', { video: this.video, isFile: this.isFile, isDownload: this.isDownload, isAudio: this.isAudio });
      modalOpcoes.present();
    });
  }

  openModalVideo() {
    let that = this;
    const modalOnline = that.modal.create('VideoOnlinePage', { video: that.video });
    modalOnline.onDidDismiss((video) => {
      if (video != undefined) {
        that.database.getUser().then((user) => {
          if (user != null) {
            that.translate.get("LOADING").subscribe((result: string) => {
              that.loader = that.loadingCtrl.create({
                content: result,
              });
              that.loader.present();
              let params = {
                course_id: that.course_id,
                user: user,
              }
              that.getData(params);
            });
          }
        });
      }
    });
    modalOnline.present();
  }

  removerAcentos(s) {
    let map = { "â": "a", "Â": "A", "à": "a", "À": "A", "á": "a", "Á": "A", "ã": "a", "Ã": "A", "ê": "e", "Ê": "E", "è": "e", "È": "E", "é": "e", "É": "E", "î": "i", "Î": "I", "ì": "i", "Ì": "I", "í": "i", "Í": "I", "õ": "o", "Õ": "O", "ô": "o", "Ô": "O", "ò": "o", "Ò": "O", "ó": "o", "Ó": "O", "ü": "u", "Ü": "U", "û": "u", "Û": "U", "ú": "u", "Ú": "U", "ù": "u", "Ù": "U", "ç": "c", "Ç": "C" }
    return s.replace(/[\W\[\] ]/g, function (a) { return map[a] || a }).split(' ').join('_');
  }
}
