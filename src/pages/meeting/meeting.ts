import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, Events, ModalController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { TranslateService } from '@ngx-translate/core';
import { DatabaseProvider } from '../../providers/database/database';
import { MeetingListPage } from '../meeting-list/meeting-list';
import { MeetingMapPage } from '../meeting-map/meeting-map';

@IonicPage()
@Component({
  selector: 'page-meeting',
  templateUrl: 'meeting.html',
})
export class MeetingPage {
  mapPage: typeof MeetingMapPage;
  listPage: typeof MeetingListPage;
  loader: any;
  result: any;
  meetings: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public service: ApiServiceProvider, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public translate: TranslateService, private database: DatabaseProvider, public events: Events, private modal: ModalController) {
    this.mapPage = MeetingMapPage;
    this.listPage = MeetingListPage;
    this.loader = null;
  }

  ionViewWillEnter() {
    this.database.getUser().then((user) => {
      if (user != null) {
        this.translate.get("LOADING").subscribe((result: string) => {
          this.loader = this.loadingCtrl.create({
            content: result,
          });
          this.loader.present();
          this.getData({ user: user });
        });
      }
    });
  }

  getData(params) {
    this.service.getDataWithToken('meeting', params).then((res) => {
      if (this.loader != null) {
        this.loader.dismiss();
      }
      this.result = res;
      if (this.result.status == true) {
        this.navParams.data.meetings = this.result.meetings;
        this.events.publish('loadMeetings', this.result.meetings);
        if (this.navParams.data.hasOwnProperty('push')) {
          this.meetings = this.result.meetings;
          this.openPush(this.navParams.data.push);
        }
      } else {
        let alert = this
          .alertCtrl
          .create({
            title: this.translate.instant('ERROR'),
            subTitle: this.translate.instant('ERROR_AVAILABLE'),
            buttons: ['Ok']
          });
        alert.present();
      }
    }, (err) => {
      if (this.loader != null) {
        this.loader.dismiss();
      }
      let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('ERROR'),
          subTitle: this.translate.instant('ERROR_AVAILABLE'),
          buttons: ['Ok']
        });
      alert.present();
    });
  }

  openPush(data) {
    this.navParams.data = {};
    Object.keys(this.meetings).forEach(key => {
      let item = this.meetings[key];
      if (item.id == data.item_id) {
        this.open(item);
      }
    });
  }

  open(meeting) {
    const modalMeeting = this.modal.create('MeetingDetailsPage', { meeting: meeting });
    modalMeeting.present();
  }
}
