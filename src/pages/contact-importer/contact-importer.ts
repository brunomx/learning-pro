import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, ViewController } from 'ionic-angular';
import { TranslateService } from '../../../node_modules/@ngx-translate/core';
import { ContactFieldType, Contacts } from '../../../node_modules/@ionic-native/contacts/ngx';
import { DomSanitizer } from '../../../node_modules/@angular/platform-browser';

@IonicPage()
@Component({
  selector: 'page-contact-importer',
  templateUrl: 'contact-importer.html',
})
export class ContactImporterPage {
  loader: any;
  filter: ContactFieldType[] = ["name.formatted"];
  contactFound: any;
  contactSelected: any;
  lastContact: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public translate: TranslateService, public loadingCtrl: LoadingController, public alertCtrl: AlertController, private view: ViewController, public contacts: Contacts, private sanitized: DomSanitizer) {
    this.search('');
    this.contactSelected = [];
    this.lastContact = null;
  }

  ionViewDidLoad() {
  }

  search(val) {
    this.contacts.find(this.filter, { filter: val, multiple: true, hasPhoneNumber: true }).then(conts => {
      conts.sort(function (a, b) {
        if (a.name.formatted < b.name.formatted) return -1;
        if (a.name.formatted > b.name.formatted) return 1;
        return 0;
      });
      this.contactFound = conts;
    }, (err) => {
      let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('ERROR'),
          subTitle: this.translate.instant('ERROR_AVAILABLE'),
          buttons: ['Ok']
        });
      alert.present();
    });
  }

  remove(contact, index) {
    contact.selected = false;
    let contacts = this.contactSelected;
    contacts.splice(index, 1);
    contacts.sort(function (a, b) {
      if (a.name.formatted < b.name.formatted) return -1;
      if (a.name.formatted > b.name.formatted) return 1;
      return 0;
    });
    this.contactSelected = contacts;
  }

  select(contact) {
    if (this.lastContact != null) {
      this.lastContact.selected = false;
    }
    contact.selected = true;
    this.lastContact = contact;

    let confirm = this
      .alertCtrl
      .create({
        title: this.translate.instant('CONFIRM_IMPORT'),
        subTitle: contact.name.formatted,
        buttons: [
          {
            text: this.translate.instant('TOUCH_CANCEL'),
            handler: () => {
              contact.selected = false;
            }
          },
          {
            text: this.translate.instant('CONFIRM'),
            handler: () => {
              this.view.dismiss(contact);
            }
          }
        ]
      });
    confirm.present();
  }

  sanitizeImage(photo) {
    return this.sanitized.bypassSecurityTrustUrl(photo);
  }

  onKeyUp(ev) {
    this.search(ev.target.value);
  }

  closeModal() {
    this.view.dismiss();
  }

}
