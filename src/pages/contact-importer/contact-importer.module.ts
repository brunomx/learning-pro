import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContactImporterPage } from './contact-importer';
import { TranslateModule } from '../../../node_modules/@ngx-translate/core';
import { OrderModule } from '../../../node_modules/ngx-order-pipe';

@NgModule({
  declarations: [
    ContactImporterPage,
  ],
  imports: [
    IonicPageModule.forChild(ContactImporterPage),
    TranslateModule,
    OrderModule
  ],
})
export class ContactImporterPageModule {}
