import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LeadershipMonthPage } from './leadership-month';

@NgModule({
  declarations: [
    LeadershipMonthPage,
  ],
  imports: [
    IonicPageModule.forChild(LeadershipMonthPage),
  ],
})
export class LeadershipMonthPageModule {}
