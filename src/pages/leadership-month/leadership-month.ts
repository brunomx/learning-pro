import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Events } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { TranslateService } from '../../../node_modules/@ngx-translate/core';
import { DatabaseProvider } from '../../providers/database/database';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@IonicPage()
@Component({
  selector: 'page-leadership-month',
  templateUrl: 'leadership-month.html',
})
export class LeadershipMonthPage {
  qualifications: any;
  loader: any;
  result: any;
  month: number;
  months: string[];
  month_name: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private service: ApiServiceProvider, public alertCtrl: AlertController, private translate: TranslateService, private loading: LoadingController, private database: DatabaseProvider, private events: Events, public loadingCtrl: LoadingController, private socialSharing: SocialSharing) {
    this.qualifications = [];
    let date = new Date();
    this.month = date.getMonth() + 1;
    this.months = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
    this.month_name = this.months[date.getMonth()];
  }

  ionViewDidLoad() {
    this.events.subscribe('loadLeadership', (qualifications) => {
      if (qualifications == false) {
        this.translate.get("LOADING").subscribe((result: string) => {
          this.loader = this.loading.create({
            content: result,
          });
          this.loader.present();
          this.database.getUser().then((user) => {
            if (user != null) {
              this.getData({ user: user });
            }
          });
        });
      } else {
        this.qualifications = qualifications;
      }
    });
  }

  getData(params) {
    this.service.getDataWithToken('qualification', params).then((res) => {
      this.loader.dismiss();
      this.result = res;
      if (this.result.status == true) {
        this.qualifications = this.result.qualifications;
      } else {
        let alert = this
          .alertCtrl
          .create({
            title: this.translate.instant('ERROR'),
            subTitle: this.translate.instant('ERROR_AVAILABLE'),
            buttons: ['Ok']
          });
        alert.present();
      }
    }, (err) => {
      this.loader.dismiss();
      let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('ERROR'),
          subTitle: this.translate.instant('ERROR_AVAILABLE'),
          buttons: ['Ok']
        });
      alert.present();
    });
  }

  share(item, social) {
    this.translate.get("LOADING").subscribe((result: string) => {
      this.loader = this.loadingCtrl.create({
        content: result,
      });
      this.loader.present();
    });
    let message = item.name + ' - ' + item.level_name;
    switch (social) {
      case '1':
        this.socialSharing.shareViaFacebook(message, item.image, null).then((res) => {
          this.loader.dismiss();
        }).catch((err) => {
          this.loader.dismiss();
        });
        break;
      case '2':
        this.socialSharing.shareViaTwitter(message, item.image).then((res) => {
          this.loader.dismiss();
        }).catch((err) => {
          this.loader.dismiss();
        });
        break;
      case '3':
        this.socialSharing.shareViaWhatsApp(message, item.image, null).then((res) => {
          this.loader.dismiss();
        }).catch((err) => {
          this.loader.dismiss();
        });
        break;
    }
  }

}
