import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SourcePdfPage } from './source-pdf';
import { TranslateModule } from '../../../node_modules/@ngx-translate/core';

@NgModule({
  declarations: [
    SourcePdfPage,
  ],
  imports: [
    IonicPageModule.forChild(SourcePdfPage),
    TranslateModule
  ],
})
export class SourcePdfPageModule {}
