import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ViewController, Platform } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { TranslateService } from '../../../node_modules/@ngx-translate/core';
import { Device } from '../../../node_modules/@ionic-native/device/ngx';
import { Network } from '../../../node_modules/@ionic-native/network/ngx';
import { DocumentViewer, DocumentViewerOptions } from '../../../node_modules/@ionic-native/document-viewer/ngx';
import { File } from '../../../node_modules/@ionic-native/file/ngx';
import { FileTransferObject, FileTransfer } from '../../../node_modules/@ionic-native/file-transfer/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { DatabaseProvider } from '../../providers/database/database';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@IonicPage()
@Component({
  selector: 'page-source-pdf',
  templateUrl: 'source-pdf.html',
})
export class SourcePdfPage {
  loader: any;
  result: any;
  pdfs: any;
  groups: any[];
  categories: any[];
  targetPath: any;
  extensao: string;
  doload3g: boolean;
  fileTransfer: FileTransferObject;
  duration: any;
  user: any;
  files: any;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, private service: ApiServiceProvider, public alertCtrl: AlertController, private translate: TranslateService, private loading: LoadingController, private device: Device, private file: File, private document: DocumentViewer, private network: Network, private transfer: FileTransfer, public alert: AlertController, private view: ViewController, private browser: InAppBrowser, private database: DatabaseProvider, public platform: Platform, private socialSharing: SocialSharing) {
    this.groups = [];
    this.extensao = '.pdf';
    this.fileTransfer = this.transfer.create();
    this.duration = null;
    let that = this;
    this.fileTransfer.onProgress((progress) => {
      if (that.duration == null) {
        that.duration = progress.total;
        that.loader = that.loading.create({
          content: that.translate.instant('DOWNLOADING')
        });
        that.loader.present();
      }
    });
    
    if (this.device.platform == "Android") {
      this.targetPath = this.file.dataDirectory;
    } else {
      this.targetPath = this.file.documentsDirectory;
    }
  }
  
  ionViewDidLoad() {
    if (this.navParams.data.hasOwnProperty('souces') == false) {
      this.translate.get("LOADING").subscribe((result: string) => {
        this.loader = this.loading.create({
          content: result,
        });
        this.loader.present();
        this.database.getUser().then((user) => {
          if (user != null) {
            this.user = user;
            this.getData({ user: user });
          }
        });
      });
    } else {
      this.database.getUser().then((user) => {
        if (user != null) {
          this.user = user;
          this.checkPdf(this.navParams.data.souces);
        }
      });
    }
  }
  
  getData(params){
    this.service.getDataWithToken('material', params).then((res) => {
      this.loader.dismiss();
      this.result = res;
      if (this.result.status == true) {
        this.checkPdf(this.result.materials);
      } else {
        let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('ERROR'),
          subTitle: this.translate.instant('ERROR_AVAILABLE'),
          buttons: ['Ok']
        });
        alert.present();
      }
    }, (err) => {
      this.loader.dismiss();
      let alert = this
      .alertCtrl
      .create({
        title: this.translate.instant('ERROR'),
        subTitle: this.translate.instant('ERROR_AVAILABLE'),
        buttons: ['Ok']
      });
      alert.present();
    });
  }
  
  checkPdf(files) {
    let elements = [],
    that = this;
    this.files = [];
    Object.keys(files).forEach(key => {
      let item = files[key];
      if (item.hasOwnProperty('type')) {
        if (item.type == 0) {
          this.files.push(item);
          if (this.device.platform != null) {
            this.checkFile(item);
          }
          if (item.category_id != null) {
            that.populate(item);
          } else {
            elements.push(item);
          }
        }
      }
    });
    if (this.navParams.data.hasOwnProperty('push')) {
      if (this.navParams.data.push.source == '0') {
        this.openPush(this.navParams.data.push);
      }
    }
    this.pdfs = elements;
    this.order();
  }
  
  openPush(data) {
    this.navParams.data = {};
    Object.keys(this.files).forEach(key => {
      let item = this.files[key];
      if (item.id == data.item_id) {
        this.open(item);
      }
    });
  }
  
  populate(item) {
    if (this.groups[item.category_id] != undefined) {
      this.groups[item.category_id].itens.push(item);
    } else {
      this.groups[item.category_id] = { name: item.category_name, itens: [] };
      this.groups[item.category_id].itens.push(item);
    }
  }
  
  order() {
    let orderGroup = [],
    that = this;
    Object.keys(this.groups).forEach(key => {
      if (this.groups[key].hasOwnProperty('itens')) {
        orderGroup.push(this.groups[key]);
      }
    });
    that.categories = orderGroup.reverse();
  }
  
  checkFile(item) {
    let fileName = this.removerAcentos(item.name) + this.extensao;
    this.file.checkFile(this.targetPath, fileName).then((res) => {
      item.exists = true;
    }, (err) => {
      item.exists = false;
    });
  }
  
  open(pdf) {
    this.browser.create(pdf.path, '_blank', 'location=yes');
  }
  
  openLocal(pdf) {
    pdf.targetFile = this.targetPath + this.removerAcentos(pdf.name) + this.extensao;
    if (this.device.platform != "Android") {
      pdf.targetFile = pdf.targetFile.replace(/^file:\/\//, '');
    }
    const options: DocumentViewerOptions = {
      title: pdf.name
    }
    this.document.viewDocument(pdf.targetFile, 'application/pdf', options)
  }
  
  delete(pdf) {
    let fileName = this.removerAcentos(pdf.name) + this.extensao;
    this.file.removeFile(this.targetPath, fileName).then((res) => {
      pdf.exists = false;
      let alert = this
      .alertCtrl
      .create({
        title: this.translate.instant('FILE'),
        subTitle: this.translate.instant('DELETED'),
        buttons: ['Ok']
      });
      alert.present().then((res) => {
        this.view.dismiss();
      });
    }, (err) => {
      let alert = this
      .alertCtrl
      .create({
        title: this.translate.instant('REMOVE_VIDEO_TITLE'),
        subTitle: this.translate.instant('REMOVE_VIDEO'),
        buttons: ['Ok']
      });
      alert.present();
    });
  }
  
  download(pdf) {
    if (this.network.type == 'wifi' || this.doload3g) {
      let that = this;
      this.file.getFreeDiskSpace().then((space) => {
        let sizeFile = pdf.size / 1000;
        if (space > sizeFile) {
          pdf.targetFile = that.targetPath + that.removerAcentos(pdf.name) + that.extensao;
          that.fileTransfer.download(pdf.path, pdf.targetFile).then((res) => {
            that.duration = null;
            that.loader.dismiss();
            pdf.exists = true;
            that.openLocal(pdf);
          }, (error) => {
            that.duration = null;
            that.loader.dismiss();
            let alert = that
            .alert
            .create({
              title: that.translate.instant('ERROR'),
              subTitle: that.translate.instant('MEMORY_FULL'),
              buttons: ['Ok']
            });
            alert.present();
          });
        } else {
          let alert = that
          .alert
          .create({
            title: that.translate.instant('ERROR'),
            subTitle: that.translate.instant('MEMORY_FULL'),
            buttons: ['Ok']
          });
          alert.present();
        }
      }, (err) => {
        let alert = that
        .alert
        .create({
          title: that.translate.instant('ERROR'),
          subTitle: that.translate.instant('MEMORY_FULL'),
          buttons: ['Ok']
        });
        alert.present();
      });
    } else {
      const confirm = this.alert.create({
        title: this.translate.instant('WIFI_NETWORK_TITLE'),
        message: this.translate.instant('WIFI_NETWORK'),
        buttons: [
          {
            text: this.translate.instant('DOWNLOAD_LATER'),
            handler: () => {
              this.view.dismiss();
            }
          },
          {
            text: this.translate.instant('DOWNLOAD_NOW'),
            handler: () => {
              this.doload3g = true;
              this.download(pdf);
            }
          }
        ]
      });
      confirm.present();
    }
  }
  
  sendEmail(pdf){
    this.translate.get("LOADING").subscribe((result: string) => {
      this.loader = this.loading.create({
        content: result,
      });
      this.loader.present();
    });
    this.socialSharing.shareViaEmail('Conteúdo em anexo disponibilizado pelo app BigProfy', 'BigProfy - ' + pdf.name, [this.user.email], null, null, pdf.path).then((res) => {
      this.loader.dismiss();
    }).catch((err) => {
      this.loader.dismiss();
    });
  }
  
  removerAcentos(s) {
    let map = { "â": "a", "Â": "A", "à": "a", "À": "A", "á": "a", "Á": "A", "ã": "a", "Ã": "A", "ê": "e", "Ê": "E", "è": "e", "È": "E", "é": "e", "É": "E", "î": "i", "Î": "I", "ì": "i", "Ì": "I", "í": "i", "Í": "I", "õ": "o", "Õ": "O", "ô": "o", "Ô": "O", "ò": "o", "Ò": "O", "ó": "o", "Ó": "O", "ü": "u", "Ü": "U", "û": "u", "Û": "U", "ú": "u", "Ú": "U", "ù": "u", "Ù": "U", "ç": "c", "Ç": "C" }
    return s.replace(/[\W\[\] ]/g, function (a) { return map[a] || a }).split(' ').join('_');
  }
}
