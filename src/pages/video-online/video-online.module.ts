import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VideoOnlinePage } from './video-online';

@NgModule({
  declarations: [
    VideoOnlinePage,
  ],
  imports: [
    IonicPageModule.forChild(VideoOnlinePage),
  ],
})
export class VideoOnlinePageModule {}
