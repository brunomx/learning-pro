import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Events } from 'ionic-angular';
import { DomSanitizer } from '../../../node_modules/@angular/platform-browser';
import { VimeoServiceProvider } from '../../providers/vimeo-service/vimeo-service';

@IonicPage()
@Component({
  selector: 'page-video-online',
  templateUrl: 'video-online.html',
})
export class VideoOnlinePage {
  video: any;
  code: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private sanitized: DomSanitizer, private view: ViewController, private vimeo: VimeoServiceProvider, public events: Events) {
    this.video = this.navParams.data.video;
    this.code = this.sanitized.bypassSecurityTrustHtml(this.video.embed);
    this.events.subscribe('endVideo', (end) => {
      if(end){
        this.closeModal();
      }
    });
  }

  ionViewDidLoad() {
    this.vimeo.initPlayer(this.video);
  }

  ionViewWillLeave() {
    this.vimeo.stopPlayer();
  }

  closeModal(){
    this.view.dismiss(this.video);
  }

}
