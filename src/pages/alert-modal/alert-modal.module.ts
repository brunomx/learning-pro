import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AlertModalPage } from './alert-modal';
import { TranslateModule } from '../../../node_modules/@ngx-translate/core';

@NgModule({
  declarations: [
    AlertModalPage,
  ],
  imports: [
    IonicPageModule.forChild(AlertModalPage),
    TranslateModule
  ],
})
export class AlertModalPageModule {}
