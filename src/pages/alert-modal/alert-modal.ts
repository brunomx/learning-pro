import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { TranslateService } from '../../../node_modules/@ngx-translate/core';
import { DatabaseProvider } from '../../providers/database/database';
import { DailyAudioPage } from '../daily-audio/daily-audio';
import { DailyPage } from '../daily/daily';
import { FirstPage } from '../first/first';
import { SourceAudioPage } from '../source-audio/source-audio';
import { SourcePdfPage } from '../source-pdf/source-pdf';
import { SourcesPage } from '../sources/sources';
import { MeetingPage } from '../meeting/meeting';

@IonicPage()
@Component({
  selector: 'page-alert-modal',
  templateUrl: 'alert-modal.html',
})
export class AlertModalPage {
  alert: any;
  loader: any;
  user: any;
  received: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private view: ViewController, private service: ApiServiceProvider, private alertCtrl: AlertController, private translate: TranslateService, private database: DatabaseProvider) {
    this.alert = this.navParams.data.alert;
    if(this.navParams.data.hasOwnProperty('received')){
      this.received = this.navParams.data.received;
    } else {
      this.received = false
    }
    // this.received = true;
  }

  ionViewWillEnter() {
    this.database.getUser().then((user) => {
      if (user != null) {
       this.user = user;
        if(this.alert.data){
          if(this.alert.data.hasOwnProperty('id')){
            let params = {
              user: user,
              status: 1,
              notification_id: this.alert.data.id,
            }
            this.updateNotification(params);
          }
       }
      } else {
        this.user = null;
      }
    });
  }

  closeModal() {
    this.view.dismiss();
  }

  delete() {
    this.alert.status = 2;
    let params = {
      user: this.user,
      status: this.alert.status,
      notification_id: this.alert.id,
    }
    this.updateNotification(params);
  }

  updateNotification(params){
    this.service.postDataWithToken('notification-status', params).then((res) => {
      this.view.dismiss();
      let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('ALERT'),
          subTitle: this.translate.instant('SUCCESSFULLY_DELETE'),
          buttons: ['Ok']
        });
      alert.present();
    }, (err) => {
      let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('ERROR'),
          subTitle: this.translate.instant('ERROR_AVAILABLE'),
          buttons: ['Ok']
        });
      alert.present();
    });
  }

  openData(){
    let data = this.alert.data;
    // let data = { item_id: 3, type: '6', source: '5' };

    switch (data.type) {
      case '1':
        this.navCtrl.push(DailyPage, { push: data });
        break;
      case '2':
        this.navCtrl.push(DailyAudioPage, { push: data });
        break;
      case '4':
        this.navCtrl.push(FirstPage, { push: data });
        break;
      case '5':
        if (data.source == '0'){
          this.navCtrl.push(SourcePdfPage, { push: data });
        } else if (data.source == '1'){
          this.navCtrl.push(SourceAudioPage, { push: data });
        } else if (data.source == '2') {
          this.navCtrl.push(SourcesPage, { push: data });
        }
        break;
      case '6':
        this.navCtrl.push(MeetingPage, { push: data });
        break;
      default:
        break;
    }
  }
}
