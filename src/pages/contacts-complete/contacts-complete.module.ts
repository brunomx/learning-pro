import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContactsCompletePage } from './contacts-complete';
import { TranslateModule } from '../../../node_modules/@ngx-translate/core';

@NgModule({
  declarations: [
    ContactsCompletePage,
  ],
  imports: [
    IonicPageModule.forChild(ContactsCompletePage),
    TranslateModule
  ],
})
export class ContactsCompletePageModule {}
