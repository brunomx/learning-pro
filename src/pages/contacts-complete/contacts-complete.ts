import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, LoadingController, AlertController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { TranslateService } from '../../../node_modules/@ngx-translate/core';
import { StorageServiceProvider } from '../../providers/storage-service/storage-service';
import { ContactDetailsPage } from '../contact-details/contact-details';
import { DatabaseProvider } from '../../providers/database/database';

@IonicPage()
@Component({
  selector: 'page-contacts-complete',
  templateUrl: 'contacts-complete.html',
})
export class ContactsCompletePage {
  contacts: any;
  user: any;
  loader: any;
  result: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public service: ApiServiceProvider, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public translate: TranslateService, private storage: StorageServiceProvider, public events: Events, private database: DatabaseProvider) {
    this.contacts = [];
    this.user = this.storage.getObject('distributor');
  }

  ionViewDidLoad() {
    this.events.subscribe('loadContacts', (data) => {
      if (data == false) {
        this.translate.get("LOADING").subscribe((result: string) => {
          this.loader = this.loadingCtrl.create({
            content: result,
          });
          this.database.getUser().then((user) => {
            if (user != null) {
              this.loader.present();
              this.getData(user);
            }
          });
        });
      } else {
        this.contacts = data;
      }
    });
  }

  getData(user) {
    let params = {
      user: user,
    }

    this.service.getDataWithToken('contact', params).then((res) => {
      this.loader.dismiss();
      this.result = res;
      if (this.result.status == true && this.result.contacts.length > 0) {
        let contacts = [];
        Object.keys(this.result.contacts).forEach(key => {
          let item = this.result.contacts[key];
          item.stars = this.countStars(item.stars);
          contacts.push(item);
        });

        this.contacts = contacts;
      } else {

      }
    }, (err) => {
      this.loader.dismiss();
      let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('ERROR'),
          subTitle: this.translate.instant('ERROR_AVAILABLE'),
          buttons: ['Ok']
        });
      alert.present();
    });
  }

  countStars(stars) {
    let itens = [];
    for (let i = 0; i < stars; i++) {
      itens.push(i);
    }
    return itens;
  }

  register() {
    this.navCtrl.push(ContactDetailsPage, {
      contact: null,
    });
  }

  edit(contact) {
    this.navCtrl.push(ContactDetailsPage, {
      contact: contact,
    });
  }

}
