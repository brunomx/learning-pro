import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { InAppBrowser } from '../../../node_modules/@ionic-native/in-app-browser/ngx';
import { StorageServiceProvider } from '../../providers/storage-service/storage-service';

@IonicPage()
@Component({
  selector: 'page-follow',
  templateUrl: 'follow.html',
})
export class FollowPage {
  user: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private browser: InAppBrowser, private storage: StorageServiceProvider) {
    this.user = this.storage.getObject('distributor');
  }

  ionViewDidLoad() {
  }

  openLink(){
    let link = 'https://yp275.isrefer.com/go/tt/' + this.user.AffCode;
    this.browser.create(link,'_blank');
  }
}
