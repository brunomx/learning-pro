import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FollowPage } from './follow';
import { TranslateModule } from '../../../node_modules/@ngx-translate/core';

@NgModule({
  declarations: [
    FollowPage,
  ],
  imports: [
    IonicPageModule.forChild(FollowPage),
    TranslateModule
  ],
})
export class FollowPageModule {}
