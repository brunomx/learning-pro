import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, ModalController } from 'ionic-angular';
import { TranslateService } from '../../../node_modules/@ngx-translate/core';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { StorageServiceProvider } from '../../providers/storage-service/storage-service';
import { DatabaseProvider } from '../../providers/database/database';

@IonicPage()
@Component({
  selector: 'page-alerts',
  templateUrl: 'alerts.html',
})
export class AlertsPage {
  loader: any;
  result: any;
  alerts: any;
  user: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public translate: TranslateService, private loadingCtrl: LoadingController, private service: ApiServiceProvider, private alertCtrl: AlertController, private modal: ModalController, public storage: StorageServiceProvider, private database: DatabaseProvider) {
  }

  ionViewWillEnter() {
    this.translate.get("LOADING").subscribe((result: string) => {
      this.loader = this.loadingCtrl.create({
        content: result,
      });
      this.loader.present();
      this.database.getUser().then((user) => {
        if (user != null) {
          this.user = user;
          this.getData({ user: user });
        }
      });
    });
  }

  getData(params){
    this.service.getDataWithToken('notification', params).then((res) => {
      this.loader.dismiss();
      this.result = res;
      if (this.result.status == true) {
        this.alerts = this.result.notifications;
      } else {
        let alert = this
          .alertCtrl
          .create({
            title: this.translate.instant('ERROR'),
            subTitle: this.translate.instant('ERROR_AVAILABLE'),
            buttons: ['Ok']
          });
        alert.present();
      }
    }, (err) => {
      this.loader.dismiss();
      let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('ERROR'),
          subTitle: this.translate.instant('ERROR_AVAILABLE'),
          buttons: ['Ok']
        });
      alert.present();
    });
  }

  openNotification(alert) {
    if (alert.status == "0") {
      alert.status = 1;
      this.udpateNotification(alert);
    }

    const alertModal = this.modal.create('AlertModalPage', { alert: alert });
    alertModal.present();
  }

  udpateNotification(alert) {
    let params = {
      user: this.user,
      status: alert.status,
      notification_id: alert.id,
    }

    this.service.postDataWithToken('notification-status', params).then((res) => {
    }, (err) => {
      this.loader.dismiss();
      let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('ERROR'),
          subTitle: this.translate.instant('ERROR_AVAILABLE'),
          buttons: ['Ok']
        });
      alert.present();
    });
  }

}
