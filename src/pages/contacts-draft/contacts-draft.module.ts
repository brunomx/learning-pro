import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContactsDraftPage } from './contacts-draft';
import { TranslateModule } from '../../../node_modules/@ngx-translate/core';

@NgModule({
  declarations: [
    ContactsDraftPage,
  ],
  imports: [
    IonicPageModule.forChild(ContactsDraftPage),
    TranslateModule
  ],
})
export class ContactsDraftPageModule {}
