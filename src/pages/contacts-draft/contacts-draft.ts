import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { TranslateService } from '../../../node_modules/@ngx-translate/core';
import { StorageServiceProvider } from '../../providers/storage-service/storage-service';
import { ContactDetailsPage } from '../contact-details/contact-details';
import { DatabaseProvider } from '../../providers/database/database';

@IonicPage()
@Component({
  selector: 'page-contacts-draft',
  templateUrl: 'contacts-draft.html',
})
export class ContactsDraftPage {
  loader: any;
  user: any;
  result: any;
  contacts: any;
  showList: boolean;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, private service: ApiServiceProvider, public alertCtrl: AlertController, private translate: TranslateService, private loading: LoadingController, private storage: StorageServiceProvider, private database: DatabaseProvider) {
    this.user = this.storage.getObject('distributor');
    this.showList = false;
  }
  
  ionViewDidEnter(){
    this.showList = false;
    this.translate.get("LOADING").subscribe((result: string) => {
      this.loader = this.loading.create({
        content: result,
      });
      this.database.getUser().then((user) => {
        if (user != null) {
          this.loader.present();
          this.getData(user);
        }
      });
    });
  }

  getData(user) {
    let params = {
      user: user,
    }

    this.service.getDataWithToken('contact-import-list', params).then((res) => {
      this.loader.dismiss();
      this.result = res;
      if (this.result.status == true && this.result.contacts.length > 0) {
        let contacts = [];
        Object.keys(this.result.contacts).forEach(key => {
          let item = this.result.contacts[key];
          this.formatPhone(item);
          contacts.push(item);
        });
        this.contacts = contacts;
        this.showList = true;
      } else {
        this.contacts = [];
        this.showList = true;
      }
    }, (err) => {
      this.loader.dismiss();
      let alert = this
        .alertCtrl
        .create({
          title: this.translate.instant('ERROR'),
          subTitle: this.translate.instant('ERROR_AVAILABLE'),
          buttons: ['Ok']
        });
      alert.present();
    });
  }
  
  formatPhone(item){
    item.oldPhone = item.phone;
    if(item.phone.length > 0){
      if(item.phone.length <= 9){
        if(item.phone.length == 9){
          item.phone = item.phone.substr(0, 5) + "-" + item.phone.substr(5);
        } else if(item.phone.length == 8){
          item.phone = item.phone.substr(0, 4) + "-" + item.phone.substr(4);
        }
      } else {
        let phone = item.phone.match(/\d/g).join("");
        if(phone.length == 14){
          let country = phone.substr(0, 3),
          ddd = phone.substr(3, 2),
          number = phone.substr(5);
          
          if(number.length == 9){
            number = number.substr(0, 5) + "-" + number.substr(5);
          } else if(number.length == 8){
            number = number.substr(0, 4) + "-" + number.substr(4);
          }
          
          item.phone = country + ' ' + ddd + ' ' + number;
        } else if(phone.length == 13){
          let country = phone.substr(0, 2),
          ddd = phone.substr(2, 2),
          number = phone.substr(4);
          
          if(number.length == 9){
            number = number.substr(0, 5) + "-" + number.substr(5);
          } else if(number.length == 8){
            number = number.substr(0, 4) + "-" + number.substr(4);
          }
          
          item.phone = '+' + country + ' ' + ddd + ' ' + number;
        } else if(phone.length == 12){

          if(phone.substr(0, 1) == '0'){
            let ddd = phone.substr(0, 3),
            number = phone.substr(3);
            
            if(number.length == 9){
              number = number.substr(0, 5) + "-" + number.substr(5);
            } else if(number.length == 8){
              number = number.substr(0, 4) + "-" + number.substr(4);
            }
            
            item.phone = ddd + ' ' + number;
          } else {
            let country = phone.substr(0, 2),
            ddd = phone.substr(2, 2),
            number = phone.substr(4);
            
            if(number.length == 9){
              number = number.substr(0, 5) + "-" + number.substr(5);
            } else if(number.length == 8){
              number = number.substr(0, 4) + "-" + number.substr(4);
            }
            
            item.phone = '+' + country + ' ' + ddd + ' ' + number;
          }
        } else if(phone.length == 11){
          if(phone.substr(0, 1) == '0'){
            let ddd = phone.substr(0, 3),
            number = phone.substr(3);
            
            if(number.length == 9){
              number = number.substr(0, 5) + "-" + number.substr(5);
            } else if(number.length == 8){
              number = number.substr(0, 4) + "-" + number.substr(4);
            }
            
            item.phone = ddd + ' ' + number;
          } else {
            let ddd = phone.substr(0, 2),
            number = phone.substr(2);
            
            if(number.length == 9){
              number = number.substr(0, 5) + "-" + number.substr(5);
            } else if(number.length == 8){
              number = number.substr(0, 4) + "-" + number.substr(4);
            }
            
            item.phone = ddd + ' ' + number;
          }
        } else {
          let ddd = phone.substr(0, 2),
          number = phone.substr(2);
          
          if(number.length == 9){
            number = number.substr(0, 5) + "-" + number.substr(5);
          } else if(number.length == 8){
            number = number.substr(0, 4) + "-" + number.substr(4);
          }
          
          item.phone = ddd + ' ' + number;
        }
      }
    }
  }

  edit(contact) {
    let item = {
      first_name: contact.first_name,
      last_name: contact.last_name,
      email: contact.email,
      phone: contact.phone,
      oldPhone: contact.oldPhone,
      import_id: contact.import_id,
      id: contact.id,
      import: true
    }

    this.navCtrl.push(ContactDetailsPage, {
      contact: item,
    });
  }
}
