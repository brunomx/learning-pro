import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';

import { StatusBar } from '@ionic-native/status-bar/ngx';
import { HttpClientModule, HttpClient , HTTP_INTERCEPTORS} from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { OrderModule } from 'ngx-order-pipe';
import { TranslateModule, TranslateLoader } from '../../node_modules/@ngx-translate/core';
import { TranslateHttpLoader } from '../../node_modules/@ngx-translate/http-loader';
import { StorageServiceProvider } from '../providers/storage-service/storage-service';
import { ApiServiceProvider } from '../providers/api-service/api-service';
import { Device } from '../../node_modules/@ionic-native/device/ngx';
import { File } from '../../node_modules/@ionic-native/file/ngx';
import { SQLite } from '../../node_modules/@ionic-native/sqlite/ngx';

import { Network } from '../../node_modules/@ionic-native/network/ngx';
import { FileTransfer } from '../../node_modules/@ionic-native/file-transfer/ngx';
import { VimeoServiceProvider } from '../providers/vimeo-service/vimeo-service';
import { IonicStorageModule } from '@ionic/storage';
import { LazyLoadImageModule } from 'ng-lazyload-image';
// import { VideoOfflineProvider } from '../providers/video-ofngxfline/video-offline';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { DocumentViewer } from '@ionic-native/document-viewer/ngx';
import { IonicAudioModule, WebAudioProvider, CordovaMediaProvider, defaultAudioProviderFactory } from 'ionic-audio';
import { Media } from '@ionic-native/media/ngx';
import { CommonModule } from '../../node_modules/@angular/common';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';
import { BrMaskerModule } from 'brmasker-ionic-3';
import { Contacts } from '../../node_modules/@ionic-native/contacts/ngx';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx';


import { MyApp } from './app.component';
import { LoginPage } from '../pages/login/login';
// import { FollowPage } from '../pages/follow/follow';
// import { WebinarsPage } from '../pages/webinars/webinars';
import { AlertsPage } from '../pages/alerts/alerts';
import { DailyPage } from '../pages/daily/daily';
import { FirstPage } from '../pages/first/first';
import { CoursesPage } from '../pages/courses/courses';
import { VideoCoursePage } from '../pages/video-course/video-course';
// import { DisclosurePage } from '../pages/disclosure/disclosure';
// import { NotesPage } from '../pages/notes/notes';
import { SourcesPage } from '../pages/sources/sources';
import { SourceVideoPage } from '../pages/source-video/source-video';
import { SourceAudioPage } from '../pages/source-audio/source-audio';
import { SourcePdfPage } from '../pages/source-pdf/source-pdf';
import { ContactsPage } from '../pages/contacts/contacts';
import { ContactDetailsPage } from '../pages/contact-details/contact-details';
import { DailyAudioPage } from '../pages/daily-audio/daily-audio';
import { DailyAudioProdutoPage } from '../pages/daily-audio-produto/daily-audio-produto';
import { DailyAudioMotivacionalPage } from '../pages/daily-audio-motivacional/daily-audio-motivacional';
import { DisclosureBusinessPage } from '../pages/disclosure-business/disclosure-business';
import { DisclosureProdutsPage } from '../pages/disclosure-produts/disclosure-produts';
import { ContactsCompletePage } from '../pages/contacts-complete/contacts-complete';
import { ContactsDraftPage } from '../pages/contacts-draft/contacts-draft';
import { PushNotificationsProvider } from '../providers/push-notifications/push-notifications';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { DatabaseProvider } from '../providers/database/database';
import { InterceptorProvider } from '../providers/interceptor/interceptor';
import { LeadershipPage } from '../pages/leadership/leadership';
import { LeadershipMonthPage } from '../pages/leadership-month/leadership-month';
import { LeadershipListPage } from '../pages/leadership-list/leadership-list';
import { MeetingPage } from '../pages/meeting/meeting';
import { MeetingMapPage } from '../pages/meeting-map/meeting-map';
import { MeetingListPage } from '../pages/meeting-list/meeting-list';
import { ProgressDirective } from '../directives/progress/progress';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

export function myCustomAudioProviderFactory() {
  return (window.hasOwnProperty('cordova')) ? new CordovaMediaProvider() : new WebAudioProvider();
}

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    // FollowPage,
    // WebinarsPage,
    AlertsPage,
    DailyPage,
    FirstPage,
    CoursesPage,
    VideoCoursePage,
    // DisclosurePage,
    // NotesPage,
    SourcesPage,
    SourceVideoPage,
    SourceAudioPage,
    SourcePdfPage,
    ContactsPage,
    DailyAudioPage,
    DailyAudioProdutoPage,
    DailyAudioMotivacionalPage,
    DisclosureBusinessPage,
    DisclosureProdutsPage,
    ContactsCompletePage,
    ContactsDraftPage,
    ContactDetailsPage,
    LeadershipPage,
    LeadershipMonthPage,
    LeadershipListPage,
    MeetingPage,
    MeetingMapPage,
    MeetingListPage,
    ProgressDirective,
  ],
  imports: [
    IonicAudioModule.forRoot(defaultAudioProviderFactory),
    BrowserModule,
    OrderModule,
    IonicModule.forRoot(MyApp,{
      backButtonText: '',
      iconMode: 'ios',
      backButtonIcon: 'ios-arrow-back'
    }),
    HttpClientModule,
    HttpModule,
    CommonModule,
    BrMaskerModule,
    LazyLoadImageModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      },
    }),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    // FollowPage,
    // WebinarsPage,
    AlertsPage,
    DailyPage,
    FirstPage,
    CoursesPage,
    VideoCoursePage,
    // DisclosurePage,
    // NotesPage,
    SourcesPage,
    SourceVideoPage,
    SourceAudioPage,
    SourcePdfPage,
    ContactsPage,
    ContactDetailsPage,
    DailyAudioPage,
    DailyAudioProdutoPage,
    DailyAudioMotivacionalPage,
    DisclosureBusinessPage,
    DisclosureProdutsPage,
    ContactsCompletePage,
    ContactsDraftPage,
    LeadershipPage,
    LeadershipMonthPage,
    LeadershipListPage,
    MeetingPage,
    MeetingMapPage,
    MeetingListPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Device,
    File,
    Network,
    FileTransfer,
    Clipboard,
    SocialSharing,
    DocumentViewer,
    CordovaMediaProvider,
    WebAudioProvider,
    InAppBrowser,
    FingerprintAIO,
    Contacts,
    StorageServiceProvider,
    ApiServiceProvider,
    VimeoServiceProvider,
    EmailComposer,
    Geolocation,
    LaunchNavigator,
    // VideoOfflineProvider,
    Media,
    SQLite,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    {provide: HTTP_INTERCEPTORS, useClass: InterceptorProvider, multi: true},
    // {provide: ErrorHandler, useClass: SentryIonicErrorHandler},
    PushNotificationsProvider,
    AuthServiceProvider,
    DatabaseProvider,
    
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule {}
