import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, ModalController, Events, MenuController, Content } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { TranslateService } from '../../node_modules/@ngx-translate/core';
import { StorageServiceProvider } from '../providers/storage-service/storage-service';
import { DatabaseProvider } from '../providers/database/database'

import { LoginPage } from '../pages/login/login';
// import { FollowPage } from '../pages/follow/follow';
// import { WebinarsPage } from '../pages/webinars/webinars';
import { AlertsPage } from '../pages/alerts/alerts';
import { DailyPage } from '../pages/daily/daily';
import { FirstPage } from '../pages/first/first';
import { CoursesPage } from '../pages/courses/courses';
// import { DisclosurePage } from '../pages/disclosure/disclosure';
// import { NotesPage } from '../pages/notes/notes';
import { SourcesPage } from '../pages/sources/sources';
import { Device } from '../../node_modules/@ionic-native/device/ngx';
import { ApiServiceProvider } from '../providers/api-service/api-service';
import { ContactsPage } from '../pages/contacts/contacts';
import { Network } from '@ionic-native/network/ngx';
import { DailyAudioPage } from '../pages/daily-audio/daily-audio';
import { MeetingPage } from '../pages/meeting/meeting';
import { LeadershipPage } from '../pages/leadership/leadership';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  @ViewChild(Content) content: Content;
  rootPage: any = LoginPage;
  public pages: any;
  modules: any;
  lang: string;
  promisse: NodeJS.Timer;
  user: any;
  result: any;
  isOffline: boolean;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, private translate: TranslateService, private modal: ModalController, private storage: StorageServiceProvider, public device: Device, public service: ApiServiceProvider, private events: Events, private network: Network, public menuCtrl: MenuController, public dbProvider: DatabaseProvider) {
    this.user = null;
    if(this.user == null){
      this.user = {FirstName : ''};
    }
    this.pages = [];
    this.isOffline = false;
    this.initializeApp(); 

    this.events.subscribe('afterLogin', (user) => {
      this.user = user;
      this.renderMenu();
    });

    this.events.subscribe('logout', (data) => {
      this.logout();
    });

    this.modules = [
      { title: 'DAILY_MOTIVATIONAL', component: DailyPage, icon: 'ios-calendar', params: { course_id: 2 }, route: 'course.video.index/2', type: 1 },
      { title: 'DAILY_AUDIO', component: DailyAudioPage, icon: 'ios-mic', params: {}, route: 'audio.index', type: 1 },
      { title: 'LIST_NAMES', component: ContactsPage, icon: 'ios-people', params: {} , type: 0 },
      { title: 'TOOLS', component: FirstPage, icon: 'ios-build', params: { course_id: 1 }, route: 'course.video.index/1', type: 1 },
      { title: 'COURSES', component: CoursesPage, icon: 'ios-videocam', params: {}, route: 'course.index', type: 1 },
      // { title: 'DISCLOSURE', component: DisclosurePage, icon: 'ios-share-alt', params: {} },
      // { title: 'REPORTS', component: NotesPage, icon: 'ios-document', params: {} },
      { title: 'MATERIALS', component: SourcesPage, icon: 'ios-paper', params: {}, route: 'material.index', type: 1 },
      { title: 'ALERTS', component: AlertsPage, icon: 'ios-alert', params: {}, route: 'notification.index', type: 1 },
      // { title: 'WEBNEWS', component: WebinarsPage, icon: 'ios-laptop', params: {} },
      { title: 'MEETINGS', component: MeetingPage, icon: 'ios-laptop', params: {}, route: 'meeting.index', type: 1 },
      { title: 'LEADERSHIP', component: LeadershipPage, icon: 'ios-laptop', params: {}, route: 'qualification.index', type: 1 },
      // { title: 'FOLLOW_UP', component: FollowPage, icon: 'ios-person-add', params: {} },
    ];
  }

  renderMenu() {
    if (this.pages.length == 0){
      const options = [];
      this.dbProvider.getModules().then((itens) => {
        if (itens != null){
          Object.keys(this.modules).forEach(i => {
            const module = this.modules[i];
            module.hidden = true;
            if(module.type == 0){
              module.order = 1000;
              module.hidden = false;
              options.push(module);
            } else {
              Object.keys(itens).forEach(key => {
                let item = itens[key];
                if (item.param == null) {
                  if (module.route == item.route){
                    module.hidden = false;
                    module.title = item.name;
                    module.order = item.sequence == null ? 0 : item.sequence;
                    options.push(module);
                  }
                } else {
                  if (module.route === item.route + '/' + item.param) {
                    module.hidden = false;
                    module.title = item.name;
                    module.order = item.sequence;
                    options.push(module);
                  }
                }
              });
            }
          });
          this.pages = options.sort(function (a, b) {
            if (a.order > b.order) {
              return 1;
            }
            if (a.order < b.order) {
              return -1;
            }
            return 0;
          });
          console.log(this.pages);
        }
      });
    }
  }

  initializeApp() {
    this.platform.ready().then(() => {
      if (this.device.platform == "Android") {
        // this.statusBar.backgroundColorByHexString('#ffffff');
      } else {
        this.statusBar.backgroundColorByHexString('#ffffff');
      }
      this.splashScreen.hide();

      this.dbProvider.createDatabase().then((res) => {
        this.dbProvider.getLanguage().then((res) => {
            if (res != null) {
              this.translate.setDefaultLang(res.language);
            } else {
              this.translate.setDefaultLang('pt');
            }
            this.renderMenu();
        });
      }).catch((err) => {
        console.log(err);
      });
    });
  }

  openPage(page) {
    this.content.scrollToTop();
    this.nav.setRoot(page.component, page.params);
  }

  logout() {
    this.content.scrollToTop();
    let that = this;
    this.dbProvider.getUser().then((user) => {
      if (user != null){
        that.dbProvider.deleteUser(user.id).then((res) => {
          that.nav.setRoot(LoginPage);
        });
      }
    });
  }

  changeLanguage() {
    this.content.scrollToTop();
    const modalLanguage = this.modal.create('LanguagePage', {});
    modalLanguage.present();
  }

  checkSession() {
    let params = {
      user_id: this.user.Id,
      uuid: this.device.uuid
    }
    if(this.device.uuid != null){
      this.service.postData('checkSession', params).then((res) => {
        this.result = res;
        if (this.result.status == true) {
          this.menuCtrl.close();
          clearInterval(this.promisse);
          this.storage.clearTouch();
          this.logout();
        }
      }, (err) => {
      });
    } else {
      clearInterval(this.promisse);
    }
  }
}

