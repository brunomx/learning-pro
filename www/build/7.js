webpackJsonp([7],{

/***/ 772:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MeetingDetailsPageModule", function() { return MeetingDetailsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__meeting_details__ = __webpack_require__(797);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var MeetingDetailsPageModule = /** @class */ (function () {
    function MeetingDetailsPageModule() {
    }
    MeetingDetailsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__meeting_details__["a" /* MeetingDetailsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__meeting_details__["a" /* MeetingDetailsPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */]
            ],
        })
    ], MeetingDetailsPageModule);
    return MeetingDetailsPageModule;
}());

//# sourceMappingURL=meeting-details.module.js.map

/***/ }),

/***/ 797:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MeetingDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_launch_navigator_ngx__ = __webpack_require__(188);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MeetingDetailsPage = /** @class */ (function () {
    function MeetingDetailsPage(navCtrl, navParams, view, launchNavigator, alertCtrl, translate, loading) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.view = view;
        this.launchNavigator = launchNavigator;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.loading = loading;
        this.meeting = this.navParams.data.meeting;
    }
    MeetingDetailsPage.prototype.closeModal = function () {
        this.view.dismiss();
    };
    MeetingDetailsPage.prototype.goLocation = function () {
        var _this = this;
        var options = {
            start: null,
        };
        this.launchNavigator.navigate([this.meeting.address_latitude, this.meeting.address_longitude], options).then(function (res) {
        }, function (err) {
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('NOT_TRACE'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    MeetingDetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-meeting-details',template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/meeting-details/meeting-details.html"*/'<ion-header>\n  <ion-navbar color="dark">\n    <ion-title>{{meeting.name}}</ion-title>\n    <ion-buttons end>\n      <button ion-button (click)=\'closeModal()\' class="modal-buttom">\n        <ion-icon name="close-circle"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <ion-card>\n    <img [src]="meeting.image == null ? \'assets/imgs/header/followup.png\' : meeting.image" />\n    <ion-card-content>\n      <ion-card-title>{{meeting.name}}</ion-card-title>\n      <p>{{meeting.description}}</p>\n      <p><strong>{{meeting.date}} - {{meeting.address_address}}</strong></p>\n      <button ion-button icon-left block class="button-full" color="light" (click)="goLocation()">\n        <ion-icon name="ios-navigate"></ion-icon> {{ \'DRIVE_PLACE\' | translate }}\n      </button>\n    </ion-card-content>\n  </ion-card>\n</ion-content>'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/meeting-details/meeting-details.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_launch_navigator_ngx__["a" /* LaunchNavigator */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2__node_modules_ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */]])
    ], MeetingDetailsPage);
    return MeetingDetailsPage;
}());

//# sourceMappingURL=meeting-details.js.map

/***/ })

});
//# sourceMappingURL=7.js.map