webpackJsonp([2],{

/***/ 785:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VideoOptionsPageModule", function() { return VideoOptionsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__video_options__ = __webpack_require__(804);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var VideoOptionsPageModule = /** @class */ (function () {
    function VideoOptionsPageModule() {
    }
    VideoOptionsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__video_options__["a" /* VideoOptionsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__video_options__["a" /* VideoOptionsPage */]),
                __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__["b" /* TranslateModule */],
            ],
        })
    ], VideoOptionsPageModule);
    return VideoOptionsPageModule;
}());

//# sourceMappingURL=video-options.module.js.map

/***/ }),

/***/ 804:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VideoOptionsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__node_modules_ionic_native_network_ngx__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__node_modules_ionic_native_file_ngx__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__node_modules_ionic_native_file_transfer_ngx__ = __webpack_require__(179);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var VideoOptionsPage = /** @class */ (function () {
    function VideoOptionsPage(navCtrl, navParams, view, modal, network, alertCtrl, translate, file, transfer, zone) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.view = view;
        this.modal = modal;
        this.network = network;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.file = file;
        this.transfer = transfer;
        this.zone = zone;
        this.video = this.navParams.data.video;
        this.isFile = this.navParams.data.isFile;
        this.isDownload = this.navParams.data.isDownload;
        this.doload3g = false;
        this.downloading = false;
        this.fileTransfer = this.transfer.create();
        this.downloadProgress = 0;
        var that = this;
        this.fileTransfer.onProgress(function (progress) {
            that.zone.run(function () {
                that.downloadProgress = parseInt(((progress.loaded / progress.total) * 100).toFixed(2));
            });
        });
    }
    VideoOptionsPage.prototype.ionViewDidLoad = function () {
    };
    VideoOptionsPage.prototype.closeModal = function () {
        this.doload3g = false;
        this.view.dismiss();
    };
    VideoOptionsPage.prototype.openModalVideo = function (download) {
        this.video.offline = false;
        if (download) {
            this.showVideo = false;
            this.isVimeo = false;
            if (this.isDownload) {
                this.downloadFile();
            }
        }
        else {
            this.isVimeo = true;
            var modalOnline = this.modal.create('VideoOnlinePage', { video: this.video });
            modalOnline.present();
        }
    };
    VideoOptionsPage.prototype.downloadFile = function () {
        var _this = this;
        if (this.network.type == 'wifi' || this.doload3g) {
            this.file.getFreeDiskSpace().then(function (space) {
                var sizeFile = _this.video.size / 1000;
                if (space > sizeFile) {
                    _this.downloading = true;
                    _this.fileTransfer.download(_this.video.download, _this.video.targetFile).then(function (res) {
                        var modalOffline = _this.modal.create('VideoOfflinePage', { video: _this.video, download: true });
                        modalOffline.onDidDismiss(function () {
                            _this.closeModal();
                        });
                        modalOffline.present().then(function (res) {
                            _this.downloading = false;
                            _this.downloadProgress = 0;
                        });
                    }, function (error) {
                        _this.downloading = false;
                        var alert = _this
                            .alertCtrl
                            .create({
                            title: _this.translate.instant('ERROR'),
                            subTitle: _this.translate.instant('MEMORY_FULL'),
                            buttons: ['Ok']
                        });
                        alert.present();
                    });
                }
                else {
                    var alert_1 = _this
                        .alertCtrl
                        .create({
                        title: _this.translate.instant('ERROR'),
                        subTitle: _this.translate.instant('MEMORY_FULL'),
                        buttons: ['Ok']
                    });
                    alert_1.present();
                }
            }, function (err) {
                var alert = _this
                    .alertCtrl
                    .create({
                    title: _this.translate.instant('ERROR'),
                    subTitle: _this.translate.instant('MEMORY_FULL'),
                    buttons: ['Ok']
                });
                alert.present();
            });
        }
        else {
            var confirm_1 = this.alertCtrl.create({
                title: this.translate.instant('WIFI_NETWORK_TITLE'),
                message: this.translate.instant('WIFI_NETWORK'),
                buttons: [
                    {
                        text: this.translate.instant('DOWNLOAD_LATER'),
                        handler: function () {
                            _this.view.dismiss();
                        }
                    },
                    {
                        text: this.translate.instant('DOWNLOAD_NOW'),
                        handler: function () {
                            _this.doload3g = true;
                            _this.downloadFile();
                        }
                    }
                ]
            });
            confirm_1.present();
        }
    };
    VideoOptionsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-video-options',template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/video-options/video-options.html"*/'<ion-header>\n  <ion-navbar color="dark">\n    <ion-title>{{video.name}}</ion-title>\n    <ion-buttons end>\n      <button ion-button (click)=\'closeModal()\' class="modal-buttom" *ngIf="!downloading">\n        <ion-icon name="close-circle"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content padding>\n  <div *ngIf="downloading">\n    <h2 class="title-download">{{\'DOWNLOAD_VIDEO\' | translate}}</h2>\n    <div class="progress-outer">\n      <div class="progress-inner" [style.width]="downloadProgress + \'%\'">\n        {{downloadProgress}}%\n      </div>\n    </div>\n  </div>\n\n  <ion-list *ngIf="!downloading">\n    <ion-item *ngIf="!isFile" (click)=\'openModalVideo(false)\'>\n      <ion-icon name="logo-youtube" item-start></ion-icon> {{\'WATCH_ONLINE\' | translate}}\n      <ion-icon name="ios-arrow-forward" item-end></ion-icon>\n    </ion-item>\n    <ion-item *ngIf="isDownload" (click)=\'openModalVideo(true)\'>\n      <ion-icon name="ios-cloud-download-outline" item-start></ion-icon> {{\'WATCH_LATER\' | translate}}\n      <ion-icon name="ios-arrow-forward" item-end></ion-icon>\n    </ion-item>\n    <ion-item *ngIf="isFile" (click)=\'openModalVideo(true)\'>\n      <ion-icon name="logo-youtube" item-start></ion-icon> {{\'WATCH_VIDEO\' | translate}}\n      <ion-icon name="ios-arrow-forward" item-end></ion-icon>\n    </ion-item>\n    <ion-item *ngIf="isAudio" (click)=\'openModalAudio(true,true)\'>\n      <ion-icon name="ios-mic" item-start></ion-icon> {{\'AUDIO_VERSION\' | translate}}\n      <ion-icon name="ios-arrow-forward" item-end></ion-icon>\n    </ion-item>\n  </ion-list>\n</ion-content>'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/video-options/video-options.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */], __WEBPACK_IMPORTED_MODULE_2__node_modules_ionic_native_network_ngx__["a" /* Network */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_4__node_modules_ionic_native_file_ngx__["a" /* File */], __WEBPACK_IMPORTED_MODULE_5__node_modules_ionic_native_file_transfer_ngx__["a" /* FileTransfer */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["N" /* NgZone */]])
    ], VideoOptionsPage);
    return VideoOptionsPage;
}());

//# sourceMappingURL=video-options.js.map

/***/ })

});
//# sourceMappingURL=2.js.map