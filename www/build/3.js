webpackJsonp([3],{

/***/ 784:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VideoOnlinePageModule", function() { return VideoOnlinePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__video_online__ = __webpack_require__(803);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var VideoOnlinePageModule = /** @class */ (function () {
    function VideoOnlinePageModule() {
    }
    VideoOnlinePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__video_online__["a" /* VideoOnlinePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__video_online__["a" /* VideoOnlinePage */]),
            ],
        })
    ], VideoOnlinePageModule);
    return VideoOnlinePageModule;
}());

//# sourceMappingURL=video-online.module.js.map

/***/ }),

/***/ 803:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VideoOnlinePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__node_modules_angular_platform_browser__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_vimeo_service_vimeo_service__ = __webpack_require__(398);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var VideoOnlinePage = /** @class */ (function () {
    function VideoOnlinePage(navCtrl, navParams, sanitized, view, vimeo, events) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.sanitized = sanitized;
        this.view = view;
        this.vimeo = vimeo;
        this.events = events;
        this.video = this.navParams.data.video;
        this.code = this.sanitized.bypassSecurityTrustHtml(this.video.embed);
        this.events.subscribe('endVideo', function (end) {
            if (end) {
                _this.closeModal();
            }
        });
    }
    VideoOnlinePage.prototype.ionViewDidLoad = function () {
        this.vimeo.initPlayer(this.video);
    };
    VideoOnlinePage.prototype.ionViewWillLeave = function () {
        this.vimeo.stopPlayer();
    };
    VideoOnlinePage.prototype.closeModal = function () {
        this.view.dismiss(this.video);
    };
    VideoOnlinePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-video-online',template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/video-online/video-online.html"*/'<ion-header>\n  <ion-navbar color="dark">\n    <ion-title>{{video.name}}</ion-title>\n    <ion-buttons end>\n      <button ion-button (click)=\'closeModal()\' class="modal-buttom">\n        <ion-icon name="close-circle"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <div [innerHTML]="code" id=\'video-embed\'></div>\n</ion-content>\n'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/video-online/video-online.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__node_modules_angular_platform_browser__["c" /* DomSanitizer */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */], __WEBPACK_IMPORTED_MODULE_3__providers_vimeo_service_vimeo_service__["a" /* VimeoServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */]])
    ], VideoOnlinePage);
    return VideoOnlinePage;
}());

//# sourceMappingURL=video-online.js.map

/***/ })

});
//# sourceMappingURL=3.js.map