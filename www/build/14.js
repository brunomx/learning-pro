webpackJsonp([14],{

/***/ 750:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlertModalPageModule", function() { return AlertModalPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__alert_modal__ = __webpack_require__(790);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var AlertModalPageModule = /** @class */ (function () {
    function AlertModalPageModule() {
    }
    AlertModalPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__alert_modal__["a" /* AlertModalPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__alert_modal__["a" /* AlertModalPage */]),
                __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__["b" /* TranslateModule */]
            ],
        })
    ], AlertModalPageModule);
    return AlertModalPageModule;
}());

//# sourceMappingURL=alert-modal.module.js.map

/***/ }),

/***/ 790:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AlertModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_database_database__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__daily_audio_daily_audio__ = __webpack_require__(175);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__daily_daily__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__first_first__ = __webpack_require__(176);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__source_audio_source_audio__ = __webpack_require__(177);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__source_pdf_source_pdf__ = __webpack_require__(178);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__sources_sources__ = __webpack_require__(181);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__meeting_meeting__ = __webpack_require__(182);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var AlertModalPage = /** @class */ (function () {
    function AlertModalPage(navCtrl, navParams, view, service, alertCtrl, translate, database) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.view = view;
        this.service = service;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.database = database;
        this.alert = this.navParams.data.alert;
        if (this.navParams.data.hasOwnProperty('received')) {
            this.received = this.navParams.data.received;
        }
        else {
            this.received = false;
        }
        // this.received = true;
    }
    AlertModalPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.database.getUser().then(function (user) {
            if (user != null) {
                _this.user = user;
                if (_this.alert.data) {
                    if (_this.alert.data.hasOwnProperty('id')) {
                        var params = {
                            user: user,
                            status: 1,
                            notification_id: _this.alert.data.id,
                        };
                        _this.updateNotification(params);
                    }
                }
            }
            else {
                _this.user = null;
            }
        });
    };
    AlertModalPage.prototype.closeModal = function () {
        this.view.dismiss();
    };
    AlertModalPage.prototype.delete = function () {
        this.alert.status = 2;
        var params = {
            user: this.user,
            status: this.alert.status,
            notification_id: this.alert.id,
        };
        this.updateNotification(params);
    };
    AlertModalPage.prototype.updateNotification = function (params) {
        var _this = this;
        this.service.postDataWithToken('notification-status', params).then(function (res) {
            _this.view.dismiss();
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ALERT'),
                subTitle: _this.translate.instant('SUCCESSFULLY_DELETE'),
                buttons: ['Ok']
            });
            alert.present();
        }, function (err) {
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    AlertModalPage.prototype.openData = function () {
        var data = this.alert.data;
        // let data = { item_id: 3, type: '6', source: '5' };
        switch (data.type) {
            case '1':
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__daily_daily__["a" /* DailyPage */], { push: data });
                break;
            case '2':
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__daily_audio_daily_audio__["a" /* DailyAudioPage */], { push: data });
                break;
            case '4':
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__first_first__["a" /* FirstPage */], { push: data });
                break;
            case '5':
                if (data.source == '0') {
                    this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__source_pdf_source_pdf__["a" /* SourcePdfPage */], { push: data });
                }
                else if (data.source == '1') {
                    this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__source_audio_source_audio__["a" /* SourceAudioPage */], { push: data });
                }
                else if (data.source == '2') {
                    this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__sources_sources__["a" /* SourcesPage */], { push: data });
                }
                break;
            case '6':
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__meeting_meeting__["a" /* MeetingPage */], { push: data });
                break;
            default:
                break;
        }
    };
    AlertModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-alert-modal',template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/alert-modal/alert-modal.html"*/'<ion-header>\n  <ion-navbar color="dark">\n    <ion-title>{{ \'ALERT\' | translate }}</ion-title>\n    <ion-buttons end>\n      <button ion-button (click)=\'closeModal()\' class="modal-buttom">\n        <ion-icon name="close-circle"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-list>\n    <ion-item>\n      <h2>{{alert.title}}</h2>\n      <p [innerHTML]="alert.message"></p>\n    </ion-item>\n  </ion-list>\n  <!-- <button ion-button full (click)="openData()" *ngIf="received == true">{{\'SEE_NOW\' | translate}}</button> -->\n  <!-- <button ion-button full (click)="closeModal()" *ngIf="received == true">{{\'SEE_AFTER\' | translate}}</button> -->\n\n  <button ion-button full icon-start (click)="delete()" *ngIf="received == false">\n    <ion-icon name="ios-trash-outline"></ion-icon> {{\'DELETE_ALERTS\' | translate}}\n  </button>\n</ion-content>\n'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/alert-modal/alert-modal.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_4__providers_database_database__["a" /* DatabaseProvider */]])
    ], AlertModalPage);
    return AlertModalPage;
}());

//# sourceMappingURL=alert-modal.js.map

/***/ })

});
//# sourceMappingURL=14.js.map