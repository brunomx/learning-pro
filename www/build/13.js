webpackJsonp([13],{

/***/ 752:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AudioOfflinePageModule", function() { return AudioOfflinePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__audio_offline__ = __webpack_require__(791);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__node_modules_ionic_audio__ = __webpack_require__(393);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var AudioOfflinePageModule = /** @class */ (function () {
    function AudioOfflinePageModule() {
    }
    AudioOfflinePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__audio_offline__["a" /* AudioOfflinePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__audio_offline__["a" /* AudioOfflinePage */]),
                __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__["b" /* TranslateModule */],
                __WEBPACK_IMPORTED_MODULE_4__node_modules_ionic_audio__["d" /* IonicAudioModule */],
            ],
        })
    ], AudioOfflinePageModule);
    return AudioOfflinePageModule;
}());

//# sourceMappingURL=audio-offline.module.js.map

/***/ }),

/***/ 791:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AudioOfflinePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__node_modules_ionic_native_file_ngx__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ionic_native_file_transfer_ngx__ = __webpack_require__(179);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__node_modules_ionic_native_network_ngx__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_device_ngx__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__node_modules_ionic_audio__ = __webpack_require__(393);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var AudioOfflinePage = /** @class */ (function () {
    function AudioOfflinePage(navCtrl, navParams, network, file, zone, translate, alert, view, device, transfer, _audioProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.network = network;
        this.file = file;
        this.zone = zone;
        this.translate = translate;
        this.alert = alert;
        this.view = view;
        this.device = device;
        this.transfer = transfer;
        this._audioProvider = _audioProvider;
        this.audio = this.navParams.data.audio;
        this.downloadStart = this.navParams.data.download;
        this.downloading = true;
        this.fileTransfer = this.transfer.create();
        this.downloadProgress = 0;
        var that = this;
        this.fileTransfer.onProgress(function (progress) {
            that.zone.run(function () {
                that.downloadProgress = parseInt(((progress.loaded / progress.total) * 100).toFixed(2));
            });
        });
        if (this.device.platform == "Android") {
            this.targetPath = this.file.dataDirectory;
        }
        else {
            this.targetPath = this.file.documentsDirectory;
        }
    }
    AudioOfflinePage.prototype.ionViewDidLoad = function () {
        if (this.downloadStart) {
            this.download();
        }
        else {
            this.downloading = false;
            this.audio.targetFile = this.targetPath + this.removerAcentos(this.audio.name) + ".mp3";
            if (this.device.platform != "Android") {
                this.audio.targetFile = this.audio.targetFile.replace(/^file:\/\//, '');
            }
            this.track = {
                src: this.audio.targetFile,
                title: this.audio.name,
                preload: 'metadata'
            };
        }
    };
    AudioOfflinePage.prototype.onTrackFinished = function (track) {
        // console.log('Track finished', track);
    };
    AudioOfflinePage.prototype.download = function () {
        var _this = this;
        if (this.network.type == 'wifi' || this.doload3g) {
            var that_1 = this;
            this.file.getFreeDiskSpace().then(function (space) {
                var sizeFile = that_1.audio.size / 1000;
                if (space > sizeFile) {
                    that_1.downloading = true;
                    that_1.audio.targetFile = that_1.targetPath + that_1.removerAcentos(that_1.audio.name) + ".mp3";
                    that_1.fileTransfer.download(that_1.audio.audio_url, that_1.audio.targetFile).then(function (res) {
                        that_1.downloading = false;
                        that_1.downloadProgress = 0;
                        if (that_1.device.platform != "Android") {
                            that_1.audio.targetFile = that_1.audio.targetFile.replace(/^file:\/\//, '');
                        }
                        that_1.track = {
                            src: that_1.audio.targetFile,
                            title: that_1.audio.name,
                            preload: 'metadata'
                        };
                        that_1.audio.exists = true;
                    }, function (error) {
                        that_1.downloading = false;
                        that_1.downloadProgress = 0;
                        var alert = that_1
                            .alert
                            .create({
                            title: that_1.translate.instant('ERROR'),
                            subTitle: that_1.translate.instant('MEMORY_FULL'),
                            buttons: ['Ok']
                        });
                        alert.present();
                    });
                }
                else {
                    var alert_1 = that_1
                        .alert
                        .create({
                        title: that_1.translate.instant('ERROR'),
                        subTitle: that_1.translate.instant('MEMORY_FULL'),
                        buttons: ['Ok']
                    });
                    alert_1.present();
                }
            }, function (err) {
                var alert = that_1
                    .alert
                    .create({
                    title: that_1.translate.instant('ERROR'),
                    subTitle: that_1.translate.instant('MEMORY_FULL'),
                    buttons: ['Ok']
                });
                alert.present();
            });
        }
        else {
            var confirm_1 = this.alert.create({
                title: this.translate.instant('WIFI_NETWORK_TITLE'),
                message: this.translate.instant('WIFI_NETWORK'),
                buttons: [
                    {
                        text: this.translate.instant('DOWNLOAD_LATER'),
                        handler: function () {
                            _this.view.dismiss();
                        }
                    },
                    {
                        text: this.translate.instant('DOWNLOAD_NOW'),
                        handler: function () {
                            _this.doload3g = true;
                            _this.download();
                        }
                    }
                ]
            });
            confirm_1.present();
        }
    };
    AudioOfflinePage.prototype.closeModal = function () {
        this.doload3g = false;
        this._audioProvider.pause();
        this.view.dismiss(this.audio);
    };
    AudioOfflinePage.prototype.removerAcentos = function (s) {
        var map = { "â": "a", "Â": "A", "à": "a", "À": "A", "á": "a", "Á": "A", "ã": "a", "Ã": "A", "ê": "e", "Ê": "E", "è": "e", "È": "E", "é": "e", "É": "E", "î": "i", "Î": "I", "ì": "i", "Ì": "I", "í": "i", "Í": "I", "õ": "o", "Õ": "O", "ô": "o", "Ô": "O", "ò": "o", "Ò": "O", "ó": "o", "Ó": "O", "ü": "u", "Ü": "U", "û": "u", "Û": "U", "ú": "u", "Ú": "U", "ù": "u", "Ù": "U", "ç": "c", "Ç": "C" };
        return s.replace(/[\W\[\] ]/g, function (a) { return map[a] || a; }).split(' ').join('_');
    };
    AudioOfflinePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-audio-offline',template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/audio-offline/audio-offline.html"*/'<ion-header>\n  <ion-navbar color="dark">\n    <ion-title>{{audio.name}}</ion-title>\n    <ion-buttons end>\n      <button ion-button (click)=\'closeModal()\' class="modal-buttom">\n        <ion-icon name="close-circle"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <div *ngIf="downloading">\n    <h2 class="title-download">{{\'DOWNLOADING\' | translate}}</h2>\n    <div class="progress-outer">\n      <div class="progress-inner" [style.width]="downloadProgress + \'%\'">\n        {{downloadProgress}}%\n      </div>\n    </div>\n  </div>\n\n  <ion-card *ngIf="!downloading">\n    <ion-card-content>\n      <audio-track #audio [track]="track" (onFinish)="onTrackFinished($event)">\n        <ion-item class="controls">\n          <audio-track-play dark [audioTrack]="audio">\n            <ion-spinner></ion-spinner>\n          </audio-track-play>\n        </ion-item>\n        <ion-item>\n          <h2>{{audio.title}}</h2>\n          <audio-track-progress-bar dark duration progress [audioTrack]="audio"></audio-track-progress-bar>\n        </ion-item>\n      </audio-track>\n    </ion-card-content>\n  </ion-card>\n</ion-content>'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/audio-offline/audio-offline.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__node_modules_ionic_native_network_ngx__["a" /* Network */], __WEBPACK_IMPORTED_MODULE_2__node_modules_ionic_native_file_ngx__["a" /* File */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["N" /* NgZone */], __WEBPACK_IMPORTED_MODULE_5__node_modules_ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_device_ngx__["a" /* Device */], __WEBPACK_IMPORTED_MODULE_3__node_modules_ionic_native_file_transfer_ngx__["a" /* FileTransfer */], __WEBPACK_IMPORTED_MODULE_7__node_modules_ionic_audio__["a" /* AudioProvider */]])
    ], AudioOfflinePage);
    return AudioOfflinePage;
}());

//# sourceMappingURL=audio-offline.js.map

/***/ })

});
//# sourceMappingURL=13.js.map