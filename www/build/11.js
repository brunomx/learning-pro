webpackJsonp([11],{

/***/ 754:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactImporterPageModule", function() { return ContactImporterPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contact_importer__ = __webpack_require__(793);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__node_modules_ngx_order_pipe__ = __webpack_require__(394);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var ContactImporterPageModule = /** @class */ (function () {
    function ContactImporterPageModule() {
    }
    ContactImporterPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__contact_importer__["a" /* ContactImporterPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__contact_importer__["a" /* ContactImporterPage */]),
                __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__["b" /* TranslateModule */],
                __WEBPACK_IMPORTED_MODULE_4__node_modules_ngx_order_pipe__["a" /* OrderModule */]
            ],
        })
    ], ContactImporterPageModule);
    return ContactImporterPageModule;
}());

//# sourceMappingURL=contact-importer.module.js.map

/***/ }),

/***/ 793:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactImporterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ionic_native_contacts_ngx__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__node_modules_angular_platform_browser__ = __webpack_require__(48);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ContactImporterPage = /** @class */ (function () {
    function ContactImporterPage(navCtrl, navParams, translate, loadingCtrl, alertCtrl, view, contacts, sanitized) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.translate = translate;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.view = view;
        this.contacts = contacts;
        this.sanitized = sanitized;
        this.filter = ["name.formatted"];
        this.search('');
        this.contactSelected = [];
        this.lastContact = null;
    }
    ContactImporterPage.prototype.ionViewDidLoad = function () {
    };
    ContactImporterPage.prototype.search = function (val) {
        var _this = this;
        this.contacts.find(this.filter, { filter: val, multiple: true, hasPhoneNumber: true }).then(function (conts) {
            conts.sort(function (a, b) {
                if (a.name.formatted < b.name.formatted)
                    return -1;
                if (a.name.formatted > b.name.formatted)
                    return 1;
                return 0;
            });
            _this.contactFound = conts;
        }, function (err) {
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    ContactImporterPage.prototype.remove = function (contact, index) {
        contact.selected = false;
        var contacts = this.contactSelected;
        contacts.splice(index, 1);
        contacts.sort(function (a, b) {
            if (a.name.formatted < b.name.formatted)
                return -1;
            if (a.name.formatted > b.name.formatted)
                return 1;
            return 0;
        });
        this.contactSelected = contacts;
    };
    ContactImporterPage.prototype.select = function (contact) {
        var _this = this;
        if (this.lastContact != null) {
            this.lastContact.selected = false;
        }
        contact.selected = true;
        this.lastContact = contact;
        var confirm = this
            .alertCtrl
            .create({
            title: this.translate.instant('CONFIRM_IMPORT'),
            subTitle: contact.name.formatted,
            buttons: [
                {
                    text: this.translate.instant('TOUCH_CANCEL'),
                    handler: function () {
                        contact.selected = false;
                    }
                },
                {
                    text: this.translate.instant('CONFIRM'),
                    handler: function () {
                        _this.view.dismiss(contact);
                    }
                }
            ]
        });
        confirm.present();
    };
    ContactImporterPage.prototype.sanitizeImage = function (photo) {
        return this.sanitized.bypassSecurityTrustUrl(photo);
    };
    ContactImporterPage.prototype.onKeyUp = function (ev) {
        this.search(ev.target.value);
    };
    ContactImporterPage.prototype.closeModal = function () {
        this.view.dismiss();
    };
    ContactImporterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-contact-importer',template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/contact-importer/contact-importer.html"*/'<ion-header>\n  <ion-navbar color="dark">\n    <ion-searchbar (keyup)="onKeyUp($event)" show-cancel="true" placeholder="{{ \'NAME\' | translate }}"></ion-searchbar>\n    <ion-buttons end>\n      <button ion-button (click)=\'closeModal()\' class="modal-buttom">\n        <ion-icon name="close-circle"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <ion-card>\n    <div *ngFor="let contact of contactFound" (click)="select(contact)">\n      <ion-item *ngIf="contact.phoneNumbers != null" class="item-import">\n        <ion-avatar item-start>\n          <img src="assets/imgs/placeholder-person.png" *ngIf="contact.photos == null">\n          <img [src]="sanitizeImage(contact.photos[0].value)" *ngIf="contact.photos != null">\n        </ion-avatar>\n        <h2>{{contact.name.formatted}}</h2>\n        <p *ngFor="let phone of contact.phoneNumbers"><ion-icon name="ios-call-outline"></ion-icon> {{phone.value}}</p>\n        <p *ngFor="let email of contact.emails"><ion-icon name="ios-mail-outline"></ion-icon> {{email.value}}</p>\n        <ion-icon name="ios-checkmark-outline" item-end *ngIf="contact.selected == true"></ion-icon>\n      </ion-item>\n    </div>\n  </ion-card>\n</ion-content>'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/contact-importer/contact-importer.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__node_modules_ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */], __WEBPACK_IMPORTED_MODULE_3__node_modules_ionic_native_contacts_ngx__["a" /* Contacts */], __WEBPACK_IMPORTED_MODULE_4__node_modules_angular_platform_browser__["c" /* DomSanitizer */]])
    ], ContactImporterPage);
    return ContactImporterPage;
}());

//# sourceMappingURL=contact-importer.js.map

/***/ })

});
//# sourceMappingURL=11.js.map