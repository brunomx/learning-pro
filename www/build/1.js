webpackJsonp([1],{

/***/ 786:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WebinarsPageModule", function() { return WebinarsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__webinars__ = __webpack_require__(805);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var WebinarsPageModule = /** @class */ (function () {
    function WebinarsPageModule() {
    }
    WebinarsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__webinars__["a" /* WebinarsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__webinars__["a" /* WebinarsPage */]),
                __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__["b" /* TranslateModule */]
            ],
        })
    ], WebinarsPageModule);
    return WebinarsPageModule;
}());

//# sourceMappingURL=webinars.module.js.map

/***/ }),

/***/ 805:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WebinarsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_database_database__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_in_app_browser_ngx__ = __webpack_require__(180);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var WebinarsPage = /** @class */ (function () {
    function WebinarsPage(navCtrl, navParams, translate, loadingCtrl, service, alertCtrl, database, browser, modal) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.translate = translate;
        this.loadingCtrl = loadingCtrl;
        this.service = service;
        this.alertCtrl = alertCtrl;
        this.database = database;
        this.browser = browser;
        this.modal = modal;
    }
    WebinarsPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.translate.get("LOADING").subscribe(function (result) {
            _this.loader = _this.loadingCtrl.create({
                content: result,
            });
            _this.loader.present();
            _this.database.getUser().then(function (user) {
                if (user != null) {
                    _this.getData({ user: user });
                }
            });
        });
    };
    WebinarsPage.prototype.getData = function (params) {
        var _this = this;
        this.service.getDataWithToken('webinar', params).then(function (res) {
            _this.loader.dismiss();
            _this.result = res;
            if (_this.result.status == true) {
                _this.videos = _this.result.webinars;
            }
            else {
                var alert_1 = _this
                    .alertCtrl
                    .create({
                    title: _this.translate.instant('ERROR'),
                    subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                    buttons: ['Ok']
                });
                alert_1.present();
            }
        }, function (err) {
            _this.loader.dismiss();
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    WebinarsPage.prototype.open = function (video) {
        if (video.url != null) {
            this.browser.create(video.url, '_blank', 'location=yes');
        }
        else {
            var modalOnline = this.modal.create('VideoOnlinePage', { video: video });
            modalOnline.present();
        }
    };
    WebinarsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-webinars',template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/webinars/webinars.html"*/'<ion-header>\n  <ion-navbar color="dark">\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>{{ \'WEBNEWS\' | translate }}</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <div class="card-background">\n    <ion-card>\n      <img src="assets/imgs/header/webinars.png"/>\n      <div class="card-title">{{ \'WEBNEWS\' | translate }}</div>\n    </ion-card>\n  </div>\n  <ion-card *ngFor="let video of videos" class="content-col">\n    <ion-card-header>\n      <button ion-button icon-left class="button-full" (click)="open(video)">\n        <ion-icon name="ios-play"></ion-icon> {{video.name}}\n      </button>\n    </ion-card-header>\n    <!-- <ion-card-content>\n      <div [innerHTML]="video.link" id=\'video-embed\'></div>\n    </ion-card-content> -->\n  </ion-card>\n</ion-content>'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/webinars/webinars.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__node_modules_ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_4__providers_database_database__["a" /* DatabaseProvider */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_in_app_browser_ngx__["a" /* InAppBrowser */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */]])
    ], WebinarsPage);
    return WebinarsPage;
}());

//# sourceMappingURL=webinars.js.map

/***/ })

});
//# sourceMappingURL=1.js.map