webpackJsonp([40],{

/***/ 10:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(8);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ApiServiceProvider = /** @class */ (function () {
    function ApiServiceProvider(http, events) {
        this.http = http;
        this.events = events;
        this.apiUrl = "http://bigprofy-env.eba-dxdk5gwn.sa-east-1.elasticbeanstalk.com/api/";
        // this.apiUrl = "http://18.231.88.224/api/";
    }
    ApiServiceProvider.prototype.getData = function (type, params) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get(_this.apiUrl + type + "?" + _this.serializeObj(params)).subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    ApiServiceProvider.prototype.getDataWithToken = function (type, params) {
        var _this = this;
        var that = this;
        return new Promise(function (resolve, reject) {
            var token = params.user.token_type + ' ' + params.user.access_token, headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({ 'Accept': 'application/json', 'Authorization': token });
            _this.http.get(_this.apiUrl + type, { headers: headers }).subscribe(function (res) {
                resolve(res.json());
            }, function (err) {
                if (err.status == 401) {
                    that.logout();
                }
                reject(err);
            });
        });
    };
    ApiServiceProvider.prototype.postDataWithToken = function (type, params) {
        var _this = this;
        var that = this;
        return new Promise(function (resolve, reject) {
            var token = params.user.token_type + ' ' + params.user.access_token, headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': token });
            delete params.user;
            _this.http.post(_this.apiUrl + type, JSON.stringify(params), { headers: headers }).subscribe(function (res) {
                resolve(res.json());
            }, function (err) {
                if (err.status == 401) {
                    that.logout();
                }
                reject(err);
            });
        });
    };
    ApiServiceProvider.prototype.postData = function (type, params) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json', 'Accept': 'application/json' });
            _this.http.post(_this.apiUrl + type, JSON.stringify(params), { headers: headers }).subscribe(function (res) {
                resolve(res.json());
            }, function (err) {
                reject(err);
            });
        });
    };
    ApiServiceProvider.prototype.serializeObj = function (obj) {
        var result = [];
        for (var property in obj) {
            result.push(encodeURIComponent(property) + "=" + encodeURIComponent(obj[property]));
        }
        return result.join("&");
    };
    ApiServiceProvider.prototype.logout = function () {
        this.events.publish('logout');
    };
    ApiServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* Events */]])
    ], ApiServiceProvider);
    return ApiServiceProvider;
}());

//# sourceMappingURL=api-service.js.map

/***/ }),

/***/ 100:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DailyPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__node_modules_ionic_native_device_ngx__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__node_modules_ionic_native_file_ngx__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_storage_service_storage_service__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__login_login__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_push_notifications_push_notifications__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_database_database__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var DailyPage = /** @class */ (function () {
    function DailyPage(navCtrl, navParams, viewCtrl, service, loadingCtrl, alertCtrl, translate, device, file, modal, storage, pushNotifications, menuCtrl, database, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.service = service;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.device = device;
        this.file = file;
        this.modal = modal;
        this.storage = storage;
        this.pushNotifications = pushNotifications;
        this.menuCtrl = menuCtrl;
        this.database = database;
        this.events = events;
        this.loader = null;
    }
    DailyPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.database.getUser().then(function (user) {
            _this.loader = _this.loadingCtrl.create({});
            _this.loader.present();
            if (user == null) {
                _this.storage.clear();
                if (_this.loader != null) {
                    _this.loader.dismiss();
                }
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__login_login__["a" /* LoginPage */]);
            }
            else {
                _this.user = user;
                _this.events.publish('afterLogin', user);
                // this.pushNotifications.init(user);
                if (_this.navParams.data.hasOwnProperty('menu') == true) {
                    if (_this.navParams.data.menu == true) {
                        _this.menuCtrl.open();
                    }
                    else {
                        _this.menuClose = true;
                    }
                }
                else {
                    _this.menuClose = true;
                }
                _this.translate.get("LOADING").subscribe(function (result) {
                    if (_this.navParams.data.hasOwnProperty('course_id') == false) {
                        if (_this.viewCtrl.name == 'DailyPage') {
                            _this.course_id = 2;
                        }
                        else if (_this.viewCtrl.name == 'DailyPage') {
                            _this.course_id = 1;
                        }
                    }
                    else {
                        _this.course_id = _this.navParams.data.course_id;
                    }
                    var params = {
                        course_id: _this.course_id,
                        user: _this.user,
                    };
                    _this.getData(params);
                });
            }
        });
    };
    DailyPage.prototype.getData = function (params) {
        var _this = this;
        this.service.getDataWithToken('course/' + params.course_id, params).then(function (res) {
            _this.loader.dismiss();
            _this.result = res;
            if (_this.result.status == true) {
                _this.videos = _this.result.data.videos;
                if (_this.navParams.data.hasOwnProperty('push')) {
                    _this.openPush(_this.navParams.data.push);
                }
            }
            else {
                var alert = _this
                    .alertCtrl
                    .create({
                    title: _this.translate.instant('ERROR'),
                    subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                    buttons: ['Ok']
                });
                alert.present();
            }
        }, function (err) {
            _this.loader.dismiss();
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    DailyPage.prototype.openModalOpcoes = function (video) {
        this.video = video;
        this.thumb = video.thumbnail;
        this.isAudio = video.audio == null ? false : true;
        if (video.url == null) {
            this.isDownload = false;
        }
        else {
            this.isDownload = true;
        }
        if (this.video.watch) {
            if (this.isDownload) {
                this.checkFile();
            }
            else {
                this.openModalVideo();
            }
        }
        else {
            var that_1 = this, alert = this
                .alertCtrl
                .create({
                title: this.translate.instant('OOPS'),
                subTitle: this.translate.instant('OOPS_TEXT'),
                buttons: [
                    {
                        text: this.translate.instant('SEE_AFTER'),
                    },
                    {
                        text: this.translate.instant('SEE_NOW'),
                        handler: function () {
                            that_1.openBefore();
                        }
                    }
                ]
            });
            alert.present();
        }
    };
    DailyPage.prototype.openBefore = function () {
        var _this = this;
        Object.keys(this.videos).forEach(function (key) {
            var item = _this.videos[key];
            if (item.id == _this.video.before_id) {
                _this.openModalOpcoes(item);
            }
        });
    };
    DailyPage.prototype.openPush = function (data) {
        var _this = this;
        this.navParams.data = {};
        Object.keys(this.videos).forEach(function (key) {
            var item = _this.videos[key];
            if (item.id == data.item_id) {
                _this.openModalOpcoes(item);
            }
        });
    };
    DailyPage.prototype.checkFile = function () {
        var _this = this;
        if (this.device.platform == "Android") {
            this.targetPath = this.file.dataDirectory;
        }
        else {
            this.targetPath = this.file.documentsDirectory;
        }
        var fileName = this.video.path + ".mp4";
        this.video.targetFile = this.targetPath + fileName;
        this.file.checkFile(this.targetPath, fileName).then(function (res) {
            var modalOffline = _this.modal.create('VideoOfflinePage', { video: _this.video });
            modalOffline.present();
        }, function (err) {
            _this.isFile = false;
            _this.showEmbed = false;
            var modalOpcoes = _this.modal.create('VideoOptionsPage', { video: _this.video, isFile: _this.isFile, isDownload: _this.isDownload, isAudio: _this.isAudio });
            modalOpcoes.present();
        });
    };
    DailyPage.prototype.openModalVideo = function () {
        var that = this;
        var modalOnline = this.modal.create('VideoOnlinePage', { video: that.video });
        modalOnline.onDidDismiss(function (video) {
            if (video != undefined) {
                that.database.getUser().then(function (user) {
                    if (user != null) {
                        that.translate.get("LOADING").subscribe(function (result) {
                            that.loader = that.loadingCtrl.create({
                                content: result,
                            });
                            that.loader.present();
                            var params = {
                                course_id: that.course_id,
                                user: user,
                            };
                            that.getData(params);
                        });
                    }
                });
            }
        });
        modalOnline.present();
    };
    DailyPage.prototype.removerAcentos = function (s) {
        var map = { "â": "a", "Â": "A", "à": "a", "À": "A", "á": "a", "Á": "A", "ã": "a", "Ã": "A", "ê": "e", "Ê": "E", "è": "e", "È": "E", "é": "e", "É": "E", "î": "i", "Î": "I", "ì": "i", "Ì": "I", "í": "i", "Í": "I", "õ": "o", "Õ": "O", "ô": "o", "Ô": "O", "ò": "o", "Ò": "O", "ó": "o", "Ó": "O", "ü": "u", "Ü": "U", "û": "u", "Û": "U", "ú": "u", "Ú": "U", "ù": "u", "Ù": "U", "ç": "c", "Ç": "C" };
        return s.replace(/[\W\[\] ]/g, function (a) { return map[a] || a; }).split(' ').join('_');
    };
    var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p, _q;
    DailyPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-daily',template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/daily/daily.html"*/'<ion-header>\n  <ion-navbar color="dark">\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>{{\'DAILY_MOTIVATIONAL\' | translate}}</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <div class="card-background">\n    <ion-card>\n      <img src="assets/imgs/header/daily.png"/>\n      <div class="card-title">{{\'DAILY_MOTIVATIONAL\' | translate}}</div>\n    </ion-card>\n  </div>\n  <ion-grid *ngIf="menuClose">\n    <ion-row>\n      <ion-col col-6 *ngFor="let video of videos" class="content-col">\n        <ion-card (click)="openModalOpcoes(video)">\n          <ion-card-content>\n            <img src="assets/imgs/default-img.gif" [lazyLoad]="video.thumbnail" *ngIf="video.thumbnail != null" />\n            <h2>{{video.name}}</h2>\n            <div class="progress-bar">\n              <div class="current" [progress]=\'video.progress\'></div>\n            </div>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>    \n  </ion-grid>\n</ion-content>'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/daily/daily.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__["c" /* TranslateService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__["c" /* TranslateService */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_4__node_modules_ionic_native_device_ngx__["a" /* Device */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__node_modules_ionic_native_device_ngx__["a" /* Device */]) === "function" && _h || Object, typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_5__node_modules_ionic_native_file_ngx__["a" /* File */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__node_modules_ionic_native_file_ngx__["a" /* File */]) === "function" && _j || Object, typeof (_k = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */]) === "function" && _k || Object, typeof (_l = typeof __WEBPACK_IMPORTED_MODULE_6__providers_storage_service_storage_service__["a" /* StorageServiceProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__providers_storage_service_storage_service__["a" /* StorageServiceProvider */]) === "function" && _l || Object, typeof (_m = typeof __WEBPACK_IMPORTED_MODULE_8__providers_push_notifications_push_notifications__["a" /* PushNotificationsProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8__providers_push_notifications_push_notifications__["a" /* PushNotificationsProvider */]) === "function" && _m || Object, typeof (_o = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */]) === "function" && _o || Object, typeof (_p = typeof __WEBPACK_IMPORTED_MODULE_9__providers_database_database__["a" /* DatabaseProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_9__providers_database_database__["a" /* DatabaseProvider */]) === "function" && _p || Object, typeof (_q = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */]) === "function" && _q || Object])
    ], DailyPage);
    return DailyPage;
}());

//# sourceMappingURL=daily.js.map

/***/ }),

/***/ 102:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ionic_native_device_ngx__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__node_modules_angular_forms__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_storage_service_storage_service__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__daily_daily__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__node_modules_ionic_native_fingerprint_aio_ngx__ = __webpack_require__(341);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_database_database__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__daily_audio_daily_audio__ = __webpack_require__(175);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__contacts_contacts__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__first_first__ = __webpack_require__(176);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__disclosure_disclosure__ = __webpack_require__(794);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__sources_sources__ = __webpack_require__(181);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__alerts_alerts__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__meeting_meeting__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__leadership_leadership__ = __webpack_require__(197);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


















var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, modal, translate, formBuilder, loadingCtrl, alertCtrl, service, device, storage, events, platform, faio, database) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modal = modal;
        this.translate = translate;
        this.formBuilder = formBuilder;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.service = service;
        this.device = device;
        this.storage = storage;
        this.events = events;
        this.platform = platform;
        this.faio = faio;
        this.database = database;
        this.formGroup = formBuilder.group({
            username: ['', __WEBPACK_IMPORTED_MODULE_4__node_modules_angular_forms__["f" /* Validators */].required],
            password: ['', __WEBPACK_IMPORTED_MODULE_4__node_modules_angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_4__node_modules_angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_4__node_modules_angular_forms__["f" /* Validators */].minLength(4)])]
        });
        this.username = this.formGroup.controls['username'];
        this.password = this.formGroup.controls['password'];
        this.videoLoad = false;
        this.modules = [
            { component: __WEBPACK_IMPORTED_MODULE_7__daily_daily__["a" /* DailyPage */], params: { course_id: 2 }, route: 'course.video.index/2' },
            { component: __WEBPACK_IMPORTED_MODULE_10__daily_audio_daily_audio__["a" /* DailyAudioPage */], params: {}, route: 'audio.index' },
            { component: __WEBPACK_IMPORTED_MODULE_11__contacts_contacts__["a" /* ContactsPage */], params: {} },
            { component: __WEBPACK_IMPORTED_MODULE_12__first_first__["a" /* FirstPage */], params: { course_id: 1 }, route: 'course.video.index/1' },
            { component: __WEBPACK_IMPORTED_MODULE_13__disclosure_disclosure__["a" /* DisclosurePage */], params: {} },
            { component: __WEBPACK_IMPORTED_MODULE_14__sources_sources__["a" /* SourcesPage */], params: {}, route: 'material.index' },
            { component: __WEBPACK_IMPORTED_MODULE_15__alerts_alerts__["a" /* AlertsPage */], params: {}, route: 'notification.index' },
            { component: __WEBPACK_IMPORTED_MODULE_16__meeting_meeting__["a" /* MeetingPage */], params: {}, route: 'meeting.index' },
            { component: __WEBPACK_IMPORTED_MODULE_17__leadership_leadership__["a" /* LeadershipPage */], params: {}, route: 'qualification.index' },
        ];
    }
    LoginPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.faio.isAvailable().then(function (res) {
                _this.touchAvaliable = true;
                if (_this.storage.get('infusion_id') != null) {
                    _this.touchId = true;
                    _this.touchIdOff = false;
                }
                else {
                    _this.touchId = false;
                    _this.touchIdOff = true;
                }
            }).catch(function (err) {
                _this.touchAvaliable = false;
                _this.touchId = false;
                _this.touchIdOff = true;
            });
            _this.database.getUser().then(function (user) {
                if (user != null) {
                    _this.database.getFirstModule().then(function (module) {
                        if (module == null) {
                            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__daily_daily__["a" /* DailyPage */]);
                        }
                        else {
                            _this.redirect(module);
                        }
                    });
                }
                else {
                    _this.hasSession = false;
                    _this.video = document.getElementById('video');
                    _this.database.getLanguage().then(function (res) {
                        if (res == null) {
                            var modalLanguage = _this.modal.create('LanguagePage', {});
                            modalLanguage.present();
                        }
                    });
                }
            });
        });
    };
    LoginPage.prototype.login = function () {
        var _this = this;
        this.loader = this.loadingCtrl.create();
        this.loader.present();
        var params = {
            email: this.username.value,
            password: this.password.value,
            uuid: this.device.uuid,
        };
        this.service.postData('auth/login', params).then(function (res) {
            _this.loader.dismiss();
            _this.result = res;
            if (_this.result.status == true) {
                _this.database.saveUser(_this.result).then(function (res) {
                    _this.events.publish('afterLogin', _this.result.user);
                    if (_this.result.user.hasOwnProperty('modules')) {
                        Object.keys(_this.result.user.modules).forEach(function (key) {
                            var module = _this.result.user.modules[key];
                            _this.database.saveModule(module);
                            if (_this.result.user.modules.length - 1 == parseInt(key)) {
                                // this.navCtrl.setRoot(DailyPage);
                                _this.redirect(_this.result.user.modules[0]);
                            }
                        });
                    }
                    else {
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__daily_daily__["a" /* DailyPage */]);
                    }
                });
                // this.storage.setObject('distributor', this.result.data);
                // @todo Criar login com touch
                // if (this.touchAvaliable && this.storage.get('infusion_id') == null) {
                //   let that = this,
                //     confirm = this.alertCtrl.create({
                //       title: this.translate.instant('TOUCH_TITLE'),
                //       message: this.translate.instant('TOUCH_TITLE_MESSAGE'),
                //       buttons: [
                //         {
                //           text: this.translate.instant('REGISTER_AFTER'),
                //           handler: () => {
                //             // that.events.publish('afterLogin', that.result.data);
                //             that.navCtrl.setRoot(DailyPage,{menu: true});
                //           }
                //         },
                //         {
                //           text: this.translate.instant('REGISTER_NOW'),
                //           handler: () => {
                //             that.touchRegister(that.result.data);
                //           }
                //         },
                //       ]
                //     });
                //   confirm.present();
                // } else {
                //   // this.events.publish('afterLogin', this.result.data);
                // this.navCtrl.setRoot(DailyPage,{menu: true});
                // }
            }
            else {
                var alert = _this
                    .alertCtrl
                    .create({
                    title: _this.translate.instant('ERROR'),
                    subTitle: _this.translate.instant('INVALID_LOGIN'),
                    buttons: ['Ok']
                });
                alert.present();
            }
        }, function (err) {
            _this.loader.dismiss();
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('INVALID_LOGIN'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    LoginPage.prototype.redirect = function (first) {
        var _this = this;
        Object.keys(this.modules).forEach(function (key) {
            var module = _this.modules[key];
            if (first.param == null) {
                if (module.route == first.route) {
                    _this.navCtrl.setRoot(module.component);
                }
            }
            else {
                if (module.route === first.route + '/' + first.param) {
                    _this.navCtrl.setRoot(module.component, module.param);
                }
            }
        });
    };
    LoginPage.prototype.recovery = function () {
        var modalRecovery = this.modal.create('RecoveryPage');
        modalRecovery.present();
    };
    LoginPage.prototype.register = function () {
        var modalRegister = this.modal.create('RegisterPage');
        modalRegister.present();
    };
    LoginPage.prototype.touchRegister = function (user) {
        var _this = this;
        this.faio.show({
            clientId: 'com.monterest.app',
            clientSecret: this.password.value,
            disableBackup: true,
            localizedFallbackTitle: this.translate.instant('TOUCH_CANCEL'),
            localizedReason: this.translate.instant('TOUCH_MESSAGE2')
        }).then(function (res) {
            _this.events.publish('afterLogin', user);
            _this.storage.set('infusion_id', user.Id);
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__daily_daily__["a" /* DailyPage */], { menu: true });
        }).catch(function (err) {
            _this.events.publish('afterLogin', user);
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__daily_daily__["a" /* DailyPage */], { menu: true });
        });
    };
    LoginPage.prototype.changeLogin = function () {
        this.touchId = false;
        this.touchIdOff = true;
    };
    LoginPage.prototype.touchLogin = function () {
        var _this = this;
        this.loader = this.loadingCtrl.create({
            content: this.translate.instant('AUTHENTICATING'),
        });
        this.loader.present();
        var params = {
            infusion_id: this.storage.get('infusion_id'),
            uuid: this.device.uuid,
        };
        this.faio.show({
            clientId: 'br.com.maquinamultinivel.app',
            clientSecret: 'password',
            disableBackup: true,
            localizedFallbackTitle: this.translate.instant('TOUCH_CANCEL'),
            localizedReason: this.translate.instant('TOUCH_MESSAGE')
        }).then(function (res) {
            _this.service.postData('authenticateTouch', params).then(function (res) {
                _this.loader.dismiss();
                _this.result = res;
                if (_this.result.status == true) {
                    var user = _this.result.user;
                    user.token = _this.result.token_type = " " + _this.result.access_token;
                    // this.storage.setObject('distributor', user);
                    // this.events.publish('afterLogin', this.result.data);
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__daily_daily__["a" /* DailyPage */], { menu: true });
                }
                else {
                    var alert = _this
                        .alertCtrl
                        .create({
                        title: _this.translate.instant('ERROR'),
                        subTitle: _this.translate.instant('INVALID_LOGIN'),
                        buttons: ['Ok']
                    });
                    alert.present();
                }
            }, function (err) {
                _this.loader.dismiss();
                var alert = _this
                    .alertCtrl
                    .create({
                    title: _this.translate.instant('ERROR'),
                    subTitle: _this.translate.instant('INVALID_LOGIN'),
                    buttons: ['Ok']
                });
                alert.present();
            });
        }).catch(function (err) {
            _this.loader.dismiss();
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('INVALID_LOGIN'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p;
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/login/login.html"*/'<ion-content no-bounce slot="fixed">\n  <div *ngIf="hasSession == false">\n    <div class="auth-bg">\n      <span class="r"></span>\n      <span class="r"></span>\n    </div>\n    <div class="logo" *ngIf="touchIdOff || touchId">\n      <img src="assets/imgs/bigprofy.png" />\n    </div>\n    <div id="content-box" padding>\n      <div class="touch-box" *ngIf="touchId">\n        <button color="primary" ion-button icon-left block (click)="touchLogin()">\n          <ion-icon name="ios-finger-print"></ion-icon> {{ \'SIGN_FINGERPRINT\' | translate }}\n        </button>\n        <button color="primary" ion-button icon-left block (click)="changeLogin()">\n          <ion-icon name="ios-key"></ion-icon> {{ \'SIGN_PASSWORD\' | translate }}\n        </button>\n      </div>\n      <div class="box-form" *ngIf="touchIdOff">\n        <form [formGroup]="formGroup" id="formLogin" (ngSubmit)="login()">\n          <ion-list>\n            <ion-item>\n              <ion-icon name="mail-outline" item-left></ion-icon>\n              <ion-input type="text" placeholder="{{ \'EMAIL\' | translate }}" autocomplete="off" autocorrect="off" formControlName="username" [ngClass]="{\'error\': username.invalid && username.touched}"></ion-input>\n            </ion-item>\n            <ion-item>\n              <ion-icon name="key-outline" item-left></ion-icon>\n              <ion-input type="password" placeholder="{{ \'PASSWORD\' | translate }}" autocomplete="off" autocorrect="off" formControlName="password" [ngClass]="{\'error\': password.invalid && username.touched}"></ion-input>\n            </ion-item>\n          </ion-list>\n          <button ion-button color="light" block [disabled]="formGroup.invalid">{{ \'LOGIN\' | translate }}</button>\n        </form>\n        <div class="box-links">\n          <!-- <a href="javascript:;" (click)="recovery()">{{ \'FORGOT\' | translate }}</a> -->\n        </div>\n      </div>\n    </div>\n  </div>\n  </ion-content>'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__node_modules_ngx_translate_core__["c" /* TranslateService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__node_modules_ngx_translate_core__["c" /* TranslateService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__node_modules_angular_forms__["a" /* FormBuilder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__node_modules_angular_forms__["a" /* FormBuilder */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_5__providers_api_service_api_service__["a" /* ApiServiceProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__providers_api_service_api_service__["a" /* ApiServiceProvider */]) === "function" && _h || Object, typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_3__node_modules_ionic_native_device_ngx__["a" /* Device */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__node_modules_ionic_native_device_ngx__["a" /* Device */]) === "function" && _j || Object, typeof (_k = typeof __WEBPACK_IMPORTED_MODULE_6__providers_storage_service_storage_service__["a" /* StorageServiceProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__providers_storage_service_storage_service__["a" /* StorageServiceProvider */]) === "function" && _k || Object, typeof (_l = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */]) === "function" && _l || Object, typeof (_m = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */]) === "function" && _m || Object, typeof (_o = typeof __WEBPACK_IMPORTED_MODULE_8__node_modules_ionic_native_fingerprint_aio_ngx__["a" /* FingerprintAIO */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8__node_modules_ionic_native_fingerprint_aio_ngx__["a" /* FingerprintAIO */]) === "function" && _o || Object, typeof (_p = typeof __WEBPACK_IMPORTED_MODULE_9__providers_database_database__["a" /* DatabaseProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_9__providers_database_database__["a" /* DatabaseProvider */]) === "function" && _p || Object])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 103:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__contact_details_contact_details__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__contacts_complete_contacts_complete__ = __webpack_require__(191);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__contacts_draft_contacts_draft__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_database_database__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ContactsPage = /** @class */ (function () {
    function ContactsPage(navCtrl, navParams, service, loadingCtrl, alertCtrl, translate, events, database) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.events = events;
        this.database = database;
        this.complete = __WEBPACK_IMPORTED_MODULE_5__contacts_complete_contacts_complete__["a" /* ContactsCompletePage */];
        this.draft = __WEBPACK_IMPORTED_MODULE_6__contacts_draft_contacts_draft__["a" /* ContactsDraftPage */];
        this.events.subscribe('reloadContacts', function (data) {
            if (data == true) {
                _this.loadResults(false);
            }
        });
    }
    ContactsPage.prototype.ionViewDidEnter = function () {
        if (this.result != undefined) {
            this.loadResults(false);
        }
    };
    ContactsPage.prototype.ionViewDidLoad = function () {
        this.loadResults(true);
    };
    ContactsPage.prototype.loadResults = function (empty) {
        var _this = this;
        this.translate.get("LOADING").subscribe(function (result) {
            _this.loader = _this.loadingCtrl.create({
                content: result,
            });
            _this.database.getUser().then(function (user) {
                if (user != null) {
                    _this.loader.present();
                    _this.getData(user, empty);
                }
            });
        });
    };
    ContactsPage.prototype.getData = function (user, empty) {
        var _this = this;
        var params = {
            user: user,
        };
        this.service.getDataWithToken('contact', params).then(function (res) {
            _this.loader.dismiss();
            _this.result = res;
            if (_this.result.status == true && _this.result.contacts.length > 0) {
                var contacts_1 = [];
                Object.keys(_this.result.contacts).forEach(function (key) {
                    var item = _this.result.contacts[key];
                    item.stars = _this.countStars(item.stars);
                    contacts_1.push(item);
                });
                _this.navParams.data.contacts = contacts_1;
                _this.events.publish('loadContacts', contacts_1);
            }
            else {
                if (empty) {
                    _this.register();
                }
            }
        }, function (err) {
            _this.events.publish('loadContacts', false);
            _this.loader.dismiss();
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    ContactsPage.prototype.countStars = function (stars) {
        var itens = [];
        for (var i = 0; i < stars; i++) {
            itens.push(i);
        }
        return itens;
    };
    ContactsPage.prototype.register = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__contact_details_contact_details__["a" /* ContactDetailsPage */], {
            contact: null,
        });
    };
    ContactsPage.prototype.edit = function (contact) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__contact_details_contact_details__["a" /* ContactDetailsPage */], {
            contact: contact,
        });
    };
    ContactsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-contacts',template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/contacts/contacts.html"*/'<ion-header>\n  <ion-navbar color="dark">\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-buttons right>\n      <button ion-button icon-only (click)="register()">\n        <ion-icon name="ios-add-circle"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>{{ \'LIST_NAMES\' | translate }}</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-tabs tabsLayout="icon-start">\n  <ion-tab tabIcon="ios-flame" tabTitle="{{ \'LIST_NAMES_HOT\' | translate }}" [root]="complete" [rootParams]="navParams.data"></ion-tab>\n  <ion-tab tabIcon="ios-thermometer" tabTitle="{{ \'LIST_NAMES_COLD\' | translate }}" [root]="draft" [rootParams]="navParams.data"></ion-tab>\n</ion-tabs>'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/contacts/contacts.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_7__providers_database_database__["a" /* DatabaseProvider */]])
    ], ContactsPage);
    return ContactsPage;
}());

//# sourceMappingURL=contacts.js.map

/***/ }),

/***/ 13:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DatabaseProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite_ngx__ = __webpack_require__(254);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_device_ngx__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DatabaseProvider = /** @class */ (function () {
    function DatabaseProvider(http, sqlite, device) {
        this.http = http;
        this.sqlite = sqlite;
        this.device = device;
        this.fetch = function (result) {
            if (result.rows.length > 0) {
                return result.rows.item(0);
            }
            else {
                return null;
            }
        };
        this.dbo = null;
        this.tables = [
            {
                name: 'prefefences',
                columns: [
                    { name: 'id', type: 'integer primary key' },
                    { name: 'language', type: 'text' },
                ]
            },
            {
                name: 'users',
                columns: [
                    { name: 'id', type: 'integer primary key' },
                    { name: 'first_name', type: 'text' },
                    { name: 'last_name', type: 'text' },
                    { name: 'email', type: 'text' },
                    { name: 'birth', type: 'text' },
                    { name: 'gender', type: 'text' },
                    { name: 'rg', type: 'text' },
                    { name: 'cpf', type: 'text' },
                    { name: 'aff_code', type: 'text' },
                    { name: 'aff_id', type: 'integer' },
                    { name: 'access_token', type: 'text' },
                    { name: 'token_type', type: 'text' },
                    { name: 'user_id', type: 'integer' },
                ]
            },
            {
                name: 'devices',
                columns: [
                    { name: 'id', type: 'integer primary key' },
                    { name: 'user_id', type: 'integer' },
                    { name: 'email', type: 'text' },
                    { name: 'ud', type: 'integer' },
                ]
            },
            {
                name: 'modules',
                columns: [
                    { name: 'id', type: 'integer primary key' },
                    { name: 'name', type: 'integer' },
                    { name: 'description', type: 'text' },
                    { name: 'param', type: 'text' },
                    { name: 'route', type: 'text' },
                    { name: 'icon', type: 'text' },
                    { name: 'sequence', type: 'integer' },
                    { name: 'module_id', type: 'integer' },
                ]
            },
        ];
    }
    DatabaseProvider.prototype.query = function (query, bindings) {
        if (bindings === void 0) { bindings = []; }
        try {
            var that_1 = this;
            return new Promise(function (resolve, reject) {
                that_1.dbo.transaction(function (transaction) {
                    transaction.executeSql(query, bindings, function (transaction, result) {
                        return resolve(result);
                    });
                }, reject);
            });
        }
        catch (error) {
            console.log(error);
        }
    };
    DatabaseProvider.prototype.fetchAll = function (result) {
        if (result.rows.length > 0) {
            return result.rows;
        }
        else {
            return null;
        }
    };
    DatabaseProvider.prototype.start = function () {
        var _this = this;
        Object.keys(this.tables).forEach(function (index) {
            var table = _this.tables[index], columns = [];
            Object.keys(table.columns).forEach(function (i) {
                columns.push(table.columns[i].name + ' ' + table.columns[i].type);
            });
            var query = 'CREATE TABLE IF NOT EXISTS ' + table.name + ' (' + columns.join(',') + ')';
            _this.query(query);
        });
    };
    DatabaseProvider.prototype.createDatabase = function () {
        var that = this;
        if (this.dbo == null) {
            if (this.device.platform != null) {
                return new Promise(function (resolve, reject) {
                    that.sqlite.create({
                        name: 'data.db',
                        location: 'default'
                    }).then(function (db) {
                        that.dbo = db;
                        that.start();
                        resolve(db);
                    }).catch(function (e) {
                        reject(e);
                    });
                });
            }
            else {
                return new Promise(function (resolve, reject) {
                    try {
                        that.dbo = window.openDatabase('data.db', '1.0', 'data', 65536);
                        that.start();
                        resolve(that.dbo);
                    }
                    catch (error) {
                        reject(error);
                    }
                });
            }
        }
    };
    DatabaseProvider.prototype.savePreferences = function (item) {
        var query = "", values = [];
        if (item.id == null) {
            query = "INSERT INTO prefefences (language) VALUES (?)";
            values = [item.language];
        }
        else {
            query = "UPDATE prefefences SET language = ? WHERE id = ?";
            values = [item.language, item.id];
        }
        this.query(query, values);
    };
    DatabaseProvider.prototype.saveUser = function (data) {
        var _this = this;
        var query = "", values = [];
        return this.getUserFromUser_id(data.user.id).then(function (user) {
            if (user == null) {
                query = "INSERT INTO users (first_name, last_name, email, birth, gender, rg, cpf, aff_code, aff_id, token_type, access_token, user_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                values = [data.user.first_name, data.user.last_name, data.user.email, data.user.birth, data.user.gender, data.user.rg, data.user.cpf, data.user.username, null, data.token_type, data.access_token, data.user.id];
            }
            else {
                query = "UPDATE users SET first_name = ?, last_name = ?, email = ?, birth = ?, gender = ?, rg = ?, cpf = ?, aff_code = ?, aff_id = ?, access_token = ?, token_type = ?  WHERE id = ?";
                values = [data.user.first_name, data.user.last_name, data.user.email, data.user.birth, data.user.gender, data.user.rg, data.user.cpf, data.user.username, null, data.token_type, data.access_token, user.id];
            }
            return _this.query(query, values);
        });
    };
    DatabaseProvider.prototype.saveModule = function (data) {
        var query = "", values = [];
        query = "INSERT INTO modules (name, description, param, route, icon, sequence, module_id) VALUES (?, ?, ?, ?, ?, ?, ?)";
        values = [data.name, data.description, data.param, data.route, data.icon, data.sequence, data.id];
        return this.query(query, values);
    };
    DatabaseProvider.prototype.getModules = function () {
        var _this = this;
        var that = this, query = "SELECT * FROM modules";
        if (this.dbo == null) {
            return this.createDatabase().then(function (db) {
                return _this.query(query, []).then(function (res) {
                    return that.fetchAll(res);
                });
            });
        }
        else {
            return this.query(query, []).then(function (res) {
                return that.fetchAll(res);
            });
        }
    };
    DatabaseProvider.prototype.getFirstModule = function () {
        var _this = this;
        var that = this, query = "SELECT * FROM modules";
        if (this.dbo == null) {
            return this.createDatabase().then(function (db) {
                return _this.query(query, []).then(function (res) {
                    return that.fetch(res);
                });
            });
        }
        else {
            return this.query(query, []).then(function (res) {
                return that.fetch(res);
            });
        }
    };
    DatabaseProvider.prototype.getUser = function () {
        var _this = this;
        var that = this, query = "SELECT * FROM users";
        if (this.dbo == null) {
            return this.createDatabase().then(function (db) {
                return _this.query(query, []).then(function (res) {
                    return that.fetch(res);
                });
            });
        }
        else {
            return this.query(query, []).then(function (res) {
                return that.fetch(res);
            });
        }
    };
    DatabaseProvider.prototype.deleteUser = function (id) {
        var that = this, query = "DELETE FROM users WHERE id = ?", values = [id];
        return this.query(query, values).then(function (res) {
            that.deleteModules();
            return that.fetch(res);
        });
    };
    DatabaseProvider.prototype.deleteModules = function () {
        var that = this, query = "DELETE FROM modules";
        return this.query(query, []).then(function (res) {
            return that.fetch(res);
        });
    };
    DatabaseProvider.prototype.getUserFromUser_id = function (user_id) {
        var that = this, query = "SELECT id FROM users WHERE user_id = ?", values = [user_id];
        return this.query(query, values).then(function (res) {
            return that.fetch(res);
        });
    };
    DatabaseProvider.prototype.getLanguage = function () {
        var that = this;
        var query = "SELECT id,language FROM prefefences";
        if (this.dbo == null) {
            return this.createDatabase().then(function (db) {
                return that.query(query, []).then(function (res) {
                    return that.fetch(res);
                });
            });
        }
        else {
            return that.query(query, []).then(function (res) {
                return that.fetch(res);
            });
        }
    };
    var _a, _b, _c;
    DatabaseProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClient */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite_ngx__["a" /* SQLite */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite_ngx__["a" /* SQLite */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__ionic_native_device_ngx__["a" /* Device */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__ionic_native_device_ngx__["a" /* Device */]) === "function" && _c || Object])
    ], DatabaseProvider);
    return DatabaseProvider;
}());

//# sourceMappingURL=database.js.map

/***/ }),

/***/ 175:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DailyAudioPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__daily_audio_produto_daily_audio_produto__ = __webpack_require__(183);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__daily_audio_motivacional_daily_audio_motivacional__ = __webpack_require__(184);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_database_database__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var DailyAudioPage = /** @class */ (function () {
    function DailyAudioPage(navCtrl, navParams, service, events, alertCtrl, translate, loading, database) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.events = events;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.loading = loading;
        this.database = database;
        this.produto = __WEBPACK_IMPORTED_MODULE_2__daily_audio_produto_daily_audio_produto__["a" /* DailyAudioProdutoPage */];
        this.motivacional = __WEBPACK_IMPORTED_MODULE_3__daily_audio_motivacional_daily_audio_motivacional__["a" /* DailyAudioMotivacionalPage */];
    }
    DailyAudioPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.translate.get("LOADING").subscribe(function (result) {
            _this.loader = _this.loading.create({
                content: result,
            });
            _this.loader.present();
            _this.database.getUser().then(function (user) {
                if (user != null) {
                    _this.getData({ user: user });
                }
            });
        });
    };
    DailyAudioPage.prototype.getData = function (params) {
        var _this = this;
        this.service.getDataWithToken('audio', params).then(function (res) {
            _this.loader.dismiss();
            _this.result = res;
            if (_this.result.status == true) {
                _this.navParams.data.audios = _this.result.audios;
                _this.events.publish('loadAudios', _this.result.audios);
            }
            else {
                var alert_1 = _this
                    .alertCtrl
                    .create({
                    title: _this.translate.instant('ERROR'),
                    subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                    buttons: ['Ok']
                });
                alert_1.present();
            }
        }, function (err) {
            _this.loader.dismiss();
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    DailyAudioPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-daily-audio',template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/daily-audio/daily-audio.html"*/'<ion-header>\n  <ion-navbar color="dark">\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>{{ \'DAILY_AUDIO\' | translate }}</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-tabs tabsLayout="icon-start">\n  <ion-tab tabIcon="ios-cart" tabTitle="{{ \'DAILY_AUDIO_PRODUCTS\' | translate }}" [root]="produto" [rootParams]="navParams.data"></ion-tab>\n  <ion-tab tabIcon="ios-body" tabTitle="{{ \'DAILY_AUDIO_MOTIVATIONAL\' | translate }}" [root]="motivacional" [rootParams]="navParams.data"></ion-tab>\n</ion-tabs>'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/daily-audio/daily-audio.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_5__node_modules_ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_6__providers_database_database__["a" /* DatabaseProvider */]])
    ], DailyAudioPage);
    return DailyAudioPage;
}());

//# sourceMappingURL=daily-audio.js.map

/***/ }),

/***/ 176:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FirstPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_device_ngx__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ionic_native_file_ngx__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_database_database__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var FirstPage = /** @class */ (function () {
    function FirstPage(navCtrl, navParams, viewCtrl, service, loadingCtrl, alertCtrl, translate, device, file, modal, database) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.service = service;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.device = device;
        this.file = file;
        this.modal = modal;
        this.database = database;
        this.loader = null;
    }
    FirstPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.database.getUser().then(function (user) {
            if (user != null) {
                _this.translate.get("LOADING").subscribe(function (result) {
                    _this.loader = _this.loadingCtrl.create({
                        content: result,
                    });
                    _this.loader.present();
                });
                if (_this.navParams.data.hasOwnProperty('course_id') == false) {
                    if (_this.viewCtrl.name == 'DailyPage') {
                        _this.course_id = 2;
                    }
                    else if (_this.viewCtrl.name == 'FirstPage') {
                        _this.course_id = 1;
                    }
                }
                else {
                    _this.course_id = _this.navParams.data.course_id;
                }
                var params = {
                    course_id: _this.course_id,
                    user: user,
                };
                _this.getData(params);
            }
        });
    };
    FirstPage.prototype.getData = function (params) {
        var _this = this;
        this.service.getDataWithToken('course/' + params.course_id, params).then(function (res) {
            if (_this.loader != null) {
                _this.loader.dismiss();
            }
            _this.result = res;
            if (_this.result.status == true) {
                _this.videos = _this.result.data.videos;
                if (_this.navParams.data.hasOwnProperty('push')) {
                    _this.openPush(_this.navParams.data.push);
                }
            }
            else {
                var alert_1 = _this
                    .alertCtrl
                    .create({
                    title: _this.translate.instant('ERROR'),
                    subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                    buttons: ['Ok']
                });
                alert_1.present();
            }
        }, function (err) {
            if (_this.loader != null) {
                _this.loader.dismiss();
            }
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    FirstPage.prototype.openPush = function (data) {
        var _this = this;
        this.navParams.data = {};
        Object.keys(this.videos).forEach(function (key) {
            var item = _this.videos[key];
            if (item.id == data.item_id) {
                _this.openModalOpcoes(item);
            }
        });
    };
    FirstPage.prototype.openModalOpcoes = function (video) {
        this.video = video;
        this.thumb = video.thumbnail;
        this.isAudio = video.audio == null ? false : true;
        if (video.url == null) {
            this.isDownload = false;
        }
        else {
            this.isDownload = true;
        }
        if (this.video.watch) {
            if (this.isDownload) {
                this.checkFile();
            }
            else {
                this.openModalVideo();
            }
        }
        else {
            var that_1 = this, alert_2 = this
                .alertCtrl
                .create({
                title: this.translate.instant('OOPS'),
                subTitle: this.translate.instant('OOPS_TEXT'),
                buttons: [
                    {
                        text: this.translate.instant('SEE_AFTER'),
                    },
                    {
                        text: this.translate.instant('SEE_NOW'),
                        handler: function () {
                            that_1.openBefore();
                        }
                    }
                ]
            });
            alert_2.present();
        }
    };
    FirstPage.prototype.openBefore = function () {
        var _this = this;
        Object.keys(this.videos).forEach(function (key) {
            var item = _this.videos[key];
            if (item.id == _this.video.before_id) {
                _this.openModalOpcoes(item);
            }
        });
    };
    FirstPage.prototype.checkFile = function () {
        var _this = this;
        if (this.device.platform == "Android") {
            this.targetPath = this.file.dataDirectory;
        }
        else {
            this.targetPath = this.file.documentsDirectory;
        }
        var fileName = this.video.path + ".mp4";
        this.video.targetFile = this.targetPath + fileName;
        this.file.checkFile(this.targetPath, fileName).then(function (res) {
            var modalOffline = _this.modal.create('VideoOfflinePage', { video: _this.video });
            modalOffline.present();
        }, function (err) {
            _this.isFile = false;
            _this.showEmbed = false;
            var modalOpcoes = _this.modal.create('VideoOptionsPage', { video: _this.video, isFile: _this.isFile, isDownload: _this.isDownload, isAudio: _this.isAudio });
            modalOpcoes.present();
        });
    };
    FirstPage.prototype.openModalVideo = function () {
        var that = this;
        var modalOnline = this.modal.create('VideoOnlinePage', { video: that.video });
        modalOnline.onDidDismiss(function (video) {
            if (video != undefined) {
                that.database.getUser().then(function (user) {
                    if (user != null) {
                        that.translate.get("LOADING").subscribe(function (result) {
                            that.loader = that.loadingCtrl.create({
                                content: result,
                            });
                            that.loader.present();
                            var params = {
                                course_id: that.course_id,
                                user: user,
                            };
                            that.getData(params);
                        });
                    }
                });
            }
        });
        modalOnline.present();
    };
    FirstPage.prototype.removerAcentos = function (s) {
        var map = { "â": "a", "Â": "A", "à": "a", "À": "A", "á": "a", "Á": "A", "ã": "a", "Ã": "A", "ê": "e", "Ê": "E", "è": "e", "È": "E", "é": "e", "É": "E", "î": "i", "Î": "I", "ì": "i", "Ì": "I", "í": "i", "Í": "I", "õ": "o", "Õ": "O", "ô": "o", "Ô": "O", "ò": "o", "Ò": "O", "ó": "o", "Ó": "O", "ü": "u", "Ü": "U", "û": "u", "Û": "U", "ú": "u", "Ú": "U", "ù": "u", "Ù": "U", "ç": "c", "Ç": "C" };
        return s.replace(/[\W\[\] ]/g, function (a) { return map[a] || a; }).split(' ').join('_');
    };
    FirstPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-first',template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/first/first.html"*/'<ion-header>\n  <ion-navbar color="dark">\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>{{ \'TOOLS\' | translate }}</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <div class="card-background">\n    <ion-card>\n      <img src="assets/imgs/header/first.png"/>\n      <div class="card-title">{{ \'TOOLS\' | translate }}</div>\n    </ion-card>\n  </div>\n  <ion-grid>\n    <ion-row>\n      <ion-col col-6 *ngFor="let video of videos" class="content-col" (click)="openModalOpcoes(video)">\n        <ion-card>\n          <ion-card-content>\n            <img src="assets/imgs/default-img.gif" [lazyLoad]="video.thumbnail" *ngIf="video.thumbnail != null" offset="100" />\n            <h2>{{video.name}}</h2>\n            <div class="progress-bar">\n              <div class="current" [progress]=\'video.progress\'></div>\n            </div>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>    \n  </ion-grid>\n</ion-content>'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/first/first.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */], __WEBPACK_IMPORTED_MODULE_4__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_5__node_modules_ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_device_ngx__["a" /* Device */], __WEBPACK_IMPORTED_MODULE_3__node_modules_ionic_native_file_ngx__["a" /* File */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */], __WEBPACK_IMPORTED_MODULE_6__providers_database_database__["a" /* DatabaseProvider */]])
    ], FirstPage);
    return FirstPage;
}());

//# sourceMappingURL=first.js.map

/***/ }),

/***/ 177:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SourceAudioPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__node_modules_ionic_native_device_ngx__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ionic_native_file_ngx__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_database_database__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var SourceAudioPage = /** @class */ (function () {
    function SourceAudioPage(navCtrl, navParams, service, alertCtrl, translate, loading, device, file, modal, view, database) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.loading = loading;
        this.device = device;
        this.file = file;
        this.modal = modal;
        this.view = view;
        this.database = database;
        this.groups = [];
        this.extensao = '.mp3';
        if (this.device.platform == "Android") {
            this.targetPath = this.file.dataDirectory;
        }
        else {
            this.targetPath = this.file.documentsDirectory;
        }
    }
    SourceAudioPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        if (this.navParams.data.hasOwnProperty('souces') == false) {
            this.translate.get("LOADING").subscribe(function (result) {
                _this.loader = _this.loading.create({
                    content: result,
                });
                _this.loader.present();
                _this.database.getUser().then(function (user) {
                    if (user != null) {
                        _this.getData({ user: user });
                    }
                });
            });
        }
        else {
            this.checkAudio(this.navParams.data.souces);
        }
    };
    SourceAudioPage.prototype.getData = function (params) {
        var _this = this;
        this.service.getDataWithToken('material', params).then(function (res) {
            _this.loader.dismiss();
            _this.result = res;
            if (_this.result.status == true) {
                _this.checkAudio(_this.result.materials);
            }
            else {
                var alert_1 = _this
                    .alertCtrl
                    .create({
                    title: _this.translate.instant('ERROR'),
                    subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                    buttons: ['Ok']
                });
                alert_1.present();
            }
        }, function (err) {
            _this.loader.dismiss();
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    SourceAudioPage.prototype.checkAudio = function (audios) {
        var _this = this;
        var elements = [], that = this;
        this.files = [];
        Object.keys(audios).forEach(function (key) {
            var item = audios[key];
            if (item.hasOwnProperty('type')) {
                if (item.type == 1) {
                    _this.files.push(item);
                    if (_this.device.platform != null) {
                        _this.checkFile(item);
                    }
                    if (item.category_id != null) {
                        that.populate(item);
                    }
                    else {
                        elements.push(item);
                    }
                }
            }
        });
        if (this.navParams.data.hasOwnProperty('push')) {
            if (this.navParams.data.push.source == '1') {
                this.openPush(this.navParams.data.push);
            }
        }
        this.audios = elements;
        this.order();
    };
    SourceAudioPage.prototype.openPush = function (data) {
        var _this = this;
        this.navParams.data = {};
        Object.keys(this.files).forEach(function (key) {
            var item = _this.files[key];
            if (item.id == data.item_id) {
                _this.open(item);
            }
        });
    };
    SourceAudioPage.prototype.populate = function (item) {
        if (this.groups[item.category_id] != undefined) {
            this.groups[item.category_id].itens.push(item);
        }
        else {
            this.groups[item.category_id] = { name: item.category_name, itens: [] };
            this.groups[item.category_id].itens.push(item);
        }
    };
    SourceAudioPage.prototype.order = function () {
        var _this = this;
        var orderGroup = [], that = this;
        Object.keys(this.groups).forEach(function (key) {
            if (_this.groups[key].hasOwnProperty('itens')) {
                orderGroup.push(_this.groups[key]);
            }
        });
        that.categories = orderGroup.reverse();
    };
    SourceAudioPage.prototype.checkFile = function (item) {
        var fileName = this.removerAcentos(item.name) + this.extensao;
        this.file.checkFile(this.targetPath, fileName).then(function (res) {
            item.exists = true;
        }, function (err) {
            item.exists = false;
        });
    };
    SourceAudioPage.prototype.open = function (audio) {
        var modal = this.modal.create('AudioOnlinePage', { audio: audio });
        modal.present();
    };
    SourceAudioPage.prototype.download = function (audio) {
        var modal = this.modal.create('AudioOfflinePage', { audio: audio, download: true });
        modal.onDidDismiss(function (res) {
            audio = res;
        });
        modal.present();
    };
    SourceAudioPage.prototype.openLocal = function (audio) {
        var modal = this.modal.create('AudioOfflinePage', { audio: audio, download: false });
        modal.present();
    };
    SourceAudioPage.prototype.delete = function (audio) {
        var _this = this;
        var fileName = this.removerAcentos(audio.name) + this.extensao;
        this.file.removeFile(this.targetPath, fileName).then(function (res) {
            audio.exists = false;
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('FILE'),
                subTitle: _this.translate.instant('DELETED'),
                buttons: ['Ok']
            });
            alert.present().then(function (res) {
                _this.view.dismiss();
            });
        }, function (err) {
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('REMOVE_VIDEO_TITLE'),
                subTitle: _this.translate.instant('REMOVE_VIDEO'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    SourceAudioPage.prototype.removerAcentos = function (s) {
        var map = { "â": "a", "Â": "A", "à": "a", "À": "A", "á": "a", "Á": "A", "ã": "a", "Ã": "A", "ê": "e", "Ê": "E", "è": "e", "È": "E", "é": "e", "É": "E", "î": "i", "Î": "I", "ì": "i", "Ì": "I", "í": "i", "Í": "I", "õ": "o", "Õ": "O", "ô": "o", "Ô": "O", "ò": "o", "Ò": "O", "ó": "o", "Ó": "O", "ü": "u", "Ü": "U", "û": "u", "Û": "U", "ú": "u", "Ú": "U", "ù": "u", "Ù": "U", "ç": "c", "Ç": "C" };
        return s.replace(/[\W\[\] ]/g, function (a) { return map[a] || a; }).split(' ').join('_');
    };
    SourceAudioPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-source-audio',template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/source-audio/source-audio.html"*/'<ion-header>\n  <ion-navbar color="dark">\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title></ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <div class="card-background">\n    <ion-card>\n      <img src="assets/imgs/header/sources.png"/>\n      <div class="card-title">{{ \'AUDIO\' | translate }}</div>\n    </ion-card>\n  </div>\n  <ion-card *ngFor="let group of categories" class="card-categories">\n    <ion-card-header>{{group.name}}</ion-card-header>\n    <ion-card-content>\n      <ion-list>\n        <ion-item *ngFor="let item of group.itens">\n          <ion-thumbnail item-start>\n            <ion-icon name="ios-mic"></ion-icon>\n          </ion-thumbnail>\n          <h2>{{item.name}}</h2>\n          <div class="category-buttons">\n            <button ion-button icon-start (click)="open(item)" *ngIf="!item.exists">\n              <ion-icon name="ios-play"></ion-icon> {{\'LISTEN\' | translate}}\n            </button>\n            <button ion-button icon-start (click)="download(item)" *ngIf="!item.exists">\n              <ion-icon name="ios-cloud-download"></ion-icon> {{\'DOWNLOAD\' | translate}}\n            </button>\n            <button ion-button icon-start (click)="openLocal(item)" *ngIf="item.exists">\n              <ion-icon name="ios-play"></ion-icon> {{\'LISTEN\' | translate}}\n            </button>\n            <button ion-button icon-start (click)="delete(item)" *ngIf="item.exists">\n              <ion-icon name="ios-trash-outline"></ion-icon> {{\'DELETE\' | translate}}\n            </button>\n          </div>\n        </ion-item>\n      </ion-list>\n    </ion-card-content>\n  </ion-card>\n  <ion-card *ngFor="let audio of audios" class="content-col">\n    <ion-card-header>{{audio.name}}</ion-card-header>\n    <ion-card-content>\n      <ion-icon name="ios-mic"></ion-icon>\n    </ion-card-content>\n    <ion-row class="cardfooter">\n      <ion-col *ngIf="!audio.exists">\n        <button ion-button full icon-start (click)="open(audio)">\n          <ion-icon name="ios-play"></ion-icon> {{\'LISTEN\' | translate}}\n        </button>\n      </ion-col>\n      <ion-col *ngIf="!audio.exists">\n        <button ion-button full icon-start (click)="download(audio)">\n          <ion-icon name="ios-cloud-download"></ion-icon> {{\'DOWNLOAD\' | translate}}\n        </button>\n      </ion-col>\n      <ion-col *ngIf="audio.exists">\n        <button ion-button full icon-start (click)="openLocal(audio)">\n          <ion-icon name="ios-play"></ion-icon> {{\'LISTEN\' | translate}}\n        </button>\n      </ion-col>\n      <ion-col *ngIf="audio.exists">\n        <button ion-button full icon-start (click)="delete(audio)">\n          <ion-icon name="ios-trash-outline"></ion-icon> {{\'DELETE\' | translate}}\n        </button>\n      </ion-col>\n    </ion-row>\n  </ion-card>\n</ion-content>'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/source-audio/source-audio.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_5__node_modules_ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_2__node_modules_ionic_native_device_ngx__["a" /* Device */], __WEBPACK_IMPORTED_MODULE_3__node_modules_ionic_native_file_ngx__["a" /* File */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */], __WEBPACK_IMPORTED_MODULE_6__providers_database_database__["a" /* DatabaseProvider */]])
    ], SourceAudioPage);
    return SourceAudioPage;
}());

//# sourceMappingURL=source-audio.js.map

/***/ }),

/***/ 178:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SourcePdfPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__node_modules_ionic_native_device_ngx__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__node_modules_ionic_native_network_ngx__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__node_modules_ionic_native_document_viewer_ngx__ = __webpack_require__(343);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__node_modules_ionic_native_file_ngx__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__node_modules_ionic_native_file_transfer_ngx__ = __webpack_require__(179);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_in_app_browser_ngx__ = __webpack_require__(180);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_database_database__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_social_sharing_ngx__ = __webpack_require__(53);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var SourcePdfPage = /** @class */ (function () {
    function SourcePdfPage(navCtrl, navParams, service, alertCtrl, translate, loading, device, file, document, network, transfer, alert, view, browser, database, platform, socialSharing) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.loading = loading;
        this.device = device;
        this.file = file;
        this.document = document;
        this.network = network;
        this.transfer = transfer;
        this.alert = alert;
        this.view = view;
        this.browser = browser;
        this.database = database;
        this.platform = platform;
        this.socialSharing = socialSharing;
        this.groups = [];
        this.extensao = '.pdf';
        this.fileTransfer = this.transfer.create();
        this.duration = null;
        var that = this;
        this.fileTransfer.onProgress(function (progress) {
            if (that.duration == null) {
                that.duration = progress.total;
                that.loader = that.loading.create({
                    content: that.translate.instant('DOWNLOADING')
                });
                that.loader.present();
            }
        });
        if (this.device.platform == "Android") {
            this.targetPath = this.file.dataDirectory;
        }
        else {
            this.targetPath = this.file.documentsDirectory;
        }
    }
    SourcePdfPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        if (this.navParams.data.hasOwnProperty('souces') == false) {
            this.translate.get("LOADING").subscribe(function (result) {
                _this.loader = _this.loading.create({
                    content: result,
                });
                _this.loader.present();
                _this.database.getUser().then(function (user) {
                    if (user != null) {
                        _this.user = user;
                        _this.getData({ user: user });
                    }
                });
            });
        }
        else {
            this.database.getUser().then(function (user) {
                if (user != null) {
                    _this.user = user;
                    _this.checkPdf(_this.navParams.data.souces);
                }
            });
        }
    };
    SourcePdfPage.prototype.getData = function (params) {
        var _this = this;
        this.service.getDataWithToken('material', params).then(function (res) {
            _this.loader.dismiss();
            _this.result = res;
            if (_this.result.status == true) {
                _this.checkPdf(_this.result.materials);
            }
            else {
                var alert_1 = _this
                    .alertCtrl
                    .create({
                    title: _this.translate.instant('ERROR'),
                    subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                    buttons: ['Ok']
                });
                alert_1.present();
            }
        }, function (err) {
            _this.loader.dismiss();
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    SourcePdfPage.prototype.checkPdf = function (files) {
        var _this = this;
        var elements = [], that = this;
        this.files = [];
        Object.keys(files).forEach(function (key) {
            var item = files[key];
            if (item.hasOwnProperty('type')) {
                if (item.type == 0) {
                    _this.files.push(item);
                    if (_this.device.platform != null) {
                        _this.checkFile(item);
                    }
                    if (item.category_id != null) {
                        that.populate(item);
                    }
                    else {
                        elements.push(item);
                    }
                }
            }
        });
        if (this.navParams.data.hasOwnProperty('push')) {
            if (this.navParams.data.push.source == '0') {
                this.openPush(this.navParams.data.push);
            }
        }
        this.pdfs = elements;
        this.order();
    };
    SourcePdfPage.prototype.openPush = function (data) {
        var _this = this;
        this.navParams.data = {};
        Object.keys(this.files).forEach(function (key) {
            var item = _this.files[key];
            if (item.id == data.item_id) {
                _this.open(item);
            }
        });
    };
    SourcePdfPage.prototype.populate = function (item) {
        if (this.groups[item.category_id] != undefined) {
            this.groups[item.category_id].itens.push(item);
        }
        else {
            this.groups[item.category_id] = { name: item.category_name, itens: [] };
            this.groups[item.category_id].itens.push(item);
        }
    };
    SourcePdfPage.prototype.order = function () {
        var _this = this;
        var orderGroup = [], that = this;
        Object.keys(this.groups).forEach(function (key) {
            if (_this.groups[key].hasOwnProperty('itens')) {
                orderGroup.push(_this.groups[key]);
            }
        });
        that.categories = orderGroup.reverse();
    };
    SourcePdfPage.prototype.checkFile = function (item) {
        var fileName = this.removerAcentos(item.name) + this.extensao;
        this.file.checkFile(this.targetPath, fileName).then(function (res) {
            item.exists = true;
        }, function (err) {
            item.exists = false;
        });
    };
    SourcePdfPage.prototype.open = function (pdf) {
        this.browser.create(pdf.path, '_blank', 'location=yes');
    };
    SourcePdfPage.prototype.openLocal = function (pdf) {
        pdf.targetFile = this.targetPath + this.removerAcentos(pdf.name) + this.extensao;
        if (this.device.platform != "Android") {
            pdf.targetFile = pdf.targetFile.replace(/^file:\/\//, '');
        }
        var options = {
            title: pdf.name
        };
        this.document.viewDocument(pdf.targetFile, 'application/pdf', options);
    };
    SourcePdfPage.prototype.delete = function (pdf) {
        var _this = this;
        var fileName = this.removerAcentos(pdf.name) + this.extensao;
        this.file.removeFile(this.targetPath, fileName).then(function (res) {
            pdf.exists = false;
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('FILE'),
                subTitle: _this.translate.instant('DELETED'),
                buttons: ['Ok']
            });
            alert.present().then(function (res) {
                _this.view.dismiss();
            });
        }, function (err) {
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('REMOVE_VIDEO_TITLE'),
                subTitle: _this.translate.instant('REMOVE_VIDEO'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    SourcePdfPage.prototype.download = function (pdf) {
        var _this = this;
        if (this.network.type == 'wifi' || this.doload3g) {
            var that_1 = this;
            this.file.getFreeDiskSpace().then(function (space) {
                var sizeFile = pdf.size / 1000;
                if (space > sizeFile) {
                    pdf.targetFile = that_1.targetPath + that_1.removerAcentos(pdf.name) + that_1.extensao;
                    that_1.fileTransfer.download(pdf.path, pdf.targetFile).then(function (res) {
                        that_1.duration = null;
                        that_1.loader.dismiss();
                        pdf.exists = true;
                        that_1.openLocal(pdf);
                    }, function (error) {
                        that_1.duration = null;
                        that_1.loader.dismiss();
                        var alert = that_1
                            .alert
                            .create({
                            title: that_1.translate.instant('ERROR'),
                            subTitle: that_1.translate.instant('MEMORY_FULL'),
                            buttons: ['Ok']
                        });
                        alert.present();
                    });
                }
                else {
                    var alert_2 = that_1
                        .alert
                        .create({
                        title: that_1.translate.instant('ERROR'),
                        subTitle: that_1.translate.instant('MEMORY_FULL'),
                        buttons: ['Ok']
                    });
                    alert_2.present();
                }
            }, function (err) {
                var alert = that_1
                    .alert
                    .create({
                    title: that_1.translate.instant('ERROR'),
                    subTitle: that_1.translate.instant('MEMORY_FULL'),
                    buttons: ['Ok']
                });
                alert.present();
            });
        }
        else {
            var confirm_1 = this.alert.create({
                title: this.translate.instant('WIFI_NETWORK_TITLE'),
                message: this.translate.instant('WIFI_NETWORK'),
                buttons: [
                    {
                        text: this.translate.instant('DOWNLOAD_LATER'),
                        handler: function () {
                            _this.view.dismiss();
                        }
                    },
                    {
                        text: this.translate.instant('DOWNLOAD_NOW'),
                        handler: function () {
                            _this.doload3g = true;
                            _this.download(pdf);
                        }
                    }
                ]
            });
            confirm_1.present();
        }
    };
    SourcePdfPage.prototype.sendEmail = function (pdf) {
        var _this = this;
        this.translate.get("LOADING").subscribe(function (result) {
            _this.loader = _this.loading.create({
                content: result,
            });
            _this.loader.present();
        });
        this.socialSharing.shareViaEmail('Conteúdo em anexo disponibilizado pelo app BigProfy', 'BigProfy - ' + pdf.name, [this.user.email], null, null, pdf.path).then(function (res) {
            _this.loader.dismiss();
        }).catch(function (err) {
            _this.loader.dismiss();
        });
    };
    SourcePdfPage.prototype.removerAcentos = function (s) {
        var map = { "â": "a", "Â": "A", "à": "a", "À": "A", "á": "a", "Á": "A", "ã": "a", "Ã": "A", "ê": "e", "Ê": "E", "è": "e", "È": "E", "é": "e", "É": "E", "î": "i", "Î": "I", "ì": "i", "Ì": "I", "í": "i", "Í": "I", "õ": "o", "Õ": "O", "ô": "o", "Ô": "O", "ò": "o", "Ò": "O", "ó": "o", "Ó": "O", "ü": "u", "Ü": "U", "û": "u", "Û": "U", "ú": "u", "Ú": "U", "ù": "u", "Ù": "U", "ç": "c", "Ç": "C" };
        return s.replace(/[\W\[\] ]/g, function (a) { return map[a] || a; }).split(' ').join('_');
    };
    SourcePdfPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-source-pdf',template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/source-pdf/source-pdf.html"*/'<ion-header>\n  <ion-navbar color="dark">\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title></ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <div class="card-background">\n    <ion-card>\n      <img src="assets/imgs/header/sources.png"/>\n      <div class="card-title">{{ \'PDF\' | translate }}</div>\n    </ion-card>\n  </div>\n  <ion-card *ngFor="let group of categories" class="card-categories">\n    <ion-card-header>{{group.name}}</ion-card-header>\n    <ion-card-content>\n      <ion-list>\n        <ion-item *ngFor="let item of group.itens">\n          <ion-thumbnail item-start>\n            <ion-icon name="ios-document"></ion-icon>\n          </ion-thumbnail>\n          <h2>{{item.name}}</h2>\n          <div class="category-buttons">\n            <button ion-button icon-start (click)="open(item)" *ngIf="!item.exists">\n              <ion-icon name="ios-play"></ion-icon> {{\'TO_VIEW\' | translate}}\n            </button>\n            <button ion-button icon-start (click)="download(item)" *ngIf="!item.exists">\n              <ion-icon name="ios-cloud-download"></ion-icon> {{\'DOWNLOAD\' | translate}}\n            </button>\n            <button ion-button icon-start (click)="openLocal(item)" *ngIf="item.exists">\n              <ion-icon name="ios-play"></ion-icon> {{\'OPEN\' | translate}}\n            </button>\n            <button ion-button icon-start (click)="delete(item)" *ngIf="item.exists">\n              <ion-icon name="ios-trash-outline"></ion-icon> {{\'DELETE\' | translate}}\n            </button>\n          </div>\n          <div class="category-buttons">\n            <button ion-button icon-start (click)="sendEmail(item)">\n              <ion-icon name="ios-mail"></ion-icon> {{\'SEND_BY_EMAIL\' | translate}}\n            </button>\n          </div>\n        </ion-item>\n      </ion-list>\n    </ion-card-content>\n  </ion-card>\n  <ion-card *ngFor="let pdf of pdfs" class="content-col">\n    <ion-card-header>{{pdf.name}}</ion-card-header>\n    <ion-card-content>\n      <ion-icon name="ios-document"></ion-icon>\n    </ion-card-content>\n    <ion-row class="cardfooter">\n      <ion-col *ngIf="!pdf.exists">\n        <button ion-button full icon-start (click)="open(pdf)">\n          <ion-icon name="ios-play"></ion-icon> {{\'TO_VIEW\' | translate}}\n        </button>\n      </ion-col>\n      <ion-col *ngIf="!pdf.exists">\n        <button ion-button full icon-start (click)="download(pdf)">\n          <ion-icon name="ios-cloud-download"></ion-icon> {{\'DOWNLOAD\' | translate}}\n        </button>\n      </ion-col>\n      <ion-col *ngIf="pdf.exists">\n        <button ion-button full icon-start (click)="openLocal(pdf)">\n          <ion-icon name="ios-play"></ion-icon> {{\'OPEN\' | translate}}\n        </button>\n      </ion-col>\n      <ion-col *ngIf="pdf.exists">\n        <button ion-button full icon-start (click)="delete(pdf)">\n          <ion-icon name="ios-trash-outline"></ion-icon> {{\'DELETE\' | translate}}\n        </button>\n      </ion-col>\n      <ion-col col-12>\n        <button ion-button full icon-start (click)="sendEmail(pdf)">\n          <ion-icon name="ios-mail"></ion-icon> {{\'SEND_BY_EMAIL\' | translate}}\n        </button>\n      </ion-col>\n    </ion-row>\n  </ion-card>\n</ion-content>'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/source-pdf/source-pdf.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_4__node_modules_ionic_native_device_ngx__["a" /* Device */], __WEBPACK_IMPORTED_MODULE_7__node_modules_ionic_native_file_ngx__["a" /* File */], __WEBPACK_IMPORTED_MODULE_6__node_modules_ionic_native_document_viewer_ngx__["a" /* DocumentViewer */], __WEBPACK_IMPORTED_MODULE_5__node_modules_ionic_native_network_ngx__["a" /* Network */], __WEBPACK_IMPORTED_MODULE_8__node_modules_ionic_native_file_transfer_ngx__["a" /* FileTransfer */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */], __WEBPACK_IMPORTED_MODULE_9__ionic_native_in_app_browser_ngx__["a" /* InAppBrowser */], __WEBPACK_IMPORTED_MODULE_10__providers_database_database__["a" /* DatabaseProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */], __WEBPACK_IMPORTED_MODULE_11__ionic_native_social_sharing_ngx__["a" /* SocialSharing */]])
    ], SourcePdfPage);
    return SourcePdfPage;
}());

//# sourceMappingURL=source-pdf.js.map

/***/ }),

/***/ 181:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SourcesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__source_video_source_video__ = __webpack_require__(185);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__source_audio_source_audio__ = __webpack_require__(177);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__source_pdf_source_pdf__ = __webpack_require__(178);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_database_database__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var SourcesPage = /** @class */ (function () {
    function SourcesPage(navCtrl, navParams, translate, service, alertCtrl, events, loading, database, menuCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.translate = translate;
        this.service = service;
        this.alertCtrl = alertCtrl;
        this.events = events;
        this.loading = loading;
        this.database = database;
        this.menuCtrl = menuCtrl;
        this.video = __WEBPACK_IMPORTED_MODULE_4__source_video_source_video__["a" /* SourceVideoPage */];
        this.audio = __WEBPACK_IMPORTED_MODULE_5__source_audio_source_audio__["a" /* SourceAudioPage */];
        this.pdf = __WEBPACK_IMPORTED_MODULE_6__source_pdf_source_pdf__["a" /* SourcePdfPage */];
    }
    SourcesPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.translate.get("LOADING").subscribe(function (result) {
            _this.loader = _this.loading.create({
                content: result,
            });
            _this.loader.present();
            _this.database.getUser().then(function (user) {
                if (user != null) {
                    _this.getData({ user: user });
                }
            });
        });
    };
    SourcesPage.prototype.getData = function (params) {
        var _this = this;
        this.service.getDataWithToken('material', params).then(function (res) {
            _this.loader.dismiss();
            _this.result = res;
            if (_this.result.status == true) {
                _this.navParams.data.souces = _this.result.materials;
                _this.events.publish('loadSources', _this.result.materials);
            }
            else {
                var alert_1 = _this
                    .alertCtrl
                    .create({
                    title: _this.translate.instant('ERROR'),
                    subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                    buttons: ['Ok']
                });
                alert_1.present();
            }
        }, function (err) {
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    SourcesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-sources',template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/sources/sources.html"*/'<ion-header>\n  <ion-navbar color="dark">\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>{{ \'MATERIALS\' | translate }}</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-tabs tabsLayout="icon-start" #sourceTabs>\n  <ion-tab tabIcon="logo-youtube" tabTitle="{{ \'VIDEO\' | translate }}" [root]="video" [rootParams]="navParams.data"></ion-tab>\n  <ion-tab tabIcon="ios-mic-outline" tabTitle="{{ \'AUDIO\' | translate }}" [root]="audio" [rootParams]="navParams.data"></ion-tab>\n  <ion-tab tabIcon="ios-document-outline" tabTitle="{{ \'PDF\' | translate }}" [root]="pdf" [rootParams]="navParams.data"></ion-tab>\n</ion-tabs>'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/sources/sources.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__node_modules_ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_7__providers_database_database__["a" /* DatabaseProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */]])
    ], SourcesPage);
    return SourcesPage;
}());

//# sourceMappingURL=sources.js.map

/***/ }),

/***/ 182:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MeetingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_database_database__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__meeting_list_meeting_list__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__meeting_map_meeting_map__ = __webpack_require__(187);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MeetingPage = /** @class */ (function () {
    function MeetingPage(navCtrl, navParams, service, loadingCtrl, alertCtrl, translate, database, events, modal) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.database = database;
        this.events = events;
        this.modal = modal;
        this.mapPage = __WEBPACK_IMPORTED_MODULE_6__meeting_map_meeting_map__["a" /* MeetingMapPage */];
        this.listPage = __WEBPACK_IMPORTED_MODULE_5__meeting_list_meeting_list__["a" /* MeetingListPage */];
        this.loader = null;
    }
    MeetingPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.database.getUser().then(function (user) {
            if (user != null) {
                _this.translate.get("LOADING").subscribe(function (result) {
                    _this.loader = _this.loadingCtrl.create({
                        content: result,
                    });
                    _this.loader.present();
                    _this.getData({ user: user });
                });
            }
        });
    };
    MeetingPage.prototype.getData = function (params) {
        var _this = this;
        this.service.getDataWithToken('meeting', params).then(function (res) {
            if (_this.loader != null) {
                _this.loader.dismiss();
            }
            _this.result = res;
            if (_this.result.status == true) {
                _this.navParams.data.meetings = _this.result.meetings;
                _this.events.publish('loadMeetings', _this.result.meetings);
                if (_this.navParams.data.hasOwnProperty('push')) {
                    _this.meetings = _this.result.meetings;
                    _this.openPush(_this.navParams.data.push);
                }
            }
            else {
                var alert_1 = _this
                    .alertCtrl
                    .create({
                    title: _this.translate.instant('ERROR'),
                    subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                    buttons: ['Ok']
                });
                alert_1.present();
            }
        }, function (err) {
            if (_this.loader != null) {
                _this.loader.dismiss();
            }
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    MeetingPage.prototype.openPush = function (data) {
        var _this = this;
        this.navParams.data = {};
        Object.keys(this.meetings).forEach(function (key) {
            var item = _this.meetings[key];
            if (item.id == data.item_id) {
                _this.open(item);
            }
        });
    };
    MeetingPage.prototype.open = function (meeting) {
        var modalMeeting = this.modal.create('MeetingDetailsPage', { meeting: meeting });
        modalMeeting.present();
    };
    MeetingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-meeting',template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/meeting/meeting.html"*/'<ion-header>\n  <ion-navbar color="dark">\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>{{ \'MEETINGS\' | translate }}</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-tabs tabsLayout="icon-start">\n  <ion-tab tabIcon="md-map" tabTitle="{{ \'MAP\' | translate }}" [root]="mapPage" [rootParams]="navParams.data"></ion-tab>\n  <ion-tab tabIcon="md-list-box" tabTitle="{{ \'LIST\' | translate }}" [root]="listPage" [rootParams]="navParams.data"></ion-tab>\n</ion-tabs>'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/meeting/meeting.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_4__providers_database_database__["a" /* DatabaseProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */]])
    ], MeetingPage);
    return MeetingPage;
}());

//# sourceMappingURL=meeting.js.map

/***/ }),

/***/ 183:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DailyAudioProdutoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_database_database__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var DailyAudioProdutoPage = /** @class */ (function () {
    function DailyAudioProdutoPage(navCtrl, navParams, service, alertCtrl, translate, loading, modal, events, database, tabs) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.loading = loading;
        this.modal = modal;
        this.events = events;
        this.database = database;
        this.tabs = tabs;
        this.audios = [];
    }
    DailyAudioProdutoPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.events.subscribe('loadAudios', function (audios) {
            if (audios == false) {
                _this.translate.get("LOADING").subscribe(function (result) {
                    _this.loader = _this.loading.create({
                        content: result,
                    });
                    _this.loader.present();
                    _this.database.getUser().then(function (user) {
                        if (user != null) {
                            _this.getData({ user: user });
                        }
                    });
                });
            }
            else {
                _this.audios = audios;
                if (_this.navParams.data.hasOwnProperty('push')) {
                    _this.openPush(_this.navParams.data.push);
                }
                else {
                    _this.checkEmpty();
                }
            }
        });
    };
    DailyAudioProdutoPage.prototype.getData = function (params) {
        var _this = this;
        this.service.getDataWithToken('audio', params).then(function (res) {
            _this.loader.dismiss();
            _this.result = res;
            if (_this.result.status == true) {
                _this.audios = _this.result.audios;
            }
            else {
                var alert_1 = _this
                    .alertCtrl
                    .create({
                    title: _this.translate.instant('ERROR'),
                    subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                    buttons: ['Ok']
                });
                alert_1.present();
            }
        }, function (err) {
            _this.loader.dismiss();
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    DailyAudioProdutoPage.prototype.checkEmpty = function () {
        var _this = this;
        var empty = true;
        Object.keys(this.audios).forEach(function (key) {
            var item = _this.audios[key];
            if (item.type == "1") {
                empty = false;
            }
        });
        var currentTab = this.tabs.getActiveChildNavs()[0].index;
        if (empty && currentTab != 1) {
            this.navCtrl.parent.select(1);
        }
    };
    DailyAudioProdutoPage.prototype.openPush = function (data) {
        var _this = this;
        this.navParams.data = {};
        Object.keys(this.audios).forEach(function (key) {
            var item = _this.audios[key];
            if (item.id == data.item_id) {
                _this.open(item);
            }
        });
    };
    DailyAudioProdutoPage.prototype.open = function (audio) {
        var that = this;
        audio.audio_url = audio.path;
        var modal = this.modal.create('AudioOnlinePage', { audio: audio });
        modal.onDidDismiss(function (audio) {
            if (audio != undefined) {
                that.database.getUser().then(function (user) {
                    if (user != null) {
                        that.translate.get("LOADING").subscribe(function (result) {
                            that.loader = that.loading.create({
                                content: result,
                            });
                            that.loader.present();
                            that.getData({ user: user });
                        });
                    }
                });
            }
        });
        modal.present();
    };
    DailyAudioProdutoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-daily-audio-produto',template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/daily-audio-produto/daily-audio-produto.html"*/'<ion-content>\n  <div class="card-background">\n    <ion-card>\n      <img src="assets/imgs/header/audio.png"/>\n      <div class="card-title">{{ \'DAILY_AUDIO_PRODUCTS\' | translate }}</div>\n    </ion-card>\n  </div>\n  <ion-grid>\n    <ion-row>\n      <ion-col col-6 class="content-col {{audio.type == \'1\' ? \'\' : \'hide-col\'}}"  *ngFor="let audio of audios">\n        <ion-card (click)="open(audio)">\n          <ion-card-content>\n            <img src="assets/imgs/default-img.gif" [lazyLoad]="audio.image" *ngIf="audio.image != null"/>\n            <h2>{{audio.name}}</h2>\n            <div class="progress-bar">\n              <div class="current" [progress]=\'audio.progress\'></div>\n            </div>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/daily-audio-produto/daily-audio-produto.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_4__providers_database_database__["a" /* DatabaseProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* Tabs */]])
    ], DailyAudioProdutoPage);
    return DailyAudioProdutoPage;
}());

//# sourceMappingURL=daily-audio-produto.js.map

/***/ }),

/***/ 184:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DailyAudioMotivacionalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_database_database__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var DailyAudioMotivacionalPage = /** @class */ (function () {
    function DailyAudioMotivacionalPage(navCtrl, navParams, service, alertCtrl, translate, loading, modal, database) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.loading = loading;
        this.modal = modal;
        this.database = database;
        this.audios = [];
    }
    DailyAudioMotivacionalPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        if (this.navParams.data.hasOwnProperty('audios') == false) {
            this.translate.get("LOADING").subscribe(function (result) {
                _this.loader = _this.loading.create({
                    content: result,
                });
                _this.loader.present();
                _this.database.getUser().then(function (user) {
                    if (user != null) {
                        _this.getData({ user: user });
                    }
                });
            });
        }
        else {
            this.audios = this.navParams.data.audios;
        }
    };
    DailyAudioMotivacionalPage.prototype.getData = function (params) {
        var _this = this;
        this.service.getDataWithToken('audio', params).then(function (res) {
            _this.loader.dismiss();
            _this.result = res;
            if (_this.result.status == true) {
                _this.audios = _this.result.audios;
            }
            else {
                var alert_1 = _this
                    .alertCtrl
                    .create({
                    title: _this.translate.instant('ERROR'),
                    subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                    buttons: ['Ok']
                });
                alert_1.present();
            }
        }, function (err) {
            _this.loader.dismiss();
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    DailyAudioMotivacionalPage.prototype.open = function (audio) {
        var that = this;
        audio.audio_url = audio.path;
        var modal = this.modal.create('AudioOnlinePage', { audio: audio });
        modal.onDidDismiss(function (audio) {
            if (audio != undefined) {
                that.database.getUser().then(function (user) {
                    if (user != null) {
                        that.translate.get("LOADING").subscribe(function (result) {
                            that.loader = that.loading.create({
                                content: result,
                            });
                            that.loader.present();
                            that.getData({ user: user });
                        });
                    }
                });
            }
        });
        modal.present();
    };
    DailyAudioMotivacionalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-daily-audio-motivacional',template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/daily-audio-motivacional/daily-audio-motivacional.html"*/'<ion-content>\n  <div class="card-background">\n    <ion-card>\n      <img src="assets/imgs/header/audio.png"/>\n      <div class="card-title">{{ \'DAILY_AUDIO_MOTIVATIONAL\' | translate }}</div>\n    </ion-card>\n  </div>\n  <ion-grid>\n    <ion-row>\n      <ion-col col-6 class="content-col {{audio.type == \'0\' ? \'\' : \'hide-col\'}}"  *ngFor="let audio of audios">\n        <ion-card (click)="open(audio)">\n          <ion-card-content>\n            <img src="assets/imgs/default-img.gif" [lazyLoad]="audio.image" *ngIf="audio.image != null" offset="100"/>\n            <h2>{{audio.name}}</h2>\n            <div class="progress-bar">\n              <div class="current" [progress]=\'audio.progress\'></div>\n            </div>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/daily-audio-motivacional/daily-audio-motivacional.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */], __WEBPACK_IMPORTED_MODULE_4__providers_database_database__["a" /* DatabaseProvider */]])
    ], DailyAudioMotivacionalPage);
    return DailyAudioMotivacionalPage;
}());

//# sourceMappingURL=daily-audio-motivacional.js.map

/***/ }),

/***/ 185:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SourceVideoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__node_modules_ionic_native_device_ngx__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__node_modules_ionic_native_file_ngx__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_database_database__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var SourceVideoPage = /** @class */ (function () {
    function SourceVideoPage(navCtrl, navParams, service, alertCtrl, translate, loading, file, device, modal, events, database, app, platform, tabs) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.loading = loading;
        this.file = file;
        this.device = device;
        this.modal = modal;
        this.events = events;
        this.database = database;
        this.app = app;
        this.platform = platform;
        this.tabs = tabs;
        this.groups = [];
    }
    SourceVideoPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.events.subscribe('loadSources', function (sources) {
            if (sources == false) {
                _this.translate.get("LOADING").subscribe(function (result) {
                    _this.loader = _this.loading.create({
                        content: result,
                    });
                    _this.loader.present();
                    _this.database.getUser().then(function (user) {
                        if (user != null) {
                            _this.getData({ user: user });
                        }
                    });
                });
            }
            else {
                _this.sources = sources;
                _this.checkVideo(sources);
            }
        });
    };
    SourceVideoPage.prototype.getData = function (params) {
        var _this = this;
        this.service.getDataWithToken('material', params).then(function (res) {
            _this.loader.dismiss();
            _this.result = res;
            if (_this.result.status == true) {
                _this.sources = _this.result.materials;
                _this.checkVideo(_this.result.materials);
            }
            else {
                var alert_1 = _this
                    .alertCtrl
                    .create({
                    title: _this.translate.instant('ERROR'),
                    subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                    buttons: ['Ok']
                });
                alert_1.present();
            }
        }, function (err) {
            _this.loader.dismiss();
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    SourceVideoPage.prototype.checkVideo = function (videos) {
        var elements = [], that = this;
        this.videos = [];
        Object.keys(videos).forEach(function (key) {
            var item = videos[key];
            if (item.hasOwnProperty('type')) {
                if (item.type == 2) {
                    if (item.category_id != null) {
                        that.populate(item.category_id, item.category_name, item);
                    }
                    else {
                        elements.push(item);
                    }
                }
            }
        });
        this.videos = elements;
        if (this.navParams.data.hasOwnProperty('push')) {
            if (this.navParams.data.push.source == '2') {
                this.openPush(this.navParams.data.push);
            }
        }
        this.checkEmpty();
        this.order();
    };
    SourceVideoPage.prototype.openPush = function (data) {
        var _this = this;
        this.navParams.data = {};
        Object.keys(this.videos).forEach(function (key) {
            var item = _this.videos[key];
            if (item.id == data.item_id) {
                _this.video = item;
                _this.openModalVideo();
            }
        });
    };
    SourceVideoPage.prototype.checkEmpty = function () {
        var _this = this;
        this.platform.ready().then(function () {
            var currentTab = 0;
            if (_this.tabs != null) {
                currentTab = _this.tabs.getActiveChildNavs()[0].index;
            }
            else {
                currentTab = _this.app.getRootNav().getActiveChildNav();
            }
            if (_this.videos.length == 0) {
                var pdfs_1 = 0, audios_1 = 0;
                Object.keys(_this.sources).forEach(function (key) {
                    var item = _this.sources[key];
                    if (item.type == "1") {
                        audios_1 += 1;
                    }
                    if (item.type == "0") {
                        pdfs_1 += 1;
                    }
                });
                if (audios_1 > 0 && currentTab != 1) {
                    if (_this.app.getRootNav().getActiveChildNav() == undefined) {
                        _this.tabs.select(_this.tabs.getByIndex(1));
                    }
                    else {
                        _this.app.getRootNav().getActiveChildNav().select(1);
                    }
                }
                else if (pdfs_1 > 0 && currentTab != 2) {
                    if (_this.app.getRootNav().getActiveChildNav() == undefined) {
                        _this.tabs.select(_this.tabs.getByIndex(2));
                    }
                    else {
                        _this.app.getRootNav().getActiveChildNav().select(2);
                    }
                }
            }
        });
    };
    SourceVideoPage.prototype.populate = function (indice, name, item) {
        if (this.groups[indice] != undefined) {
            this.groups[indice].itens.push(item);
        }
        else {
            this.groups[indice] = { name: name, itens: [] };
            this.groups[indice].itens.push(item);
        }
    };
    SourceVideoPage.prototype.order = function () {
        var _this = this;
        var orderGroup = [], that = this;
        Object.keys(this.groups).forEach(function (key) {
            if (_this.groups[key].hasOwnProperty('itens')) {
                orderGroup.push(_this.groups[key]);
            }
        });
        that.categories = orderGroup.reverse();
    };
    SourceVideoPage.prototype.openModalOpcoes = function (video) {
        this.video = video;
        this.thumb = video.thumb;
        this.isAudio = video.audio_url == null ? false : true;
        if (video.download == null || video.download == '') {
            this.isDownload = false;
        }
        else {
            this.isDownload = true;
        }
        // if (this.video.watch) {
        if (true) {
            if (this.isDownload) {
                this.checkFile();
            }
            else {
                this.openModalVideo();
            }
        }
        else {
            var alert_2 = this
                .alertCtrl
                .create({
                title: this.translate.instant('OOPS'),
                subTitle: this.translate.instant('OOPS_TEXT'),
                buttons: ['Ok']
            });
            alert_2.present();
        }
    };
    SourceVideoPage.prototype.checkFile = function () {
        var _this = this;
        if (this.device.platform == "Android") {
            this.targetPath = this.file.dataDirectory;
        }
        else {
            this.targetPath = this.file.documentsDirectory;
        }
        var fileName = this.video.path + ".mp4";
        this.video.targetFile = this.targetPath + fileName;
        this.file.checkFile(this.targetPath, fileName).then(function (res) {
            var modalOffline = _this.modal.create('VideoOfflinePage', { video: _this.video });
            modalOffline.present();
        }, function (err) {
            _this.isFile = false;
            _this.showEmbed = false;
            var modalOpcoes = _this.modal.create('VideoOptionsPage', { video: _this.video, isFile: _this.isFile, isDownload: _this.isDownload, isAudio: _this.isAudio });
            modalOpcoes.present();
        });
    };
    SourceVideoPage.prototype.openModalVideo = function () {
        var modalOnline = this.modal.create('VideoOnlinePage', { video: this.video });
        modalOnline.present();
    };
    SourceVideoPage.prototype.removerAcentos = function (s) {
        var map = { "â": "a", "Â": "A", "à": "a", "À": "A", "á": "a", "Á": "A", "ã": "a", "Ã": "A", "ê": "e", "Ê": "E", "è": "e", "È": "E", "é": "e", "É": "E", "î": "i", "Î": "I", "ì": "i", "Ì": "I", "í": "i", "Í": "I", "õ": "o", "Õ": "O", "ô": "o", "Ô": "O", "ò": "o", "Ò": "O", "ó": "o", "Ó": "O", "ü": "u", "Ü": "U", "û": "u", "Û": "U", "ú": "u", "Ú": "U", "ù": "u", "Ù": "U", "ç": "c", "Ç": "C" };
        return s.replace(/[\W\[\] ]/g, function (a) { return map[a] || a; }).split(' ').join('_');
    };
    SourceVideoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-source-video',template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/source-video/source-video.html"*/'<ion-header>\n  <ion-navbar color="dark">\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title></ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <div class="card-background">\n    <ion-card>\n      <img src="assets/imgs/header/sources.png"/>\n      <div class="card-title">{{ \'VIDEO\' | translate }}</div>\n    </ion-card>\n  </div>\n  <ion-card *ngFor="let group of categories" class="card-categories">\n    <ion-card-header>{{group.name}}</ion-card-header>\n    <ion-card-content>\n      <ion-list>\n        <ion-item *ngFor="let item of group.itens" (click)="openModalOpcoes(item)">\n          <ion-thumbnail item-start>\n            <img [src]="item.thumbnail" />\n          </ion-thumbnail>\n          <h2>{{item.name}}</h2>\n        </ion-item>\n      </ion-list>\n    </ion-card-content>\n  </ion-card>\n  <ion-grid>\n    <ion-row>\n      <ion-col col-6 *ngFor="let video of videos" class="content-col" (click)="openModalOpcoes(video)">\n        <ion-card>\n          <ion-card-content>\n            <img [src]="video.thumbnail" />\n            <h2>{{video.name}}</h2>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>    \n  </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/source-video/source-video.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_5__node_modules_ionic_native_file_ngx__["a" /* File */], __WEBPACK_IMPORTED_MODULE_4__node_modules_ionic_native_device_ngx__["a" /* Device */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_6__providers_database_database__["a" /* DatabaseProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* Tabs */]])
    ], SourceVideoPage);
    return SourceVideoPage;
}());

//# sourceMappingURL=source-video.js.map

/***/ }),

/***/ 186:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MeetingListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_database_database__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MeetingListPage = /** @class */ (function () {
    function MeetingListPage(navCtrl, navParams, service, alertCtrl, translate, loading, database, loadingCtrl, modal) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.loading = loading;
        this.database = database;
        this.loadingCtrl = loadingCtrl;
        this.modal = modal;
    }
    MeetingListPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        if (this.navParams.data.hasOwnProperty('meetings') == false) {
            this.translate.get("LOADING").subscribe(function (result) {
                _this.loader = _this.loading.create({
                    content: result,
                });
                _this.loader.present();
                _this.database.getUser().then(function (user) {
                    if (user != null) {
                        _this.getData({ user: user });
                    }
                });
            });
        }
        else {
            this.meetings = this.navParams.data.meetings;
        }
    };
    MeetingListPage.prototype.getData = function (params) {
        var _this = this;
        this.service.getDataWithToken('meeting', params).then(function (res) {
            _this.loader.dismiss();
            _this.result = res;
            if (_this.result.status == true) {
                _this.meetings = _this.result.meetings;
            }
            else {
                var alert_1 = _this
                    .alertCtrl
                    .create({
                    title: _this.translate.instant('ERROR'),
                    subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                    buttons: ['Ok']
                });
                alert_1.present();
            }
        }, function (err) {
            _this.loader.dismiss();
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    MeetingListPage.prototype.open = function (meeting) {
        var modalMeeting = this.modal.create('MeetingDetailsPage', { meeting: meeting });
        modalMeeting.present();
    };
    MeetingListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-meeting-list',template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/meeting-list/meeting-list.html"*/'<ion-header>\n  <ion-navbar color="dark">\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title></ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <div class="card-background">\n    <ion-card>\n      <img src="assets/imgs/header/followup.png" />\n      <div class="card-title">{{ \'LIST\' | translate }}</div>\n    </ion-card>\n  </div>\n  <ion-card *ngFor="let meeting of meetings">\n    <ion-item (click)="open(meeting)">\n      <ion-icon name="md-map" item-start></ion-icon>\n      <h2>{{meeting.name}}</h2>\n      <p><strong>{{meeting.date}}</strong></p>\n      <ion-icon name="ios-arrow-forward" item-end></ion-icon>\n    </ion-item>\n  </ion-card>\n</ion-content>'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/meeting-list/meeting-list.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_4__providers_database_database__["a" /* DatabaseProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */]])
    ], MeetingListPage);
    return MeetingListPage;
}());

//# sourceMappingURL=meeting-list.js.map

/***/ }),

/***/ 187:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MeetingMapPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_database_database__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation_ngx__ = __webpack_require__(344);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_launch_navigator_ngx__ = __webpack_require__(188);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MeetingMapPage = /** @class */ (function () {
    function MeetingMapPage(navCtrl, navParams, service, alertCtrl, translate, loading, database, events, loadingCtrl, geolocation, launchNavigator) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.loading = loading;
        this.database = database;
        this.events = events;
        this.loadingCtrl = loadingCtrl;
        this.geolocation = geolocation;
        this.launchNavigator = launchNavigator;
        this.meetings = null;
    }
    MeetingMapPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.events.subscribe('loadMeetings', function (meetings) {
            if (meetings == false) {
                _this.translate.get("LOADING").subscribe(function (result) {
                    _this.loader = _this.loading.create({
                        content: result,
                    });
                    _this.loader.present();
                    _this.database.getUser().then(function (user) {
                        if (user != null) {
                            _this.getData({ user: user });
                        }
                    });
                });
            }
            else {
                _this.meetings = meetings;
                _this.loadMap();
            }
        });
    };
    MeetingMapPage.prototype.getData = function (params) {
        var _this = this;
        this.service.getDataWithToken('meeting', params).then(function (res) {
            _this.loader.dismiss();
            _this.result = res;
            if (_this.result.status == true) {
                _this.meetings = _this.result.meetings;
                _this.loadMap();
            }
            else {
                var alert_1 = _this
                    .alertCtrl
                    .create({
                    title: _this.translate.instant('ERROR'),
                    subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                    buttons: ['Ok']
                });
                alert_1.present();
            }
        }, function (err) {
            _this.loader.dismiss();
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    MeetingMapPage.prototype.checkLocations = function () {
        var _this = this;
        if (this.meetings != null) {
            Object.keys(this.meetings).forEach(function (key) {
                var item = _this.meetings[key];
                _this.addMarker(item, key);
            });
        }
    };
    MeetingMapPage.prototype.loadMap = function () {
        var _this = this;
        this.translate.get("LOADING").subscribe(function (result) {
            _this.loader = _this.loading.create({
                content: result,
            });
            _this.loader.present();
            _this.geolocation.getCurrentPosition().then(function (position) {
                var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                var mapOptions = {
                    center: latLng,
                    zoom: 5,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                _this.map = new google.maps.Map(_this.mapElement.nativeElement, mapOptions);
                if (_this.meetings != null) {
                    _this.checkLocations();
                }
                _this.loader.dismiss();
            }, function (err) {
                var latLng = new google.maps.LatLng('-8.0462087', '-34.8955189');
                var mapOptions = {
                    center: latLng,
                    zoom: 5,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                _this.map = new google.maps.Map(_this.mapElement.nativeElement, mapOptions);
                if (_this.meetings != null) {
                    _this.checkLocations();
                }
                _this.loader.dismiss();
            });
        });
    };
    MeetingMapPage.prototype.addMarker = function (item, key) {
        var marker = new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            icon: 'assets/imgs/marker.png',
            position: { lat: item.address_latitude, lng: item.address_longitude },
            title: item.name
        }), content = '<div class="content-map"><strong>' + item.name + '</strong><br/>';
        content += '<br/><strong> Endereço:</strong> ' + item.address_address;
        content += '<br/><br/><strong> Data:</strong> ' + item.date;
        content += '<br><br><button id="tap" class="button button-dark button-full text-center" index="' + key + '">' + this.translate.instant('DRIVE_PLACE') + '</button></div>';
        this.addInfoWindow(marker, content);
    };
    MeetingMapPage.prototype.addInfoWindow = function (marker, content) {
        var _this = this;
        var infoWindow = new google.maps.InfoWindow({
            content: content
        });
        google.maps.event.addListener(marker, 'click', function () {
            infoWindow.open(_this.map, marker);
            google.maps.event.addListenerOnce(infoWindow, 'domready', function () {
                document.getElementById('tap').addEventListener('click', function (res) {
                    var key = document.getElementById('tap').getAttribute('index');
                    _this.goLocation(key);
                    infoWindow.close();
                });
            });
        });
    };
    MeetingMapPage.prototype.goLocation = function (key) {
        var _this = this;
        var location = this.meetings[key], options = {
            start: null,
        };
        this.launchNavigator.navigate([location.address_latitude, location.address_longitude], options).then(function (res) {
        }, function (err) {
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('NOT_TRACE'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */])
    ], MeetingMapPage.prototype, "mapElement", void 0);
    MeetingMapPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-meeting-map',template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/meeting-map/meeting-map.html"*/'<ion-header>\n  <ion-navbar color="dark">\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title></ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n    <div #map id="map"></div>\n</ion-content>'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/meeting-map/meeting-map.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_4__providers_database_database__["a" /* DatabaseProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation_ngx__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_launch_navigator_ngx__["a" /* LaunchNavigator */]])
    ], MeetingMapPage);
    return MeetingMapPage;
}());

//# sourceMappingURL=meeting-map.js.map

/***/ }),

/***/ 189:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AlertsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_storage_service_storage_service__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_database_database__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AlertsPage = /** @class */ (function () {
    function AlertsPage(navCtrl, navParams, translate, loadingCtrl, service, alertCtrl, modal, storage, database) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.translate = translate;
        this.loadingCtrl = loadingCtrl;
        this.service = service;
        this.alertCtrl = alertCtrl;
        this.modal = modal;
        this.storage = storage;
        this.database = database;
    }
    AlertsPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.translate.get("LOADING").subscribe(function (result) {
            _this.loader = _this.loadingCtrl.create({
                content: result,
            });
            _this.loader.present();
            _this.database.getUser().then(function (user) {
                if (user != null) {
                    _this.user = user;
                    _this.getData({ user: user });
                }
            });
        });
    };
    AlertsPage.prototype.getData = function (params) {
        var _this = this;
        this.service.getDataWithToken('notification', params).then(function (res) {
            _this.loader.dismiss();
            _this.result = res;
            if (_this.result.status == true) {
                _this.alerts = _this.result.notifications;
            }
            else {
                var alert_1 = _this
                    .alertCtrl
                    .create({
                    title: _this.translate.instant('ERROR'),
                    subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                    buttons: ['Ok']
                });
                alert_1.present();
            }
        }, function (err) {
            _this.loader.dismiss();
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    AlertsPage.prototype.openNotification = function (alert) {
        if (alert.status == "0") {
            alert.status = 1;
            this.udpateNotification(alert);
        }
        var alertModal = this.modal.create('AlertModalPage', { alert: alert });
        alertModal.present();
    };
    AlertsPage.prototype.udpateNotification = function (alert) {
        var _this = this;
        var params = {
            user: this.user,
            status: alert.status,
            notification_id: alert.id,
        };
        this.service.postDataWithToken('notification-status', params).then(function (res) {
        }, function (err) {
            _this.loader.dismiss();
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    AlertsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-alerts',template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/alerts/alerts.html"*/'<ion-header>\n  <ion-navbar color="dark">\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>{{ \'ALERTS\' | translate }}</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <div class="card-background">\n    <ion-card>  \n      <img src="assets/imgs/header/alerts.png"/>\n      <div class="card-title">{{ \'ALERTS\' | translate }}</div>\n    </ion-card>\n  </div>\n  <ion-list>\n    <div *ngFor="let alert of alerts">\n      <ion-item *ngIf="alert.status != \'2\'" (click)="openNotification(alert)">\n        <ion-icon name="{{alert.status == \'1\' ? \'ios-mail-outline\' : \'ios-mail\'}}" item-start></ion-icon>\n        <h2>{{alert.title}}</h2>\n        <p>{{alert.message}}</p>\n        <ion-icon name="ios-arrow-forward-outline" item-end></ion-icon>\n      </ion-item>\n    </div>\n  </ion-list>\n</ion-content>'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/alerts/alerts.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__node_modules_ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */], __WEBPACK_IMPORTED_MODULE_4__providers_storage_service_storage_service__["a" /* StorageServiceProvider */], __WEBPACK_IMPORTED_MODULE_5__providers_database_database__["a" /* DatabaseProvider */]])
    ], AlertsPage);
    return AlertsPage;
}());

//# sourceMappingURL=alerts.js.map

/***/ }),

/***/ 191:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactsCompletePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_storage_service_storage_service__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__contact_details_contact_details__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_database_database__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ContactsCompletePage = /** @class */ (function () {
    function ContactsCompletePage(navCtrl, navParams, service, loadingCtrl, alertCtrl, translate, storage, events, database) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.storage = storage;
        this.events = events;
        this.database = database;
        this.contacts = [];
        this.user = this.storage.getObject('distributor');
    }
    ContactsCompletePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.events.subscribe('loadContacts', function (data) {
            if (data == false) {
                _this.translate.get("LOADING").subscribe(function (result) {
                    _this.loader = _this.loadingCtrl.create({
                        content: result,
                    });
                    _this.database.getUser().then(function (user) {
                        if (user != null) {
                            _this.loader.present();
                            _this.getData(user);
                        }
                    });
                });
            }
            else {
                _this.contacts = data;
            }
        });
    };
    ContactsCompletePage.prototype.getData = function (user) {
        var _this = this;
        var params = {
            user: user,
        };
        this.service.getDataWithToken('contact', params).then(function (res) {
            _this.loader.dismiss();
            _this.result = res;
            if (_this.result.status == true && _this.result.contacts.length > 0) {
                var contacts_1 = [];
                Object.keys(_this.result.contacts).forEach(function (key) {
                    var item = _this.result.contacts[key];
                    item.stars = _this.countStars(item.stars);
                    contacts_1.push(item);
                });
                _this.contacts = contacts_1;
            }
            else {
            }
        }, function (err) {
            _this.loader.dismiss();
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    ContactsCompletePage.prototype.countStars = function (stars) {
        var itens = [];
        for (var i = 0; i < stars; i++) {
            itens.push(i);
        }
        return itens;
    };
    ContactsCompletePage.prototype.register = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__contact_details_contact_details__["a" /* ContactDetailsPage */], {
            contact: null,
        });
    };
    ContactsCompletePage.prototype.edit = function (contact) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__contact_details_contact_details__["a" /* ContactDetailsPage */], {
            contact: contact,
        });
    };
    ContactsCompletePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-contacts-complete',template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/contacts-complete/contacts-complete.html"*/'<ion-header>\n  <ion-navbar color="dark">\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-buttons right>\n      <button ion-button icon-only (click)="register()">\n        <ion-icon name="ios-add-circle"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>{{ \'LIST_NAMES\' | translate }}</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <div class="card-background">\n    <ion-card>\n      <img src="assets/imgs/header/names.png"/>\n      <div class="card-title">{{ \'LIST_NAMES_HOT\' | translate }}</div>\n    </ion-card>\n  </div>\n  <ion-card>\n    <ion-item *ngFor="let contact of contacts" (click)="edit(contact)">\n      <ion-avatar item-start>\n        <img src="assets/imgs/placeholder-person.png">\n      </ion-avatar>\n      <h2>{{contact.first_name}} {{contact.last_name}}</h2>\n      <p>{{contact.email}}</p>\n      <p>\n        <ion-icon name="ios-star" *ngFor="let star of contact.stars"></ion-icon>\n      </p>\n      <ion-icon name="ios-arrow-forward" item-end></ion-icon>\n    </ion-item>\n  </ion-card>\n</ion-content>'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/contacts-complete/contacts-complete.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_4__providers_storage_service_storage_service__["a" /* StorageServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_6__providers_database_database__["a" /* DatabaseProvider */]])
    ], ContactsCompletePage);
    return ContactsCompletePage;
}());

//# sourceMappingURL=contacts-complete.js.map

/***/ }),

/***/ 192:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactsDraftPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_storage_service_storage_service__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__contact_details_contact_details__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_database_database__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ContactsDraftPage = /** @class */ (function () {
    function ContactsDraftPage(navCtrl, navParams, service, alertCtrl, translate, loading, storage, database) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.loading = loading;
        this.storage = storage;
        this.database = database;
        this.user = this.storage.getObject('distributor');
        this.showList = false;
    }
    ContactsDraftPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.showList = false;
        this.translate.get("LOADING").subscribe(function (result) {
            _this.loader = _this.loading.create({
                content: result,
            });
            _this.database.getUser().then(function (user) {
                if (user != null) {
                    _this.loader.present();
                    _this.getData(user);
                }
            });
        });
    };
    ContactsDraftPage.prototype.getData = function (user) {
        var _this = this;
        var params = {
            user: user,
        };
        this.service.getDataWithToken('contact-import-list', params).then(function (res) {
            _this.loader.dismiss();
            _this.result = res;
            if (_this.result.status == true && _this.result.contacts.length > 0) {
                var contacts_1 = [];
                Object.keys(_this.result.contacts).forEach(function (key) {
                    var item = _this.result.contacts[key];
                    _this.formatPhone(item);
                    contacts_1.push(item);
                });
                _this.contacts = contacts_1;
                _this.showList = true;
            }
            else {
                _this.contacts = [];
                _this.showList = true;
            }
        }, function (err) {
            _this.loader.dismiss();
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    ContactsDraftPage.prototype.formatPhone = function (item) {
        item.oldPhone = item.phone;
        if (item.phone.length > 0) {
            if (item.phone.length <= 9) {
                if (item.phone.length == 9) {
                    item.phone = item.phone.substr(0, 5) + "-" + item.phone.substr(5);
                }
                else if (item.phone.length == 8) {
                    item.phone = item.phone.substr(0, 4) + "-" + item.phone.substr(4);
                }
            }
            else {
                var phone = item.phone.match(/\d/g).join("");
                if (phone.length == 14) {
                    var country = phone.substr(0, 3), ddd = phone.substr(3, 2), number = phone.substr(5);
                    if (number.length == 9) {
                        number = number.substr(0, 5) + "-" + number.substr(5);
                    }
                    else if (number.length == 8) {
                        number = number.substr(0, 4) + "-" + number.substr(4);
                    }
                    item.phone = country + ' ' + ddd + ' ' + number;
                }
                else if (phone.length == 13) {
                    var country = phone.substr(0, 2), ddd = phone.substr(2, 2), number = phone.substr(4);
                    if (number.length == 9) {
                        number = number.substr(0, 5) + "-" + number.substr(5);
                    }
                    else if (number.length == 8) {
                        number = number.substr(0, 4) + "-" + number.substr(4);
                    }
                    item.phone = '+' + country + ' ' + ddd + ' ' + number;
                }
                else if (phone.length == 12) {
                    if (phone.substr(0, 1) == '0') {
                        var ddd = phone.substr(0, 3), number = phone.substr(3);
                        if (number.length == 9) {
                            number = number.substr(0, 5) + "-" + number.substr(5);
                        }
                        else if (number.length == 8) {
                            number = number.substr(0, 4) + "-" + number.substr(4);
                        }
                        item.phone = ddd + ' ' + number;
                    }
                    else {
                        var country = phone.substr(0, 2), ddd = phone.substr(2, 2), number = phone.substr(4);
                        if (number.length == 9) {
                            number = number.substr(0, 5) + "-" + number.substr(5);
                        }
                        else if (number.length == 8) {
                            number = number.substr(0, 4) + "-" + number.substr(4);
                        }
                        item.phone = '+' + country + ' ' + ddd + ' ' + number;
                    }
                }
                else if (phone.length == 11) {
                    if (phone.substr(0, 1) == '0') {
                        var ddd = phone.substr(0, 3), number = phone.substr(3);
                        if (number.length == 9) {
                            number = number.substr(0, 5) + "-" + number.substr(5);
                        }
                        else if (number.length == 8) {
                            number = number.substr(0, 4) + "-" + number.substr(4);
                        }
                        item.phone = ddd + ' ' + number;
                    }
                    else {
                        var ddd = phone.substr(0, 2), number = phone.substr(2);
                        if (number.length == 9) {
                            number = number.substr(0, 5) + "-" + number.substr(5);
                        }
                        else if (number.length == 8) {
                            number = number.substr(0, 4) + "-" + number.substr(4);
                        }
                        item.phone = ddd + ' ' + number;
                    }
                }
                else {
                    var ddd = phone.substr(0, 2), number = phone.substr(2);
                    if (number.length == 9) {
                        number = number.substr(0, 5) + "-" + number.substr(5);
                    }
                    else if (number.length == 8) {
                        number = number.substr(0, 4) + "-" + number.substr(4);
                    }
                    item.phone = ddd + ' ' + number;
                }
            }
        }
    };
    ContactsDraftPage.prototype.edit = function (contact) {
        var item = {
            first_name: contact.first_name,
            last_name: contact.last_name,
            email: contact.email,
            phone: contact.phone,
            oldPhone: contact.oldPhone,
            import_id: contact.import_id,
            id: contact.id,
            import: true
        };
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__contact_details_contact_details__["a" /* ContactDetailsPage */], {
            contact: item,
        });
    };
    ContactsDraftPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-contacts-draft',template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/contacts-draft/contacts-draft.html"*/'<ion-header>\n  <ion-navbar color="dark">\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-buttons right>\n      <button ion-button icon-only (click)="register()">\n        <ion-icon name="ios-add-circle"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>{{ \'LIST_NAMES\' | translate }}</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <div class="card-background">\n    <ion-card>\n      <img src="assets/imgs/header/names.png"/>\n      <div class="card-title">{{ \'LIST_NAMES_COLD\' | translate }}</div>\n    </ion-card>\n  </div>\n  <ion-card *ngIf="showList">\n    <div *ngFor="let contact of contacts">\n      <ion-item (click)="edit(contact)">\n        <ion-avatar item-start>\n          <img src="assets/imgs/placeholder-person.png">\n        </ion-avatar>\n        <h2>{{contact.first_name}} {{contact.last_name}}</h2>\n        <p>{{contact.email}}</p>\n        <p>{{contact.phone}}</p>\n        <p>\n        </p>\n        <ion-icon name="ios-arrow-forward" item-end></ion-icon>\n      </ion-item>\n    </div>\n  </ion-card>\n</ion-content>'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/contacts-draft/contacts-draft.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_4__providers_storage_service_storage_service__["a" /* StorageServiceProvider */], __WEBPACK_IMPORTED_MODULE_6__providers_database_database__["a" /* DatabaseProvider */]])
    ], ContactsDraftPage);
    return ContactsDraftPage;
}());

//# sourceMappingURL=contacts-draft.js.map

/***/ }),

/***/ 193:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CoursesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__video_course_video_course__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_database_database__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CoursesPage = /** @class */ (function () {
    function CoursesPage(navCtrl, navParams, service, loadingCtrl, alertCtrl, translate, database) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.database = database;
        this.translate.get("LOADING").subscribe(function (result) {
            _this.loader = _this.loadingCtrl.create({
                content: result,
            });
            _this.loader.present();
        });
    }
    CoursesPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.database.getUser().then(function (user) {
            if (user != null) {
                _this.getData({ user: user });
            }
        });
    };
    CoursesPage.prototype.getData = function (params) {
        var _this = this;
        this.service.getDataWithToken('course', params).then(function (res) {
            _this.loader.dismiss();
            _this.result = res;
            if (_this.result.status == true) {
                _this.courses = _this.result.courses;
                if (_this.navParams.data.hasOwnProperty('push')) {
                    _this.openPush(_this.navParams.data.push);
                }
            }
            else {
                var alert_1 = _this
                    .alertCtrl
                    .create({
                    title: _this.translate.instant('ERROR'),
                    subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                    buttons: ['Ok']
                });
                alert_1.present();
            }
        }, function (err) {
            _this.loader.dismiss();
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    CoursesPage.prototype.openPush = function (data) {
        var _this = this;
        this.navParams.data = {};
        Object.keys(this.courses).forEach(function (key) {
            var item = _this.courses[key];
            if (item.id == data.course_id) {
                item.push = data;
                _this.openVideos(item);
            }
        });
    };
    CoursesPage.prototype.openVideos = function (course) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__video_course_video_course__["a" /* VideoCoursePage */], {
            id: course.id,
            name: course.name,
            image: course.image,
            push: course.push == undefined ? null : course.push,
        });
    };
    CoursesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-courses',template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/courses/courses.html"*/'<ion-header>\n  <ion-navbar color="dark">\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>{{ \'COURSES\' | translate }}</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <div class="card-background">\n    <ion-card>\n      <img src="assets/imgs/header/courses.png"/>\n      <div class="card-title">{{ \'COURSES\' | translate }}</div>\n    </ion-card>\n  </div>\n  <ion-grid>\n    <ion-row>\n      <ion-col col-6 *ngFor="let course of courses" class="content-col">\n        <ion-card (click)="openVideos(course)">\n          <ion-card-content>\n            <img src="assets/imgs/default-img.gif" [lazyLoad]="course.image" *ngIf="course.image != null" offset="100" />\n            <h2>{{course.name}}</h2>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>    \n  </ion-grid>\n</ion-content>'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/courses/courses.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_5__providers_database_database__["a" /* DatabaseProvider */]])
    ], CoursesPage);
    return CoursesPage;
}());

//# sourceMappingURL=courses.js.map

/***/ }),

/***/ 194:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VideoCoursePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_device_ngx__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__node_modules_ionic_native_file_ngx__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_database_database__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var VideoCoursePage = /** @class */ (function () {
    function VideoCoursePage(navCtrl, navParams, viewCtrl, service, loadingCtrl, alertCtrl, translate, device, file, modal, database) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.service = service;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.device = device;
        this.file = file;
        this.modal = modal;
        this.database = database;
        this.title = this.navParams.data.name;
        this.course_id = this.navParams.data.id;
        this.push = this.navParams.data.push;
    }
    VideoCoursePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.image = this.navParams.data.image;
        this.translate.get("LOADING").subscribe(function (result) {
            _this.loader = _this.loadingCtrl.create({
                content: result,
            });
            _this.loader.present();
        });
        this.database.getUser().then(function (user) {
            if (user != null) {
                var params = {
                    course_id: _this.course_id,
                    user: user,
                };
                _this.getData(params);
            }
        });
    };
    VideoCoursePage.prototype.getData = function (params) {
        var _this = this;
        this.service.getDataWithToken('course/' + params.course_id, params).then(function (res) {
            _this.loader.dismiss();
            _this.result = res;
            if (_this.result.status == true) {
                _this.videos = _this.result.data.videos;
                if (_this.push != null) {
                    _this.openPush(_this.push);
                }
            }
            else {
                var alert_1 = _this
                    .alertCtrl
                    .create({
                    title: _this.translate.instant('ERROR'),
                    subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                    buttons: ['Ok']
                });
                alert_1.present();
            }
        }, function (err) {
            _this.loader.dismiss();
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    VideoCoursePage.prototype.openPush = function (data) {
        var _this = this;
        this.push = null;
        Object.keys(this.videos).forEach(function (key) {
            var item = _this.videos[key];
            if (item.id == data.item_id) {
                _this.openModalOpcoes(item);
            }
        });
    };
    VideoCoursePage.prototype.openModalOpcoes = function (video) {
        this.video = video;
        this.thumb = video.thumbnail;
        this.isAudio = video.audio == null ? false : true;
        if (video.url == null) {
            this.isDownload = false;
        }
        else {
            this.isDownload = true;
        }
        if (this.video.watch) {
            if (this.isDownload) {
                this.checkFile();
            }
            else {
                this.openModalVideo();
            }
        }
        else {
            var that_1 = this, alert_2 = this
                .alertCtrl
                .create({
                title: this.translate.instant('OOPS'),
                subTitle: this.translate.instant('OOPS_TEXT'),
                buttons: [
                    {
                        text: this.translate.instant('SEE_AFTER'),
                    },
                    {
                        text: this.translate.instant('SEE_NOW'),
                        handler: function () {
                            that_1.openBefore();
                        }
                    }
                ]
            });
            alert_2.present();
        }
    };
    VideoCoursePage.prototype.openBefore = function () {
        var _this = this;
        Object.keys(this.videos).forEach(function (key) {
            var item = _this.videos[key];
            if (item.id == _this.video.before_id) {
                _this.openModalOpcoes(item);
            }
        });
    };
    VideoCoursePage.prototype.checkFile = function () {
        var _this = this;
        if (this.device.platform == "Android") {
            this.targetPath = this.file.dataDirectory;
        }
        else {
            this.targetPath = this.file.documentsDirectory;
        }
        var fileName = this.video.path + ".mp4";
        this.video.targetFile = this.targetPath + fileName;
        this.file.checkFile(this.targetPath, fileName).then(function (res) {
            var modalOffline = _this.modal.create('VideoOfflinePage', { video: _this.video });
            modalOffline.present();
        }, function (err) {
            _this.isFile = false;
            _this.showEmbed = false;
            var modalOpcoes = _this.modal.create('VideoOptionsPage', { video: _this.video, isFile: _this.isFile, isDownload: _this.isDownload, isAudio: _this.isAudio });
            modalOpcoes.present();
        });
    };
    VideoCoursePage.prototype.openModalVideo = function () {
        var that = this;
        var modalOnline = that.modal.create('VideoOnlinePage', { video: that.video });
        modalOnline.onDidDismiss(function (video) {
            if (video != undefined) {
                that.database.getUser().then(function (user) {
                    if (user != null) {
                        that.translate.get("LOADING").subscribe(function (result) {
                            that.loader = that.loadingCtrl.create({
                                content: result,
                            });
                            that.loader.present();
                            var params = {
                                course_id: that.course_id,
                                user: user,
                            };
                            that.getData(params);
                        });
                    }
                });
            }
        });
        modalOnline.present();
    };
    VideoCoursePage.prototype.removerAcentos = function (s) {
        var map = { "â": "a", "Â": "A", "à": "a", "À": "A", "á": "a", "Á": "A", "ã": "a", "Ã": "A", "ê": "e", "Ê": "E", "è": "e", "È": "E", "é": "e", "É": "E", "î": "i", "Î": "I", "ì": "i", "Ì": "I", "í": "i", "Í": "I", "õ": "o", "Õ": "O", "ô": "o", "Ô": "O", "ò": "o", "Ò": "O", "ó": "o", "Ó": "O", "ü": "u", "Ü": "U", "û": "u", "Û": "U", "ú": "u", "Ú": "U", "ù": "u", "Ù": "U", "ç": "c", "Ç": "C" };
        return s.replace(/[\W\[\] ]/g, function (a) { return map[a] || a; }).split(' ').join('_');
    };
    VideoCoursePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-video-course',template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/video-course/video-course.html"*/'<ion-header>\n  <ion-navbar color="dark">\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>{{title}}</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <div class="card-background">\n    <ion-card>\n      <img src="assets/imgs/default-img.gif" [lazyLoad]="image"/>\n      <div class="card-title">{{title}}</div>\n    </ion-card>\n  </div>\n  <ion-grid>\n    <ion-row>\n      <ion-col col-6 *ngFor="let video of videos" class="content-col">\n        <ion-card (click)="openModalOpcoes(video)">\n          <ion-card-content>\n            <img src="assets/imgs/default-img.gif" [lazyLoad]="video.thumbnail" *ngIf="video.thumbnail != null" offset="100" />\n            <h2>{{video.name}}</h2>\n            <div class="progress-bar">\n              <div class="current" [progress]=\'video.progress\'></div>\n            </div>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>    \n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/video-course/video-course.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_5__node_modules_ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_device_ngx__["a" /* Device */], __WEBPACK_IMPORTED_MODULE_4__node_modules_ionic_native_file_ngx__["a" /* File */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */], __WEBPACK_IMPORTED_MODULE_6__providers_database_database__["a" /* DatabaseProvider */]])
    ], VideoCoursePage);
    return VideoCoursePage;
}());

//# sourceMappingURL=video-course.js.map

/***/ }),

/***/ 195:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LeadershipListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_database_database__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_social_sharing_ngx__ = __webpack_require__(53);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var LeadershipListPage = /** @class */ (function () {
    function LeadershipListPage(navCtrl, navParams, service, alertCtrl, translate, loading, database, socialSharing, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.loading = loading;
        this.database = database;
        this.socialSharing = socialSharing;
        this.loadingCtrl = loadingCtrl;
        this.qualifications = [];
        var date = new Date();
        this.year = date.getFullYear();
    }
    LeadershipListPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        if (this.navParams.data.hasOwnProperty('qualifications') == false) {
            this.translate.get("LOADING").subscribe(function (result) {
                _this.loader = _this.loading.create({
                    content: result,
                });
                _this.loader.present();
                _this.database.getUser().then(function (user) {
                    if (user != null) {
                        _this.getData({ user: user });
                    }
                });
            });
        }
        else {
            this.qualifications = this.navParams.data.qualifications;
        }
    };
    LeadershipListPage.prototype.getData = function (params) {
        var _this = this;
        this.service.getDataWithToken('qualification', params).then(function (res) {
            _this.loader.dismiss();
            _this.result = res;
            if (_this.result.status == true) {
                _this.qualifications = _this.result.qualifications;
            }
            else {
                var alert_1 = _this
                    .alertCtrl
                    .create({
                    title: _this.translate.instant('ERROR'),
                    subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                    buttons: ['Ok']
                });
                alert_1.present();
            }
        }, function (err) {
            _this.loader.dismiss();
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    LeadershipListPage.prototype.share = function (item, social) {
        var _this = this;
        this.translate.get("LOADING").subscribe(function (result) {
            _this.loader = _this.loadingCtrl.create({
                content: result,
            });
            _this.loader.present();
        });
        var message = item.name + ' - ' + item.level_name;
        switch (social) {
            case '1':
                this.socialSharing.shareViaFacebook(message, item.image, null).then(function (res) {
                    _this.loader.dismiss();
                }).catch(function (err) {
                    _this.loader.dismiss();
                });
                break;
            case '2':
                this.socialSharing.shareViaTwitter(message, item.image).then(function (res) {
                    _this.loader.dismiss();
                }).catch(function (err) {
                    _this.loader.dismiss();
                });
                break;
            case '3':
                this.socialSharing.shareViaWhatsApp(message, item.image, null).then(function (res) {
                    _this.loader.dismiss();
                }).catch(function (err) {
                    _this.loader.dismiss();
                });
                break;
        }
    };
    LeadershipListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-leadership-list',template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/leadership-list/leadership-list.html"*/'<ion-header>\n  <ion-navbar color="dark">\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title></ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <div class="card-background">\n    <ion-card>\n      <img src="assets/imgs/header/webinars.png" />\n      <div class="card-title">{{year}}</div>\n    </ion-card>\n  </div>\n  <ion-card *ngFor="let qualification of qualifications">\n    <div *ngIf="qualification.image == null">\n      <ion-item>\n        <ion-avatar item-start>\n          <img src="assets/imgs/placeholder-person.png">\n        </ion-avatar>\n        <h2>{{qualification.name}}</h2>\n        <p><strong>{{qualification.level_name}}</strong></p>\n        <p>{{qualification.city == null ? \'\' : qualification.city }}\n          {{qualification.state == null ? \'\' : \' - \' + qualification.state}}</p>\n      </ion-item>\n    </div>\n    <div *ngIf="qualification.image != null">\n      <ion-item>\n        <ion-avatar item-start>\n          <img src="assets/imgs/placeholder-person.png">\n        </ion-avatar>\n        <h2>{{qualification.name}}</h2>\n        <p><strong>{{qualification.level_name}}</strong></p>\n      </ion-item>\n      <img [src]="qualification.image" />\n      <ion-row no-padding>\n        <ion-col col-4>\n          <button ion-button clear small color="danger" icon-start (click)="share(qualification, \'1\')">\n            <ion-icon name=\'logo-facebook\'></ion-icon>\n            Facebook\n          </button>\n        </ion-col>\n        <ion-col text-center col-4>\n          <button ion-button clear small color="danger" icon-start (click)="share(qualification, \'2\')">\n            <ion-icon name=\'logo-twitter\'></ion-icon>\n            Twitter\n          </button>\n        </ion-col>\n        <ion-col text-right col-4>\n          <button ion-button clear small color="danger" icon-start (click)="share(qualification, \'3\')">\n            <ion-icon name=\'logo-whatsapp\'></ion-icon>\n            Whatsapp\n          </button>\n        </ion-col>\n      </ion-row>\n    </div>\n  </ion-card>\n</ion-content>'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/leadership-list/leadership-list.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_4__providers_database_database__["a" /* DatabaseProvider */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_social_sharing_ngx__["a" /* SocialSharing */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */]])
    ], LeadershipListPage);
    return LeadershipListPage;
}());

//# sourceMappingURL=leadership-list.js.map

/***/ }),

/***/ 196:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LeadershipMonthPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_database_database__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_social_sharing_ngx__ = __webpack_require__(53);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var LeadershipMonthPage = /** @class */ (function () {
    function LeadershipMonthPage(navCtrl, navParams, service, alertCtrl, translate, loading, database, events, loadingCtrl, socialSharing) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.loading = loading;
        this.database = database;
        this.events = events;
        this.loadingCtrl = loadingCtrl;
        this.socialSharing = socialSharing;
        this.qualifications = [];
        var date = new Date();
        this.month = date.getMonth() + 1;
        this.months = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
        this.month_name = this.months[date.getMonth()];
    }
    LeadershipMonthPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.events.subscribe('loadLeadership', function (qualifications) {
            if (qualifications == false) {
                _this.translate.get("LOADING").subscribe(function (result) {
                    _this.loader = _this.loading.create({
                        content: result,
                    });
                    _this.loader.present();
                    _this.database.getUser().then(function (user) {
                        if (user != null) {
                            _this.getData({ user: user });
                        }
                    });
                });
            }
            else {
                _this.qualifications = qualifications;
            }
        });
    };
    LeadershipMonthPage.prototype.getData = function (params) {
        var _this = this;
        this.service.getDataWithToken('qualification', params).then(function (res) {
            _this.loader.dismiss();
            _this.result = res;
            if (_this.result.status == true) {
                _this.qualifications = _this.result.qualifications;
            }
            else {
                var alert_1 = _this
                    .alertCtrl
                    .create({
                    title: _this.translate.instant('ERROR'),
                    subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                    buttons: ['Ok']
                });
                alert_1.present();
            }
        }, function (err) {
            _this.loader.dismiss();
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    LeadershipMonthPage.prototype.share = function (item, social) {
        var _this = this;
        this.translate.get("LOADING").subscribe(function (result) {
            _this.loader = _this.loadingCtrl.create({
                content: result,
            });
            _this.loader.present();
        });
        var message = item.name + ' - ' + item.level_name;
        switch (social) {
            case '1':
                this.socialSharing.shareViaFacebook(message, item.image, null).then(function (res) {
                    _this.loader.dismiss();
                }).catch(function (err) {
                    _this.loader.dismiss();
                });
                break;
            case '2':
                this.socialSharing.shareViaTwitter(message, item.image).then(function (res) {
                    _this.loader.dismiss();
                }).catch(function (err) {
                    _this.loader.dismiss();
                });
                break;
            case '3':
                this.socialSharing.shareViaWhatsApp(message, item.image, null).then(function (res) {
                    _this.loader.dismiss();
                }).catch(function (err) {
                    _this.loader.dismiss();
                });
                break;
        }
    };
    LeadershipMonthPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-leadership-month',template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/leadership-month/leadership-month.html"*/'<ion-header>\n  <ion-navbar color="dark">\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title></ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <div class="card-background">\n    <ion-card>\n      <img src="assets/imgs/header/webinars.png"/>\n      <div class="card-title">{{month_name}}</div>\n    </ion-card>\n  </div>\n  <ion-card *ngFor="let qualification of qualifications">\n    <div *ngIf="qualification.month == month">\n      <div *ngIf="qualification.image == null">\n        <ion-item>\n          <ion-avatar item-start>\n            <img src="assets/imgs/placeholder-person.png">\n          </ion-avatar>\n          <h2>{{qualification.name}}</h2>\n          <p><strong>{{qualification.level_name}}</strong></p>\n          <p>{{qualification.city == null ? \'\' : qualification.city }} {{qualification.state == null ? \'\' : \' - \' + qualification.state}}</p>\n        </ion-item>\n      </div>\n      <div *ngIf="qualification.image != null">\n        <ion-item>\n          <ion-avatar item-start>\n            <img src="assets/imgs/placeholder-person.png">\n          </ion-avatar>\n          <h2>{{qualification.name}}</h2>\n          <p><strong>{{qualification.level_name}}</strong></p>\n        </ion-item>\n        <img [src]="qualification.image" />\n        <ion-row no-padding>\n          <ion-col col-4>\n            <button ion-button clear small color="danger" icon-start (click)="share(qualification, \'1\')">\n              <ion-icon name=\'logo-facebook\'></ion-icon>\n              Facebook\n            </button>\n          </ion-col>\n          <ion-col text-center col-4>\n            <button ion-button clear small color="danger" icon-start (click)="share(qualification, \'2\')">\n              <ion-icon name=\'logo-twitter\'></ion-icon>\n              Twitter\n            </button>\n          </ion-col>\n          <ion-col text-right col-4>\n            <button ion-button clear small color="danger" icon-start (click)="share(qualification, \'3\')">\n              <ion-icon name=\'logo-whatsapp\'></ion-icon>\n              Whatsapp\n            </button>\n          </ion-col>\n        </ion-row>\n      </div>\n    </div>\n  </ion-card>\n</ion-content>'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/leadership-month/leadership-month.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_4__providers_database_database__["a" /* DatabaseProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_social_sharing_ngx__["a" /* SocialSharing */]])
    ], LeadershipMonthPage);
    return LeadershipMonthPage;
}());

//# sourceMappingURL=leadership-month.js.map

/***/ }),

/***/ 197:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LeadershipPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__leadership_month_leadership_month__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__leadership_list_leadership_list__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_database_database__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var LeadershipPage = /** @class */ (function () {
    function LeadershipPage(navCtrl, navParams, service, events, alertCtrl, translate, loading, database) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.events = events;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.loading = loading;
        this.database = database;
        this.monthPage = __WEBPACK_IMPORTED_MODULE_2__leadership_month_leadership_month__["a" /* LeadershipMonthPage */];
        this.listPage = __WEBPACK_IMPORTED_MODULE_3__leadership_list_leadership_list__["a" /* LeadershipListPage */];
        var date = new Date();
        this.months = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
        this.month = this.months[date.getMonth()];
        this.year = date.getFullYear();
    }
    LeadershipPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.translate.get("LOADING").subscribe(function (result) {
            _this.loader = _this.loading.create({
                content: result,
            });
            _this.loader.present();
            _this.database.getUser().then(function (user) {
                if (user != null) {
                    _this.getData({ user: user });
                }
            });
        });
    };
    LeadershipPage.prototype.getData = function (params) {
        var _this = this;
        this.service.getDataWithToken('qualification', params).then(function (res) {
            _this.loader.dismiss();
            _this.result = res;
            if (_this.result.status == true) {
                _this.navParams.data.qualifications = _this.result.qualifications;
                _this.events.publish('loadLeadership', _this.result.qualifications);
            }
            else {
                var alert_1 = _this
                    .alertCtrl
                    .create({
                    title: _this.translate.instant('ERROR'),
                    subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                    buttons: ['Ok']
                });
                alert_1.present();
            }
        }, function (err) {
            _this.loader.dismiss();
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    LeadershipPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-leadership',template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/leadership/leadership.html"*/'<ion-header>\n  <ion-navbar color="dark">\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>{{ \'LEADERSHIP\' | translate }}</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-tabs tabsLayout="icon-start">\n  <ion-tab tabIcon="md-calendar" tabTitle="{{month}}" [root]="monthPage" [rootParams]="navParams.data"></ion-tab>\n  <ion-tab tabIcon="md-star" tabTitle="{{year}}" [root]="listPage" [rootParams]="navParams.data"></ion-tab>\n</ion-tabs>'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/leadership/leadership.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_5__node_modules_ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_6__providers_database_database__["a" /* DatabaseProvider */]])
    ], LeadershipPage);
    return LeadershipPage;
}());

//# sourceMappingURL=leadership.js.map

/***/ }),

/***/ 207:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 207;

/***/ }),

/***/ 251:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/alert-modal/alert-modal.module": [
		750,
		14
	],
	"../pages/alerts/alerts.module": [
		751,
		39
	],
	"../pages/audio-offline/audio-offline.module": [
		752,
		13
	],
	"../pages/audio-online/audio-online.module": [
		753,
		12
	],
	"../pages/contact-details/contact-details.module": [
		789,
		38
	],
	"../pages/contact-importer/contact-importer.module": [
		754,
		11
	],
	"../pages/contacts-complete/contacts-complete.module": [
		755,
		37
	],
	"../pages/contacts-draft/contacts-draft.module": [
		756,
		36
	],
	"../pages/contacts/contacts.module": [
		757,
		35
	],
	"../pages/courses/courses.module": [
		758,
		34
	],
	"../pages/daily-audio-motivacional/daily-audio-motivacional.module": [
		759,
		33
	],
	"../pages/daily-audio-produto/daily-audio-produto.module": [
		760,
		32
	],
	"../pages/daily-audio/daily-audio.module": [
		761,
		31
	],
	"../pages/daily/daily.module": [
		762,
		30
	],
	"../pages/disclosure-business/disclosure-business.module": [
		766,
		29
	],
	"../pages/disclosure-produts/disclosure-produts.module": [
		763,
		28
	],
	"../pages/disclosure/disclosure.module": [
		764,
		10
	],
	"../pages/first/first.module": [
		765,
		27
	],
	"../pages/follow/follow.module": [
		767,
		9
	],
	"../pages/language/language.module": [
		768,
		8
	],
	"../pages/leadership-list/leadership-list.module": [
		769,
		26
	],
	"../pages/leadership-month/leadership-month.module": [
		770,
		25
	],
	"../pages/leadership/leadership.module": [
		771,
		24
	],
	"../pages/login/login.module": [
		787,
		23
	],
	"../pages/meeting-details/meeting-details.module": [
		772,
		7
	],
	"../pages/meeting-list/meeting-list.module": [
		773,
		22
	],
	"../pages/meeting-map/meeting-map.module": [
		774,
		21
	],
	"../pages/meeting/meeting.module": [
		775,
		20
	],
	"../pages/notes/notes.module": [
		776,
		6
	],
	"../pages/recovery/recovery.module": [
		777,
		5
	],
	"../pages/register/register.module": [
		778,
		4
	],
	"../pages/source-audio/source-audio.module": [
		779,
		19
	],
	"../pages/source-pdf/source-pdf.module": [
		788,
		18
	],
	"../pages/source-video/source-video.module": [
		780,
		17
	],
	"../pages/sources/sources.module": [
		781,
		16
	],
	"../pages/video-course/video-course.module": [
		782,
		15
	],
	"../pages/video-offline/video-offline.module": [
		783,
		0
	],
	"../pages/video-online/video-online.module": [
		784,
		3
	],
	"../pages/video-options/video-options.module": [
		785,
		2
	],
	"../pages/webinars/webinars.module": [
		786,
		1
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 251;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 32:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StorageServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var StorageServiceProvider = /** @class */ (function () {
    function StorageServiceProvider() {
        this.label = 'distributor';
        this.label_touch = 'infusion_id';
    }
    StorageServiceProvider.prototype.set = function (key, value) {
        window.localStorage[key] = value;
        return this.get(key);
    };
    StorageServiceProvider.prototype.get = function (key) {
        return window.localStorage[key] || null;
    };
    StorageServiceProvider.prototype.setObject = function (key, value) {
        window.localStorage[key] = JSON.stringify(value);
        return this.get(key);
    };
    StorageServiceProvider.prototype.getObject = function (key) {
        return JSON.parse(window.localStorage[key] || null);
    };
    StorageServiceProvider.prototype.clear = function () {
        this.set(this.label, null);
    };
    StorageServiceProvider.prototype.clearTouch = function () {
        this.set(this.label_touch, null);
    };
    StorageServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], StorageServiceProvider);
    return StorageServiceProvider;
}());

//# sourceMappingURL=storage-service.js.map

/***/ }),

/***/ 342:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PushNotificationsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_device_ngx__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__storage_service_storage_service__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var PushNotificationsProvider = /** @class */ (function () {
    function PushNotificationsProvider(http, platform, device, service, storage, modal) {
        this.http = http;
        this.platform = platform;
        this.device = device;
        this.service = service;
        this.storage = storage;
        this.modal = modal;
        this.oneSignalId = 'b98069fb-331e-45c5-8e6a-ab658c1bbaba';
        this.projectNumberGoogle = '469172553493';
    }
    PushNotificationsProvider.prototype.init = function (user) {
        var that = this;
        that.platform.ready().then(function (readySource) {
            if (that.device.uuid != null) {
                that.OneSignal = window["plugins"].OneSignal;
                that.user = user;
                that.OneSignal.getIds(function (ids) {
                    that.OneSignal.sendTag("platform", that.device.platform);
                    var params = {
                        user: that.user,
                        token: ids.pushToken,
                        player_ids: ids.userId,
                        manufacturer: that.device.manufacturer,
                        model: that.device.model,
                        platform: that.device.platform,
                        serial: that.device.serial,
                        uuid: that.device.uuid,
                        version: that.device.version,
                    };
                    that.service.postDataWithToken('contact-device', params);
                });
                that.OneSignal
                    .startInit(that.oneSignalId, that.projectNumberGoogle)
                    .handleNotificationReceived(function (data) {
                    var alert = {
                        title: data.payload.title,
                        message: data.payload.body,
                        data: data.payload.additionalData,
                    };
                    var alertModal = that.modal.create('AlertModalPage', { alert: alert, received: true });
                    alertModal.present();
                })
                    .handleNotificationOpened(function (data) {
                    var alert = {
                        title: data.notification.payload.title,
                        message: data.notification.payload.body,
                        data: data.notification.payload.additionalData,
                    };
                    var alertModal = that.modal.create('AlertModalPage', { alert: alert, received: true });
                    alertModal.present();
                })
                    .inFocusDisplaying(that.OneSignal.OSInFocusDisplayOption.None)
                    .endInit();
            }
        });
    };
    PushNotificationsProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["o" /* Platform */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_device_ngx__["a" /* Device */], __WEBPACK_IMPORTED_MODULE_4__api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_5__storage_service_storage_service__["a" /* StorageServiceProvider */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* ModalController */]])
    ], PushNotificationsProvider);
    return PushNotificationsProvider;
}());

//# sourceMappingURL=push-notifications.js.map

/***/ }),

/***/ 395:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DisclosureProdutsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing_ngx__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_clipboard_ngx__ = __webpack_require__(153);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_device_ngx__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_storage_service_storage_service__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var DisclosureProdutsPage = /** @class */ (function () {
    function DisclosureProdutsPage(navCtrl, navParams, service, loadingCtrl, alertCtrl, socialSharing, clipboard, translate, device, storage, loading) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.socialSharing = socialSharing;
        this.clipboard = clipboard;
        this.translate = translate;
        this.device = device;
        this.storage = storage;
        this.loading = loading;
        this.user = this.storage.getObject('distributor');
        this.links = [];
    }
    DisclosureProdutsPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        if (this.navParams.data.hasOwnProperty('links') == false) {
            this.translate.get("LOADING").subscribe(function (result) {
                _this.loader = _this.loading.create({
                    content: result,
                });
                _this.loader.present();
            });
            var data = {
                user_id: this.user.Id,
                affId: this.user.AffId,
                affCode: this.user.AffCode,
            };
            this.service.postData('listLinksForAffiliateTabs', data).then(function (res) {
                _this.loader.dismiss();
                _this.result = res;
                if (_this.result.status == true) {
                    _this.links = _this.result.links;
                }
                else {
                    var alert_1 = _this
                        .alertCtrl
                        .create({
                        title: _this.translate.instant('ERROR'),
                        subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                        buttons: ['Ok']
                    });
                    alert_1.present();
                }
            }, function (err) {
                _this.loader.dismiss();
                var alert = _this
                    .alertCtrl
                    .create({
                    title: _this.translate.instant('ERROR'),
                    subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                    buttons: ['Ok']
                });
                alert.present();
            });
        }
        else {
            this.links = this.navParams.data.links;
        }
    };
    DisclosureProdutsPage.prototype.copy = function (item) {
        var _this = this;
        var url = item.redirect + '?' + item.code;
        this.clipboard.copy(url).then(function (res) {
            var alert = _this
                .alertCtrl
                .create({
                title: 'Link',
                subTitle: _this.translate.instant('COPIED_SUCCESSFULLY'),
                buttons: ['Ok']
            });
            alert.present();
        }).catch(function (err) {
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    DisclosureProdutsPage.prototype.share = function (item, social) {
        var _this = this;
        this.translate.get("LOADING").subscribe(function (result) {
            _this.loader = _this.loadingCtrl.create({
                content: result,
            });
            _this.loader.present();
        });
        switch (social) {
            case 'facebook':
                var url = item.redirect + '?' + item.code;
                this.socialSharing.shareViaFacebookWithPasteMessageHint(null, null, url, item.name).then(function (res) {
                    _this.loader.dismiss();
                }).catch(function (err) {
                    _this.loader.dismiss();
                });
                break;
            case 'whatsapp':
                if (this.device.platform == "iOS") {
                    item.image = null;
                }
                this.socialSharing.shareViaWhatsApp(item.name, item.image, item.url).then(function (res) {
                    _this.loader.dismiss();
                }).catch(function (err) {
                    _this.loader.dismiss();
                });
                break;
            case 'twitter':
                this.socialSharing.shareViaTwitter(item.name, item.image, item.url).then(function (res) {
                    _this.loader.dismiss();
                }).catch(function (err) {
                    _this.loader.dismiss();
                });
                break;
        }
    };
    DisclosureProdutsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-disclosure-produts',template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/disclosure-produts/disclosure-produts.html"*/'<ion-header>\n  <ion-navbar color="dark">\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>{{ \'DISCLOSURE\' | translate }}</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <div class="card-background">\n    <ion-card>\n      <img src="assets/imgs/header/disclosure.png"/>\n      <div class="card-title">{{ \'DISCLOSURE_PRODUCTS\' | translate }}</div>\n    </ion-card>\n  </div>\n  <div *ngFor="let item of links">\n    <ion-card *ngIf="item.type == \'produtos\'">\n      <ion-card-header>{{item.name}}</ion-card-header>\n      <ion-card-content>\n        <img [src]="item.image" />\n      </ion-card-content>\n      <ion-grid>\n        <ion-row>\n          <ion-col col-12>\n            <button ion-button icon-start full (click)="copy(item)">\n              <ion-icon name="ios-link-outline"></ion-icon> {{ \'COPY_LINK\' | translate }}</button>\n            </ion-col>\n            <ion-col>\n              <button ion-button icon-only full (click)="share(item,\'facebook\')">\n                <ion-icon name="logo-facebook"></ion-icon>\n              </button>\n            </ion-col>\n            <ion-col>\n              <button ion-button icon-only full (click)="share(item,\'twitter\')">\n                <ion-icon name="logo-twitter"></ion-icon>\n              </button>\n            </ion-col>\n            <ion-col>\n              <button ion-button icon-only full (click)="share(item,\'whatsapp\')">\n                <ion-icon name="logo-whatsapp"></ion-icon>\n              </button>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </ion-card>\n    </div>\n  </ion-content>'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/disclosure-produts/disclosure-produts.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing_ngx__["a" /* SocialSharing */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_clipboard_ngx__["a" /* Clipboard */], __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_device_ngx__["a" /* Device */], __WEBPACK_IMPORTED_MODULE_7__providers_storage_service_storage_service__["a" /* StorageServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */]])
    ], DisclosureProdutsPage);
    return DisclosureProdutsPage;
}());

//# sourceMappingURL=disclosure-produts.js.map

/***/ }),

/***/ 396:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DisclosureBusinessPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_device_ngx__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_storage_service_storage_service__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_clipboard_ngx__ = __webpack_require__(153);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_social_sharing_ngx__ = __webpack_require__(53);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var DisclosureBusinessPage = /** @class */ (function () {
    function DisclosureBusinessPage(navCtrl, navParams, service, loadingCtrl, alertCtrl, socialSharing, clipboard, translate, device, storage, events, loading) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.socialSharing = socialSharing;
        this.clipboard = clipboard;
        this.translate = translate;
        this.device = device;
        this.storage = storage;
        this.events = events;
        this.loading = loading;
        this.user = this.storage.getObject('distributor');
        this.links = [];
    }
    DisclosureBusinessPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.events.subscribe('loadLinks', function (links) {
            if (links == false) {
                _this.translate.get("LOADING").subscribe(function (result) {
                    _this.loader = _this.loading.create({
                        content: result,
                    });
                    _this.loader.present();
                });
                var data = {
                    user_id: _this.user.Id,
                    affId: _this.user.AffId,
                    affCode: _this.user.AffCode,
                };
                _this.service.postData('listLinksForAffiliateTabs', data).then(function (res) {
                    _this.loader.dismiss();
                    _this.result = res;
                    if (_this.result.status == true) {
                        _this.links = _this.result.links;
                    }
                    else {
                        var alert_1 = _this
                            .alertCtrl
                            .create({
                            title: _this.translate.instant('ERROR'),
                            subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                            buttons: ['Ok']
                        });
                        alert_1.present();
                    }
                }, function (err) {
                    _this.loader.dismiss();
                    var alert = _this
                        .alertCtrl
                        .create({
                        title: _this.translate.instant('ERROR'),
                        subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                        buttons: ['Ok']
                    });
                    alert.present();
                });
            }
            else {
                _this.links = links;
            }
        });
    };
    DisclosureBusinessPage.prototype.copy = function (item) {
        var _this = this;
        var url = item.redirect + '?' + item.code;
        this.clipboard.copy(url).then(function (res) {
            var alert = _this
                .alertCtrl
                .create({
                title: 'Link',
                subTitle: _this.translate.instant('COPIED_SUCCESSFULLY'),
                buttons: ['Ok']
            });
            alert.present();
        }).catch(function (err) {
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    DisclosureBusinessPage.prototype.share = function (item, social) {
        var _this = this;
        this.translate.get("LOADING").subscribe(function (result) {
            _this.loader = _this.loadingCtrl.create({
                content: result,
            });
            _this.loader.present();
        });
        switch (social) {
            case 'facebook':
                var url = item.redirect + '?' + item.code;
                this.socialSharing.shareViaFacebookWithPasteMessageHint(null, null, url, item.name).then(function (res) {
                    _this.loader.dismiss();
                }).catch(function (err) {
                    _this.loader.dismiss();
                });
                break;
            case 'whatsapp':
                if (this.device.platform == "iOS") {
                    item.image = null;
                }
                this.socialSharing.shareViaWhatsApp(item.name, item.image, item.url).then(function (res) {
                    _this.loader.dismiss();
                }).catch(function (err) {
                    _this.loader.dismiss();
                });
                break;
            case 'twitter':
                this.socialSharing.shareViaTwitter(item.name, item.image, item.url).then(function (res) {
                    _this.loader.dismiss();
                }).catch(function (err) {
                    _this.loader.dismiss();
                });
                break;
        }
    };
    DisclosureBusinessPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-disclosure-business',template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/disclosure-business/disclosure-business.html"*/'<ion-header>\n  <ion-navbar color="dark">\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>{{ \'DISCLOSURE\' | translate }}</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <div class="card-background">\n    <ion-card>\n      <img src="assets/imgs/header/disclosure.png"/>\n      <div class="card-title">{{ \'DISCLOSURE_BUSINESS\' | translate }}</div>\n    </ion-card>\n  </div>\n  <div *ngFor="let item of links">\n    <ion-card *ngIf="item.type == \'negocios\'">\n      <ion-card-header>{{item.name}}</ion-card-header>\n      <ion-card-content>\n        <img [src]="item.image" />\n      </ion-card-content>\n      <ion-grid>\n        <ion-row>\n          <ion-col col-12>\n            <button ion-button icon-start full (click)="copy(item)">\n              <ion-icon name="ios-link-outline"></ion-icon> {{ \'COPY_LINK\' | translate }}</button>\n            </ion-col>\n            <ion-col>\n              <button ion-button icon-only full (click)="share(item,\'facebook\')">\n                <ion-icon name="logo-facebook"></ion-icon>\n              </button>\n            </ion-col>\n            <ion-col>\n              <button ion-button icon-only full (click)="share(item,\'twitter\')">\n                <ion-icon name="logo-twitter"></ion-icon>\n              </button>\n            </ion-col>\n            <ion-col>\n              <button ion-button icon-only full (click)="share(item,\'whatsapp\')">\n                <ion-icon name="logo-whatsapp"></ion-icon>\n              </button>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </ion-card>\n    </div>\n  </ion-content>'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/disclosure-business/disclosure-business.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_social_sharing_ngx__["a" /* SocialSharing */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_clipboard_ngx__["a" /* Clipboard */], __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_device_ngx__["a" /* Device */], __WEBPACK_IMPORTED_MODULE_5__providers_storage_service_storage_service__["a" /* StorageServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */]])
    ], DisclosureBusinessPage);
    return DisclosureBusinessPage;
}());

//# sourceMappingURL=disclosure-business.js.map

/***/ }),

/***/ 398:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VimeoServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__vimeo_player__ = __webpack_require__(717);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__database_database__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var VimeoServiceProvider = /** @class */ (function () {
    function VimeoServiceProvider(http, service, translate, loadingCtrl, database, events, alertCtrl) {
        var _this = this;
        this.http = http;
        this.service = service;
        this.translate = translate;
        this.loadingCtrl = loadingCtrl;
        this.database = database;
        this.events = events;
        this.alertCtrl = alertCtrl;
        this.duration = 0;
        this.database.getUser().then(function (user) {
            _this.user = user;
        });
        this.translate.get("LOADING").subscribe(function (result) {
            _this.loader = _this.loadingCtrl.create({
                content: result,
            });
            _this.loader.present();
        });
    }
    VimeoServiceProvider.prototype.initPlayer = function (video) {
        this.video = video;
        this.player = new __WEBPACK_IMPORTED_MODULE_5__vimeo_player__["a" /* default */]('video-embed', {});
        var that = this;
        if (this.video.duration == null) {
            this.player.getDuration().then(function (seconds) {
                that.video.duration = seconds;
                that.duration = seconds;
            });
        }
        this.player.ready().then(function (res) {
            that.loader.dismiss();
            if (that.video.watch_time == 0 || that.video.progress == 100) {
                that.player.play();
            }
            else {
                that.player.setCurrentTime(that.video.watch_time).then(function (res) {
                    that.player.play();
                });
            }
        });
        this.player.on('pause', function (res) {
            that.saveWatchTime(Math.round(res.seconds), 0);
        });
        this.player.on('ended', function (res) {
            that.saveWatchTime(Math.round(res.seconds), 1);
            that.events.publish('endVideo', true);
        });
    };
    VimeoServiceProvider.prototype.stopPlayer = function () {
        var that = this;
        this.player.getCurrentTime().then(function (seconds) {
            that.saveWatchTime(Math.round(seconds), 0);
        });
    };
    VimeoServiceProvider.prototype.saveWatchTime = function (seconds, completed) {
        var _this = this;
        var params = {
            video_id: this.video.id,
            user: this.user,
            watch_time: seconds,
            completed: completed,
            duration: this.duration,
        };
        this.service.postDataWithToken('userVideo-register', params).then(function (res) {
        }, function (err) {
            _this.loader.dismiss();
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    VimeoServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_2__api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_6__database_database__["a" /* DatabaseProvider */], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["a" /* AlertController */]])
    ], VimeoServiceProvider);
    return VimeoServiceProvider;
}());

//# sourceMappingURL=vimeo-service.js.map

/***/ }),

/***/ 399:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(400);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(404);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__node_modules_angular_core__ = __webpack_require__(0);



Object(__WEBPACK_IMPORTED_MODULE_2__node_modules_angular_core__["_15" /* enableProdMode */])();
Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 404:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export createTranslateLoader */
/* unused harmony export myCustomAudioProviderFactory */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen_ngx__ = __webpack_require__(389);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar_ngx__ = __webpack_require__(390);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common_http__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_http__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ngx_order_pipe__ = __webpack_require__(394);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__node_modules_ngx_translate_http_loader__ = __webpack_require__(735);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_storage_service_storage_service__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__node_modules_ionic_native_device_ngx__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__node_modules_ionic_native_file_ngx__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__node_modules_ionic_native_sqlite_ngx__ = __webpack_require__(254);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__node_modules_ionic_native_network_ngx__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__node_modules_ionic_native_file_transfer_ngx__ = __webpack_require__(179);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__providers_vimeo_service_vimeo_service__ = __webpack_require__(398);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ionic_storage__ = __webpack_require__(737);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19_ng_lazyload_image__ = __webpack_require__(397);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_clipboard_ngx__ = __webpack_require__(153);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_native_social_sharing_ngx__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_native_document_viewer_ngx__ = __webpack_require__(343);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23_ionic_audio__ = __webpack_require__(393);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_native_media_ngx__ = __webpack_require__(741);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__node_modules_angular_common__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__ionic_native_in_app_browser_ngx__ = __webpack_require__(180);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__ionic_native_fingerprint_aio_ngx__ = __webpack_require__(341);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28_brmasker_ionic_3__ = __webpack_require__(742);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__node_modules_ionic_native_contacts_ngx__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__ionic_native_email_composer_ngx__ = __webpack_require__(745);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__ionic_native_geolocation_ngx__ = __webpack_require__(344);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__ionic_native_launch_navigator_ngx__ = __webpack_require__(188);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__app_component__ = __webpack_require__(746);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_login_login__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_alerts_alerts__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__pages_daily_daily__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__pages_first_first__ = __webpack_require__(176);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__pages_courses_courses__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__pages_video_course_video_course__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__pages_sources_sources__ = __webpack_require__(181);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__pages_source_video_source_video__ = __webpack_require__(185);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__pages_source_audio_source_audio__ = __webpack_require__(177);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__pages_source_pdf_source_pdf__ = __webpack_require__(178);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__pages_contacts_contacts__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__pages_contact_details_contact_details__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__pages_daily_audio_daily_audio__ = __webpack_require__(175);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__pages_daily_audio_produto_daily_audio_produto__ = __webpack_require__(183);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__pages_daily_audio_motivacional_daily_audio_motivacional__ = __webpack_require__(184);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__pages_disclosure_business_disclosure_business__ = __webpack_require__(396);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__pages_disclosure_produts_disclosure_produts__ = __webpack_require__(395);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51__pages_contacts_complete_contacts_complete__ = __webpack_require__(191);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_52__pages_contacts_draft_contacts_draft__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_53__providers_push_notifications_push_notifications__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_54__providers_auth_service_auth_service__ = __webpack_require__(747);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_55__providers_database_database__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_56__providers_interceptor_interceptor__ = __webpack_require__(748);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_57__pages_leadership_leadership__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_58__pages_leadership_month_leadership_month__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_59__pages_leadership_list_leadership_list__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_60__pages_meeting_meeting__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_61__pages_meeting_map_meeting_map__ = __webpack_require__(187);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_62__pages_meeting_list_meeting_list__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_63__directives_progress_progress__ = __webpack_require__(749);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




















// import { VideoOfflineProvider } from '../providers/video-ofngxfline/video-offline';















// import { FollowPage } from '../pages/follow/follow';
// import { WebinarsPage } from '../pages/webinars/webinars';





// import { DisclosurePage } from '../pages/disclosure/disclosure';
// import { NotesPage } from '../pages/notes/notes';
























function createTranslateLoader(http) {
    return new __WEBPACK_IMPORTED_MODULE_9__node_modules_ngx_translate_http_loader__["a" /* TranslateHttpLoader */](http, './assets/i18n/', '.json');
}
function myCustomAudioProviderFactory() {
    return (window.hasOwnProperty('cordova')) ? new __WEBPACK_IMPORTED_MODULE_23_ionic_audio__["c" /* CordovaMediaProvider */]() : new __WEBPACK_IMPORTED_MODULE_23_ionic_audio__["e" /* WebAudioProvider */]();
}
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_33__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_34__pages_login_login__["a" /* LoginPage */],
                // FollowPage,
                // WebinarsPage,
                __WEBPACK_IMPORTED_MODULE_35__pages_alerts_alerts__["a" /* AlertsPage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_daily_daily__["a" /* DailyPage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_first_first__["a" /* FirstPage */],
                __WEBPACK_IMPORTED_MODULE_38__pages_courses_courses__["a" /* CoursesPage */],
                __WEBPACK_IMPORTED_MODULE_39__pages_video_course_video_course__["a" /* VideoCoursePage */],
                // DisclosurePage,
                // NotesPage,
                __WEBPACK_IMPORTED_MODULE_40__pages_sources_sources__["a" /* SourcesPage */],
                __WEBPACK_IMPORTED_MODULE_41__pages_source_video_source_video__["a" /* SourceVideoPage */],
                __WEBPACK_IMPORTED_MODULE_42__pages_source_audio_source_audio__["a" /* SourceAudioPage */],
                __WEBPACK_IMPORTED_MODULE_43__pages_source_pdf_source_pdf__["a" /* SourcePdfPage */],
                __WEBPACK_IMPORTED_MODULE_44__pages_contacts_contacts__["a" /* ContactsPage */],
                __WEBPACK_IMPORTED_MODULE_46__pages_daily_audio_daily_audio__["a" /* DailyAudioPage */],
                __WEBPACK_IMPORTED_MODULE_47__pages_daily_audio_produto_daily_audio_produto__["a" /* DailyAudioProdutoPage */],
                __WEBPACK_IMPORTED_MODULE_48__pages_daily_audio_motivacional_daily_audio_motivacional__["a" /* DailyAudioMotivacionalPage */],
                __WEBPACK_IMPORTED_MODULE_49__pages_disclosure_business_disclosure_business__["a" /* DisclosureBusinessPage */],
                __WEBPACK_IMPORTED_MODULE_50__pages_disclosure_produts_disclosure_produts__["a" /* DisclosureProdutsPage */],
                __WEBPACK_IMPORTED_MODULE_51__pages_contacts_complete_contacts_complete__["a" /* ContactsCompletePage */],
                __WEBPACK_IMPORTED_MODULE_52__pages_contacts_draft_contacts_draft__["a" /* ContactsDraftPage */],
                __WEBPACK_IMPORTED_MODULE_45__pages_contact_details_contact_details__["a" /* ContactDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_57__pages_leadership_leadership__["a" /* LeadershipPage */],
                __WEBPACK_IMPORTED_MODULE_58__pages_leadership_month_leadership_month__["a" /* LeadershipMonthPage */],
                __WEBPACK_IMPORTED_MODULE_59__pages_leadership_list_leadership_list__["a" /* LeadershipListPage */],
                __WEBPACK_IMPORTED_MODULE_60__pages_meeting_meeting__["a" /* MeetingPage */],
                __WEBPACK_IMPORTED_MODULE_61__pages_meeting_map_meeting_map__["a" /* MeetingMapPage */],
                __WEBPACK_IMPORTED_MODULE_62__pages_meeting_list_meeting_list__["a" /* MeetingListPage */],
                __WEBPACK_IMPORTED_MODULE_63__directives_progress_progress__["a" /* ProgressDirective */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_23_ionic_audio__["d" /* IonicAudioModule */].forRoot(__WEBPACK_IMPORTED_MODULE_23_ionic_audio__["f" /* defaultAudioProviderFactory */]),
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_7_ngx_order_pipe__["a" /* OrderModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_33__app_component__["a" /* MyApp */], {
                    backButtonText: '',
                    iconMode: 'ios',
                    backButtonIcon: 'ios-arrow-back'
                }, {
                    links: [
                        { loadChildren: '../pages/alert-modal/alert-modal.module#AlertModalPageModule', name: 'AlertModalPage', segment: 'alert-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/alerts/alerts.module#AlertsPageModule', name: 'AlertsPage', segment: 'alerts', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/audio-offline/audio-offline.module#AudioOfflinePageModule', name: 'AudioOfflinePage', segment: 'audio-offline', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/audio-online/audio-online.module#AudioOnlinePageModule', name: 'AudioOnlinePage', segment: 'audio-online', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/contact-importer/contact-importer.module#ContactImporterPageModule', name: 'ContactImporterPage', segment: 'contact-importer', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/contacts-complete/contacts-complete.module#ContactsCompletePageModule', name: 'ContactsCompletePage', segment: 'contacts-complete', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/contacts-draft/contacts-draft.module#ContactsDraftPageModule', name: 'ContactsDraftPage', segment: 'contacts-draft', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/contacts/contacts.module#ContactsPageModule', name: 'ContactsPage', segment: 'contacts', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/courses/courses.module#CoursesPageModule', name: 'CoursesPage', segment: 'courses', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/daily-audio-motivacional/daily-audio-motivacional.module#DailyAudioMotivacionalPageModule', name: 'DailyAudioMotivacionalPage', segment: 'daily-audio-motivacional', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/daily-audio-produto/daily-audio-produto.module#DailyAudioProdutoPageModule', name: 'DailyAudioProdutoPage', segment: 'daily-audio-produto', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/daily-audio/daily-audio.module#DailyAudioPageModule', name: 'DailyAudioPage', segment: 'daily-audio', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/daily/daily.module#DailyPageModule', name: 'DailyPage', segment: 'daily', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/disclosure-produts/disclosure-produts.module#DisclosureProdutsPageModule', name: 'DisclosureProdutsPage', segment: 'disclosure-produts', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/disclosure/disclosure.module#DisclosurePageModule', name: 'DisclosurePage', segment: 'disclosure', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/first/first.module#FirstPageModule', name: 'FirstPage', segment: 'first', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/disclosure-business/disclosure-business.module#DisclosureBusinessPageModule', name: 'DisclosureBusinessPage', segment: 'disclosure-business', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/follow/follow.module#FollowPageModule', name: 'FollowPage', segment: 'follow', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/language/language.module#LanguagePageModule', name: 'LanguagePage', segment: 'language', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/leadership-list/leadership-list.module#LeadershipListPageModule', name: 'LeadershipListPage', segment: 'leadership-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/leadership-month/leadership-month.module#LeadershipMonthPageModule', name: 'LeadershipMonthPage', segment: 'leadership-month', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/leadership/leadership.module#LeadershipPageModule', name: 'LeadershipPage', segment: 'leadership', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/meeting-details/meeting-details.module#MeetingDetailsPageModule', name: 'MeetingDetailsPage', segment: 'meeting-details', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/meeting-list/meeting-list.module#MeetingListPageModule', name: 'MeetingListPage', segment: 'meeting-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/meeting-map/meeting-map.module#MeetingMapPageModule', name: 'MeetingMapPage', segment: 'meeting-map', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/meeting/meeting.module#MeetingPageModule', name: 'MeetingPage', segment: 'meeting', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/notes/notes.module#NotesPageModule', name: 'NotesPage', segment: 'notes', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/recovery/recovery.module#RecoveryPageModule', name: 'RecoveryPage', segment: 'recovery', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/register/register.module#RegisterPageModule', name: 'RegisterPage', segment: 'register', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/source-audio/source-audio.module#SourceAudioPageModule', name: 'SourceAudioPage', segment: 'source-audio', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/source-video/source-video.module#SourceVideoPageModule', name: 'SourceVideoPage', segment: 'source-video', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sources/sources.module#SourcesPageModule', name: 'SourcesPage', segment: 'sources', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/video-course/video-course.module#VideoCoursePageModule', name: 'VideoCoursePage', segment: 'video-course', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/video-offline/video-offline.module#VideoOfflinePageModule', name: 'VideoOfflinePage', segment: 'video-offline', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/video-online/video-online.module#VideoOnlinePageModule', name: 'VideoOnlinePage', segment: 'video-online', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/video-options/video-options.module#VideoOptionsPageModule', name: 'VideoOptionsPage', segment: 'video-options', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/webinars/webinars.module#WebinarsPageModule', name: 'WebinarsPage', segment: 'webinars', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/source-pdf/source-pdf.module#SourcePdfPageModule', name: 'SourcePdfPage', segment: 'source-pdf', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/contact-details/contact-details.module#ContactDetailsPageModule', name: 'ContactDetailsPage', segment: 'contact-details', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_5__angular_common_http__["c" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_25__node_modules_angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_28_brmasker_ionic_3__["a" /* BrMaskerModule */],
                __WEBPACK_IMPORTED_MODULE_19_ng_lazyload_image__["a" /* LazyLoadImageModule */],
                __WEBPACK_IMPORTED_MODULE_8__node_modules_ngx_translate_core__["b" /* TranslateModule */].forRoot({
                    loader: {
                        provide: __WEBPACK_IMPORTED_MODULE_8__node_modules_ngx_translate_core__["a" /* TranslateLoader */],
                        useFactory: createTranslateLoader,
                        deps: [__WEBPACK_IMPORTED_MODULE_5__angular_common_http__["b" /* HttpClient */]]
                    },
                }),
                __WEBPACK_IMPORTED_MODULE_18__ionic_storage__["a" /* IonicStorageModule */].forRoot()
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_33__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_34__pages_login_login__["a" /* LoginPage */],
                // FollowPage,
                // WebinarsPage,
                __WEBPACK_IMPORTED_MODULE_35__pages_alerts_alerts__["a" /* AlertsPage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_daily_daily__["a" /* DailyPage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_first_first__["a" /* FirstPage */],
                __WEBPACK_IMPORTED_MODULE_38__pages_courses_courses__["a" /* CoursesPage */],
                __WEBPACK_IMPORTED_MODULE_39__pages_video_course_video_course__["a" /* VideoCoursePage */],
                // DisclosurePage,
                // NotesPage,
                __WEBPACK_IMPORTED_MODULE_40__pages_sources_sources__["a" /* SourcesPage */],
                __WEBPACK_IMPORTED_MODULE_41__pages_source_video_source_video__["a" /* SourceVideoPage */],
                __WEBPACK_IMPORTED_MODULE_42__pages_source_audio_source_audio__["a" /* SourceAudioPage */],
                __WEBPACK_IMPORTED_MODULE_43__pages_source_pdf_source_pdf__["a" /* SourcePdfPage */],
                __WEBPACK_IMPORTED_MODULE_44__pages_contacts_contacts__["a" /* ContactsPage */],
                __WEBPACK_IMPORTED_MODULE_45__pages_contact_details_contact_details__["a" /* ContactDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_46__pages_daily_audio_daily_audio__["a" /* DailyAudioPage */],
                __WEBPACK_IMPORTED_MODULE_47__pages_daily_audio_produto_daily_audio_produto__["a" /* DailyAudioProdutoPage */],
                __WEBPACK_IMPORTED_MODULE_48__pages_daily_audio_motivacional_daily_audio_motivacional__["a" /* DailyAudioMotivacionalPage */],
                __WEBPACK_IMPORTED_MODULE_49__pages_disclosure_business_disclosure_business__["a" /* DisclosureBusinessPage */],
                __WEBPACK_IMPORTED_MODULE_50__pages_disclosure_produts_disclosure_produts__["a" /* DisclosureProdutsPage */],
                __WEBPACK_IMPORTED_MODULE_51__pages_contacts_complete_contacts_complete__["a" /* ContactsCompletePage */],
                __WEBPACK_IMPORTED_MODULE_52__pages_contacts_draft_contacts_draft__["a" /* ContactsDraftPage */],
                __WEBPACK_IMPORTED_MODULE_57__pages_leadership_leadership__["a" /* LeadershipPage */],
                __WEBPACK_IMPORTED_MODULE_58__pages_leadership_month_leadership_month__["a" /* LeadershipMonthPage */],
                __WEBPACK_IMPORTED_MODULE_59__pages_leadership_list_leadership_list__["a" /* LeadershipListPage */],
                __WEBPACK_IMPORTED_MODULE_60__pages_meeting_meeting__["a" /* MeetingPage */],
                __WEBPACK_IMPORTED_MODULE_61__pages_meeting_map_meeting_map__["a" /* MeetingMapPage */],
                __WEBPACK_IMPORTED_MODULE_62__pages_meeting_list_meeting_list__["a" /* MeetingListPage */],
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar_ngx__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen_ngx__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_12__node_modules_ionic_native_device_ngx__["a" /* Device */],
                __WEBPACK_IMPORTED_MODULE_13__node_modules_ionic_native_file_ngx__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_15__node_modules_ionic_native_network_ngx__["a" /* Network */],
                __WEBPACK_IMPORTED_MODULE_16__node_modules_ionic_native_file_transfer_ngx__["a" /* FileTransfer */],
                __WEBPACK_IMPORTED_MODULE_20__ionic_native_clipboard_ngx__["a" /* Clipboard */],
                __WEBPACK_IMPORTED_MODULE_21__ionic_native_social_sharing_ngx__["a" /* SocialSharing */],
                __WEBPACK_IMPORTED_MODULE_22__ionic_native_document_viewer_ngx__["a" /* DocumentViewer */],
                __WEBPACK_IMPORTED_MODULE_23_ionic_audio__["c" /* CordovaMediaProvider */],
                __WEBPACK_IMPORTED_MODULE_23_ionic_audio__["e" /* WebAudioProvider */],
                __WEBPACK_IMPORTED_MODULE_26__ionic_native_in_app_browser_ngx__["a" /* InAppBrowser */],
                __WEBPACK_IMPORTED_MODULE_27__ionic_native_fingerprint_aio_ngx__["a" /* FingerprintAIO */],
                __WEBPACK_IMPORTED_MODULE_29__node_modules_ionic_native_contacts_ngx__["a" /* Contacts */],
                __WEBPACK_IMPORTED_MODULE_10__providers_storage_service_storage_service__["a" /* StorageServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_11__providers_api_service_api_service__["a" /* ApiServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_17__providers_vimeo_service_vimeo_service__["a" /* VimeoServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_30__ionic_native_email_composer_ngx__["a" /* EmailComposer */],
                __WEBPACK_IMPORTED_MODULE_31__ionic_native_geolocation_ngx__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_32__ionic_native_launch_navigator_ngx__["a" /* LaunchNavigator */],
                // VideoOfflineProvider,
                __WEBPACK_IMPORTED_MODULE_24__ionic_native_media_ngx__["a" /* Media */],
                __WEBPACK_IMPORTED_MODULE_14__node_modules_ionic_native_sqlite_ngx__["a" /* SQLite */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicErrorHandler */] },
                { provide: __WEBPACK_IMPORTED_MODULE_5__angular_common_http__["a" /* HTTP_INTERCEPTORS */], useClass: __WEBPACK_IMPORTED_MODULE_56__providers_interceptor_interceptor__["a" /* InterceptorProvider */], multi: true },
                // {provide: ErrorHandler, useClass: SentryIonicErrorHandler},
                __WEBPACK_IMPORTED_MODULE_53__providers_push_notifications_push_notifications__["a" /* PushNotificationsProvider */],
                __WEBPACK_IMPORTED_MODULE_54__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_55__providers_database_database__["a" /* DatabaseProvider */],
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_1__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 74:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_angular_forms__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__node_modules_ionic_native_contacts_ngx__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__contacts_contacts__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_database_database__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_social_sharing_ngx__ = __webpack_require__(53);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var ContactDetailsPage = /** @class */ (function () {
    function ContactDetailsPage(navCtrl, navParams, formBuilder, service, loadingCtrl, translate, alertCtrl, modal, platform, contacts, events, database, socialSharing) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.service = service;
        this.loadingCtrl = loadingCtrl;
        this.translate = translate;
        this.alertCtrl = alertCtrl;
        this.modal = modal;
        this.platform = platform;
        this.contacts = contacts;
        this.events = events;
        this.database = database;
        this.socialSharing = socialSharing;
        this.filter = ["name.formatted"];
        this.dados = navParams.data.contact;
        this.manual = false;
        this.change = false;
        this.importBack = false;
        this.id = null;
        this.url = 'http://192.168.15.2:8002/share/';
        this.database.getUser().then(function (user) {
            _this.user = user;
        });
        this.contactGroups = [
            { 'id': '1', 'name': 'FAMILY' },
            { 'id': '2', 'name': 'FRIENDS' },
            { 'id': '3', 'name': 'JOB' },
            { 'id': '4', 'name': 'SCHOOL' },
            { 'id': '5', 'name': 'COLLEGE' },
            { 'id': '6', 'name': 'TRAVELS' },
            { 'id': '7', 'name': 'SERVICE_PROVIDER' },
            { 'id': '8', 'name': 'PARTIES' },
            { 'id': '9', 'name': 'OTHERS' },
        ];
        this.contactTypes = [
            { 'id': '1', 'name': 'HOT' },
            { 'id': '2', 'name': 'WARM' },
            { 'id': '3', 'name': 'COLD' },
        ];
        this.platform.ready().then(function (readySource) {
            // console.log('Width: ' + platform.width());
            // console.log('Height: ' + platform.height());
        });
        this.formGroup = formBuilder.group({
            name: ['', __WEBPACK_IMPORTED_MODULE_3__node_modules_angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__node_modules_angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_3__node_modules_angular_forms__["f" /* Validators */].minLength(3)])],
            nickname: ['', __WEBPACK_IMPORTED_MODULE_3__node_modules_angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__node_modules_angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_3__node_modules_angular_forms__["f" /* Validators */].minLength(3)])],
            phone: ['', __WEBPACK_IMPORTED_MODULE_3__node_modules_angular_forms__["f" /* Validators */].required],
            email: ['', __WEBPACK_IMPORTED_MODULE_3__node_modules_angular_forms__["f" /* Validators */].email],
            group: [],
            types: [],
            observations: [],
            entrepreneur: [],
            credility: [],
            networking: [],
            empowerment: [],
            dreamer: [],
            invitation: ['', __WEBPACK_IMPORTED_MODULE_3__node_modules_angular_forms__["f" /* Validators */].minLength(10)],
            plan: ['', __WEBPACK_IMPORTED_MODULE_3__node_modules_angular_forms__["f" /* Validators */].minLength(10)],
            fup: ['', __WEBPACK_IMPORTED_MODULE_3__node_modules_angular_forms__["f" /* Validators */].minLength(10)],
            closing: ['', __WEBPACK_IMPORTED_MODULE_3__node_modules_angular_forms__["f" /* Validators */].minLength(10)],
            register: [],
            comments: [],
        });
        this.name = this.formGroup.controls['name'];
        this.nickname = this.formGroup.controls['nickname'];
        this.phone = this.formGroup.controls['phone'];
        this.email = this.formGroup.controls['email'];
        this.group = this.formGroup.controls['group'];
        this.types = this.formGroup.controls['types'];
        this.observations = this.formGroup.controls['observations'];
        this.entrepreneur = this.formGroup.controls['entrepreneur'];
        this.credility = this.formGroup.controls['credility'];
        this.networking = this.formGroup.controls['networking'];
        this.empowerment = this.formGroup.controls['empowerment'];
        this.dreamer = this.formGroup.controls['dreamer'];
        this.invitation = this.formGroup.controls['invitation'];
        this.plan = this.formGroup.controls['plan'];
        this.fup = this.formGroup.controls['fup'];
        this.closing = this.formGroup.controls['closing'];
        this.register = this.formGroup.controls['register'];
        this.comments = this.formGroup.controls['comments'];
        if (this.dados != null) {
            this.loadData();
        }
    }
    ContactDetailsPage.prototype.loadData = function () {
        this.name.setValue(this.dados.first_name);
        this.nickname.setValue(this.dados.last_name);
        this.email.setValue(this.dados.email);
        this.importBack = this.dados.import;
        if (this.importBack) {
            this.dados.phone = this.dados.oldPhone;
            this.formatPhone(this.dados);
        }
        else {
            this.formatPhone(this.dados);
        }
        this.phone.setValue(this.dados.phone);
        this.group.setValue(this.dados.grupo == undefined ? null : this.dados.grupo);
        this.types.setValue(this.dados.type == undefined ? null : this.dados.type);
        this.entrepreneur.setValue(this.dados.entrepreneur == '0' || this.dados.entrepreneur == undefined ? false : true);
        this.credility.setValue(this.dados.credility == '0' || this.dados.credility == undefined ? false : true);
        this.networking.setValue(this.dados.networking == '0' || this.dados.networking == undefined ? false : true);
        this.empowerment.setValue(this.dados.empowerment == '0' || this.dados.empowerment == undefined ? false : true);
        this.dreamer.setValue(this.dados.dreamer == '0' || this.dados.dreamer == undefined ? false : true);
        this.register.setValue(this.dados.register == '0' || this.dados.register == undefined ? false : true);
        this.invitation.setValue(this.dados.invitation == undefined ? null : this.dados.invitation);
        this.plan.setValue(this.dados.plan == undefined ? null : this.dados.plan);
        this.fup.setValue(this.dados.fup == undefined ? null : this.dados.fup);
        this.closing.setValue(this.dados.closing == undefined ? null : this.dados.closing);
        this.observations.setValue(this.dados.observation == undefined ? '' : this.dados.observation);
        this.comments.setValue(this.dados.comments == undefined ? '' : this.dados.comments);
        this.id = this.dados.id == undefined ? null : this.dados.id;
        this.manual = true;
    };
    ContactDetailsPage.prototype.formatPhone = function (item) {
        if (item.phone.length > 0) {
            if (item.phone.length <= 9) {
                if (item.phone.length == 9) {
                    item.phone = item.phone.substr(0, 5) + "-" + item.phone.substr(5);
                }
                else if (item.phone.length == 8) {
                    item.phone = item.phone.substr(0, 4) + "-" + item.phone.substr(4);
                }
            }
            else {
                var phone = item.phone.match(/\d/g).join("");
                if (phone.length == 14) {
                    var country = phone.substr(0, 3), ddd = phone.substr(3, 2), number = phone.substr(5);
                    if (number.length == 9) {
                        number = number.substr(0, 5) + "-" + number.substr(5);
                    }
                    else if (number.length == 8) {
                        number = number.substr(0, 4) + "-" + number.substr(4);
                    }
                    item.phone = country + ' ' + ddd + ' ' + number;
                }
                else if (phone.length == 13) {
                    var country = phone.substr(0, 2), ddd = phone.substr(2, 2), number = phone.substr(4);
                    if (number.length == 9) {
                        number = number.substr(0, 5) + "-" + number.substr(5);
                    }
                    else if (number.length == 8) {
                        number = number.substr(0, 4) + "-" + number.substr(4);
                    }
                    item.phone = '+' + country + ' ' + ddd + ' ' + number;
                }
                else if (phone.length == 12) {
                    if (phone.substr(0, 1) == '0') {
                        var ddd = phone.substr(0, 3), number = phone.substr(3);
                        if (number.length == 9) {
                            number = number.substr(0, 5) + "-" + number.substr(5);
                        }
                        else if (number.length == 8) {
                            number = number.substr(0, 4) + "-" + number.substr(4);
                        }
                        item.phone = ddd + ' ' + number;
                    }
                    else {
                        var country = phone.substr(0, 2), ddd = phone.substr(2, 2), number = phone.substr(4);
                        if (number.length == 9) {
                            number = number.substr(0, 5) + "-" + number.substr(5);
                        }
                        else if (number.length == 8) {
                            number = number.substr(0, 4) + "-" + number.substr(4);
                        }
                        item.phone = '+' + country + ' ' + ddd + ' ' + number;
                    }
                }
                else if (phone.length == 11) {
                    if (phone.substr(0, 1) == '0') {
                        var ddd = phone.substr(0, 3), number = phone.substr(3);
                        if (number.length == 9) {
                            number = number.substr(0, 5) + "-" + number.substr(5);
                        }
                        else if (number.length == 8) {
                            number = number.substr(0, 4) + "-" + number.substr(4);
                        }
                        item.phone = ddd + ' ' + number;
                    }
                    else {
                        var ddd = phone.substr(0, 2), number = phone.substr(2);
                        if (number.length == 9) {
                            number = number.substr(0, 5) + "-" + number.substr(5);
                        }
                        else if (number.length == 8) {
                            number = number.substr(0, 4) + "-" + number.substr(4);
                        }
                        item.phone = ddd + ' ' + number;
                    }
                }
                else {
                    var ddd = phone.substr(0, 2), number = phone.substr(2);
                    if (number.length == 9) {
                        number = number.substr(0, 5) + "-" + number.substr(5);
                    }
                    else if (number.length == 8) {
                        number = number.substr(0, 4) + "-" + number.substr(4);
                    }
                    item.phone = ddd + ' ' + number;
                }
            }
        }
    };
    ContactDetailsPage.prototype.manualEnabled = function () {
        this.manual = true;
    };
    ContactDetailsPage.prototype.search = function () {
        var _this = this;
        var modalOnline = this.modal.create('ContactImporterPage');
        modalOnline.onDidDismiss(function (contact) {
            if (contact != undefined) {
                var fullName = contact.name.formatted.split(' '), emailImport = contact.emails != null ? contact.emails[0].value : '', phoneImport = contact.phoneNumbers != null ? contact.phoneNumbers[0].value : '';
                _this.name.setValue(fullName.shift());
                _this.nickname.setValue(fullName.join(' '));
                _this.email.setValue(emailImport);
                var phone = phoneImport.match(/\d/g).join(""), ddd = phone.substr(0, 2), number = phone.substr(2);
                if (number.length == 9) {
                    number = number.substr(0, 5) + "-" + number.substr(5);
                }
                else {
                    number = number.substr(0, 4) + "-" + number.substr(4);
                }
                _this.phone.setValue('(' + ddd + ') ' + number);
                _this.manual = true;
                _this.importBack = true;
                _this.dados = contact;
                _this.dados.import_id = contact.id;
            }
        });
        modalOnline.present();
    };
    ContactDetailsPage.prototype.import = function (val) {
        var _this = this;
        if (val === void 0) { val = ''; }
        this.loader = this.loadingCtrl.create({
            content: this.translate.instant('LOADING'),
        });
        this.loader.present();
        this.contacts.find(this.filter, { filter: val, multiple: true, hasPhoneNumber: true }).then(function (conts) {
            conts.sort(function (a, b) {
                if (a.name.formatted < b.name.formatted)
                    return -1;
                if (a.name.formatted > b.name.formatted)
                    return 1;
                return 0;
            });
            _this.prepareSaveImport(conts);
        }, function (err) {
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    ContactDetailsPage.prototype.prepareSaveImport = function (contacts) {
        var persons = [];
        Object.keys(contacts).forEach(function (key) {
            var item = contacts[key], person = {
                name: '',
                nickname: '',
                email: '',
                phone: '',
                import_id: item.id,
            };
            if (item.name.formatted != null) {
                var fullName = item.name.formatted.split(' ');
                person.name = fullName.shift();
                person.nickname = fullName.join(' ');
            }
            if (item.emails != null) {
                person.email = item.emails[0].value;
            }
            if (item.phoneNumbers != null) {
                var phoneImport = item.phoneNumbers[0].value, phone = phoneImport.match(/\d/g).join("");
                person.phone = phone;
            }
            persons.push(person);
        });
        this.saveImport(persons);
    };
    ContactDetailsPage.prototype.saveImport = function (persons) {
        var _this = this;
        var params = {
            contacts: persons,
            user: this.user
        };
        this.service.postDataWithToken('contact-import-register', params).then(function (res) {
            _this.loader.dismiss();
            _this.result = res;
            if (_this.result.status == true) {
                var msg = '';
                if (_this.result.data.create == 0) {
                    msg = 'Todos os contatos da agenda já foram importados';
                }
                else if (_this.result.data.create == 1) {
                    msg = _this.result.data.create + " contato foi importado";
                }
                else {
                    msg = _this.result.data.create + " contatos foram importados";
                }
                var alert_1 = _this
                    .alertCtrl
                    .create({
                    title: msg,
                    buttons: [{
                            text: 'Ok',
                            handler: function () {
                                _this.navCtrl.pop();
                            }
                        }]
                });
                alert_1.present();
            }
            else {
                var alert_2 = _this
                    .alertCtrl
                    .create({
                    title: _this.translate.instant('ERROR'),
                    subTitle: _this.translate.instant('ERROR_SAVE'),
                    buttons: ['Ok']
                });
                alert_2.present();
            }
        }, function (err) {
            _this.loader.dismiss();
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('ERROR_SAVE'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    ContactDetailsPage.prototype.back = function () {
        this.navCtrl.pop();
    };
    ContactDetailsPage.prototype.delete = function () {
        var _this = this;
        var confirm = this
            .alertCtrl
            .create({
            title: this.translate.instant('CONFIRM_DELETE'),
            subTitle: this.dados.first_name == null ? '' : this.dados.first_name,
            buttons: [
                {
                    text: this.translate.instant('TOUCH_CANCEL'),
                },
                {
                    text: this.translate.instant('CONFIRM'),
                    handler: function () {
                        _this.deleteContact();
                    }
                }
            ]
        });
        confirm.present();
    };
    ContactDetailsPage.prototype.deleteContact = function () {
        var _this = this;
        this.loader = this.loadingCtrl.create({
            content: this.translate.instant('LOADING'),
        });
        this.loader.present();
        var params = {
            contact_id: this.dados.id,
            user: this.user
        };
        this.service.postDataWithToken('contact-remove', params).then(function (res) {
            _this.loader.dismiss();
            _this.result = res;
            if (_this.result.status == true) {
                var alert_3 = _this
                    .alertCtrl
                    .create({
                    title: _this.translate.instant('SUCCESSFULLY_DELETE'),
                    buttons: [
                        {
                            text: 'Ok',
                            handler: function () {
                                _this.navCtrl.pop();
                            }
                        }
                    ]
                });
                alert_3.present();
            }
            else {
                var alert_4 = _this
                    .alertCtrl
                    .create({
                    title: _this.translate.instant('ERROR'),
                    subTitle: _this.translate.instant('ERROR_SAVE'),
                    buttons: ['Ok']
                });
                alert_4.present();
            }
        }, function (err) {
            _this.loader.dismiss();
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('ERROR_SAVE'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    ContactDetailsPage.prototype.send = function () {
        var _this = this;
        this.loader = this.loadingCtrl.create({
            content: this.translate.instant('LOADING'),
        });
        this.loader.present();
        var params = {
            name: this.name.value,
            nickname: this.nickname.value,
            phone: this.phone.value,
            email: this.email.value,
            group: this.group.value,
            types: this.types.value,
            observations: this.observations.value,
            entrepreneur: this.entrepreneur.value == null ? false : this.entrepreneur.value,
            credility: this.credility.value == null ? false : this.credility.value,
            networking: this.networking.value == null ? false : this.networking.value,
            empowerment: this.empowerment.value == null ? false : this.empowerment.value,
            dreamer: this.dreamer.value == null ? false : this.dreamer.value,
            invitation: this.invitation.value,
            plan: this.plan.value,
            fup: this.fup.value,
            closing: this.closing.value,
            register: this.register.value == null ? false : this.register.value,
            comments: this.comments.value,
            id: this.id,
            import_id: null,
            user: this.user,
        };
        if (this.importBack) {
            params.import_id = this.dados.import_id;
        }
        this.service.postDataWithToken('contact-register', params).then(function (res) {
            _this.loader.dismiss();
            _this.result = res;
            if (_this.result.status == true) {
                var alert_5 = _this
                    .alertCtrl
                    .create({
                    title: _this.translate.instant('SAVE_CONTACTS'),
                    buttons: [
                        {
                            text: 'Ok',
                            handler: function () {
                                _this.change = false;
                                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__contacts_contacts__["a" /* ContactsPage */]);
                            }
                        }
                    ]
                });
                alert_5.present();
            }
            else {
                var alert_6 = _this
                    .alertCtrl
                    .create({
                    title: _this.translate.instant('ERROR'),
                    subTitle: _this.translate.instant('ERROR_SAVE'),
                    buttons: ['Ok']
                });
                alert_6.present();
            }
        }, function (err) {
            _this.loader.dismiss();
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('ERROR_SAVE'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    ContactDetailsPage.prototype.share = function () {
        var _this = this;
        console.log(this.dados, this.user);
        this.translate.get("LOADING").subscribe(function (result) {
            _this.loader = _this.loadingCtrl.create({
                content: result,
            });
            _this.loader.present();
        });
        this.socialSharing.shareViaWhatsApp('Convite', null, this.url + this.user.aff_code).then(function (res) {
            _this.loader.dismiss();
        }).catch(function (err) {
            _this.loader.dismiss();
        });
    };
    ContactDetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-contact-details',template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/contact-details/contact-details.html"*/'<ion-header>\n  <ion-navbar color="dark">\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>{{\'CONTACT_INFORMATION\' | translate}}</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content padding>\n  <button ion-button icon-left class="contact-button" color="light" block (click)="import()" *ngIf="manual == false"><ion-icon name="ios-bookmarks"></ion-icon> Importar Agenda</button>\n  <button ion-button icon-left class="contact-button" color="light" block (click)="search()" *ngIf="manual == false"><ion-icon name="ios-contact"></ion-icon> Selecionar Contato</button>\n  <button ion-button icon-left class="contact-button" color="light" block (click)="manualEnabled()" *ngIf="id == null && manual == false"><ion-icon name="ios-add-circle"></ion-icon> Cadastro Manual</button>\n  <button ion-button icon-left class="contact-button back" color="light" block  *ngIf="importBack" (click)="back()"><ion-icon name="ios-arrow-back"></ion-icon> {{ \'COME_BACK\' | translate }}</button>\n  <button ion-button icon-left class="contact-button back" color="light" block  *ngIf="importBack != true && id != null" (click)="back()"><ion-icon name="ios-arrow-back"></ion-icon> {{ \'COME_BACK\' | translate }}</button>\n  <button ion-button icon-left class="contact-button back" color="danger" block  *ngIf="id != null" (click)="delete()"><ion-icon name="ios-trash"></ion-icon> {{ \'DELETE\' | translate }}</button>\n  <button ion-button icon-left class="contact-button back" color="secondary" block  *ngIf="id != null" (click)="share()"><ion-icon name="logo-whatsapp"></ion-icon> {{ \'SEND_INVITE\' | translate }}</button>\n\n  <form [formGroup]="formGroup" id="contactForm" (ngSubmit)="send()" *ngIf="manual">\n    <ion-list>\n      <ion-item>\n        <ion-input type="text" formControlName="name" placeholder="{{ \'NAME\' | translate }}"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-input type="text" formControlName="nickname" placeholder="{{ \'NICKNAME\' | translate }}"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-input type="text" formControlName="phone" placeholder="{{ \'PHONE\' | translate}}" [brmasker]="{mask:\'+00 00 00000-0000\', len:18, type:\'num\'}"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-input type="email" formControlName="email" placeholder="{{ \'EMAIL\' | translate }}"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label>{{ \'GROUP\' | translate }}</ion-label>\n        <ion-select formControlName="group" cancelText="{{ \'TOUCH_CANCEL\' | translate }}" okText="Ok">\n          <ion-option value="{{item.id}}" *ngFor="let item of contactGroups"> {{ item.name | translate }}</ion-option>\n        </ion-select>\n      </ion-item>\n      <ion-item>\n        <ion-label>{{ \'TYPE_CONTACT\' | translate }}</ion-label>\n        <ion-select formControlName="types" cancelText="{{ \'TOUCH_CANCEL\' | translate }}" okText="Ok">\n          <ion-option value="{{item.id}}" *ngFor="let item of contactTypes"> {{ item.name | translate }}</ion-option>\n        </ion-select>\n      </ion-item>\n      <ion-item>\n        <ion-textarea formControlName="observations" placeholder="{{ \'OBSERVATIONS\' | translate }}" autocomplete="on" autocorrect="on"></ion-textarea>\n      </ion-item>\n      <ion-item>\n        <ion-label>{{\'ENTREPRENEUR\' | translate}}</ion-label>\n        <ion-toggle color="light" checked="false" formControlName="entrepreneur"></ion-toggle>\n      </ion-item>\n      <ion-item>\n        <ion-label>{{\'CREDILITY\' | translate}}</ion-label>\n        <ion-toggle color="light" checked="false" formControlName="credility"></ion-toggle>\n      </ion-item>\n      <ion-item>\n        <ion-label>{{\'NETWORKING\' | translate}}</ion-label>\n        <ion-toggle color="light" checked="false" formControlName="networking"></ion-toggle>\n      </ion-item>\n      <ion-item>\n        <ion-label>{{\'EMPOWERMENT\' | translate}}</ion-label>\n        <ion-toggle color="light" checked="false" formControlName="empowerment"></ion-toggle>\n      </ion-item>\n      <ion-item>\n        <ion-label>{{\'DREAMER\' | translate}}</ion-label>\n        <ion-toggle color="light" checked="false" formControlName="dreamer"></ion-toggle>\n      </ion-item>\n      <ion-item>\n        <ion-input type="text" formControlName="invitation" placeholder="{{ \'DATE_INVITATION\' | translate }}" [brmasker]="{mask:\'00/00/0000\', len:10, type:\'num\'}"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-input type="text" formControlName="plan" placeholder="{{ \'DATE_PLAN\' | translate }}" [brmasker]="{mask:\'00/00/0000\', len:10, type:\'num\'}"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-input type="text" formControlName="fup" placeholder="{{ \'DATE_FUP\' | translate }}" [brmasker]="{mask:\'00/00/0000\', len:10, type:\'num\'}"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-input type="text" formControlName="closing" placeholder="{{ \'CLOSING\' | translate }}" [brmasker]="{mask:\'00/00/0000\', len:10, type:\'num\'}"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label>{{\'REGISTER\' | translate}}</ion-label>\n        <ion-toggle color="light" checked="false" formControlName="register"></ion-toggle>\n      </ion-item>\n      <ion-item>\n        <ion-textarea formControlName="comments" placeholder="{{ \'COMMENTS\' | translate }}" autocomplete="on" autocorrect="on"></ion-textarea>\n      </ion-item>\n    </ion-list>\n    <button ion-button color="light" block type="submit" [disabled]="formGroup.invalid">{{ \'SAVE\' | translate }}</button>\n  </form>\n</ion-content>'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/contact-details/contact-details.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__node_modules_angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_4__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_2__node_modules_ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */], __WEBPACK_IMPORTED_MODULE_5__node_modules_ionic_native_contacts_ngx__["a" /* Contacts */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_7__providers_database_database__["a" /* DatabaseProvider */], __WEBPACK_IMPORTED_MODULE_8__ionic_native_social_sharing_ngx__["a" /* SocialSharing */]])
    ], ContactDetailsPage);
    return ContactDetailsPage;
}());

//# sourceMappingURL=contact-details.js.map

/***/ }),

/***/ 746:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar_ngx__ = __webpack_require__(390);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen_ngx__ = __webpack_require__(389);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_storage_service_storage_service__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_database_database__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_login_login__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_alerts_alerts__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_daily_daily__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_first_first__ = __webpack_require__(176);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_courses_courses__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_sources_sources__ = __webpack_require__(181);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__node_modules_ionic_native_device_ngx__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_contacts_contacts__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_network_ngx__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_daily_audio_daily_audio__ = __webpack_require__(175);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_meeting_meeting__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_leadership_leadership__ = __webpack_require__(197);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








// import { FollowPage } from '../pages/follow/follow';
// import { WebinarsPage } from '../pages/webinars/webinars';




// import { DisclosurePage } from '../pages/disclosure/disclosure';
// import { NotesPage } from '../pages/notes/notes';








var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, translate, modal, storage, device, service, events, network, menuCtrl, dbProvider) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.translate = translate;
        this.modal = modal;
        this.storage = storage;
        this.device = device;
        this.service = service;
        this.events = events;
        this.network = network;
        this.menuCtrl = menuCtrl;
        this.dbProvider = dbProvider;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */];
        this.user = null;
        if (this.user == null) {
            this.user = { FirstName: '' };
        }
        this.pages = [];
        this.isOffline = false;
        this.initializeApp();
        this.events.subscribe('afterLogin', function (user) {
            _this.user = user;
            _this.renderMenu();
        });
        this.events.subscribe('logout', function (data) {
            _this.logout();
        });
        this.modules = [
            { title: 'DAILY_MOTIVATIONAL', component: __WEBPACK_IMPORTED_MODULE_9__pages_daily_daily__["a" /* DailyPage */], icon: 'ios-calendar', params: { course_id: 2 }, route: 'course.video.index/2', type: 1 },
            { title: 'DAILY_AUDIO', component: __WEBPACK_IMPORTED_MODULE_17__pages_daily_audio_daily_audio__["a" /* DailyAudioPage */], icon: 'ios-mic', params: {}, route: 'audio.index', type: 1 },
            { title: 'LIST_NAMES', component: __WEBPACK_IMPORTED_MODULE_15__pages_contacts_contacts__["a" /* ContactsPage */], icon: 'ios-people', params: {}, type: 0 },
            { title: 'TOOLS', component: __WEBPACK_IMPORTED_MODULE_10__pages_first_first__["a" /* FirstPage */], icon: 'ios-build', params: { course_id: 1 }, route: 'course.video.index/1', type: 1 },
            { title: 'COURSES', component: __WEBPACK_IMPORTED_MODULE_11__pages_courses_courses__["a" /* CoursesPage */], icon: 'ios-videocam', params: {}, route: 'course.index', type: 1 },
            // { title: 'DISCLOSURE', component: DisclosurePage, icon: 'ios-share-alt', params: {} },
            // { title: 'REPORTS', component: NotesPage, icon: 'ios-document', params: {} },
            { title: 'MATERIALS', component: __WEBPACK_IMPORTED_MODULE_12__pages_sources_sources__["a" /* SourcesPage */], icon: 'ios-paper', params: {}, route: 'material.index', type: 1 },
            { title: 'ALERTS', component: __WEBPACK_IMPORTED_MODULE_8__pages_alerts_alerts__["a" /* AlertsPage */], icon: 'ios-alert', params: {}, route: 'notification.index', type: 1 },
            // { title: 'WEBNEWS', component: WebinarsPage, icon: 'ios-laptop', params: {} },
            { title: 'MEETINGS', component: __WEBPACK_IMPORTED_MODULE_18__pages_meeting_meeting__["a" /* MeetingPage */], icon: 'ios-laptop', params: {}, route: 'meeting.index', type: 1 },
            { title: 'LEADERSHIP', component: __WEBPACK_IMPORTED_MODULE_19__pages_leadership_leadership__["a" /* LeadershipPage */], icon: 'ios-laptop', params: {}, route: 'qualification.index', type: 1 },
        ];
    }
    MyApp.prototype.renderMenu = function () {
        var _this = this;
        if (this.pages.length == 0) {
            var options_1 = [];
            this.dbProvider.getModules().then(function (itens) {
                if (itens != null) {
                    Object.keys(_this.modules).forEach(function (i) {
                        var module = _this.modules[i];
                        module.hidden = true;
                        if (module.type == 0) {
                            module.order = 1000;
                            module.hidden = false;
                            options_1.push(module);
                        }
                        else {
                            Object.keys(itens).forEach(function (key) {
                                var item = itens[key];
                                if (item.param == null) {
                                    if (module.route == item.route) {
                                        module.hidden = false;
                                        module.title = item.name;
                                        module.order = item.sequence == null ? 0 : item.sequence;
                                        options_1.push(module);
                                    }
                                }
                                else {
                                    if (module.route === item.route + '/' + item.param) {
                                        module.hidden = false;
                                        module.title = item.name;
                                        module.order = item.sequence;
                                        options_1.push(module);
                                    }
                                }
                            });
                        }
                    });
                    _this.pages = options_1.sort(function (a, b) {
                        if (a.order > b.order) {
                            return 1;
                        }
                        if (a.order < b.order) {
                            return -1;
                        }
                        return 0;
                    });
                    console.log(_this.pages);
                }
            });
        }
    };
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            if (_this.device.platform == "Android") {
                // this.statusBar.backgroundColorByHexString('#ffffff');
            }
            else {
                _this.statusBar.backgroundColorByHexString('#ffffff');
            }
            _this.splashScreen.hide();
            _this.dbProvider.createDatabase().then(function (res) {
                _this.dbProvider.getLanguage().then(function (res) {
                    if (res != null) {
                        _this.translate.setDefaultLang(res.language);
                    }
                    else {
                        _this.translate.setDefaultLang('pt');
                    }
                    _this.renderMenu();
                });
            }).catch(function (err) {
                console.log(err);
            });
        });
    };
    MyApp.prototype.openPage = function (page) {
        this.content.scrollToTop();
        this.nav.setRoot(page.component, page.params);
    };
    MyApp.prototype.logout = function () {
        this.content.scrollToTop();
        var that = this;
        this.dbProvider.getUser().then(function (user) {
            if (user != null) {
                that.dbProvider.deleteUser(user.id).then(function (res) {
                    that.nav.setRoot(__WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */]);
                });
            }
        });
    };
    MyApp.prototype.changeLanguage = function () {
        this.content.scrollToTop();
        var modalLanguage = this.modal.create('LanguagePage', {});
        modalLanguage.present();
    };
    MyApp.prototype.checkSession = function () {
        var _this = this;
        var params = {
            user_id: this.user.Id,
            uuid: this.device.uuid
        };
        if (this.device.uuid != null) {
            this.service.postData('checkSession', params).then(function (res) {
                _this.result = res;
                if (_this.result.status == true) {
                    _this.menuCtrl.close();
                    clearInterval(_this.promisse);
                    _this.storage.clearTouch();
                    _this.logout();
                }
            }, function (err) {
            });
        }
        else {
            clearInterval(this.promisse);
        }
    };
    var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p;
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Nav */]),
        __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Nav */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Nav */]) === "function" && _a || Object)
    ], MyApp.prototype, "nav", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */]),
        __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */]) === "function" && _b || Object)
    ], MyApp.prototype, "content", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/app/app.html"*/'<ion-menu side="left" [content]="content">\n    <ion-header>\n        <ion-toolbar color="dark">\n            <ion-title>{{ \'HELLO\' | translate }} {{user.first_name}}</ion-title>\n        </ion-toolbar>\n    </ion-header>\n    <ion-content class="page-menu" padding>\n        <ion-list id="listMenu">\n            <button menuClose ion-item icon-end *ngFor="let p of pages" (click)="openPage(p)">\n                {{ p.title | translate }} <ion-icon name="ios-arrow-forward"></ion-icon>\n            </button>\n            <button menuClose ion-item icon-end (click)="changeLanguage()">\n                {{\'CHANGE_LANGUAGE\' | translate }} <ion-icon name="ios-arrow-forward"></ion-icon>\n            </button>\n            <button menuClose ion-item icon-end (click)="logout()">\n                {{\'LOGOUT\' | translate }} <ion-icon name="ios-arrow-forward"></ion-icon>\n            </button>\n        </ion-list>\n    </ion-content>\n</ion-menu>\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar_ngx__["a" /* StatusBar */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar_ngx__["a" /* StatusBar */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen_ngx__["a" /* SplashScreen */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen_ngx__["a" /* SplashScreen */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_4__node_modules_ngx_translate_core__["c" /* TranslateService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__node_modules_ngx_translate_core__["c" /* TranslateService */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_5__providers_storage_service_storage_service__["a" /* StorageServiceProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__providers_storage_service_storage_service__["a" /* StorageServiceProvider */]) === "function" && _h || Object, typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_13__node_modules_ionic_native_device_ngx__["a" /* Device */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_13__node_modules_ionic_native_device_ngx__["a" /* Device */]) === "function" && _j || Object, typeof (_k = typeof __WEBPACK_IMPORTED_MODULE_14__providers_api_service_api_service__["a" /* ApiServiceProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_14__providers_api_service_api_service__["a" /* ApiServiceProvider */]) === "function" && _k || Object, typeof (_l = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */]) === "function" && _l || Object, typeof (_m = typeof __WEBPACK_IMPORTED_MODULE_16__ionic_native_network_ngx__["a" /* Network */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_16__ionic_native_network_ngx__["a" /* Network */]) === "function" && _m || Object, typeof (_o = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */]) === "function" && _o || Object, typeof (_p = typeof __WEBPACK_IMPORTED_MODULE_6__providers_database_database__["a" /* DatabaseProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__providers_database_database__["a" /* DatabaseProvider */]) === "function" && _p || Object])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 747:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AuthServiceProvider = /** @class */ (function () {
    function AuthServiceProvider(http) {
        this.http = http;
        this.apiUrl = "http://127.0.0.1:8000/";
    }
    AuthServiceProvider.prototype.postData = function (credentials, type) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]();
            _this.http.post(_this.apiUrl + type, JSON.stringify(credentials), { headers: headers }).subscribe(function (res) {
                resolve(res.json());
            }), function (err) {
                reject(err);
            };
        });
    };
    AuthServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */]])
    ], AuthServiceProvider);
    return AuthServiceProvider;
}());

//# sourceMappingURL=auth-service.js.map

/***/ }),

/***/ 748:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InterceptorProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_operators__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_operators___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__database_database__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var InterceptorProvider = /** @class */ (function () {
    function InterceptorProvider(database) {
        this.database = database;
    }
    InterceptorProvider.prototype.intercept = function (request, next) {
        var _this = this;
        var promisse = this.database.getUser();
        return __WEBPACK_IMPORTED_MODULE_1_rxjs__["Observable"].fromPromise(promisse)
            .mergeMap(function (user) {
            if (user == null) {
                user = { access_token: false };
            }
            var cloneReq = _this.addToken(request, user.access_token);
            return next.handle(cloneReq).pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["catchError"])(function (error) {
                return [];
            }));
        });
    };
    InterceptorProvider.prototype.addToken = function (request, token) {
        if (token) {
            var clone = void 0;
            clone = request.clone({
                setHeaders: {
                    Accept: "application/json",
                    'Content-Type': "text/html",
                    Authorization: "Bearer " + token
                }
            });
            return clone;
        }
        return request;
    };
    InterceptorProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__database_database__["a" /* DatabaseProvider */]])
    ], InterceptorProvider);
    return InterceptorProvider;
}());

//# sourceMappingURL=interceptor.js.map

/***/ }),

/***/ 749:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProgressDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ProgressDirective = /** @class */ (function () {
    function ProgressDirective(renderer, elementRef) {
        this.renderer = renderer;
        this.elementRef = elementRef;
    }
    ProgressDirective.prototype.ngOnInit = function () {
        this.renderer.setElementStyle(this.elementRef.nativeElement, 'width', this.progress + '%');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])('progress'),
        __metadata("design:type", Number)
    ], ProgressDirective.prototype, "progress", void 0);
    ProgressDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* Directive */])({
            selector: '[progress]'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["W" /* Renderer */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */]])
    ], ProgressDirective);
    return ProgressDirective;
}());

//# sourceMappingURL=progress.js.map

/***/ }),

/***/ 794:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DisclosurePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_storage_service_storage_service__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__disclosure_business_disclosure_business__ = __webpack_require__(396);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__disclosure_produts_disclosure_produts__ = __webpack_require__(395);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var DisclosurePage = /** @class */ (function () {
    function DisclosurePage(navCtrl, navParams, events, service, loadingCtrl, alertCtrl, translate, storage) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.events = events;
        this.service = service;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.storage = storage;
        this.user = this.storage.getObject('distributor');
        this.translate.get("LOADING").subscribe(function (result) {
            _this.loader = _this.loadingCtrl.create({
                content: result,
            });
            _this.loader.present();
        });
        this.business = __WEBPACK_IMPORTED_MODULE_5__disclosure_business_disclosure_business__["a" /* DisclosureBusinessPage */];
        this.produts = __WEBPACK_IMPORTED_MODULE_6__disclosure_produts_disclosure_produts__["a" /* DisclosureProdutsPage */];
    }
    DisclosurePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        var data = {
            user_id: this.user.Id,
            affId: this.user.AffId,
            affCode: this.user.AffCode,
        };
        this.service.postData('listLinksForAffiliateTabs', data).then(function (res) {
            _this.loader.dismiss();
            _this.result = res;
            if (_this.result.status == true) {
                _this.navParams.data.links = _this.result.links;
                _this.events.publish('loadLinks', _this.result.links);
            }
            else {
                var alert_1 = _this
                    .alertCtrl
                    .create({
                    title: _this.translate.instant('ERROR'),
                    subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                    buttons: ['Ok']
                });
                alert_1.present();
            }
        }, function (err) {
            _this.loader.dismiss();
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    DisclosurePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-disclosure',template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/disclosure/disclosure.html"*/'<ion-header>\n  <ion-navbar color="dark">\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>{{ \'DISCLOSURE\' | translate }}</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-tabs tabsLayout="icon-start">\n  <ion-tab tabIcon="logo-usd" tabTitle="{{ \'DISCLOSURE_BUSINESS\' | translate }}" [root]="business" [rootParams]="navParams.data"></ion-tab>\n  <ion-tab tabIcon="ios-cart" tabTitle="{{ \'DISCLOSURE_PRODUCTS\' | translate }}" [root]="produts" [rootParams]="navParams.data"></ion-tab>\n</ion-tabs>'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/disclosure/disclosure.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_4__providers_storage_service_storage_service__["a" /* StorageServiceProvider */]])
    ], DisclosurePage);
    return DisclosurePage;
}());

//# sourceMappingURL=disclosure.js.map

/***/ })

},[399]);
//# sourceMappingURL=main.js.map