webpackJsonp([5],{

/***/ 777:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecoveryPageModule", function() { return RecoveryPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__recovery__ = __webpack_require__(799);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var RecoveryPageModule = /** @class */ (function () {
    function RecoveryPageModule() {
    }
    RecoveryPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__recovery__["a" /* RecoveryPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__recovery__["a" /* RecoveryPage */]),
                __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__["b" /* TranslateModule */]
            ],
        })
    ], RecoveryPageModule);
    return RecoveryPageModule;
}());

//# sourceMappingURL=recovery.module.js.map

/***/ }),

/***/ 799:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RecoveryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__node_modules_angular_forms__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__node_modules_ngx_translate_core__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var RecoveryPage = /** @class */ (function () {
    function RecoveryPage(navCtrl, navParams, view, formBuilder, service, loadingCtrl, translate, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.view = view;
        this.formBuilder = formBuilder;
        this.service = service;
        this.loadingCtrl = loadingCtrl;
        this.translate = translate;
        this.alertCtrl = alertCtrl;
        this.formGroup = formBuilder.group({
            email: ['', __WEBPACK_IMPORTED_MODULE_2__node_modules_angular_forms__["f" /* Validators */].email]
        });
        this.email = this.formGroup.controls['email'];
    }
    RecoveryPage.prototype.ionViewDidLoad = function () {
    };
    RecoveryPage.prototype.closeModal = function () {
        this.view.dismiss();
    };
    RecoveryPage.prototype.send = function () {
        var _this = this;
        this.loader = this.loadingCtrl.create({
            content: this.translate.instant('LOADING'),
        });
        this.loader.present();
        var params = {
            email: this.email.value,
        };
        this.service.postData('recoveryPassword', params).then(function (res) {
            _this.loader.dismiss();
            _this.result = res;
            if (_this.result.status == true) {
                var alert_1 = _this
                    .alertCtrl
                    .create({
                    title: _this.translate.instant('SENT'),
                    subTitle: _this.translate.instant('SENT_PASSWORD'),
                    buttons: [
                        {
                            text: 'Ok',
                            handler: function () {
                                _this.closeModal();
                            }
                        }
                    ]
                });
                alert_1.present();
            }
            else {
                var alert_2 = _this
                    .alertCtrl
                    .create({
                    title: _this.translate.instant('ERROR'),
                    subTitle: _this.translate.instant('ERROR_REGISTRATION'),
                    buttons: ['Ok']
                });
                alert_2.present();
            }
        }, function (err) {
            _this.loader.dismiss();
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('ERROR_REGISTRATION'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    RecoveryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-recovery',template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/recovery/recovery.html"*/'<ion-header>\n  <ion-navbar color="dark">\n    <ion-title>{{\'FORGOT_PASSWORD\' | translate}}</ion-title>\n    <ion-buttons end>\n      <button ion-button (click)=\'closeModal()\' class="modal-buttom"><ion-icon name="close-circle"></ion-icon></button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content padding>\n  <form [formGroup]="formGroup" id="formRecovery" (ngSubmit)="send()">\n    <ion-list> \n      <ion-item>\n        <ion-input type="email" formControlName="email" placeholder="{{ \'EMAIL\' | translate }}" autocomplete="off" autocorrect="off" [ngClass]="{\'error\': email.invalid && email.touched}"></ion-input>\n      </ion-item>\n    </ion-list>\n    <button ion-button block type="submit" [disabled]="formGroup.invalid">{{\'SEND\' | translate}}</button>\n  </form>\n</ion-content>'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/recovery/recovery.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2__node_modules_angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_4__node_modules_ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], RecoveryPage);
    return RecoveryPage;
}());

//# sourceMappingURL=recovery.js.map

/***/ })

});
//# sourceMappingURL=5.js.map