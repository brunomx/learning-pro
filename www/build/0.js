webpackJsonp([0],{

/***/ 783:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VideoOfflinePageModule", function() { return VideoOfflinePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__video_offline__ = __webpack_require__(801);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var VideoOfflinePageModule = /** @class */ (function () {
    function VideoOfflinePageModule() {
    }
    VideoOfflinePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__video_offline__["a" /* VideoOfflinePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__video_offline__["a" /* VideoOfflinePage */]),
                __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__["b" /* TranslateModule */]
            ],
        })
    ], VideoOfflinePageModule);
    return VideoOfflinePageModule;
}());

//# sourceMappingURL=video-offline.module.js.map

/***/ }),

/***/ 801:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VideoOfflinePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ionic_native_device_ngx__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_video_offline_video_offline__ = __webpack_require__(802);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__node_modules_ionic_native_file_ngx__ = __webpack_require__(47);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var VideoOfflinePage = /** @class */ (function () {
    function VideoOfflinePage(navCtrl, navParams, device, view, translate, file, alertCtrl, offline) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.device = device;
        this.view = view;
        this.translate = translate;
        this.file = file;
        this.alertCtrl = alertCtrl;
        this.offline = offline;
        this.video = this.navParams.data.video;
    }
    VideoOfflinePage.prototype.ionViewDidLoad = function () {
        if (this.device.platform != "Android") {
            this.video.targetFile = this.video.targetFile.replace(/^file:\/\//, '');
        }
        var that = this;
        this.promisse = setInterval(function () {
            document.getElementById('video-player').ontimeupdate = function (event) {
                that.target = event.target;
                var seconds = parseInt(that.target.currentTime, 10);
                if (seconds > 0) {
                    that.offline.initPlayer(that.video, seconds);
                }
            };
        }, 1000);
    };
    VideoOfflinePage.prototype.closeModal = function () {
        clearInterval(this.promisse);
        this.view.dismiss();
    };
    VideoOfflinePage.prototype.delete = function () {
        var _this = this;
        if (this.device.platform == "Android") {
            this.targetPath = this.file.dataDirectory;
        }
        else {
            this.targetPath = this.file.syncedDataDirectory;
        }
        var fileName = this.video.path + ".mp4";
        this.file.removeFile(this.targetPath, fileName).then(function (res) {
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('SUCCESSFULLY_REMOVED'),
                subTitle: _this.translate.instant('SUCCESSFULLY_REMOVED_TEXT'),
                buttons: ['Ok']
            });
            alert.present().then(function (res) {
                _this.view.dismiss();
            });
        }, function (err) {
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('REMOVE_VIDEO_TITLE'),
                subTitle: _this.translate.instant('REMOVE_VIDEO'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    VideoOfflinePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-video-offline',template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/video-offline/video-offline.html"*/'<ion-header>\n  <ion-navbar color="dark">\n    <ion-title>{{video.name}}</ion-title>\n    <ion-buttons end>\n      <button ion-button (click)=\'closeModal()\' class="modal-buttom">\n        <ion-icon name="close-circle"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <video id="video-player" controls="controls" preload="metadata" autoplay="autoplay" webkit-playsinline="webkit-playsinline" class="videoPlayer">\n    <source [src]="video.targetFile" />\n  </video>\n  <button ion-button full icon-start (click)="delete()">\n    <ion-icon name="ios-trash-outline"></ion-icon> {{\'DELETE_VIDEO\' | translate}}\n  </button>\n</ion-content>'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/video-offline/video-offline.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__node_modules_ionic_native_device_ngx__["a" /* Device */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2__node_modules_ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_5__node_modules_ionic_native_file_ngx__["a" /* File */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_4__providers_video_offline_video_offline__["a" /* VideoOfflineProvider */]])
    ], VideoOfflinePage);
    return VideoOfflinePage;
}());

//# sourceMappingURL=video-offline.js.map

/***/ }),

/***/ 802:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VideoOfflineProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__storage_service_storage_service__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var VideoOfflineProvider = /** @class */ (function () {
    function VideoOfflineProvider(http, service, storage) {
        this.http = http;
        this.service = service;
        this.storage = storage;
        this.user = this.storage.getObject('distributor');
        this.checkTags = true;
    }
    VideoOfflineProvider.prototype.initPlayer = function (video, second) {
        this.video = video;
        this.checkTime(second);
    };
    VideoOfflineProvider.prototype.checkTime = function (second) {
        if (this.video.tags != null) {
            var that_1 = this;
            Object.keys(this.video.tags).forEach(function (key) {
                var item = that_1.video.tags[key];
                if (second >= item.time) {
                    that_1.applyTag(item.tag.Id);
                    delete that_1.video.tags[key];
                    if (that_1.video.tags.length == 0) {
                        that_1.checkTags = false;
                    }
                }
            });
        }
    };
    VideoOfflineProvider.prototype.applyTag = function (tag) {
        var params = {
            user_id: this.user.Id,
            tag: tag
        };
        this.service.postData('applyTag', params);
    };
    VideoOfflineProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_2__api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_3__storage_service_storage_service__["a" /* StorageServiceProvider */]])
    ], VideoOfflineProvider);
    return VideoOfflineProvider;
}());

//# sourceMappingURL=video-offline.js.map

/***/ })

});
//# sourceMappingURL=0.js.map