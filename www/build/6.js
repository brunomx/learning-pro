webpackJsonp([6],{

/***/ 776:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotesPageModule", function() { return NotesPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__notes__ = __webpack_require__(798);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_order_pipe__ = __webpack_require__(394);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__node_modules_ngx_translate_core__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var NotesPageModule = /** @class */ (function () {
    function NotesPageModule() {
    }
    NotesPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__notes__["a" /* NotesPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__notes__["a" /* NotesPage */]),
                __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_order_pipe__["a" /* OrderModule */],
                __WEBPACK_IMPORTED_MODULE_4__node_modules_ngx_translate_core__["b" /* TranslateModule */],
            ],
        })
    ], NotesPageModule);
    return NotesPageModule;
}());

//# sourceMappingURL=notes.module.js.map

/***/ }),

/***/ 798:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_storage_service_storage_service__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var NotesPage = /** @class */ (function () {
    function NotesPage(navCtrl, navParams, loadingCtrl, alertCtrl, translate, service, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.service = service;
        this.storage = storage;
        this.months = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
        this.notes = { mounth: null };
        this.user = this.storage.getObject('distributor');
    }
    NotesPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.translate.get("LOADING").subscribe(function (result) {
            _this.loader = _this.loadingCtrl.create({
                content: result,
            });
            _this.loader.present();
        });
        var params = {
            user_id: this.user.Id,
            affId: this.user.AffId,
            page: 0
        };
        this.service.postData('listAffiliates2', params).then(function (res) {
            _this.loader.dismiss();
            _this.result = res;
            if (_this.result.status == true) {
                _this.populateNotes();
            }
            else {
                var alert_1 = _this
                    .alertCtrl
                    .create({
                    title: _this.translate.instant('ERROR'),
                    subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                    buttons: ['Ok']
                });
                alert_1.present();
            }
        }, function (err) {
            _this.loader.dismiss();
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    NotesPage.prototype.populateNotes = function () {
        var _this = this;
        if (this.result.affiliates.length > 0) {
            var that_1 = this;
            Object.keys(this.result.affiliates).forEach(function (key) {
                var item = _this.result.affiliates[key], data_parts = item.data.split(' '), data = data_parts[0].split('/');
                if (that_1.notes.mounth == null) {
                    that_1.notes.mounth = [];
                    var obj = {
                        name: that_1.months[parseInt(data[1]) - 1] + ' - ' + data[2],
                        itens: [],
                        id: parseInt(data[2].substr(2, 4) + data[1]),
                    };
                    that_1.notes.mounth.name = that_1.months[parseInt(data[1]) - 1];
                    item.data = data_parts[0];
                    obj.itens.push(item);
                    that_1.notes.mounth[obj.id] = obj;
                }
                else {
                    Object.keys(that_1.notes.mounth).forEach(function (key2) {
                        var mounth = that_1.notes.mounth[key2];
                        data_parts = item.data.split(' '),
                            data = data_parts[0].split('/');
                        if (mounth.id == parseInt(data[2].substr(2, 4) + data[1])) {
                            item.data = data_parts[0];
                            var checkItem_1 = true;
                            Object.keys(mounth.itens).forEach(function (key3) {
                                var person = mounth.itens[key3];
                                if (checkItem_1) {
                                    if (person.Id == item.Id) {
                                        checkItem_1 = false;
                                    }
                                }
                            });
                            if (checkItem_1) {
                                mounth.itens.push(item);
                            }
                        }
                        else {
                            if (data_parts.length > 1) {
                                var obj_1 = {
                                    name: that_1.months[parseInt(data[1]) - 1] + ' - ' + data[2],
                                    itens: [],
                                    id: parseInt(data[2].substr(2, 4) + data[1]),
                                };
                                item.data = data_parts[0];
                                obj_1.itens.push(item);
                                var checkMouth_1 = true;
                                Object.keys(that_1.notes.mounth).forEach(function (key4) {
                                    var mounth2 = that_1.notes.mounth[key4];
                                    if (checkMouth_1) {
                                        if (mounth2.id == obj_1.id) {
                                            checkMouth_1 = false;
                                        }
                                    }
                                });
                                if (checkMouth_1) {
                                    that_1.notes.mounth[obj_1.id] = obj_1;
                                }
                            }
                        }
                    });
                }
            });
            this.orderMouth();
        }
        else {
            var alert_2 = this
                .alertCtrl
                .create({
                title: this.translate.instant('ERROR'),
                subTitle: this.translate.instant('ERROR_AVAILABLE'),
                buttons: ['Ok']
            });
            alert_2.present();
        }
    };
    NotesPage.prototype.orderMouth = function () {
        var orderMouth = [], that = this;
        Object.keys(that.notes.mounth).forEach(function (key) {
            if (that.notes.mounth[key].hasOwnProperty('itens')) {
                orderMouth.push(that.notes.mounth[key]);
            }
        });
        that.notes.mounth = orderMouth.reverse();
    };
    NotesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-notes',template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/notes/notes.html"*/'<ion-header>\n  <ion-navbar color="dark">\n    <ion-buttons left>\n      <button ion-button icon-only menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>{{ \'REPORTS\' | translate }}</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <div class="card-background">\n    <ion-card>\n      <img src="assets/imgs/header/notes.png"/>\n      <div class="card-title">{{ \'REPORTS\' | translate }}</div>\n    </ion-card>\n  </div>\n  <ion-card *ngFor="let mounth of notes.mounth">\n    <ion-card-header>{{mounth.name}}</ion-card-header>\n    <ion-list>\n      <ion-item *ngFor="let item of mounth.itens">\n        <ion-grid>\n          <ion-col col-12>\n            <ion-icon name="ios-person-outline" item-start></ion-icon> {{item.FirstName}}\n          </ion-col>\n          <ion-col col-12>\n            <ion-icon name="ios-mail-outline" item-start></ion-icon> {{item.Email}}\n          </ion-col>\n          <ion-col col-12>\n            <ion-icon name="ios-calendar-outline" item-start></ion-icon> {{item.data}}\n          </ion-col>\n        </ion-grid>\n      </ion-item>\n    </ion-list>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/notes/notes.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2__node_modules_ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_4__providers_storage_service_storage_service__["a" /* StorageServiceProvider */]])
    ], NotesPage);
    return NotesPage;
}());

//# sourceMappingURL=notes.js.map

/***/ })

});
//# sourceMappingURL=6.js.map