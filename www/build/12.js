webpackJsonp([12],{

/***/ 753:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AudioOnlinePageModule", function() { return AudioOnlinePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__audio_online__ = __webpack_require__(792);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__node_modules_ionic_audio__ = __webpack_require__(393);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng_lazyload_image__ = __webpack_require__(397);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var AudioOnlinePageModule = /** @class */ (function () {
    function AudioOnlinePageModule() {
    }
    AudioOnlinePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__audio_online__["a" /* AudioOnlinePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__audio_online__["a" /* AudioOnlinePage */]),
                __WEBPACK_IMPORTED_MODULE_3__node_modules_ngx_translate_core__["b" /* TranslateModule */],
                __WEBPACK_IMPORTED_MODULE_4__node_modules_ionic_audio__["d" /* IonicAudioModule */],
                __WEBPACK_IMPORTED_MODULE_5_ng_lazyload_image__["a" /* LazyLoadImageModule */],
            ],
        })
    ], AudioOnlinePageModule);
    return AudioOnlinePageModule;
}());

//# sourceMappingURL=audio-online.module.js.map

/***/ }),

/***/ 792:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AudioOnlinePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__node_modules_ngx_translate_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_database_database__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_audio__ = __webpack_require__(393);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_device_ngx__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var AudioOnlinePage = /** @class */ (function () {
    function AudioOnlinePage(navCtrl, navParams, view, translate, database, service, alertCtrl, _audioProvider, loading, device) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.view = view;
        this.translate = translate;
        this.database = database;
        this.service = service;
        this.alertCtrl = alertCtrl;
        this._audioProvider = _audioProvider;
        this.loading = loading;
        this.device = device;
        this.duration = 0;
        this.audio = this.navParams.data.audio;
        this.database.getUser().then(function (user) {
            _this.user = user;
        });
        this.track = null;
        this.setTime = true;
        this.finished = false;
    }
    AudioOnlinePage.prototype.ionViewWillEnter = function () {
    };
    AudioOnlinePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.translate.get("LOADING").subscribe(function (result) {
            _this.loader = _this.loading.create({
                content: result,
            });
            _this.loader.present();
        });
        this.image = this.navParams.data.audio.image;
        this.track = {
            src: this.audio.path,
            title: this.audio.name,
            preload: 'metadata'
        };
    };
    AudioOnlinePage.prototype.checkLoad = function () {
        this.current.play();
        var that = this;
        this.interval = setInterval(function () {
            if (that.device.platform == "Android") {
                if (that.current.isPlaying == true && that.current._isLoading == false) {
                    if (that.audio.progress != 100) {
                        that.audioTrack.seekTo(that.audio.watch_time);
                    }
                    clearInterval(that.interval);
                    that.isPlaying();
                }
            }
            else {
                if (that.current._progress > 0) {
                    if (that.audio.progress != 100) {
                        that.audioTrack.seekTo(that.audio.watch_time);
                    }
                    clearInterval(that.interval);
                    that.isPlaying();
                }
            }
        }, 1000);
    };
    AudioOnlinePage.prototype.isPlaying = function () {
        var that = this;
        this.intervalPlay = setInterval(function () {
            if (that.audioTrack.isPlaying) {
                that.loader.dismiss();
                clearInterval(that.intervalPlay);
            }
        }, 1000);
    };
    AudioOnlinePage.prototype.onTrackFinished = function (track) {
        if (this.finished == false) {
            this.finished = true;
            this.saveWatchTime(this.current.progress, 1);
        }
    };
    AudioOnlinePage.prototype.onTrackPause = function (track) {
        this.saveWatchTime(this.current.progress, 0);
    };
    AudioOnlinePage.prototype.onTrackLoad = function (track) {
        this.current = track;
        if (this.setTime == true) {
            this.setTime = false;
            this.checkLoad();
        }
    };
    AudioOnlinePage.prototype.closeModal = function () {
        this.audioTrack.pause();
        this.onTrackPause(null);
        this.view.dismiss(this.audio);
    };
    AudioOnlinePage.prototype.saveWatchTime = function (seconds, completed) {
        var _this = this;
        if (this.audio.duration == null) {
            this.duration = this.current.duration;
        }
        if (seconds == 0 && completed == 1) {
            seconds = this.current.duration;
        }
        if (this.current.duration == seconds && completed == 0) {
            completed = 1;
        }
        var params = {
            audio_id: this.audio.id,
            user: this.user,
            watch_time: seconds,
            completed: completed,
            duration: this.duration
        };
        this.service.postDataWithToken('userAudio-register', params).then(function (res) {
        }, function (err) {
            _this.loader.dismiss();
            var alert = _this
                .alertCtrl
                .create({
                title: _this.translate.instant('ERROR'),
                subTitle: _this.translate.instant('ERROR_AVAILABLE'),
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_5_ionic_audio__["b" /* AudioTrackComponent */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_5_ionic_audio__["b" /* AudioTrackComponent */])
    ], AudioOnlinePage.prototype, "audioTrack", void 0);
    AudioOnlinePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-audio-online',template:/*ion-inline-start:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/audio-online/audio-online.html"*/'<ion-header>\n  <ion-navbar color="dark">\n    <ion-title>{{audio.title}}</ion-title>\n    <ion-buttons end>\n      <button ion-button (click)=\'closeModal()\' class="modal-buttom">\n        <ion-icon name="close-circle"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-card>\n    <img src="assets/imgs/default-img.gif" [lazyLoad]="image" *ngIf="image != null" offset="100" />\n    <ion-card-content>\n      <audio-track #audio [track]="track" (onFinish)="onTrackFinished($event)" (onLoad)="onTrackLoad($event)"\n        (onPause)="onTrackPause($event)">\n        <ion-item class="controls">\n          <audio-track-play dark [audioTrack]="audio">\n            <ion-spinner></ion-spinner>\n          </audio-track-play>\n        </ion-item>\n        <ion-item>\n          <h2>{{audio.title}}</h2>\n          <audio-track-progress-bar dark duration progress [audioTrack]="audio"></audio-track-progress-bar>\n        </ion-item>\n      </audio-track>\n    </ion-card-content>\n  </ion-card>\n</ion-content>'/*ion-inline-end:"/Applications/MAMP/htdocs/ionic3/learning-pro/src/pages/audio-online/audio-online.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2__node_modules_ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_3__providers_database_database__["a" /* DatabaseProvider */], __WEBPACK_IMPORTED_MODULE_4__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_5_ionic_audio__["a" /* AudioProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_device_ngx__["a" /* Device */]])
    ], AudioOnlinePage);
    return AudioOnlinePage;
}());

//# sourceMappingURL=audio-online.js.map

/***/ })

});
//# sourceMappingURL=12.js.map